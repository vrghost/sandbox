import random
import itertools

EXT_COEF = 2
START_MATRIX_DIM = 13

def avg(vals):
    return sum(vals)/float(len(vals))

class Surface(object):

    def __init__(self):
        self._surface = [
            [random.random() for _x in xrange(START_MATRIX_DIM)]
            for _y in xrange(START_MATRIX_DIM)
        ]

    def _extendList(self, input):
        for _el in input:
            for _id in xrange(EXT_COEF):
                yield _el

    def _getExtendedSurface(self):
        _out = []
        for _el in self._surface:
            _extEl = list(self._extendList(_el))
            _out.extend(list(_extEl) for _id in xrange(EXT_COEF))
        return _out

    DELTAS = (-2, -1, 0, 1, 2)
    def _getNeighbours(self, surf, x, y):
        _out = []
        for _dx in self.DELTAS:
            for _dy in self.DELTAS:
                if random.random() * abs(_dx) + random.random() * abs(_dy) > 0.5:
                    continue
                try:
                    _out.append(surf[_dy + y][_dx + x])
                except IndexError:
                    # not found
                    pass
        return _out

    def _pertube(self, vals):
        _val = avg(vals)
        if random.random() > 0.9 and 0:
            _rv = random.random() * _val
        else:
            _rv = _val
        return _rv

    def iter(self):
        if len(self._surface) < 100:
            _extSurf = self._getExtendedSurface()
        else:
            _extSurf = self._surface
        _newSurf = []
        _pertube = (lambda val: avg(val)) \
            if len(self._surface) > 100 and 0 else self._pertube
        for _y in xrange(len(_extSurf)):
            _newRow = []
            for _x in xrange(len(_extSurf[0])):
                _neighbours = self._getNeighbours(_extSurf, _x, _y)
                _newRow.append(_pertube(_neighbours))
            _newSurf.append(_newRow)
        self._surface = _newSurf

    def getSurface(self):
        _surf = self._surface
        _min = min(itertools.chain(*_surf))
        _max = max(itertools.chain(*_surf))
        return [[(_el - _min)/_max for _el in _row] for _row in _surf]

# vim: set sts=4 sw=4 et :
