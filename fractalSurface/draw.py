import pygame
import time

from surface import Surface

def draw_surface(width, height):
    _screen = pygame.display.set_mode((width, height))
    _surf = Surface()
    while 1:
        # draw screen
        _data = _surf.getSurface()
        _rectH = height / float(len(_data))
        _rectW = width / float(len(_data[0]))
        _x = _y = 0
        for (_idY, _row) in enumerate(_data):
            for (_idX, _val) in enumerate(_row):
                _col = 255.0 * _val
                pygame.draw.rect(_screen, (_col, _col, _col),
                    (_idX * _rectW, _idY * _rectH, _rectW, _rectH))
        pygame.display.flip()
        time.sleep(1.5)
        # render next iter
        _surf.iter()

if __name__ == "__main__":
    draw_surface(800, 800)

# vim: set sts=4 sw=4 et :
