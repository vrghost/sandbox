import os
import itertools
from datetime import datetime

from sqlobject import *

import track

class Ship(SQLObject):

    mmsi = IntCol() # It appears that IntCol is capable of storing nine decimal digits
    lastCheckedDate = DateTimeCol(default=datetime(1000, 1, 1))
    positions = MultipleJoin("Pos")

    def getTrack(self, select=None, **kwargs):
        _args = [Pos.q.ship == self]
        if select:
            _args.append(select)
        return track.Track(_pos.getAsPoint() for _pos in Pos.select(AND(*_args), **kwargs))

    def extendTrack(self, newTrack):
        _existingRecords = tuple(Pos.select(AND(
            Pos.q.timestamp >= newTrack.startDateTime,
            Pos.q.timestamp <= newTrack.endDateTime,
        )))
        # Get new elements
        _existingData = [_el.timestamp for _el in _existingRecords]
        _added = 0
        for _newEl in newTrack.track:
            if _newEl.timestamp not in _existingData:
                Pos(ship=self, **dict((_key, getattr(_newEl, _key)) for _key in track.POS_FIELDS))
                _added += 1
        print("%i records added to db" % _added)

class Pos(SQLObject):

    ship = ForeignKey("Ship")
    lon = FloatCol()
    lat = FloatCol()
    speed = IntCol()
    course = IntCol()
    timestamp = DateTimeCol(unique=True)

    baseIndex = DatabaseIndex("lon", "lat", "speed", "timestamp", "course")
    posIndex = DatabaseIndex("lon", "lat")

    def getAsPoint(self):
        _data = dict((_field, getattr(self, _field))
            for _field in track.POS_FIELDS)
        return track.Pos(**_data)


ALL_TABLES = (Ship, Pos)

# vim: set sts=4 sw=4 et :
