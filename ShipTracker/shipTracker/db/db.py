import os
import itertools

from sqlobject import *

#from .
import tables

class TrackingDb(object):

    def __init__(self, fname):
        self._fname = fname

    def open(self):
        _connString = "sqlite://" + os.path.abspath(self._fname).replace("\\", "/")
        self._conn = sqlhub.processConnection = connectionForURI(_connString)
        for _table in tables.ALL_TABLES:
            _table.createTable(ifNotExists = True)

    def close(self):
        self._conn.close()

    def getShip(self, mmsi):
        _sel = tuple(tables.Ship.select(tables.Ship.q.mmsi == mmsi))
        if len(_sel) == 0:
            _rv = tables.Ship(mmsi = mmsi)
        else:
            _rv = _sel[0]
        return _rv

# vim: set sts=4 sw=4 et :
