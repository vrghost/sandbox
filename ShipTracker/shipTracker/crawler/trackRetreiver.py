import datetime
import urllib
import urllib2
from xml.etree import ElementTree as ET

# from .
import track

class TrackRetreiver(object):

    _urlBase = r"http://www.marinetraffic.com/ais/gettrackxml.aspx"

    def __init__(self, mmsi, date):
        _xml = self._getXmlStr(mmsi, date)
        _tree = ET.fromstring(_xml)
        assert _tree.tag.lower() == "track"
        self._dictTrack = [self.parseElement(_el) for _el in _tree]

    def _getXmlStr(self, mmsi, date):
        # XXX: for some reason target server does not understand
        # `urlencode()`'d args
        _amPm = "AM"
        _hours = date.hour
        if _hours > 12:
            _hours -= 12
            _amPm = "PM"
        _min = date.minute
        _sec = date.second
        _time = "%s:%02i:%02i %s" % (_hours, _min, _sec, _amPm)
        _date = " ".join((date.strftime("%m/%d/%Y"), _time))
        _args ="mmsi=%s&date=%s" % (mmsi, _date)
        _url = "%s?%s" % (self._urlBase, _args.replace(" ", "%20"))
        return urllib2.urlopen(_url).read()

    _parsers = {
        "lat": float,
        "lon": float,
        "speed": int,
        "course": int,
        "timestamp": lambda val: datetime.datetime.strptime(val, "%Y-%m-%dT%H:%M:%S")
    }

    def parseElement(self, element):
        assert element.tag.lower() == "pos"
        _attribs = dict((_key.lower(), _value)
            for (_key, _value) in element.attrib.items())
        return dict((_key, self._parsers[_key](_val))
            for (_key, _val) in _attribs.items())

    @property
    def track(self):
        return track.Track(self._dictTrack)

    @classmethod
    def getFromWeb(cls, *args, **kwargs):
        _obj = cls(*args, **kwargs)
        return _obj.track


# vim: set sts=4 sw=4 et :
