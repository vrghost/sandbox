import argparse
import os
from datetime import (datetime, timedelta)

#from .
import tracker

def _date_time_parser(val):
    return datetime.strptime(val, "%d.%m.%Y %H:%M:%S")

def get_parser():
    _parser = argparse.ArgumentParser(description='Track certain ship by its MMSI.')
    _parser.add_argument("--mmsi", type=int, dest="mmsi",
        required=True, help="MMSI of a ship to track")
    _parser.add_argument("--db", dest="database",
        help = "SQLite database file to use.", default=os.path.join("out", "tracking.sqlite.db"))

    _subparsers = _parser.add_subparsers(help="Alternative parsing options")

    _updateSub = _subparsers.add_parser("update", help="Update tracking info")
    _updateSub.set_defaults(_mode="update")
    _updateSub.add_argument("--start-date", type=_date_time_parser, dest="start_date", default=datetime(1000, 1, 1),
        help = "Start date of period for tracking to start (dd.mm.yyyy HH:MM:SS format, 24-hour clock, UTC time zone)")
    _updateSub.add_argument("--end-date", type=_date_time_parser, dest="end_date",
        default = datetime.utcnow() - timedelta(minutes=30),
        help = "End date of period for tracking to start (dd.mm.yyyy HH:MM:SS format, 24-hour clock)")

    _exportSub = _subparsers.add_parser("export", help="Export KML track.")
    _exportSub.set_defaults(_mode="export")
    _exportSub.add_argument("--output-file", dest="out_file", required=True,
        help="Name of KML file to be written.")
    return _parser

if __name__ == "__main__":
    _parser = get_parser()
    _args = _parser.parse_args()
    _vars = vars(_args)
    _mode = _vars.pop("_mode")

    _tracker = tracker.ShipTracker(_vars.pop("mmsi"), _vars.pop("database"))
    _tracker.open()
    if _mode == "update":
        _tracker.update(**_vars)
    elif _mode == "export":
        _tracker.exportKML(**_vars)
    else:
        raise Exception("Unknwon mode %s" % _mode)
    _tracker.close()

# vim: set sts=4 sw=4 et :
