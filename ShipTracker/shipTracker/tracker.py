import datetime
import time

#from .
import db
import track
import crawler

class ShipTracker(object):

    mmsi = None
    _ship = None

    def __init__(self, mmsi, database):
        self.db = db.TrackingDb(database)
        self.mmsi = mmsi

    def open(self):
        self.db.open()
        self._ship = self.db.getShip(self.mmsi)

    def getByDate(self, date):
        time.sleep(5)
        return crawler.TrackRetreiver.getFromWeb(self.mmsi, date)

    def close(self):
        self.db.close()

    def update(self, end_date, start_date=None):
        _possibleStartDates = [
            start_date, self._ship.lastCheckedDate
        ]
        _possibleStartDates.extend(_el.timestamp for _el in
            self._ship.getTrack(orderBy=db.DESC(db.Pos.q.timestamp), limit=1))

        _tracks = self.getTrackSinceTill(
            max([_el for _el in _possibleStartDates if _el]),
            end_date,
        )
        for _track in _tracks:
            self._ship.extendTrack(_track)

    def getTrackSinceTill(self, minDate, maxDate):
        print "Getting tracks from %s till %s" % (minDate, maxDate)
        _delta = datetime.timedelta(hours=6)
        _curDate = minDate
        _newTrack = self.getByDate(_curDate)
        if len(_newTrack) > 0:
            yield _newTrack
        while _curDate < maxDate:
            if len(_newTrack) > 0 and _newTrack.endDateTime > _curDate:
                _curDate = _newTrack.endDateTime
                print "Retreived %i new points" % len(_newTrack)
            else:
                _curDate += _delta
            _newTrack = self.getByDate(_curDate)
            if len(_newTrack) > 0:
                yield _newTrack
            print("Retreived and saved tracking for %s" % _curDate)
            self._ship.lastCheckedDate = _curDate
        if len(_newTrack) > 0:
            yield _newTrack

    def exportKML(self, out_file):
        import simplekml
        _getCoords = lambda pos: (pos.lon, pos.lat)
        _baseTrack = self._ship.getTrack()
        _tracks = _baseTrack.splitToLocalTracks()

        _kml = simplekml.Kml()
        _points = _kml.newmultigeometry(name="MultiPoint")

        def _buildLine(track, color):
            _line = _kml.newlinestring(name="Path %s - %s" % (track.startDateTime, track.endDateTime),
                coords=[_getCoords(_point) for _point in track])
            _line.linestyle.color = color
            _line.linestyle.width = 3
            _line.altitudemode = simplekml.AltitudeMode.clamptoground

        for (_id, _track) in enumerate(_tracks):
            if _id > 0:
                _connTrack = track.Track.connectionTrack(_tracks[_id - 1], _track)
                _buildLine(_connTrack, simplekml.Color.blue)
            _buildLine(_track, simplekml.Color.red)
        _kml.save(out_file)

# vim: set sts=4 sw=4 et :
