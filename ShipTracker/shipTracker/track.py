import datetime
import math
import itertools

POS_FIELDS = ("lon", "lat", "speed", "course", "timestamp")

class Earth(object):

    r = 6378.14 # Km
    circumference = 2 * math.pi * r

    def diffToDist(self, lon, lat):
        _c = self.circumference
        _lonDist = (_c / 360.0) * abs(lon)
        _latDist = (_c / 2.0 / 180.0) * abs(lat)
        return math.sqrt(_lonDist ** 2 + _latDist**2)

EARTH = Earth()

class Pos(object):

    def __init__(self, lon, lat, speed, course, timestamp):
        self.lon = lon
        self.lat = lat
        self.speed = speed
        self.course = course
        self.timestamp = timestamp

    def __repr__(self):
        return "<%s::%s>" % (self.__class__.__name__,
            ["%s=%s" % (_name, getattr(self, _name)) for _name in POS_FIELDS])

    def __eq__(self, other):
        for _name in POS_FIELDS:
            if getattr(self, _name) != getattr(other, _name):
                return False
        return True

    def dist(self, other):
        return EARTH.diffToDist(self.lon - other.lon, self.lat - other.lat)

    def asLabel(self):
        return "\n".join((
            "Longitude: %(lon)s",
            "Latituude: %(lat)s",
        )) % val(self)

class Track(object):

    def __init__(self, posArr):
        self._track = list(Pos(**data) if isinstance(data, dict) else data for data in posArr)
        assert all(isinstance(_el, Pos) for _el in self._track)

    @classmethod
    def connectionTrack(cls, startTrack, endTrack):
        assert startTrack != endTrack
        _start = startTrack.track[-1]
        _end = endTrack.track[0]
        _points = [_start, _end]
        _timeDelta = datetime.timedelta(minutes=1)

        _sparse = lambda p1, p2: p1.dist(p2) > 500
        _newPos = lambda lon, lat, timeStamp: Pos(lon, lat, 0, 0, timeStamp)
        _middle = lambda a, b: a + ((b - a) / 2)

        while any(_sparse(_p1, _p2) for (_p1, _p2) in zip(_points[0::1], _points[1::1])):
            for _id in xrange(len(_points) - 1):
                _p1 = _points[_id]
                _p2 = _points[_id + 1]
                if _sparse(_p1, _p2):
                    _points.insert(_id + 1, _newPos(
                        _middle(_p1.lon, _p2.lon),
                        _middle(_p1.lat, _p2.lat),
                        _middle(_p1.timestamp, _p2.timestamp),
                    ))

        return cls(_points)

    @property
    def track(self):
        return tuple(sorted(self._track, key=lambda el: el.timestamp))

    def extend(self, other):
        _newEls = [_el for _el in other.track if _el not in self.track]
        self._track.extend(_newEls)

    @property
    def startDateTime(self):
        if self._track:
            _rv = min(_el.timestamp for _el in self._track)
        else:
            _rv = None
        return _rv

    @property
    def endDateTime(self):
        if self._track:
            _rv = max(_el.timestamp for _el in self._track)
        else:
            _rv = None
        return _rv

    def __iter__(self):
        for _el in self.track:
            yield _el

    def __len__(self):
        return len(self._track)

    def __repr__(self):
        return "<%s:\n-----\n%s\n-----\n>" % (self.__class__.__name__,
            "\n".join(repr(_el) for _el in self.track))

    def _veryFarDist(self, p1, p2):
        return p1.dist(p2) > 1000

    def removeExtremumPoints(self):
        # Throw away errorous coordinates
        _areVeryFar = self._veryFarDist
        _track = tuple(self.track)

        for _pos in xrange(len(_track) - 2):
            _p1 = _track[_pos]
            _p2 = _track[_pos + 1]
            _p3 = _track[_pos + 2]
            if _areVeryFar(_p1, _p2) and _areVeryFar(_p2, _p3) and not _areVeryFar(_p1, _p3):
                print "Removing extremum point %s, as it is too far away from its neighbours." % _p2
                self._track.remove(_p2)

    def removeNoisePoints(self):
        def _removeCloseAheadPoints(lookForward):
            _noisePresent = True
            _removedCnt = 0
            while _noisePresent:
                _track = tuple(self.track)[1:-1]
                _noisePresent = False
                for (_p1, _p2) in itertools.izip(_track, itertools.islice(_track, lookForward, None)):
                    # If points are less than 20 meters apart, they are considered to be
                    # Results of a measurement noise rather than actual ships movement
                    if _p1.dist(_p2) < 0.02:
                        self._track.remove(_p2)
                        _noisePresent = True
                        _removedCnt += 1
            return _removedCnt

        _removed = 0
        for _lookLen in xrange(1, 15):
            _removed += _removeCloseAheadPoints(_lookLen)
        print("Removed %i noise points." % _removed)

    def splitToLocalTracks(self):
        self.removeExtremumPoints()
        self.removeNoisePoints()

        _areVeryFar = self._veryFarDist
        _out = []
        _curTrack = []
        _track = tuple(self._track)

        _cur = _track[0]
        for _el in _track:
            if _areVeryFar(_el, _cur):
                print "Splitting track between %s and %s" % (_el, _cur)
                if _curTrack:
                    _out.append(_curTrack)
                _curTrack = []
            _curTrack.append(_el)
            _cur = _el
        if _curTrack:
            _out.append(_curTrack)

        return [Track(_points) for _points in _out]


# vim: set sts=4 sw=4 et :
