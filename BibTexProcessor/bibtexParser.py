import codecs
import re


class BibtexRecord(object):
    """Class representing single bibtex record"""

    def __init__(self, type, key, values):
        self.type = type
        self.key = key
        self.values = list(values)

    @classmethod
    def fromString(cls, data):
        def get_all_in_braces(source):
            _lbrace = source.find("{")
            _braceCount = 1
            for (_pos, _char) in enumerate(source[_lbrace+1:]):
                if _char == "{":
                    _braceCount += 1
                elif _char == "}":
                    _braceCount -= 1
                if _braceCount == 0:
                    _rbrace = _pos + _lbrace + 1
                    break
                assert _braceCount > 0
            assert source[_lbrace] == "{", source[_lbrace-5:_lbrace+5]
            assert source[_rbrace] == "}", source[_rbrace-5:_rbrace+5]
            return (
                source[:_lbrace].strip(),
                source[_lbrace+1:_rbrace].strip(),
                source[_rbrace+1:].strip(),
            )
        (_prefix, _body, _rest) = get_all_in_braces(data)
        assert not _rest, "%r must contain emty line" % _rest
        assert _prefix.startswith("@"), "%r must be bibtex type" % _prefix
        _type = _prefix[1:]
        (_key, _body) = _body.split(",", 1)
        _values = []
        while _body:
            _sep = _body.find("=")
            _fieldName = _body[:_sep].strip()
            _rest = _body[_sep+1:].strip()
            if _rest.startswith('{'):
                (_prev, _value, _body) = get_all_in_braces(_rest)
                if _body:
                    assert _body.startswith(','), _body
                    _body = _body[1:]
            else:
                _prev = None
                (_value, _body) = _rest.split(',', 1)
            assert not _prev, "Name should have been removed."
            assert _fieldName not in _values
            _values.append((_fieldName, _value))
        return cls(
            _type, _key, _values)

    def toBibtex(self):
        _data = ["\t%s = %s" % (
            _key, ("%s" if _value.islower() and _value.isalnum() else "{%s}") % _value)
            for (_key, _value) in self.values]
        # add key
        _data.insert(0, self.key)
        return u"@%s{%s}" % (
            self.type,
            u",\n".join(_data),
        )

    def __getitem__(self, name):
        return self.get(name)

    def __setitem__(self, name, value):
        return self.set(name, value)

    def get(self, name):
        for _el in self.values:
            if _el[0] == name:
                return _el[1]
        raise KeyError(name)

    def set(self, name, value):
        for (_pos, _el) in enumerate(self.values):
            if _el[0] == name:
                self.values[_pos] = (name, value)
                break
        else:
            raise KeyError(name)

    def remove(self, name):
        for (_pos, _el) in enumerate(self.values):
            if _el[0] == name:
                _foundPos = _pos
                break
        else:
            raise KeyError(name)
        self.values.pop(_foundPos)

    def __repr__(self):
        return "<%s:%s>" % (self.__class__.__name__, self.toBibtex())

_bibtexStartRe = re.compile(r"^@\w+{.*")
def readFromStream(source):
    _collected = ""
    for _line in source:
        if _line.startswith("%%") or not _line.strip():
            continue
        if _bibtexStartRe.match(_line):
            if _collected:
                yield BibtexRecord.fromString(_collected)
            _collected = _line
        else:
            _collected += _line
    if _collected:
        yield BibtexRecord.fromString(_collected)

def readFromFile(fname, encoding="utf8"):
    return tuple(readFromStream(
        codecs.open(fname, "r", encoding)))

# vim: set sts=4 sw=4 et :
