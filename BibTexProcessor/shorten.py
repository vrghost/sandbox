from re import match
from urllib2 import urlopen, Request, HTTPError
from urllib import quote
import json

def shorten(url):
    _data = urlopen(Request(
        'http://goo.gl/api/url','url=%s'%quote(url),{'User-Agent':'toolbar'}))
    _data = json.loads(_data.read())
    return _data["short_url"]

def expand(url):
    f = urlopen(url)
    return f.geturl()


if __name__ == "__main__":
    # test
    _url = "http://delfi.lv"
    _short = shorten(_url)
    _long = expand(_short)
    print "Original: %r, Short: %r, Long: %r" % (_url, _short, _long)
# vim: set sts=4 sw=4 et :
