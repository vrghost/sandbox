import sys
import time
# from .
import bibtexParser
import shorten

def fix_url(record):
    try:
        _url = record.get("Url")
    except KeyError:
        # not found
        pass
    else:
        if len(_url) > 44:
            _shortUrl = shorten.shorten(_url)
            record.set("Url", _shortUrl)
            time.sleep(2)
    return record

def fix_bibtex_record(record):
    _type = record.type.lower()
    if _type in ("electronic", "misc", "url"):
        _record = fix_url(record)
    elif _type in (
        "techreport",
        "inproceedings",
        "article",
        "book",
        "phdthesis",
        "mastersthesis",
        "incollection",
        ):
        try:
            record.remove("Url")
        except KeyError:
            pass
        _record = record
    else:
        raise Exception("Unknown type %r" % _type)
    return _record

def process(fname):
    _library = bibtexParser.readFromFile(fname)
    _texts = [fix_bibtex_record(_el).toBibtex() for _el in _library]
    _str = "\n\n".join(_texts)
    open("output.bib", "wb").write(_str.encode("utf8"))

if __name__ == "__main__":
    process(sys.argv[1])

# vim: set sts=4 sw=4 et :
