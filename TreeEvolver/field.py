import itertools

STATES = {
    "DEAD": 0,
    "ALIVE": 1,
    "OLD": 2,
}

NUM_STATES = len(STATES)

_DELTAS = [
    (None, 0, 1),
    (-1, 0, None),
    (-1, 0, 1),
]

class Field(object):

    field = None

    def __init__(self, width, height, translationTable):
        self.width = width
        self.height = height
        self.field = [0 for _id in xrange(width * height)]
        self.translations = translationTable
        self.field[self._coordsToPlain(
            width/2, height/2)] = STATES["ALIVE"]

    def _coordsToPlain(self, x, y):
        if 0 <= x  and x < self.width \
            and 0 <= y and y < self.height:
            _rv = (y * self.height) + x
        else:
            _rv = None
        return _rv

    def _plainToCoords(self, pos):
        assert pos >= 0 and pos <= len(self.field)
        _y = pos / self.height
        return (pos - (_y * self.height), _y)


    def _posAround(self, pos):
        # return array of pos'es of the cells around
        # of given pos
        (_x, _y) = self._plainToCoords(pos)
        if _x == 0:
            _xVars = _DELTAS[0]
        elif _x == self.width - 1:
            _xVars = _DELTAS[1]
        else:
            _xVars = _DELTAS[2]
        if _y == 0:
            _yVars = _DELTAS[0]
        elif _y == self.height - 1:
            _yVars = _DELTAS[1]
        else:
            _yVars = _DELTAS[2]
        for _yDelta in _yVars:
            for _xDelta in _xVars:
                if _yDelta is None or _xDelta is None:
                    yield None
                else:
                    yield (pos + _yDelta * self.height + _xDelta)

    def _getSquare(self, pos, field):
        # get square 3x3 area around given position
        _out = []
        for _curPos in self._posAround(pos):
            if _curPos is None:
                _out.append(STATES["DEAD"])
            else:
                _out.append(field[_curPos])
        return _out

    def getSquare(self, pos):
        return self._getSquare(pos, self.field)

    def _squareToId(self, square):
        # convert sequence of the cell states to
        # the gridId
        _out = 0
        _dead = STATES["DEAD"]
        assert square[4] == STATES["ALIVE"]
        _aliveDiag = [_el for _el in self.getSquareDiag(square) if _el != _dead]
        _aliveStraight = [_el for _el in self.getSquareStraight(square) if _el != _dead]
        return len(_aliveDiag) * (5**1) + len(_aliveStraight)

    def getSquareDiag(self, square):
        return [square[_idx] for _idx in (0, 2, 6, 8)]

    def getSquareStraight(self, square):
        return [square[_idx] for _idx in (1, 3, 5, 7)]

    def _applySquare(self, field, pos, nextSquare):
        for (_pos, _curPos) in enumerate(self._posAround(pos)):
            if _curPos is not None:
                field[_curPos] = nextSquare[_pos]

    def grow(self, iterCount):
        for _iter in xrange(iterCount):
            self._step()

    def _step(self):
        _oldField = self.field
        _newField = _oldField[:]
        _pos = -1
        try:
            while True:
                _pos = _oldField.index(STATES["ALIVE"], _pos+1)
                _stateId = self._squareToId(
                    self._getSquare(_pos, _oldField))
                _nextState = self.translations[_stateId]
                self._applySquare(_newField, _pos, _nextState)
        except ValueError:
            # not found -> all finished
            pass
        self.field = _newField


# vim: set sts=4 sw=4 et :
