"""Implementation of NSGA-II in Python

This python module implements Problem base class which is then extended
by specific problem. Classes for ZDT1 and ZDT2 are present in this file, what
is object to future changes.

See <http://code.google.com/p/py-nsga-ii>
for additional details and package information.

------------------------------------------------------------------
Author:    Michal Fita <michal.fita(.)gmail.com>
Date:      2007-11-30
Version:   0.1.0
Copyright: (c) 2007 Michal Fita
License:   Licensed under the Apache License, Version 2.0 (the"License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in
	   writing, software distributed under the License is
	   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
	   CONDITIONS OF ANY KIND, either express or implied.  See
	   the License for the specific language governing
	   permissions and limitations under the License.
------------------------------------------------------------------
"""

from math import *
from random import choice
from copy import *
from fpconst import *

from Individual import *

__all__ = ['Problem', 'ProblemZDT1', 'ProblemZDT2']

"""
This class defines the problem in all details needed to run the experiment.
"""
class Problem(object):
    "Some important values"
    N = 100 # Size of population
    n = 30 # Number of variables
    M = 2  # Number of objective functions
    G = 250 # Number of generations in experiment
    eta_c = 20 # Crossover distribution
    eta_m = 20 # Mutation distribution
    p_c = 0.9 # Crossover propability
    p_m = 0.1 # Mutation propability
    var_bounds = [0.0, 1.0]
    obj_funcs = []

    "Variables for experiment"
    P = [] # Population

    "Initialize the problem for experiment"
    def __init__(self):
        "Create begining population"
        for i in range(1, self.N):
            self.P.append(Individual(self))

    def find_nondominated_front(self, P):
        f = set(P).pop()
        B = set([f])
        P.add(f)
        #print "P = ", P
        for p in list(P): # TODO: ??
            B.add(p)
            #print "B =",B
            for q in copy(B):
                if hash(p) != hash(q):
                    if q.dominated(p):
                        B.discard(q)
                    elif p.dominated(q):
                        B.discard(p)

        return list(B)

    """
    Deprecated. Does not work.
    """
    def old_fast_nondominated_sort(self, P):
        F = []
        i = 0 # front counter
        while len(P) != 0:
            F.append([])
            F[i] = self.find_nondominated_front(set(P))
            #print "F[",i,"] = ",F[i]
            P = set(P).difference(set(F[i]))
            F[i] = list(F[i])
            i += 1
        return F

    def fast_nondominated_sort(self, P):
        F = []
        S = dict()
        for p in P:
            S[hash(p)] = []
            p.n = 0
            for q in P:
                if p.dominated(q):
                    S[hash(p)].append(q)
                elif q.dominated(p):
                    p.n += 1
            if p.n == 0:
                p.rank = 1
                F.append([])
                F[0].append(p)
        i = 0
        while len(F[i]) != 0:
            Q = []
            for p in F[i]:
                for q in S[hash(p)]:
                    q.n -= 1
                    if q.n == 0:
                        q.rank = i + 1
                        Q.append(q)
            i += 1
            F.append([])
            F[i] = Q
        return F

    def crowding_distance_assigment(self, X):
        l = len(X)
        for x in X:
            "Initialize distance for each individual"
            x.distance = 0
        for m in range(0, self.M):
            "Sort using objective m"
            X.sort(lambda x,y: cmp(x.func_vals[m], y.func_vals[m]))
            "Boundary points are always selected"
            X[0].distance = PosInf
            X[l - 1].distance = PosInf
            for i in range(2, l - 1):
                X[i].distance += (X[i + 1].func_vals[m] - X[i - 1].func_vals[m])

    @staticmethod
    def crowded_comparasion_operator(x,y):
        "Because we sort in descending order the return values are inverted"
        if x.rank < y.rank or (x.rank == y.rank and x.distance > y.distance):
            return +1
        else:
            return -1

    """
    This method runs the whole experiment
    """
    def experiment(self):
        for iteration in range(0, self.G):
            print "iteration =", iteration
            "Create child population Q."
            self.Q = []
            while len(self.Q) < self.N:
                p1 = choice(self.P)
                p2 = choice(self.P)
                while hash(p1) == hash(p2):
                    p2 = choice(self.P)
                "Append new pair of childrens crossed over from two parents to the list of childrens."
                self.Q.extend(p1.crossover(p2))
            "Mutate all childrens"
            for c in self.Q:
                c.mutation()
            "Join both into one list R"
            R = []
            R.extend(self.P)
            R.extend(self.Q)
            "Do fast non-dominated sort on whole R"
            F = self.fast_nondominated_sort(R)
            self.processNonDominatedFront(iteration, F[0])
            "Create parent population for next iteration"
            self.P = []
            i = 0
            while len(self.P) + len(F[i]) <= self.N:
                "Crowding distance assigment"
                self.crowding_distance_assigment(F[i])
                "Include i-th non-dominated front in parent population"
                self.P.extend(F[i])
                i += 1
            #print "F[",i,"] =",F[i]
            "Sort in descending order using crowded comparasion operator"
            F[i].sort(Problem.crowded_comparasion_operator)
            "Choose first (N - |P_t+1|) elements"
            self.P.extend(F[i][0 : self.N-len(self.P)])
            "Make new population"

        "Show non-dominated fronts"
        for f in F:
            if len(f) > 0:
                print "Front (q:",len(f),") = ",
                for g in f:
                    print g.genes,
                print ""
        self.processNonDominatedFront(F)


    def processNonDominatedFront(self, generation, front):
        pass

class ProblemZDT1(Problem):
    "Objective function 1 for ZDT1: f1(x) = x1"
    def f1(self, x):
        return x.genes[0]
    #f1 = classmethod(f1)

    "g(x) = 1 + 9 (sum(for i=2 to n : xi))/(n - 1)"
    def g(self, x):
        sx = 0.0;
        for i in range(1, self.n): # n genes numerated from 0
            sx += x.genes[i]
        r = 1.0 + 9.0 * (sx / (self.n - 1.0))
        return r

    "Objective function 2 for ZDT1: f2(x) = g(x) [1 - sqrt(x1/g(x))]"
    def f2(self, x):
        gx = self.g(x)
        #print "x =", x, "gx = ", gx, "x.genes[1] = ", x.genes[1]
        r = gx * (1.0 - sqrt(x.genes[0] / gx))
        return r

    obj_funcs = [f1, f2]

class ProblemZDT2(ProblemZDT1):
    "Objective function 1 for ZDT2: f1(x) = x1"
    def f1(self, x):
        return x.genes[0]

    "Objective function 2 for ZDT2: f2(x) = g(x) [1 - pow((x1 / g(x)), 2)]"
    def f2(self, x):
        gx = self.g(x)
        r = gx * (1.0 - pow((x.genes[0] / gx), 2.0))
        return r

    obj_funcs = [f1, f2]

if __name__ == "__main__":
    p1 = ProblemZDT1()
    p2 = ProblemZDT2()

    #p1.experiment()
    p2.experiment()

