#!/usr/bin/env python
#

import logging
import re

from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.api import mail
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler


from semanticAnalysis import OpenCalais

def _uglyPatch(message):
    """Fix '8bit' decoding error."""
    def _fixEnc(body):
        _enc = body.encoding
        if _enc and _enc.lower() == "8bit":
            _enc = "7bit"
        body.encoding = _enc

    for (_type, _body) in message.bodies():
        _fixEnc(_body)
    try:
        for (_fname, _att) in mail._attachment_sequence(message.attachments):
            _fixEnc(_att)
    except AttributeError, _err:
        if "object has no attribute 'attachments'" in _err.message:
            pass
        else:
            # Unexpected attribute error.
            raise
        

class MainHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write('Hello world!')

class ForwardHandler(InboundMailHandler):

    noteRe = re.compile(r"notebook-([^@]+)@")
    subjectRe = re.compile(r"^\s*(?:Fwd?\s*:\s*)*\s*(.*?)\s*$", re.I)
    myEmail = "processed@vrg-mail-filter.appspotmail.com"
    calais = OpenCalais()


    def _guessNotebook(self, message):
        _addresses = []
        _addAddr = lambda name: _addresses.extend(name.split())
        _addField = lambda name: _addAddr(
            message.original.get(name, ""))

        _addAddr(message.to)
        _addField("Delivered-To")
        _addField("X-Forwarded-For")
        _addField("X-Forwarded-To")
        for _address in _addresses:
            _match = self.noteRe.search(_address)
            if _match:
                _rv = _match.group(1).replace("+", " ").title()
                logging.debug("Guessed notebook name: %r" % _rv)
                return _rv
        return None

    def _returnMailIter(self, message):
        # Return candidates for return address
        _orig = message.original
        _get = lambda name: _orig.get(name, "")

        _mails = _get("X-Forwarded-For").split()
        _mails.reverse()
        for _mail in _mails:
            if mail.is_email_valid(_mail):
                yield _mail

        for _field in ("From", "Return-Path"):
            _mail = _get(_field)
            if mail.is_email_valid(_mail):
                yield _mail

    def receive(self, message):
        if self.myEmail in message.sender:
            logging.info("Got an infinite loop e-mail (%r), not processing." % message.sender)
            return
        self._getSemanticTags(message)


    def _getSemanticTags(self, message):
        _chunks = []
        for (_type, _body) in message.bodies():
            _chunks.append(_body.decode())

        _text = "\n".join(_chunks)

        _resp = None
        try:
            _resp = self.calais.analyze(_text)
        except Exception, _err:
            logging.error("Failed to analyze message: %r" % _err)

        _semTags = ()
        if _resp:
            # Consider only short tags
            _tags = list(_el for _el in _resp.getTags() if len(_el[1]) < 12)
            # Sort by relevance
            _tags.sort(key=lambda el: el[2])
            _semTags = frozenset(_el[1].title() for _el in _tags[:8])
        self._outputMessage(message, _semTags)



    def _outputMessage(self, message, extraTags=()):
        _tags = ["e-mail"]
        _tags.extend(extraTags)
        _extraSubj = ""

        _notebook = self._guessNotebook(message)
        if _notebook:
            _tags.append(_notebook)
            _extraSubj += " @%s " %  _notebook


        # Compute to/from fields
        message.sender = self.myEmail
        for _email in self._returnMailIter(message):
            if "@vrg-mail-filter.appspotmail.com" not in _email:
                # This must be the reply address
                logging.debug("Reply address: %r" % _email)
                message.to = _email
                break
        else:
            logging.error("Cannot determine reply address.")
            return


        _extraSubj += " " + (" ".join("#%s" % _tag for _tag in _tags))
        logging.debug("Sending subject '%s'" % _extraSubj)

        # Compose subject
        _subj = message.subject
        _match = self.subjectRe.match(_subj)
        if _match:
            _subj = _match.group(1)

        message.subject = " ".join((_subj, _extraSubj))
        _uglyPatch(message)
        message.send()


def main():
    application = webapp.WSGIApplication(
        [
            ('/', MainHandler),
            ForwardHandler.mapping(),
        ],
        debug=True
    )
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 et :
