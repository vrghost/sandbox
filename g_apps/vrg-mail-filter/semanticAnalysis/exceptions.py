class SymanticAnalysisError(Exception):
    """Base class for all semantic analysis errors."""


class InputDataError(SymanticAnalysisError):
    """Something is wrong with input data."""

class InputContentConstraintExceeded(InputDataError):
    """Data scheduled for processing is too long."""

class ResponseParsingError(SymanticAnalysisError):
    """Raised when something went wrong with processing of response."""

class TemporaryAnalyzerError(SymanticAnalysisError):
    """Error returned when semantyc analysis is broken because of some temporal problems in parser.

    The temporal problems are such problems that will go away if parsing will be called later.
    """

class ServerSideException(SymanticAnalysisError):
    """When server returns exception of some kind."""

    def __init__(self, serverMessage):
        self._msg = serverMessage

    def getServerMessage(self):
        return self._msg

    def __str__(self):
        return "<Server side exception: %r>" % self._msg

class CommunicationWithServerDisabled(SymanticAnalysisError):
    """Raised when communication with server disabled."""

# vim: set sts=4 sw=4 et :
