from StringIO import StringIO
from django.utils import simplejson as json
from google.appengine.api import urlfetch
from urllib import urlencode
import gzip
import re
import time
import xml.etree.ElementTree as ET

from .. import remoteAnalysisBase as base

from .. import exceptions

class Calais(base.Base):

    configName = "Calais"

    caliasRestUrl=r"http://api.opencalais.com/enlighten/rest/"

    enable_gzip = True

    caliasHTTPHeaders={}

    params = """
        <c:params xmlns:c="http://s.opencalais.com/1/pred/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
        <c:processingDirectives c:docRDFaccesible="false" c:outputFormat="Application/JSON" calculateRelevanceScore="true">
        </c:processingDirectives>
        <c:userDirectives c:allowDistribution="false" c:allowSearch="false" >
        </c:userDirectives>
        <c:externalMetadata>
        </c:externalMetadata>
        </c:params>
    """

    apiKey = r"db77k7k8vmfanpmrpu42jwqs"

    def _doRequest(self, text):
        _data = urlencode({
            "licenseID": self.apiKey,
            "content": text.encode("utf8"),
            "paramsXML": self.params
        })
        _headers = dict(self.caliasHTTPHeaders)
        if self.enable_gzip:
            _headers["Accept-encoding"] = "gzip"
        try:
            _result = urlfetch.fetch(self.caliasRestUrl, payload=_data, method="POST", headers=_headers)
        except exceptions.TemporaryAnalyzerError, _err:
            _time = 15
            self.doLog("Got temporary connection error (%s). Pausing connections for %s seconds" % (
                _err, _time))
            raise
        _data = StringIO(_result.content)
        if self.enable_gzip:
            try:
                _uncompressed = gzip.GzipFile(mode="r", fileobj=_data).read()
            except IOError, _err:
                if str(_err) == "Not a gzipped file":
                    # Calais tends to return non-gzipped data on errors
                    _data.seek(0, 0)
                    # return original '_data'
                else:
                    raise
            else:
                _data = StringIO(_uncompressed)
        return _data

    _space = re.compile(r"\s+")
    def analyze(self, text):
        text = self._space.sub(' ', text).strip()
        if text:
            if len(text) > 100000:
                text = text[:100000]
            _rawAnswer = self._doRequest(text).read()
            try:
                _data = json.loads(_rawAnswer, encoding="utf8")
            except:
                # Ok, response parsing failed, maybe there is a XML message in this raw
                # response?
                self._tryParseXmlError(_rawAnswer)
            _rv = JsonResponse(self, _data)
        else:
            # No text at all
            _rv = EmptyResponse(self)
        return _rv

    def _tryParseXmlError(self, msg):
        try:
            _tree = ET.fromstring(msg)
        except:
            self.doLog("Error while decoding %r" % msg)
            raise

        if _tree.tag.lower() == "error":
            _message = _tree.find("Exception").text
            raise exceptions.ServerSideException("Server exception: %r" % _message)
        else:
            raise Exception("Not an error message: %s." % msg)

class JsonResponse(base.Results):

    doLog = lambda s, msg: s.parent.doLog(msg)

    def __init__(self, parent, parsedJson):
        super(JsonResponse, self).__init__()
        self.parent = parent
        self._parseJson(parsedJson)

    def _parseJson(self, jsonData):
        for _value in jsonData.itervalues():
            _keys = tuple(sorted(_value.keys()))
            try:
                if _keys == ("info", "meta"):
                    # Some currently irrelevant info
                    # about this request to Calias
                    continue
                _type = _value["_typeGroup"]
                if _type == "entities":
                    _rel = _value.get("relevance")

                    if "name" in _keys:
                        self.addTag(_value["_type"], _value["name"], _rel)
                elif _type == "topics":
                    _name = _value["categoryName"]
                    # no topic relevance is currently given
                    _score = _value.get("score", None)
                    self.addTag(_type, _name, _score)
                elif _type in ("relations", ):
                    # ignored type groups
                    pass
                else:
                    raise exceptions.ResponseParsingError("Unknown token %r (type group %r)." % (_value, _type))
            except Exception, _err:
                self.doLog("Error %r parsing %s responce element." % (_err, _value))

class EmptyResponse(JsonResponse):

    def __init__(self, parent):
        super(EmptyResponse, self).__init__(parent, {})

# vim: set sts=4 sw=4 et :
