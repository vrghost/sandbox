import logging

class Base(object):
    """ Base class for all remote analysis tools such as OpenCalias, Semantic Hacker or AlchemyAPI."""

    def analyze(self, text):
        raise NotImplementedError

    def doLog(self, msg):
        logging.debug(msg)

class Results(object):
    """Semantic analysis result object."""

    _data = None

    def __init__(self):
        self._data = {}

    def addTag(self, typ, value, relevance):
        self._data[(typ, value)] = max(self._data.get(value, 0), relevance)

    def getTags(self):
        return tuple(_key + (_value, ) for (_key, _value) in self._data.iteritems())

# vim: set sts=4 sw=4 et :
