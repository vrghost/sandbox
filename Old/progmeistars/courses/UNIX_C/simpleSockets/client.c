#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>

#define port 80

int main(){
	int sock_fd, connected_sock, sock_size, got;
	struct sockaddr_in sock_in;
	char buf[255];
	char msg[]="GET /\n";
	printf("Server starting.\n");

	sock_in.sin_family = AF_INET;
	sock_in.sin_port = htons(port);
	sock_in.sin_addr.s_addr = inet_addr("193.108.185.101"); // times.lv

	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(sock_fd < 0){
		printf("socket error\n");
		exit(EXIT_FAILURE);
	}
	if(connect(sock_fd, (struct sockaddr*)&sock_in, sizeof(sock_in))<0){
		printf("Connect error\n");
		exit(EXIT_FAILURE);
	}	
	printf("Connected\n");
	//send(sock_fd, msg, strlen(msg), 0);
	write(sock_fd, msg, strlen(msg));
	//got = recv(sock_fd, buf, 200, 0);
	got = read(sock_fd, buf, 200);
	if(got<0){
		printf("error obtaining data\n");
	}else{
		buf[got+1]='\0';
		printf("Got %s\n", buf);
	}
	shutdown(sock_fd, 2);
	return 0;
}

