#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>

#define port 4242

int main(){
	int sock_fd, connected_sock, sock_size;
	struct sockaddr_in sock_in;
	printf("Server starting.\n");

	sock_in.sin_family = AF_INET;
	sock_in.sin_port = htons(port);
	sock_in.sin_addr.s_addr = htonl(INADDR_ANY);

	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(sock_fd < 0){
		printf("socket error\n");
		exit(EXIT_FAILURE);
	}
	if(bind(sock_fd, (struct sockaddr*)&sock_in, sizeof(sock_in)) < 0){
		printf("Bind error\n");
		exit(EXIT_FAILURE);
	}
	printf("Binded\n");
	listen(sock_fd, 1);
	printf("Listening connections\n");
	sock_size = sizeof(sock_in);
	connected_sock = accept(sock_fd, (struct sockaddr*)&sock_in, &sock_size);
	printf("Accepted connection\n");
	write(connected_sock, "Hi!", 4);
	shutdown(connected_sock, 2);
	return 0;
}

