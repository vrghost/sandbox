#include <stdio.h>

#define panic_if(condition) if(condition) do_panic(__LINE__)
#define panic_if_not(condition) panic_if(!(condition))

void do_panic(int lineNo){
	printf("AAAA!!! Help!! Panic!!! (line %i)\n", lineNo);
	exit(1);
}

int main(){
	printf("lalala\n");
	panic_if_not(1 == 2);
	printf("lalala2\n");
	return 0;
}
