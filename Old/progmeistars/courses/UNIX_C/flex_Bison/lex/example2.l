%{
/////////////////////////////////////////////////
//        this is simple tab counter           //
/////////////////////////////////////////////////

#include <stdio.h>

int done=1;
int tabCount=0;

%}

%%

\t	{printf("Found %ith tab!\n", ++tabCount);}
.	;

%%

int yywrap(void){
	return done;
}

int main(void){
	yylex();
	printf("Found %i tabs.\n", tabCount);
	return 0;
}
