%{
#include <stdio.h>

#define MSG(m) printf(m ": '%s' (len:%i)\n", yytext, yyleng);

int done=0;

%}

%%

[0-9]+	{printf("digit: '%s'\n", yytext);}
[A-Z]+	{printf("big letter: '%s'\n", yytext);}

[a-z]{10,15}	{MSG("10-15 small letters");}
[a-z]{2,5}	{MSG("two to five small letters");}

[ \t\n]+	;	/* ignore whitespaces */

.	{
		MSG("UNKNOWN"); 
		done=1;
	}

%%

int yywrap(void){
	return done;
}

int main(void){
	yylex();
	return 0;
}
