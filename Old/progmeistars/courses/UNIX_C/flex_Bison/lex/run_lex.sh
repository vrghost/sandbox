#! /bin/sh
lex "$1"
echo "||||Compiling..."
gcc ./lex.yy.c -g -o "$1_out"
echo "||||Compiled, running...."
./"$1_out"
echo "||||Cleaning up..."
rm "$1_out"
rm lex.yy.c
