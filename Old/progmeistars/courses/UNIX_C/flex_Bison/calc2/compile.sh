#!/bin/sh

echo "::Yaccing."
yacc -v -d calc.y
echo "::Lexing."
lex calc.l
gcc lex.yy.c y.tab.c
echo "::Running"
./a.out
echo "::Cleaning up"
rm lex.yy.c y.tab.c y.tab.h
