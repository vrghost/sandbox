%{
	#include <stdio.h>
%}

%token NUM
%token PLUS
%token MINUS
%token MUL
%token DIV
%token EOL

%%

program:
	program expr EOL	{printf("=>%d\n", $2);}
	|
	;

val:
	NUM			{$$ = $1;}
	| MINUS val		{$$ = -$2;}
	| PLUS val		{$$ = $2;}
	;

expr:
	val			{$$ = $1;}
	| val MUL expr		{$$ = $1 * $3;}
	| val DIV expr		{$$ = $1 / $3;}
	| val PLUS expr	{$$ = $1 + $3;}
	| val MINUS expr	{$$ = $1 - $3;}
	;

%%

int yyerror(char *msg)
{
	printf("Error encountered: '%s' \n", msg);
}

int main(void){
	yyparse();
	return 0;
}

