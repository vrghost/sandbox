%{

#include <stdio.h>
#include <stdlib.h>

#include "y.tab.h"

%}

%%

[0-9]+	{
	yylval = atoi(yytext);
	return NUM;
}

[+]	return PLUS;
[-]	return MINUS;
[*]	return MUL;
[/]	return DIV;
\n	return EOL;

[ \t]	;

.	{printf("Unknown: '%s'\n", yytext);}

%%

int yywrap(void){
	return 0;
}
