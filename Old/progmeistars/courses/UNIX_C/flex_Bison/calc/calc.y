%{
	#include <stdio.h>
%}

%token NUM
%token MINUS

%%

program:
	program expr '\n'	{printf("=>%d\n", $2);}
	|
	;

expr:
	NUM			{$$ = $1;}
	| expr '+' expr		{$$ = $1 + $3;}
	| expr '//' expr	{$$ = $1 / $3;}
	| expr '*' expr		{$$ = $1 * $3;}
	| expr MINUS expr	{$$ = $1 - $3;}
	;

%%

int yyerror(char *msg)
{
	printf("Error encountered: '%s' \n", msg);
}

int main(void){
	yyparse();
	return 0;
}

