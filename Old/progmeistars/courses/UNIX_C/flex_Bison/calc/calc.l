%{

#include <stdio.h>
#include <stdlib.h>

#include "y.tab.h"

%}

%%

[0-9]+	{
	yylval = atoi(yytext);
	return NUM;
}

[-]	{return MINUS;}
[+/*]	{return yytext[0];}
\n	return *yytext;

[ \t]	;

.	{printf("Unknown: '%s'\n", yytext);}

%%

int yywrap(void){
	return 0;
}
