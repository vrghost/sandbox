#include <stdio.h>

int main(){
    int i=10;
    printf("In the beginning i=%i.\n", i);
    i = i++ + ++i - (i=3)*(i-- - --i);
    printf("At the end i=%i.\n", i);
    return 0;
}
