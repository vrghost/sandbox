#ifndef __HELPERS_H_INCLUDED__
#define __HELPERS_H_INCLUDED__

#include <stdio.h>

#define XSTR(q) #q
#define STR(q) XSTR(q)

#define SHOW_AND_CALL(msg, what, ...)\
	printf(msg " at address: %p\n", what);\
	if(what){what( __VA_ARGS__);}

#define CALL_PANIC(msg, obj) SHOW_AND_CALL(msg, obj->doPanic, obj)
#define SHOW_AND_CALL_PANIC(obj) CALL_PANIC("Panic handle", obj)

#define PANIC_IF(cond, obj) if(cond){\
		printf("----PANIC----\n"); \
		printf("[" STR(__FILE__) ":" STR(__LINE__) "]\n");\
		printf("Object at addr: %p\n", obj);\
		SHOW_AND_CALL("Panic function handle", obj->doPanic, obj);\
		printf("-----END PANIC-----\n");\
		exit(1);\
	}
#define PANIC_IF_NOT(cond, obj) PANIC_IF(!(cond), obj)

#endif
