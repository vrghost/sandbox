#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "field.h"
#include "game.h"
#include "player.h"

#include "helpers.h"



#define FIELD_H 5

#define WIN_L 3

int main()
{

	Game *game = initGame(FIELD_H, WIN_L, "XO", 2);

	Player *winner = NULL;
	while( !winner )
	{
		game->printField(game);
		game->readMove(game);
		winner = game->field->getWinner(game->field);
		game->nextPlayer(game);
	}
	printf("Game ended.\n");
	printf("Final field:\n");
	game->printField(game);
	return 0;

}

