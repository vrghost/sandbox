#ifndef __PLAYER_H_INCLUDED__
#define __PLAYER_H_INCLUDED__

#include <fcntl.h>

struct Player_t{
	char symbol;
	int inputfd;
	int outfd;
	void (*doPanic)(struct Player_t*);
	void (*readMove)(struct Player_t*, int *x, int *y);
	void (*sendString)(struct Player_t*, char*);
};

typedef struct Player_t Player;

Player* createPlayer(char symbol, int readfd, int printfd);

struct GamePlayers_t{
	Player **data;
	int count;
	int activePlayer;
};

typedef struct GamePlayers_t GamePlayers;

#endif
