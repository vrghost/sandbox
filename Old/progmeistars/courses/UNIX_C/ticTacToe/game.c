#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "field.h"
#include "game.h"
#include "player.h"

#include "helpers.h"

void main_panic(Game *game)
{

	CALL_PANIC("Current player", game->curPlayer);
	printf("Field at address: %p\n", game->field);
	if(game->field)
	{
		SHOW_AND_CALL_PANIC(game->field);
	}
}

GamePlayers* initGamePlayers(char* symbols, int count){
	int i;
	GamePlayers *out;
	out = (GamePlayers*)malloc(sizeof(GamePlayers));
	out->count = count;
	out->activePlayer = 0;
	out->data = (Player**)malloc(sizeof(Player*) * out->count);
	for(i=0;i<out->count;i++){
		out->data[i] = createPlayer(symbols[i], STDIN_FILENO, STDOUT_FILENO);
	}
	return out;
}


void printGameField(Game* game){
	char *data;
	PANIC_IF_NOT(game->field, game);
	data = game->field->toString(game->field);
	printf("%s", data);
	free(data);
}

void nextGamePlayer(Game *game){
	PANIC_IF_NOT(game, game);
	int player = game->players->activePlayer;

	player = (player + 1)%2;
	game->curPlayer = game->players->data[player];
	game->players->activePlayer = player;

}



void readGameMove(Game *game)
{

	int x, y, correctMove=0;
	
	int size=game->field->size;
	PANIC_IF_NOT(game, game);

	while(!correctMove){
		game->curPlayer->readMove(game->curPlayer, &x, &y);
		if(
			x >= 0 && y >= 0 && 
			x < size && y < size && 
			!(game->field->data[x][y])
		)
		{
			correctMove = 1;
		}
		else
		{
			game->curPlayer->sendString(game->curPlayer, "Incorrect move!\n");
			correctMove = 0;
		}
	}
	game->field->data[x][y] = game->curPlayer;
}

Game* initGame(int fieldSize, int winLength, char* playerSymbols, int playerCount)
{

	Game *out;
	out = (Game*)malloc(sizeof(Game));

	out->field = createField(fieldSize, winLength);
	out->players = initGamePlayers(playerSymbols, playerCount);
	out->curPlayer = out->players->data[out->players->activePlayer];

	out->doPanic = &(main_panic);
	out->printField = &(printGameField);
	out->readMove = &(readGameMove);
	out->nextPlayer = &(nextGamePlayer);

	return out;

}
