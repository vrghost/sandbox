#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "helpers.h"

#include "player.h"

void playerPanic(Player *player)
{
	printf("Player address: %p\n", player);	
	printf("Player symbol: '%c'\n", player->symbol);
	printf("Player input fd: %i", player->inputfd);
}

void readLine(Player *player, char *where, int maxsize)
{
	size_t total, res, finished;
	char *curpos;
	total = 0;
	res = 1;
	finished = 0;
	curpos = where;
	while(total <= maxsize && res > 0 && !finished){
		res = read(player->inputfd, curpos, 1);
		PANIC_IF(res < 0, player);
		finished = (*curpos == '\n');
		curpos += res;
		total += res;
	}
	PANIC_IF(total > maxsize, player);
	where[total] = '\0';
}

int sendPlayerData(Player *player, const char *data, size_t size)
{
	size_t total, written;
	total = 0;
	written = 1;
	while(total < size && written > 0){
		written = write(player->outfd, data+total, size-total);
		total += written;
	}
	PANIC_IF(written < 0, player);
	PANIC_IF_NOT(total==size, player);
	return total;
}

void readPlayerMove(Player* player, int* x, int* y)
{
	char queryString[42];
	int written;
	written = sprintf(queryString, "%c> ", player->symbol);
	PANIC_IF(written < 0, player);
	sendPlayerData(player, queryString, written);
	readLine(player, queryString, 42);
	sscanf(queryString, "%i %i", x, y);
}

void sendPlayerString(Player *player, char* str)
{
	int length;
	PANIC_IF_NOT(str, player);
	length = strlen(str);
	PANIC_IF(length <= 0, player);
	sendPlayerData(player, str, length);
}

Player* createPlayer(char symbol, int readfd, int printfd)
{
	Player *out;
	out = (Player*)malloc(sizeof(Player));
	out->symbol = symbol;
	out->inputfd = readfd;
	out->outfd = printfd;
	out->doPanic = &(playerPanic);
	out->readMove = &(readPlayerMove);
	out->sendString = &(sendPlayerString);
	return out;
}
