#ifndef __GAME_H_INCLUDED__
#define __GAME_H_INCLUDED__

struct Game_t
{

	Field *field;

	Player *curPlayer;
	GamePlayers *players;
	void (*doPanic)(struct Game_t *);
	void (*printField)(struct Game_t *);
	void (*readMove)(struct Game_t *);
	void (*nextPlayer)(struct Game_t *);
};

typedef struct Game_t Game;

Game* initGame(int fieldSize, int winLength, char* playerSymbols, int playerCount);

#endif
