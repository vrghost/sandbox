#ifndef __FIELD_H_INCLUDED__
#define __FIELD_H_INCLUDED__

#include "player.h"

struct Field_t{
	Player ***data;
	int size;
	int winLength;
	Player* (*getWinner)(const struct Field_t *);
	void (*doPanic)(const struct Field_t *);
	char* (*toString)(const struct Field_t*);
};

typedef struct Field_t Field;

Field* createField(int size, int winLength);

#endif
