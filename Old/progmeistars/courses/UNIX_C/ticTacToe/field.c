#include <stdio.h>

#include <stdlib.h>



#include "field.h"

#include "helpers.h"



void fieldPanicMessage(const Field *field)
{

	int i, t;
	printf("Field address: %p\n", field);
	if(field)
	{
		printf("Field size: %i\n", field->size);
		printf("Field win length: %i\n", field->winLength);
		printf("Field root array: ");
		for(i=0; i < field->size; i++){
			printf("%p ", field->data[i]);
		}
		printf("\n");
		printf("Field data:\n");
		for(i=0; i < field->size; i++){
			for(t=0; t < field->size; t++){
				printf("%p ", field->data[i][t]);
			}
			printf("\n");
		}
	}
	else
	{
		printf("Field is null.\n");
	}

}



Player* getHorizWinner(const Field *field)
{
	int x, y, runLength=0;
	Player *curPlayer=NULL;
    for(x=0;(x < field->size) && (runLength < field->winLength);x++)
    {
	curPlayer = NULL;
	runLength = 0;
	for(y=0;(y < field->size) && (runLength < field->winLength);y++)
	{
		if(!field->data[x][y]){
			curPlayer = NULL;
			runLength = 0;
			continue;
		}
		if(field->data[x][y] == curPlayer)
		{
			runLength++;
		}
		else
		{
			curPlayer = field->data[x][y];
			runLength = 1;
		}
	}
    }
    if(runLength < field->winLength){
	curPlayer = 0;
    }
    return curPlayer;
}

Player* getVertWinner(const Field *field)
{
	int x, y, runLength=0;
	Player *curPlayer=NULL;

	curPlayer = NULL;
	runLength = 0;

	for(x=0;(x < field->size) && (runLength < field->winLength);x++)
	{
		curPlayer = NULL;
		runLength = 0;
		for(y=0;(y < field->size) && (runLength < field->winLength) ;y++)
		{
		    	if(!field->data[y][x]){
				curPlayer = NULL;
				runLength = 0;
				continue;
			}
			if(field->data[y][x] == curPlayer)
			{
				runLength++;
			}
			else
			{
				curPlayer = field->data[y][x];
				runLength = 1;
			}
		}
	}
	if(runLength < field->winLength){
		curPlayer = 0;
	}
	return curPlayer;
}



Player* checkDiag(const Field *field, int startX, int startY, int type)
{

	int x=startX, y=startY, runLength=0;
	Player *curPlayer=NULL;
	PANIC_IF_NOT(type == 1 || type == -1, field);
	while(
    		x >= 0 && y >= 0 && 
		x < field->size && y < field->size && 
		runLength < field->winLength
	){
		if(!field->data[x][y])
		{
			curPlayer = NULL;
			runLength = 0;
		}
		else
		{
			if(field->data[x][y] == curPlayer){
			 	runLength++;
			}else{
				curPlayer = field->data[x][y];
				runLength = 1;
			}
		}
		if(type == 1)
		{
			x += 1;
			y += 1;
		}
		else
		{
			x += 1;
			y -= 1;
		}
    }
    if(runLength < field->winLength){
	curPlayer = 0;
    }
    return curPlayer;
}

Player* getDiagWinner(const Field *field)
{

	int i;
	Player *found=NULL;
	#define __CHECK(x,y,t) if(!found){ found = checkDiag(field, x, y, t);}
	for(i=0; i < field->size && !found; i++){
		__CHECK(i, 0, 1);
		__CHECK(0, i, 1);
		__CHECK(0, i, -1);
		__CHECK(i, field->size-1, -1);
	}
	#undef __CHECK
	return found;

}



Player* getWinner(const Field *field)
{
	Player *found=NULL;
	found = getHorizWinner(field);
	if(!found)
	{
		found = getVertWinner(field);
	}
	if(!found)
	{
		found = getDiagWinner(field);
	}

	return found;

}

int writeFieldLine(Player **line, int size, char* writeTo){
	int written=0, i;
	for(i=0;i < size;i++){
		if(line[i])
		{
			writeTo[written] = (line[i])->symbol;
		}
		else
		{
			writeTo[written] = ' ';
		}
		written++;
		if(i < size-1){
			writeTo[written] = '|';
			written++;
		}
	}
	writeTo[written] = '\n';
	written++;
	return written;
}

char* fieldToString(const Field *field)
{
	char *out, *curWriting;
	Player **line;
	int i, line_size;
	line_size = (field->size*2+1)*field->size + 1;
	out = (char*)malloc(sizeof(char)*line_size);
	curWriting = out;
	for(i=0;i<field->size;i++)
	{
		line = field->data[i];
		curWriting += writeFieldLine(line, field->size, curWriting);
	}
	out[line_size-1] = '\0';
	return out;
}



Field* createField(int size, int winLength)
{

	Player ***_field;

	int i, t;

	Field *out;

	_field = (Player***)malloc(sizeof(Player**)*size);

	for(i=0;i<size;i++){

		_field[i] = (Player**)malloc(sizeof(Player*)*size);

		for(t=0;t<size;t++){

			_field[i][t] = NULL;

		}

	}

	out = (Field*)malloc(sizeof(Field));
	out->data = _field;
	out->size = size;
	out->winLength = winLength;
	out->getWinner = &(getWinner);
	out->doPanic = &(fieldPanicMessage);
	out->toString = &(fieldToString);
	return out;

}

