# -*- coding: utf-8 -*-

import os, sys
dir = os.path.realpath(sys.argv[1])
print("Root dir: %s" % dir)

for _name in os.listdir(dir):
    _target = os.path.join(dir, _name)
    if os.path.isdir(_target):
        _cacls = "CACLS %(target)s /T /P %(name)s:F Administrator:F" % {
            "target": _target,
            "name": _name
        }
        print _cacls
        os.system(_cacls)

# vim: set et sts=4 sw=4 :
