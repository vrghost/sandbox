class ClassHolder(object):

	@staticmethod
	def classes():
		return ("class1", "class5", "class3")

	@staticmethod
	def class5():
		return (
			"comp501",
			"comp502",
			"comp503",
			"comp504",
			"comp505",
			"comp506",
			"comp507",
			"comp508",
			"comp509",
			"comp510",
			"comp511",
			"comp512",
		)

	@staticmethod
	def class1():
		return (
			"comp101",
			"comp102",
			"comp103",
			"comp104",
			"comp105",
			"comp106",
			"comp107",
			"comp108",
			"comp109",
			"comp110",
			"comp111",
		)

	@staticmethod
	def class3(withOldies=False):
		_list = [
			"comp307",
			"comp308",
			"comp309",
			"comp310",
			"comp311",
		]
		if withOldies:
			_list += [
				"comp306",
				"comp305",
				"comp304",
				"comp303",
				"comp302",
				"comp301",
			]
		return _list
