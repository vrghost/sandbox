import sys
import os.path

import comps
from executor import execute

_file = sys.argv[1]
_args = sys.argv[2:]

if __name__=="__main__":
	_comps = []
	if ("1" in _args) or ("class1" in _args):
		_comps += comps.class1
	if ("5" in _args) or ("class5" in _args):
		_comps += comps.class5
	if os.path.splitext(_file)[1] == ".py":
		_command = "python"
	for _comp in _comps:
		execute("%s %s %s" % (_command, _file, _comp), True)
