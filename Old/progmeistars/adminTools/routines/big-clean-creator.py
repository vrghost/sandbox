import os

from comps import ClassHolder

path = [ r"e$", r"C$\temp", r"d$"]
#path = [r"c$\FPC "]

_comps = []

if "c3" in os.sys.argv:
    _comps += ClassHolder.class3()

if "c5" in os.sys.argv:
    _comps += ClassHolder.class5()

if "c1" in os.sys.argv:
    _comps += ClassHolder.class1()

for c in _comps :
    for _p in path:
        _path = "\\\\"+ os.path.join(c, _p)
	_guard = os.path.join(_path, "Guard")
        print "rmdir /s /q " + _path
	print "mkdir " + _path
	print "fileacl \"" + _path + "\" /S Everyone:RWXD /REPLACE /PROTECT"
	print 'echo "" > ' + _guard
	print "fileacl \"" + _guard + "\" /D Everyone:RWXD /REPLACE /PROTECT"
