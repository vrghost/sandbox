import os

def execute(args, verbose=False):
	if isinstance(args, basestring):
		cmd = args
	else:
		cmd = " ".join(args)
	if verbose:
		print "executing", cmd
	output = os.system(cmd)
	if verbose:
		print output
	return output
