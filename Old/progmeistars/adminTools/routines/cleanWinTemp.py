import os
from glob import glob

from comps import ClassHolder

_windir = r"C$\WINNT"

_comps = []

if "c3" in os.sys.argv:
    _comps += ClassHolder.class3()

if "c5" in os.sys.argv:
    _comps += ClassHolder.class5()

if "c1" in os.sys.argv:
    _comps += ClassHolder.class1()

for _comp in _comps:
	_path = "\\\\" + os.path.join(_comp, _windir)
	_temps = []
	try:
		_dir = os.listdir(_path)
	except:
		print _comp, "passed"
		continue
	for _file in _dir:
		if _file[0] == _file[-1] == "$":
			# temp file!
			print "rmdir /s /q \"%s\"" % os.path.join(_path, _file)
