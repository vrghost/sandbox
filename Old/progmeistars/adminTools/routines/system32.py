import sys

from executor import execute

try:
	_comp = sys.argv[1]
except IndexError:
	raise Exception("no computer name is given!")

pathStr = r"\\%s\c$\WINNT\system32" "\\"

caclsStr = r"CACLS %(path)s /E %(perms)s"

files = (
	("rundll32.exe", r"/D Students"),
	("mshtml.dll", r"/D Students"),
	("mshtml.tlb", r"/D students"),
)

if __name__=="__main__":
	_path = pathStr %  _comp
	for (_fname, _perms) in files:
		_filePath = _path + _fname
		_cmd = caclsStr % {"path": _filePath, "perms": _perms}
		execute(_cmd, True)
