import os

import comps

_works = [
	( r"C:\ghost-priv\NTUSERS", r"\\%(comp)s\c$\Documents and Settings"),
]

def execute(args):
	if isinstance(args, basestring):
		cmd = args
	else:
		cmd = " ".join(args)
	print cmd
	output = os.system(cmd)
	return output

if __name__ == "__main__":
	_comps = comps.class1
	_copyCmd = r"""ROBOCOPY /S /NP "%(src)s" "%(dest)s" """
	_delCmd = r"""ECHO DEL /S /Q "%(dest)s" """
	for _comp in _comps:
		for _work in _works:
			(_src, _dest) = _work
			_dest = _dest % {"comp": _comp}
			_cmd = _delCmd % {"dest": _dest}
			print _cmd
			print execute(_cmd)
			_cmd = _copyCmd % {
				"src": _src,
				"dest": _dest,
			}
			print _cmd
			print execute(_cmd)
