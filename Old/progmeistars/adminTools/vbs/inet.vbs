On Error Resume Next

Const ADS_UF_SCRIPT = &H0001 
Const ADS_UF_ACCOUNTDISABLE = &H0002 
Const ADS_UF_HOMEDIR_REQUIRED = &H0008 
Const ADS_UF_LOCKOUT = &H0010 
Const ADS_UF_PASSWD_NOTREQD = &H0020 
Const ADS_UF_PASSWD_CANT_CHANGE = &H0040 
Const ADS_UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED = &H0080 
Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000 
Const ADS_UF_SMARTCARD_REQUIRED = &H40000 
Const ADS_UF_PASSWORD_EXPIRED = &H800000 


strComputer = WScript.Arguments.Item(0)

Set colAccounts = GetObject("WinNT://" & strComputer & "")


Set objUser = colAccounts.GetObject("user", "Inet")

If Err.Number > 0 OR Err.Number < 0 Then
	Set objUser = colAccounts.Create("user", "Inet")
End If

objUser.SetPassword "XXXXX"

objPasswordExpirationFlag = ADS_UF_DONT_EXPIRE_PASSWD OR _
	ADS_UF_PASSWD_CANT_CHANGE	
objUser.Put "userFlags", objPasswordExpirationFlag 



objUser.SetInfo

'Set objGroup = GetObject("WinNT://" & strComputer & "/Students,group")
'objGroup.Remove(objUser.ADsPath)

Set objGroup = GetObject("WinNT://" & strComputer & "/InetUsers,group")
objGroup.Add(objUser.ADsPath)

Set objGroup = GetObject("WinNT://" & strComputer & "/Users,group")
objGroup.Add(objUser.ADsPath)
