
 '  [installed.vbs]
 ' list installed programs on given computer, and save it to file

'sComputer = WScript.Arguments.Item(0)
Const OUTPUT_FILE_NAME = "\\Ntserver\computerInstalledFiles\"
Const sComputer = "."

compName = getComputerName(sComputer)

outString="Computer: " & compName & vbCrLf _
	& "Date: " & Now & vbCrLf _
    & "Software: " & vbCrLf _
    & "    Windows: " & getWindowsName(sComputer) & " (Version " & getWindowsVersion(sComputer) & " )" & VbCrLf _
    & "    ( Product ID: " & getWindowsProductID(sComputer) & " )" &vbCrLf _
	&  vbCrLf & "    Installed applications:" & vbCrLf _
	& InstalledApplications(sComputer)

Set objFileSystem = CreateObject("Scripting.FileSystemObject")
Set objOutputFile = objFileSystem.CreateTextFile(OUTPUT_FILE_NAME & compName &".txt", TRUE)

objOutputFile.Write(outString)

'wscript.echo outString


objOutputFile.Close

Set objFileSystem = Nothing


Function InstalledApplications(node)
 Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE
 Set oRegistry = _
  GetObject("winmgmts:{impersonationLevel=impersonate}!\\" _
  & node & "/root/default:StdRegProv")
 sBaseKey = _
  "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\"
 iRC = oRegistry.EnumKey(HKLM, sBaseKey, arSubKeys)
 For Each sKey In arSubKeys
  iRC = oRegistry.GetStringValue( _
   HKLM, sBaseKey & sKey, "DisplayName", sValue)
  If iRC <> 0 Then
   oRegistry.GetStringValue _
    HKLM, sBaseKey & sKey, "QuietDisplayName", sValue
  End If
  If sValue <> "" Then
   InstalledApplications = _
    InstalledApplications & "        " & sValue & vbCrLf
  End If
 Next
End Function


Function getComputerName(node)
 Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE
 Set oRegistry = _
  GetObject("winmgmts:{impersonationLevel=impersonate}!\\" _
  & node & "/root/default:StdRegProv")
 sBaseKey = "SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName"

  oRegistry.GetStringValue HKLM,sBaseKey,"ComputerName",out
  getComputerName=out
End Function

Function getWindowsName(node)
 Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE
 Set oRegistry = _
  GetObject("winmgmts:{impersonationLevel=impersonate}!\\" _
  & node & "/root/default:StdRegProv")
 sBaseKey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion"

  oRegistry.GetStringValue HKLM,sBaseKey,"ProductName",out
  getWindowsName=out
End Function

Function getWindowsVersion(node)
 Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE
 Set oRegistry = _
  GetObject("winmgmts:{impersonationLevel=impersonate}!\\" _
  & node & "/root/default:StdRegProv")
 sBaseKey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion"

  oRegistry.GetStringValue HKLM,sBaseKey,"CurrentVersion",out
  getWindowsVersion=out
End Function

Function getWindowsProductID(node)
 Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE
 Set oRegistry = _
  GetObject("winmgmts:{impersonationLevel=impersonate}!\\" _
  & node & "/root/default:StdRegProv")
 sBaseKey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion"

  oRegistry.GetStringValue HKLM,sBaseKey,"ProductId",out
  getWindowsProductID=out
End Function

Function getCPU(node)
 Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE
 Set oRegistry = _
  GetObject("winmgmts:{impersonationLevel=impersonate}!\\" _
  & node & "/root/default:StdRegProv")
 sBaseKey = "SYSTEM\CurrentControlSet\Enum\ACPI\"

  oRegistry.GetStringValue HKLM,sBaseKey,"ProductId",out
  getCPU=out
End Function



