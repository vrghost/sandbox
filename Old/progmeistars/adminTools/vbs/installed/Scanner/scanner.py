import re
import glob
from time import strftime

class MySet(object):

    def __init__(self, items=None):
        if items is None:
            self.items = set()
        else:
            self.items = set(items)

    def __contains__(self, item):
        for i in self.items:
            if i.match(item):
                return True
        return False

    def add(self, item):
        return self.items.add(re.compile(item))

## generate allowed/denied sets

allowed = MySet()
denied = MySet()

for line in file("list.txt"):
    line = line.strip()
    if line:
        if line[0] == "+":
            assert line[1:] not in denied
            allowed.add(line[1:])
        elif line[0] == "-":
            assert line[1:] not in allowed
            denied.add(line[1:])

## generate HTML

out = '<html><body>\n'

template = """\
<html>
  <body><center><table><tr><td>
%(tables)s
  </tr></td></table></center><i>Generated: """+strftime("%H:%M %d.%m.%Y")+"""</i></body>
</html>"""

tableTemplate = """\
    <table border="1" cellspacing="0" cellpadding="2" width="100%%">
      <tr><td><font size="-3">%(fname)s</font></td></tr>
	  <tr><th>%(compname)s</th></tr>		
	  <tr><td bgcolor="#D2EDFE">Scanning date: %(date)s</td></tr>
%(body)s
    </table>
    <br>
"""

bodyTemplate = """\
      <tr>
        <td bgcolor="%(color)s">%(name)s</td>
      </tr>\
"""

yellowColor = "#F9FDBF"
redColor = "#FCA2B1"
greenColor = "#DAFDCD"

def run():
    tables = ""
    for filename in glob.glob("..\*.txt"): 
        body = ""
        compname = ""
        upDate=""
        red = []
        yellow = []
        green = []
        appList = False
        for line in file(filename):
            line = line.strip()
            if line.find("Computer:") == 0:
				compline = line[line.find(":")+1:].strip()
            if line.find("Date:") == 0:
				upDate = line[line.find(":")+1:].strip()

            if appList:
				if line in allowed:
					green.append(line)
				elif line in denied:
					red.append(line)
				else:
					yellow.append(line)
            appList = appList or (line == "Installed applications:")
	green.sort()
	red.sort()
	yellow.sort()

        for line in red:
				body += bodyTemplate % {"color": redColor, "name": line}
        for line in yellow:
				body += bodyTemplate % {"color": yellowColor, "name": line}
        for line in green:
				body += bodyTemplate % {"color": greenColor, "name": line}

	tables += tableTemplate % {"body": body, "fname": filename, "compname":compline ,"date":upDate}

    out = template % {"tables": tables}
    file("result.html", "wt").write(out)


if __name__ == "__main__":
    run()

