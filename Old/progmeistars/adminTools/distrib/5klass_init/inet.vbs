'On Error Resume Next

Const ADS_UF_SCRIPT = &H0001 
Const ADS_UF_ACCOUNTDISABLE = &H0002 
Const ADS_UF_HOMEDIR_REQUIRED = &H0008 
Const ADS_UF_LOCKOUT = &H0010 
Const ADS_UF_PASSWD_NOTREQD = &H0020 
Const ADS_UF_PASSWD_CANT_CHANGE = &H0040 
Const ADS_UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED = &H0080 
Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000 
Const ADS_UF_SMARTCARD_REQUIRED = &H40000 
Const ADS_UF_PASSWORD_EXPIRED = &H800000 


strComputer = WScript.Arguments.Item(0)
dim names(5)
names(0) = "5klass"
names(1) = "5klass1"
names(2) = "5klass2"
names(3) = "5klass3"
names(4) = "5klass4"
names(5) = "5klass5"

dim pass(5)
pass(0) = "password"
pass(1) = "1"
pass(2) = "2"
pass(3) = "3"
pass(4) = "4"
pass(5) = "5"


for i = 0 to 5

userName = names(i)
userPass = pass(i)

Set colAccounts = GetObject("WinNT://" & strComputer & "")
'Set objUser = colAccounts.GetObject("user", userName)
'If Err.Number > 0 OR Err.Number < 0 Then
	Set objUser = colAccounts.Create("user", userName)
'End If
objUser.SetPassword userPass
objPasswordExpirationFlag = ADS_UF_DONT_EXPIRE_PASSWD OR _
	ADS_UF_PASSWD_CANT_CHANGE	
objUser.Put "userFlags", objPasswordExpirationFlag 
objUser.SetInfo
Set objGroup = GetObject("WinNT://" & strComputer & "/Users")
objGroup.Add(objUser.ADsPath)
Set objGroup = GetObject("WinNT://" & strComputer & "/Administrators")
objGroup.Add(objUser.ADsPath)

next

