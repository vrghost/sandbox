On Error resume next

strComputer = WScript.Arguments.Item(0)
startTime = WScript.Arguments.Item(1)

Set objWMI = GetObject("winmgmts:" _
	& "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

Set objLogfiles = objWMI.ExecQuery(_
	"Select * from Win32_NTLogEvent WHERE TimeWritten >= '" & startTime & "'")

Wscript.Echo "("
For Each objLog in objLogfiles
    If isNull(objLog) Then
	Escript.Echo "Null!"
    Else
	Wscript.Echo "{"
	Wscript.Echo "'Category': '''" & objLog.Category & "''',"
	Wscript.Echo "'ComputerName': '''" & objLog.ComputerName & "''',"
	Wscript.Echo "'Message': '''" & objLog.Message & "''',"
	Wscript.Echo "'EventCode': '''" & objLog.EventCode & "''',"
	Wscript.Echo "'Message': '''" & objLog.Message & "''',"
	Wscript.Echo "'User': '''" & objLog.User & "''',"
	Wscript.Echo "'Type': '''" & objLog.Type & "''',"
	Wscript.Echo "'TimeWritten': '''" & objLog.TimeWritten & "''',"
	Wscript.Echo "'SourceName': '''" & objLog.SourceName & "''',"
	Wscript.Echo "'RecordNumber': '''" & objLog.RecordNumber & "''',"
	Wscript.Echo "'Logfile': '''" & objLog.Logfile & "''',"
	Wscript.Echo "},"
    End If
Next
Wscript.Echo ")"
