# -*- coding: utf-8 -*-

#from .
import config
import util
import base

class Root(base.BaseObject):

    logger = None

    def __init__(self):
        self._loadConfig()
        self.logger = util.Logger()
        self.mailer = util.Mailer()
        self._fillBase()

    def _loadConfig(self):
        _base = base.BaseObject
        _base.config = config

    def _fillBase(self):
        _base = base.BaseObject
        # setting log handlers
        for (_name, _handler) in self.logger.handlers.iteritems():
            setattr(_base, _name, staticmethod(_handler))
        _base.toUser = staticmethod(self.toUser)

    def terminate(self):
        self.logger.terminate()

    def executeTask(self, name, args):
        self.toUser("We were called to perform a %r" % name)
        try:
            if name == "backup":
                import backuper
                backuper.run()
            elif name == "defrag":
                import defragment
                defragment.run()
            elif name == "viewEvents":
                import eventViewer
                eventViewer.run()
            elif name == "removeOldFiles":
                import oldFileRemover
                oldFileRemover.run()
            elif name == "removeForbiddenFiles":
                import forbiddenFileRemover
                forbiddenFileRemover.run()
            elif name == "setExclusivePerms":
                import exclusivePermSetter
                exclusivePermSetter.run()
            else:
                raise NotImplementedError
        except:
            self.log_exception("====== Top-level exception ======")
        if self.config.logging.sendFullLog:
            self.mailer.sendMail(
                "Script full output.", self.logger.getFullLog())
        else:
            self.mailer.sendCollectedData(
                "Script user output.")

    def toUser(self, data, eol=True):
        if not isinstance(data, basestring):
            data = str(data)
        if eol and not data.endswith("\n"):
            data += "\n"
        self.log_debug(">>> |%s|", data.rstrip())
        self.mailer.addData(data)

# vim: set et sts=4 sw=4 :
