# -*- coding: utf-8 -*-

import os

import base
from util import executor

class Defragmenter(base.BaseObject):

    def run(self):
        _retcode = executor.execute(
            os.path.join(self.config.dir.bin, "JkDefragCmd.exe"))[0]
        if _retcode != 0:
            self.log_error("Program return code: %s", _retcode)
            self.toUser("Strange return code")
        else:
            self.log_debug("Program return code: %s", _retcode)
            self.toUser("Succeeded.")

def run():
    _defrag = Defragmenter()
    _defrag.run()

# vim: set et sts=4 sw=4 :
