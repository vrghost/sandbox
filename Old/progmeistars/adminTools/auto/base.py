# -*- coding: utf-8 -*-

#from .

class BaseObject(object):

    def _noLog(self, *args):
        raise NotImplementedError("Handler not set. Yet.")

    log_debug = log_info = log_exception = \
    log_error = _noLog

# vim: set et sts=4 sw=4 :
