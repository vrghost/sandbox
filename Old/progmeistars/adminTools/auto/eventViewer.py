# -*- coding: utf-8 -*-

import itertools
import os

import base
from util import executor
import datetime

class Viewer(base.BaseObject):

    _vbsName = "getEvents.vbs"
    _dtFormat = "%Y%m%d%H%M%S.000000+000"
    _interestingFields = (
        "User",
        "SourceName",
        "ComputerName",
        "EventCode",
        "Type",
        "Message",
        "^COUNT",
    )
    _groupFields = (
        "Logfile",
        "User",
        "EventCode",
        "SourceName",
        "Type",
        "Message",
    )

    def _filter(self, data):
        _out = []
        for _item in data:
            _type = _item["Type"].lower()
            if _type in ("error", "warning"):
                _out.append(_item)
            else:
                if _type not in ("information", ):
                    self.log_error("Unknown log type: '%s'", _type)
        return _out

    def _sort(self, data):
        def _keyFunc(el):
            _out = []
            for _name in self._groupFields:
                _out.append(el[_name])
            return _out
        data.sort(key=_keyFunc)
        _groups = itertools.groupby(data, _keyFunc)
        _items = []
        for (_group, _iter) in _groups:
            _same = tuple(_iter)
            _one = _same[0]
            _one["^COUNT"] = len(_same)
            _items.append(_one)
        return itertools.groupby(_items, lambda el: el["Logfile"])

    def run(self):
        _time = datetime.datetime.utcnow() - \
            datetime.timedelta(self.config.eventViewer.checkDays)
        (_retcode, _text) = executor.execute((
            "cscript",
            "//Nologo",
            os.path.join(self.config.dir.vbs, self._vbsName),
            "%COMPUTERNAME%",
            _time.strftime(self._dtFormat)
        ))
        _data = eval(_text)
        self.output(self._sort(self._filter(_data)))

    def output(self, data):
        self.toUser("======== Filtered event list ======\n")
        for (_log, _iter) in data:
            self.toUser("======= Log: %s\n" % _log)
            for _item in _iter:
                _lines = []
                for _field in self._interestingFields:
                    _lines.append(
                        "-> %s: %s" % (_field.upper(), _item[_field]))
                _lines.append("-" * 20)
                _lines.append("")
                self.toUser("\n".join(_lines))


def run():
    Viewer().run()

# vim: set et sts=4 sw=4 :
