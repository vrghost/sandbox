# -*- coding: utf-8 -*-

import os
from fnmatch import fnmatch

import base

class Remover(base.BaseObject):

    def run(self):
        _totalErasedFiles = 0
        _masks = self.config.forbiddenFileRemover.masks
        for _dir in self.config.forbiddenFileRemover.dirs:
            self.log_debug("Cleaning up root dir '%s'", _dir)
            _erasedFiles = self._cleanupDir(_dir, _masks)
            self.log_debug("Erased %s files.", _erasedFiles)
            _totalErasedFiles += _erasedFiles
        self.toUser("Removed %s files in total." % _totalErasedFiles)

    def _cleanupDir(self, dir, masks):
        _erasedFiles = 0
        for (_root, _dirs, _files) in os.walk(dir, True):
            for _file in _files:
                _file = os.path.join(_root, _file)
                if self.isForbidden(_file, masks):
                    self.log_debug(
                        "File '%s' is forbidden. Removing it.", _file)
                    self.doUnlink(_file)
                    _erasedFiles += 1
        return _erasedFiles

    def isForbidden(self, file, masks):
        _fname = os.path.split(file)[1]
        for _mask in masks:
            if fnmatch(_fname, _mask):
                return True
        else:
            return False

    def doUnlink(self, file):
        try:
            os.unlink(file)
        except OSError, _err:
            self.toUser("Error removing file '%s' (%s)" % (file, _err))

def run():
    _remover = Remover()
    _remover.run()

# vim: set et sts=4 sw=4 :
