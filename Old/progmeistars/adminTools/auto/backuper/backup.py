from time import strftime
import fnmatch
import os
import sys

import base
from util import executor

class Backuper(base.BaseObject):

    archExt = ".zip"

    def robocopy(self, source, target):
        _options = [
            "/E",
            "/Z",
            "/NP",
            "/XD",  "System Volume Information",
            "/R:5",
        ]
        _exeName = os.path.join(self.config.dir.bin, "robocopy.exe")
        _retcode = executor.execute([_exeName, source, target] + _options)[0]
        if _retcode & 0x10:
            self.log_error(
                "Robocopy reported serious error.\n" \
                "( RoboCopy did not copy any files. " \
                " This is either a usage error or an error " \
                "due to insufficient access privileges on " \
                "the source or destination directories. )"
            )
        if _retcode & 0x08:
            self.log_error(
                "Some files or directories could not be copied " \
                "(copy errors occurred and the retry limit was exceeded)." \
                "Check these errors further."
            )
        if _retcode < 8:
            self.log_debug("Robocopy succeeded. (%s)", _retcode)

    def zip(self, source, target):
        if os.path.exists(target):
            os.unlink(target)
        _options = ["a", "-r", "-tZIP"]
        _exeName = os.path.join(self.config.dir.bin, "7za.exe")
        _retcode = executor.execute([_exeName] + _options + [target, source])[0]
        if _retcode == 0:
            self.log_debug("Archiving files succeeded.")
        else:
            self.log_error("Archiver returned non-zero code (%s)", _retcode)

    def scp(self, source):
        _fileDir = os.path.split(__file__)[0]
        _keyFile = os.path.join(_fileDir, "id_rsa.ppk")
        if not os.path.exists(_keyFile):
            self.log_fatal("No key file for scp copy found.")
            return
        _options = [
            "-batch", "-l" , self.config.backup.scp.user, "-r",
            "-q", "-P", self.config.backup.scp.port, "-i", _keyFile,
        ]
        _host = "%s:%s" % (
            self.config.backup.scp.host,
            self.config.backup.scp.dir,
        )
        _options.extend((source, _host))
        _exeName = os.path.join(self.config.dir.bin, "pscp.exe")
        _retcode = executor.execute([_exeName] + _options)[0]
        if _retcode == 0:
            self.log_debug("SCP copy succeeded.")
        else:
            self.log_error("SCP copy failed.")

    def toFilename(self, path):
        _out = path
        for _symb in (os.sep, ":"):
            _out = _out.replace(_symb, "_")
        _out = _out.strip("_")
        return _out

    def backupUserFiles(self):
        for _src in self.config.backup.copyFrom.sources:
            _target = os.path.join(
                self.config.backup.dir.unarch,
                self.toFilename(_src),
            )
            self.toUser("Backing up '%s' ==> '%s'" % (_src, _target))
            self.robocopy(_src, _target)

    def makeFullArch(self):
        _fullCur = os.path.join(
            self.config.backup.dir.arch,
            "fullCurrent" + self.archExt,
        )
        _fullPrev = os.path.join(
            self.config.backup.dir.arch,
            "fullPrev" + self.archExt,
        )
        if os.path.exists(_fullPrev):
            self.log_debug("Removing '%s'." % _fullPrev)
            os.unlink(_fullPrev)
        if os.path.exists(_fullCur):
            self.log_debug("Rotating '%s' => '%s'" % (_fullCur, _fullPrev))
            os.rename(_fullCur, _fullPrev)
        else:
            self.log_info("No previous full arch found.")
        self.log_debug("Archiving unarch to full arch.")
        self.zip(self.config.backup.dir.unarch, _fullCur)

    def archImportant(self):
        _created = []
        for _fname in self.config.backup.importantFiles:
            _unarchName = os.path.join(
                self.config.backup.dir.unarch,
                self.toFilename(_fname),
            )
            if not os.path.exists(_unarchName):
                self.log_critical("No important file '%s' found", _unarchName)
                continue
            _archFile = "".join((
                self.toFilename(_fname),
                strftime("_%Y%m%d_%H%M"),
                self.archExt,
            ))
            _archFile = os.path.join(
                self.config.backup.dir.arch,
                _archFile
            )
            self.log_debug("Archiving important file '%s' ==> '%s'",
                _unarchName, _archFile)
            self.zip(_unarchName, _archFile)
            _created.append(_archFile)
        return _created

    def sync(self):
        for (_src, _dst) in self.config.backup.sync:
            self.log_debug("Syncing '%s' ==> '%s'.", _src, _dst)
            self.robocopy(_src, _dst)

    def run(self):
        self.toUser("Collecting user files...")
        self.backupUserFiles()
        self.toUser("Creating full archive...")
        self.makeFullArch()
        self.toUser("Archiving important files...")
        _needScp = self.archImportant()
        self.toUser("Syncing with remote backup systems...")
        self.sync()
        self.toUser("Saving important files to scp host...")
        for _name in _needScp:
            self.scp(_name)
        self.toUser("Finished.")


def run():
    _bak = Backuper()
    _bak.run()

# vim: set et sts=4 sw=4 :
