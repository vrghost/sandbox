# -*- coding: utf-8 -*-

import sys

# from .
import main

assert __name__ == "__main__"

_args = sys.argv[1:]
_root = main.Root()
_root.executeTask(_args[0], _args[1:])
_root.terminate()

# vim: set et sts=4 sw=4 :
