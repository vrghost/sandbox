# -*- coding: utf-8 -*-

from smtplib import SMTP
from cStringIO import StringIO

import base

class Mailer(base.BaseObject):

    _messageTemplate = \
        "From: %(fromAddr)s\r\n" \
        "To: %(toAddr)s\r\n" \
        "Subject: %(subject)s\r\n\r\n" \
        "%(text)s"


    def __init__(self):
        self.data = StringIO()

    def addData(self, str):
        assert isinstance(str, basestring)
        self.data.write(str)

    def sendCollectedData(self, subject):
        # goin' to the start of collected data
        self.data.seek(0, 0)
        _data = self.data.read()
        if not _data:
            _data = "<<NO OUTPUT>>"
        self.sendMail(subject, _data)
        self.data.seek(0, 0)

    def sendMail(self, subject, text):
        _session = SMTP(**self.config.mail.server)
        _msg = self._messageTemplate % {
            "fromAddr": self.config.mail.fromAddr,
            "toAddr": ",".join(self.config.mail.toAddrs),
            "subject": subject,
            "text": text,
        }
        _session.sendmail(
            self.config.mail.fromAddr,
            self.config.mail.toAddrs,
            _msg,
        )
        _session.quit()

# vim: set et sts=4 sw=4 :
