import time
import os

import base

class Executor(base.BaseObject):

    _redirectHandles = " ".join((
        "%i>&1" % _num
        for _num in xrange(2, 10)
    ))

    def _normalizeWords(self, words):
        for _word in words:
            if not isinstance(_word, basestring):
                _word = str(_word)
            _word = _word.strip()
            if " " in _word and not (
                _word[0] == _word[-1] and
                _word[0] in ('"', "'")
            ):
                _word = '"%s"' % _word
            yield _word

    def execute(self, words):
        self.log_debug("Got words for execution: %s.", words)
        _fname = os.path.join(self.config.dir.tmp, "%s.tmp" % time.time())
        _batname = os.path.join(self.config.dir.tmp, "%s.bat" % time.time())
        if isinstance(words, basestring):
            words = (words, )
        command = " ".join(self._normalizeWords(words))
        file(_batname, "w").write(command)
        _com = '%s > "%s" %s'% (
            _batname,
            _fname,
            self._redirectHandles,
        )
        self.log_debug("Executing: %s", _com)
        _retcode = os.system(_com)
        _text = file(_fname).read()
        self.log_debug("\n".join((
            "Program output:",
            "=" * 20,
            _text,
            "=" * 20,
        )))
        os.unlink(_fname)
        os.unlink(_batname)
        return (_retcode, _text)

def execute(*args, **kwargs):
    _executor = Executor()
    return _executor.execute(*args, **kwargs)

# vim: set et sts=4 sw=4 :
