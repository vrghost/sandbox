from logging import  CRITICAL, FATAL, INFO, ERROR, DEBUG
from cStringIO import StringIO
import logging
import logging.handlers
import os
import sys
import traceback

import base

class Logger(base.BaseObject):

	_logger = None
        _formatter = None
        _rotatingHandler = None

        _logLevels = ("CRITICAL", "FATAL", "INFO", "ERROR", "DEBUG")
        handlers = {}

        _formatting = {
            "fmt": "%(asctime)s [%(levelname)s] %(message)s",
            "datefmt": "%Y.%M.%d %H:%M:%S",
        }


	def __init__(self):
            self._formatter = logging.Formatter(**self._formatting)
            logging.basicConfig(
                level=logging.DEBUG,
                filename=self.config.logging.rootLog,
                filemode="w",
                format=self._formatting["fmt"],
                datefmt=self._formatting["datefmt"]
            )
            _logger = logging.getLogger('')
            _logger.setLevel(DEBUG)
            _logger.addHandler(self._getNtEventHandler())
            _logger.addHandler(self._getRotatingHandler())
            # no smtp logging handler seems to be needed
            #
            #try:
            #    # not avaible on non-ntserver
            #    raise NotImplementedError
            #    _logger.addHandler(self._getSmtpHandler())
            #except:
            #    _logger.error("error initializing SMTP log handler")
            #
            # for debugging purposes
            if __debug__:
                _logger.addHandler(self._getStdoutHandler())
            self._logger = _logger
            self._initHandlers()

        def _getNtEventHandler(self):
	    _handler = logging.handlers.NTEventLogHandler(
                self.config.logging.appName)
	    _handler.setLevel(ERROR)
            _handler.setFormatter(self._formatter)
            return _handler

        def _getRotatingHandler(self):
            _rotatingName = os.path.join(self.config.dir.log, "auto_log")
            _handler = logging.handlers.RotatingFileHandler(
                filename=_rotatingName,
                mode="w",
                maxBytes=0,
                backupCount=5,
            )
            _handler.setLevel(DEBUG)
            _handler.setFormatter(self._formatter)
            # I'll need this handler for rollover later
            self._rotatingHandler = _handler
            return _handler

        def _getSmtpHandler(self):
            _handler = logging.handlers.SMTPHandler(
                self.smtpServer,
                self.fromAddr,
                self.toAddrs,
                "[AUTO] NTserver backup logger"
            )
            _handler.setLevel(INFO)
            _handler.setFormatter(self._formatter)
            return _handler

        def _getStdoutHandler(self):
            _handler = logging.StreamHandler(sys.stdout)
            _handler.setFormatter(self._formatter)
            _handler.setLevel(DEBUG)
            return _handler

	def log(self, text, level=INFO):
            if level >= ERROR:
                # XXX Sending full log data to user!
                self.config.logging.sendFullLog = True
            for _line in text.splitlines():
                self._logger.log(level, _line.strip())

        def terminate(self):
            if self._rotatingHandler:
                self._rotatingHandler.doRollover()

        def _formatLogMsg(self, text, args):
            if not isinstance(text, basestring):
                text = str(text)
            if args:
                text = text % args
            return text

        def _initHandlers(self):
            def _getLogger(name):
                _lvl = getattr(logging, name)
                def _logger(text, *args):
                    self.log(self._formatLogMsg(text, args), _lvl)
                return _logger
            _handlers = {}
            for _name in self._logLevels:
                _handlers["log_%s" % _name.lower()] = _getLogger(_name)
            _handlers["log_exception"] = self.log_exception
            self.handlers = _handlers

        def log_exception(self, text, *args):
            _text = self._formatLogMsg(text, args)
            _info = sys.exc_info()
            _tbStr = StringIO()
            traceback.print_tb(_info[2], None, _tbStr)
            _tbStr.seek(0, 0)
            _msg = "\n".join((
                _text,
                _tbStr.read(),
                "-" * 20,
                "Error: %s" % _info[1],
                "-" * 20,
            ))
            self.log(_msg, ERROR)

        def getFullLog(self):
            return open(self.config.logging.rootLog).read()


# vim: set et sts=4 sw=4 :
