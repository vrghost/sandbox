# -*- coding: utf-8 -*-

import os

import base
from util import executor

class Setter(base.BaseObject):

    def run(self):
        _roots = self.config.setExclusivePerms.dirs
        for _name in _roots:
            _name = os.path.realpath(_name)
            self.processRoot(_name)
            self.toUser("Processed '%s'." % _name)

    def processRoot(self, root):
        for _name in os.listdir(root):
            _name = os.path.join(root, _name)
            if os.path.isdir(_name):
                self.setPerm(_name)
            else:
                self.toUser("Found file '%s' in dir '%s'. "\
                    "Skipped (processing only dirs).")

    def setPerm(self, dir):
        assert os.path.isdir(dir)
        _uname = os.path.split(dir)[1]
        _cmd = (
            "echo", "Y|", # (there must be NO spac between Y and '|'
            "cacls", dir, "/T", 
            "/P", _uname + ":C", "Administrators:F", "SYSTEM:F"
        )
        _out = executor.execute(_cmd)
        _retcode = _out[0]
        if _retcode != 0:
            self.toUser("Retcode for calcs of %s is not zero (it is %s)." %\
                (dir, _retcode))
        _uname = os.path.split(dir)[1]
        print _uname

def run():
    _setter = Setter()
    _setter.run()

# vim: set et sts=4 sw=4 :
