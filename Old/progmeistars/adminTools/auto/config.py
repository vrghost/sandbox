# -*- coding: utf-8 -*-

import os

class _AttrDict(dict):

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        self[name] = value

_rootDir = os.path.split(__file__)[0]

## real config

mail = _AttrDict(
        toAddrs = ("vrghost@gmail.com", "costawork@gmail.com"),
        fromAddr = "serverBackup@progmeistars.lv",
        server=_AttrDict(
            host = "62.85.72.62", # progmeistars.lv external ip
            # port = 25,
            # local_hostname = "???",
        )
)

dir = _AttrDict(
    tmp=os.path.join(_rootDir, "tmp"),
    log=os.path.join(_rootDir, "log"),
    bin=os.path.join(_rootDir, "bin"),
    vbs=os.path.join(_rootDir, "vbs"),
)

logging = _AttrDict(
    appName = "autoPy",
    rootLog = os.path.join(dir.log, "auto.log"),
    # send full log @ program termination.
    # note that value of this field changes in runtime!
    # (e.g. on logging message > ERROR)
    sendFullLog = False,
)

backup = _AttrDict(
    copyFrom=_AttrDict(
        sources=(
            r"E:\Database",
            r"E:\Administracija\DETI\KURSY",
            r"E:\Administracija\TRIVIUM",
            r"\\Kanev\D",
            r"\\Sekret\D\Ieva",
        ),
    ),
    dir=_AttrDict(
        unarch="E:\\BackUp\\unarch\\",
        arch="E:\\BackUp\\arch\\",
    ),
    importantFiles = (r"E:\Database", ),
    scp=_AttrDict(
        host="vrghost.lv",
        port=42,
        user="progmeistarsBackup",
        dir="",
    ),
    sync=(
        (r"E:\BackUp\arch", r"\\www\backup\ntserver-backup"),
        #(r"\\www\backup\www", r"F:\BackUp\www"),
    ),
)

eventViewer = _AttrDict(
    checkDays = 7,
)

oldFileRemover = _AttrDict(
    maxFileAge = 30 * 24 * 3600, # 30 days
    checkedDirs = (
        r"D:\Temp",
    )
)

forbiddenFileRemover = _AttrDict(
    masks = (
        "*.exe",
        "*.dcu",
        "*.tmp",
        "*.o",
        "*.obj",
        "*.tpu",
        "*.ow",
        "*.ppu",
        "*.bak",
        "*.~*",
        "*.*~",
    ),
    dirs = (
        r"D:\to_Students\Students saves",
        r"D:\to_Students\student-www",
    )
)

setExclusivePerms = _AttrDict(
        dirs = (
            r"D:\Profiles",
        )
)

# tests
for _path in dir.itervalues():
    if not os.path.isdir(_path):
        raise RuntimeError("No path %r found" % _path)

# cleanup
del _AttrDict, _path, _rootDir

# vim: set et sts=4 sw=4 :
