# -*- coding: utf-8 -*-

import os
import time

import base

class Remover(base.BaseObject):

    _maxAge = None
    _curTime = None

    def run(self):
        self._maxAge = int(self.config.oldFileRemover.maxFileAge)
        self._curTime = time.time()
        self.log_debug("Max file age is %i.", self._maxAge)
        self.log_debug("Using current time %s.", self._curTime)
        _totalErasedDirs = _totalErasedFiles = 0
        for _dir in self.config.oldFileRemover.checkedDirs:
            self.log_debug("Cleaning up root dir '%s'", _dir)
            (_erasedDirs, _erasedFiles) = self._cleanupDir(_dir)
            self.log_debug(
                "Erased %s dirs and %s files.", _erasedDirs, _erasedFiles)
            _totalErasedDirs += _erasedDirs
            _totalErasedFiles += _erasedFiles
        self.toUser("Removed %s files and %s dirs in total." % (
            _totalErasedFiles, _totalErasedDirs))

    def _cleanupDir(self, dir):
        _erasedDirs = _erasedFiles = 0
        for (_root, _dirs, _files) in os.walk(dir, True):
            for _file in _files:
                _file = os.path.join(_root, _file)
                if self.isOld(_file):
                    self.log_debug("File '%s' is too old. Removing it.", _file)
                    self.doUnlink(_file)
                    _erasedFiles += 1
            for _dir in _dirs:
                _dir = os.path.join(_root, _dir)
                if self.isOld(_dir):
                    self.log_debug(
                        "Directory '%s' is too old. Removing it.", _dir)
                    self.doRmdir(_dir)
                    _erasedDirs += 1
        return (_erasedDirs, _erasedFiles)

    def isOld(self, target):
        _stat = os.stat(target)
        _age = self._curTime - max(_stat.st_ctime, _stat.st_mtime)
        return _age > self._maxAge

    def doUnlink(self, file):
        try:
            os.unlink(file)
        except OSError, _err:
            self.toUser("Error removing file '%s' (%s)" % (file, _err))

    def doRmdir(self, dir):
        try:
            os.rmdir(dir)
        except OSError, _err:
            if _err.errno == 41:
                # directory is not empty
                self.log_debug(
                    "Directory '%s' is not empty. Not removed.", dir)
            else:
                self.toUser("Error removing directory '%s' (%s)" % (dir, _err))

def run():
    _remover = Remover()
    _remover.run()

# vim: set et sts=4 sw=4 :
