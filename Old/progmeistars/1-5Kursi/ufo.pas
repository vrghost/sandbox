uses graph,crt;

const
 del=10;

procedure MoveMe(xf,yf,xt,yt:real; bg,mas,uf:pointer; ap:word; sagi:integer);
var
  x,y:real;
  i:integer;
begin
  x:=0;
  y:=0;
  i:=0;
  repeat
    GetImage(trunc(xf+x),trunc(yf+y),trunc(xf+x)+40,trunc(yf+y)+40,bg^);
    PutImage(trunc(xf+x),trunc(yf+y),mas^,AndPut);
    PutImage(trunc(xf+x),trunc(yf+y),uf^,OrPut);

    ap:=ap xor 1;
    SetActivePage(ap);
    SetVisualPage(ap xor 1);



    delay(del);

                    {
    ap:=ap xor 1;
    SetActivePage(ap);
    SetVisualPage(ap xor 1);}

    PutImage(trunc(xf+x),trunc(yf+y),bg^,NormalPut);

    x:=x+(xt-xf)* (1/sagi);
    y:=y+(yt-yf)*(1/ sagi);
    i:=i+1;
                   {
      ap:=ap xor 1;
    SetActivePage(ap);
    SetVisualPage(ap xor 1);


                                   {
    ap:=ap xor 1;
    SetActivePage(ap);
    SetVisualPage(ap xor 1);        }
  until(i=sagi+1)or(keypressed);
end;

procedure DrawMe(x,y,c:integer);
begin
  SetColor(c);
  arc(x,y,0,180,7);
  MoveTo(x+8,y);
  LineRel(-16,0);
  LineRel(-10,3);
  LineRel(17,5);
  LineRel(17,-5);
  LineRel(-33,0);
  LineRel(33,0);
  LineRel(-9,-3);
  SetColor(GetMaxColor);
  if ((c<>0)and(c<>LightGray)) then begin
    SetFillStyle(1,Red);
    FloodFill(x,y-1,c);

    SetFillStyle(1,Blue);
    FloodFill(x,y+1,c);

    SetFillStyle(1,Cyan);
    FloodFill(x,y+5,c);

    SetFillStyle(1,White);
  end;
end;


var
  GrDr,GrMd :integer;
  grError   :integer;
  uf,bg,mas :pointer;
  x,y,i,xt,yt,xl,yl,xs,ys:integer;
  ap,sagi        :word;
begin
  randomize;
  GrDr := VGA;
  GrMd:=VgaMed;
  InitGraph(grDr,grMd,'c:\bp\bgi');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
    writeln('Error: ',GraphErrorMsg(grError));
    halt(1);
  end;
(*  DrawMe(100,100,GetMaxColor);
  GetMem(uf,ImageSize(80,90,120,120));
  GetImage(80,90,120,120,uf^);
  SetFillStyle(0,0);
  bar(80,90,120,120);
  DrawMe(100,100,LightGray);
  SetFillStyle(1,White);
  FloodFill(0,0,LightGray);
  DrawMe(100,100,0);
  GetMem(mas,ImageSize(80,90,120,120));
  GetImage(80,90,120,120,mas^);
  GetMem(bg,ImageSize(80,90,120,120));

  ClearViewPort;
  {--------Stars-------}
  for i:=0 to 2000 do begin
    SetActivePage(1);
    SetVisualPage(0);
    x:=random(GetMaxX);
    y:=Random(GetMaxY);
    PutPixel(x,y,GetMaxColor);
    SetVisualPage(1);
    SetActivePage(0);
    PutPixel(x,y,GetMaxColor);
  end;              {
  SetActivePage(1);
  SetVisualPage(0);  }
  x:=random(GetMaxX);
  Y:=random(GetMaxY);

  repeat
    xt:=random(GetMaxX-50);
    yt:=random(GetMaxY-50);
    if xt>x then begin
      xl:=xt;
      xs:=x;
    end
    else begin
      xl:=x;
      xs:=yt;
    end;
    if yt>y then begin
      yl:=yt;
      ys:=y;
    end else begin
      yl:=y;
      ys:=yt
    end;
    if xl-xs>yl-ys then
     sagi := (xl-xs)* 2
    else
     sagi:=(yl-ys)* 2;

    MoveMe(x,y,xt,yt,bg,mas,uf,ap,sagi);
    x:=xt;
    y:=yt;
    {-------ap--------}    {
    ap:=ap xor 1;
    SetActivePage(ap);
    SetVisualPage(ap xor 1);}
  until keypressed;*)

  dispose(bg);
  dispose(uf);
  dispose(mas);
  readkey;
  closeGraph;
end.
