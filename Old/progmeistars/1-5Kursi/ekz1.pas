uses graph,crt;
type point=record
     x,y:real;
     end;
    otrezok=record
    p1,p2:point;
     end;


procedure egavga;external;
{$L egavga.obj}

   procedure testparalel(o1,o2:otrezok);
   var xl1,xl2,yl1,yl2,xr1,yr1,xr2,yr2:real;
   begin
    xl1:=o1.p1.x-o1.p2.x;
    yl2:=o2.p1.y-o2.p2.y;
    xl2:=o2.p1.x-o2.p2.x;
    yl1:=o1.p1.y-o1.p2.y;
     xr2:=o2.p1.x;
     xr1:=o1.p1.x;
     yr1:=o1.p1.y;
     yr2:=o2.p1.y;
   if (xl1=xl2)and(yl1=yl2)and((xr1<>xr2)or(yr1<>yr2))then begin
     OutTextXY(20,20,'Otrezki Polnostju Porolelni !!');
      readkey;
      halt(1);
    end;
   end;

   function testprenadl(o:otrezok;p:point):boolean;
   var r:point;
   xl,yl,t,t2:real;
   b:boolean;
   begin
    r:=o.p1;
    xl:=o.p1.x-o.p2.x;
    yl:=o.p1.y-o.p2.y;
    t:= (p.x-r.x)/xl;
    t2:=(p.y-r.y)/yl;
    b:=(t=t2)and((t<=0)and(t>=-1)and(t2<=0)and(t2>=-1));
    testprenadl:=b;
   end;

   procedure testperioda(o1,o2: otrezok);
   var granici:array[0..1]of point;
   b:boolean;
   i: integer;
   text,t2: string;
    begin
    i:=0;
    b:=testprenadl(o1,o2.p1);
    if b then begin granici[i]:=o2.p1; i:=i+1; end;

    b:=testprenadl(o1,o2.p2);
    if b then begin granici[i]:=o2.p2; i:=i+1; end;

      b:=testprenadl(o2,o1.p1);
    if b then begin granici[i]:=o1.p1; i:=i+1; end;

     b:=testprenadl(o2,o1.p2);
    if b then begin granici[i]:=o1.p2; i:=i+1; end;

     if i=2 then begin
     text:='Otrezki peresekajutsja v intervale x �(';
     str(granici[0].x,t2);
     text:=text+t2+',';
     str(granici[1].x,t2);
     text:=text+t2+')';
     OutTextXY(10,50,text);
      text:='I v intervale y �(';
     str(granici[0].y,t2);
     text:=text+t2+',';
     str(granici[1].y,t2);
     text:=text+t2+')';
     OutTextXY(10,60,text);
     readkey;
     halt(1);
     end;

    end;


    procedure testslitih(o1,o2: otrezok);
     var b:boolean;
     begin
       b:=(o1.p1.x=o2.p1.x);
       b:=(b)and(o1.p1.y=o2.p1.y);
       b:=(b)and(o1.p2.x=o2.p2.x);
       b:=(b)and(o1.p2.y=o2.p2.y);
      if not b then begin
       b:=(o1.p1.x=o2.p2.x);
       b:=(b)and(o1.p1.y=o2.p2.y);
       b:=(b)and(o1.p2.x=o2.p1.x);
       b:=(b)and(o1.p2.y=o2.p1.y);
      end;

      if b then begin
      OutTextXY(20,20,'Otrezki Polnostju Sovpodajut !!');
      readkey;
      halt(1);
      end;

     end;


var grDr,grMd,grError:integer;

    otrez:array[1..2]of otrezok;
    i:integer;
    ch    :char;

     x,y,t,tau,ttau,tt,treug:real;
xl1,yl2,xl2,yl1,xr2,xr1,yr1,yr2,numb:real;
                           po  :point;
      text:string;
      b:boolean;
begin
clrscr;
randomize;
  RegisterBGIDriver(@EGAVGA);
  GrDr := VGA;
  GrMd:=VgaMed;
  InitGraph(grDr,grMd,'');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
    writeln('Error: ',GraphErrorMsg(grError));
    halt(1);
  end;
  SetWriteMode(1);
  repeat
    for i:=1 to 2 do begin
     otrez[i].p1.x:=random(GetMaxX);
     otrez[i].p1.y:=random(GetMaxY);
     otrez[i].p2.x:=random(GetMaxX);
     otrez[i].p2.y:=random(GetMaxY);
    end;

       testparalel(otrez[1],otrez[2]);
       testslitih(otrez[1],otrez[2]);
       testperioda(otrez[1],otrez[2]);

       xl1:=otrez[1].p1.x-otrez[1].p2.x;
        yl2:=otrez[2].p1.y-otrez[2].p2.y;
        xl2:=otrez[2].p1.x-otrez[2].p2.x;
        yl1:=otrez[1].p1.y-otrez[1].p2.y;
        xr2:=otrez[2].p1.x;
        xr1:=otrez[1].p1.x;
        yr1:=otrez[1].p1.y;
        yr2:=otrez[2].p1.y;


        treug:=-xl1*yl2+xl2*yl1;
        tt:=-yl2*xr2+xr1*yl2+xl2*yr2-xl2*yr1;
        ttau:=xl1*yr2-xl1*yr1-xr2*yl1+xr1*yl1;
        t:=tt/treug;
        tau:=ttau/treug;
    {risuem}
                  SetColor(White);
     for i:=1 to 2 do begin
     SetColor(random(GetMaxColor-1)+1);
      line( trunc(otrez[i].p1.x),trunc(otrez[i].p1.y),trunc(otrez[i].p2.x),trunc(otrez[i].p2.y));
     end;

    if (t<=0)and(tau<=0)and(t>=-1)and(tau>=-1) then begin
      po.x:=trunc(xr1+t*xl1);
      po.y:=trunc(yr1+t*yl1);
      str(xr1+t*xl1,text);
      text:='X='+text;
      OutTextXY(10,10,text);
       str(yr1+t*yl1,text);
       text:='Y='+text;
      OutTextXY(10,20,text);
       SetColor(Blue);
       circle(trunc(po.x),trunc(po.y),5);
     end;



     ch:=readkey;
     ClearViewPort;
  until (ch='e')or(ch='E');
end.