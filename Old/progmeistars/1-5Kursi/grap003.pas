program igra;
uses graph,crt;
var GrDr,GrMd :integer;
    grError   :integer;
   x,y,i,tip  :integer;
   ch         :char;
   PolyP      :array[0..35]of PointType;
   procedure egavga;external;
   {$L egavga.obj}
begin
randomize;
RegisterBGIDriver(@EGAVGA);
GrDr := detect;
InitGraph(grDr,grMd,'c:\bp\bgi');
grError := GraphResult;
if(grError <> grOK)then
 begin
 writeln('Error: ',GraphErrorMsg(grError));
 halt(1);
 end;

 repeat
 x:=random(GetMaxX-30)+30;
 y:=random(GetMaxY-30)+30;
 i:=random(2);

 tip:=random(4);
 SetTextStyle(tip,i,7);
 i:=random(4);
 {SetLineStyle(tip,GetMaxColor,random(10)+1);}
 tip:=random(11);
 SetFillStyle(tip,random(GetmaxColor-1)+1);
 if(i=0)then begin
  circle(x,y,random(25)+4);
  FloodFill(x,y,GetMaxColor);
 end;

 if(i=1)then begin
  rectangle(x,y,random(100)+x,random(100)+y);
  FloodFill(x+1,y+1,GetMaxColor);
 end;

 if(i=2)then begin
  PieSlice(x,y,random(100),random(360),random(250)+4);
  FloodFill(x-1,y-1,GetMaxColor);
 end;

 if(i=3)then begin
  for i:=0 to random(30)+3 do begin
  PolyP[i].x:=random(GetMaxX-30)+30;
  PolyP[i].y:=random(GetMaxY-30)+30;
  end;
  PolyP[i+1].x:=PolyP[0].x;
  PolyP[i+1].y:=PolyP[0].y;
  FillPoly(i+1,PolyP);
 end;

 case tip of
  0:OutText('EmptyFill');
  1:OutText('SolidFill');
  2:OutText('LineFill');
  3:OutText('LtSlsashFill');
  4:OutText('SlashFill');
  5:OutText('BkSlsashFill');
  6:OutText('LtBkSlsashFill');
  7:OutText('HatchFill');
  8:OutText('XHatchFill');
  9:OutText('InterLeaveFill');
  10:OutText('WideDotFill');
  11:OutText('CloseDotFill');
 end;
 delay(1000);
 ClearViewPort;
 until keypressed;
CloseGraph;
end.