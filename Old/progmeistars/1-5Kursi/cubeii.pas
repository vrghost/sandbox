{$A+,B-,D+,E+,F-,G-,I+,L+,N+,O-,P-,Q+,R+,S+,T-,V+,X+,Y+}
{$M 16384,0,655360}
program cube;
uses
  Graph, Crt;

type
  TFloat = Extended;
  TPoint = record
    x, y, z : TFloat;
  end;
  TConnection = record
    pt1, pt2 : Word;
  end;

procedure RotatePointsX(var pts : array of TPoint; items : Integer; fi : TFloat);
var
  cosfi, sinfi : TFloat;
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  cosfi := Cos(fi); sinfi := Sin(fi);
  for CurrentItem := 0 to Pred(Items) do
  begin
    with pts[CurrentItem] do
    begin
      TempPt.x := x;
      TempPt.y :=  y * cosfi + z * sinfi;
      TempPt.z := -y * sinfi + z * cosfi;
    end;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure RotatePointsY(var pts : array of TPoint; items : Integer; fi : TFloat);
var
  cosfi, sinfi : TFloat;
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  cosfi := Cos(fi); sinfi := Sin(fi);
  for CurrentItem := 0 to Pred(Items) do
  begin
    with pts[CurrentItem] do
    begin
      TempPt.x := x * cosfi - z * sinfi;
      TempPt.y := y;
      TempPt.z := x * sinfi + z * cosfi;
    end;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure RotatePointsZ(var pts : array of TPoint; items : Integer; fi : TFloat);
var
  cosfi, sinfi : TFloat;
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  cosfi := Cos(fi); sinfi := Sin(fi);
  for CurrentItem := 0 to Pred(Items) do
  begin
    TempPt.x := pts[CurrentItem].x * cosfi - pts[CurrentItem].y * sinfi;
    TempPt.y := pts[CurrentItem].x * sinfi + pts[CurrentItem].y * cosfi;
    TempPt.z := pts[CurrentItem].z;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure DrawPoints(const pts : array of TPoint;
                     const con : array of TConnection;
                     Items,mastab: Integer;
                     rad       :Tpoint);
var
  CurrentItem : Integer;
  ax, ay      : Word;
  k           : TFloat;
  dx, dy      : Integer;
  x1,x2,y1,y2 :integer;
begin
  GetAspectRatio(ax, ay);
  k := ax / ay;

  dx := GetMaxX shr 1; dy := GetMaxY shr 1;

  for CurrentItem := 0 to Pred(Items) do
  begin
  x1:=Round(mastab*(pts[con[CurrentItem].pt1].x) / (pts[con[CurrentItem].pt1].z + rad.z +(rad.x))) + dx;
  y1:=Round(mastab*(pts[con[CurrentItem].pt1].y) / (pts[con[CurrentItem].pt1].z + rad.z +(rad.y)) * k) + dy ;
  x2:=Round(mastab*(pts[con[CurrentItem].pt2].x) / (pts[con[CurrentItem].pt2].z + rad.z +(rad.x))) + dx ;
  y2:=Round(mastab*(pts[con[CurrentItem].pt2].y) / (pts[con[CurrentItem].pt2].z + rad.z +(rad.y))* k) + dy;
    Line(x1, y1, x2, y2);
  end;
end;


const
  Side = 100.0;
  HalfSide = Side / 2.0;

  Points : array [0..7] of TPoint =
  (
    (x:HalfSide;  y:HalfSide;  z:HalfSide),
    (x:-HalfSide; y:HalfSide;  z:HalfSide),
    (x:-HalfSide; y:-HalfSide; z:HalfSide),
    (x:HalfSide;  y:-HalfSide; z:HalfSide),

    (x:HalfSide;  y:HalfSide;  z:-HalfSide),
    (x:-HalfSide; y:HalfSide;  z:-HalfSide),
    (x:-HalfSide; y:-HalfSide; z:-HalfSide),
    (x:HalfSide;  y:-HalfSide; z:-HalfSide)
  );
  Connections : array [1..12] of TConnection =
  (
    (pt1:0; pt2:1),
    (pt1:1; pt2:2),
    (pt1:2; pt2:3),
    (pt1:3; pt2:0),

    (pt1:4; pt2:5),
    (pt1:5; pt2:6),
    (pt1:6; pt2:7),
    (pt1:7; pt2:4),

    (pt1:0; pt2:4),
    (pt1:1; pt2:5),
    (pt1:2; pt2:6),
    (pt1:3; pt2:7)
  );

var
  grDr, grMd : Integer;
  ch,prev    : Char;
  ap         : Word;
  rad        :Tpoint;
  mastab     :integer;
begin
  grDr := VGA; grMd := VGAMed;
  InitGraph(grDr, grMd, 'c:\bp\bgi');
  rad.x:=side;
  rad.y:=side;
  rad.z:=side;
  mastab:=500;

  ap := 1;
  SetActivePage(1); SetVisualPage(0);
  repeat
    DrawPoints(Points, Connections, High(Connections),mastab, rad);

    ap := ap xor 1;
    SetActivePage(ap); SetVisualPage(ap xor 1);
    ClearViewPort;

    if KeyPressed then begin
      prev:=ch;
      ch := ReadKey;
      end;

    case UpCase(ch) of
      'X' : RotatePointsX(Points, Succ(High(Points)), Pi / 200.0);
      'Y' : RotatePointsY(Points, Succ(High(Points)), Pi / 200.0);
      'Z' : RotatePointsZ(Points, Succ(High(Points)), Pi / 200.0);
      '+' : mastab:=mastab+04;
      '-' : mastab:=mastab-04;
    end;
    if (ch='+')or(ch='-') then ch:=prev;
  until ch = #27;
  while KeyPressed do ReadKey;
  CloseGraph;
end.
