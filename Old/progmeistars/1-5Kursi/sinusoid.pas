{$A+,B-,D+,E+,F-,G-,I+,L+,N+,O-,P-,Q+,R+,S+,T-,V+,X+,Y+}
{$M 16384,0,655360}
program cube;
uses
  Graph, Crt;

type
  TFloat = Extended;
  TPoint = record
    x, y, z : TFloat;
  end;
  TConnection = record
    pt1, pt2 : Word;
  end;


procedure udalit(var pts : array of TPoint; items : Integer);
var
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  for CurrentItem := 0 to Pred(Items) do
  begin
    TempPt:=pts[CurrentItem];
      TempPt.z :=  tempPt.z+2;
      if TempPt.z>=500 then tempPt.z:=500;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure priblizit(var pts : array of TPoint; items : Integer);
var
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  for CurrentItem := 0 to Pred(Items) do
  begin
    TempPt:=pts[CurrentItem];
      TempPt.z :=  tempPt.z-2;
      if TempPt.z<=-9 then tempPt.z:=-9;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure RotatePointsX(var pts : array of TPoint; items : Integer; fi : TFloat);
var
  cosfi, sinfi : TFloat;
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  cosfi := Cos(fi); sinfi := Sin(fi);
  for CurrentItem := 0 to Pred(Items) do
  begin
    with pts[CurrentItem] do
    begin
      TempPt.x := x;
      TempPt.y :=  y * cosfi + z * sinfi;
      TempPt.z := -y * sinfi + z * cosfi;
    end;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure RotatePointsY(var pts : array of TPoint; items : Integer; fi : TFloat);
var
  cosfi, sinfi : TFloat;
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  cosfi := Cos(fi); sinfi := Sin(fi);
  for CurrentItem := 0 to Pred(Items) do
  begin
    with pts[CurrentItem] do
    begin
      TempPt.x := x * cosfi - z * sinfi;
      TempPt.y := y;
      TempPt.z := x * sinfi + z * cosfi;
    end;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure RotatePointsZ(var pts : array of TPoint; items : Integer; fi : TFloat);
var
  cosfi, sinfi : TFloat;
  CurrentItem  : Integer;
  TempPt       : TPoint;
begin
  cosfi := Cos(fi); sinfi := Sin(fi);
  for CurrentItem := 0 to Pred(Items) do
  begin
    TempPt.x := pts[CurrentItem].x * cosfi - pts[CurrentItem].y * sinfi;
    TempPt.y := pts[CurrentItem].x * sinfi + pts[CurrentItem].y * cosfi;
    TempPt.z := pts[CurrentItem].z;
    pts[CurrentItem] := TempPt;
  end;
end;

procedure DrawPoints(const pts : array of TPoint;
                     Items,mastab: Integer;
                     rad       :Tpoint);
var
  CurrentItem : Integer;
  ax, ay      : Word;
  k           : TFloat;
  dx, dy      : Integer;
  x1,x2,y1,y2 :longint;
begin
  GetAspectRatio(ax, ay);
  k := ax / ay;

  dx := GetMaxX shr 1; dy := GetMaxY shr 1;

  for CurrentItem := 0 to Items-72 do  begin
  x1:=Round(mastab*(pts[CurrentItem].x) / (pts[CurrentItem].z + rad.z +(rad.x))) + dx;
  y1:=Round(mastab*(pts[CurrentItem].y) / (pts[CurrentItem].z + rad.z +(rad.y)) * k) + dy ;
   x2:=Round(mastab*(pts[CurrentItem+1].x) / (pts[CurrentItem+1].z + rad.z +(rad.x))) + dx;
   y2:=Round(mastab*(pts[CurrentItem+1].y) / (pts[CurrentItem+1].z + rad.z +(rad.y)) * k) + dy ;
  if (x1>=0)and(y1>=0)and(x1<=GetMaxX)and(y1<=GetMaxY)then begin
   if(x2>=0)and(y2>=0)and(x2<=GetMaxX)and(y2<=GetMaxY) then  begin
  {  line(x1,y1,x2,y2);  }
     PutPixel(x1,y1,GetMaxColor);
     PutPixel(x2,y2,GetMaxColor);
   end;
  end;
{  x2:=Round(mastab*(pts[CurrentItem+72].x) / (pts[CurrentItem+72].z + rad.z +(rad.x))) + dx;
  y2:=Round(mastab*(pts[CurrentItem+72].y) / (pts[CurrentItem+72].z + rad.z +(rad.y)) * k) + dy ;

    if (x1>=0)and(y1>=0)and(x1<=GetMaxX)and(y1<=GetMaxY)then begin
  if(x2>=0)and(y2>=0)and(x2<=GetMaxX)and(y2<=GetMaxY) then  line(x1,y1,x2,y2);
  end;
 }
  end;
end;


const
  radius=10;
  fig = 5*(pi/180);

{  Connections : array [1..12] of TConnection =
  (
    (pt1:0; pt2:1),
    (pt1:1; pt2:2),
    (pt1:2; pt2:3),
    (pt1:3; pt2:0),

    (pt1:0; pt2:4),
    (pt1:1; pt2:4),
    (pt1:2; pt2:4),
    (pt1:3; pt2:4),

    (pt1:0; pt2:5),
    (pt1:1; pt2:5),
    (pt1:2; pt2:5),
    (pt1:3; pt2:5)
  );
   }
var
  grDr, grMd : Integer;
  ch,prev    : Char;
  ap         : Word;
  rad        :Tpoint;
  mastab     :integer;
  Points : array [0..1871] of TPoint;
  i:integer;
begin
  grDr := VGA; grMd := VGAMed;
  InitGraph(grDr, grMd, 'c:\bp\bgi');
  rad.x:=radius*2;
  rad.y:=radius*2;
  rad.z:=radius*2;
  mastab:=200;

  points[10].x:=radius;
  points[0].y:=radius;
  for i:=1 to 1871 do begin
  points[i].x:=-10*cos(2*fig)/cos(fig);
  points[i].y:=cos(0.1+i);
  points[i].z:=0;
  end;

  ap := 1;
  SetActivePage(1); SetVisualPage(0);
  repeat
    DrawPoints(Points,  High(Points),mastab, rad);

    ap := ap xor 1;
    SetActivePage(ap); SetVisualPage(ap xor 1);
    ClearViewPort;

    if KeyPressed then begin
      prev:=ch;
      ch := ReadKey;
      end;

    case UpCase(ch) of
      'X' : RotatePointsX(Points, Succ(High(Points)), Pi / 100.0);
      'Y' : RotatePointsY(Points, Succ(High(Points)), Pi / 100.0);
      'Z' : RotatePointsZ(Points, Succ(High(Points)), Pi / 100.0);
      '+' : mastab:=mastab+04;
      '-' : mastab:=mastab-04;
      'B' : priblizit(points,succ(high(points)));
      'D' : udalit(points,succ(high(points)));
    end;
     if ch='3' then begin
     RotatePointsX(Points, Succ(High(Points)), Pi / 100.0);
      RotatePointsY(Points, Succ(High(Points)), Pi / 100.0);
      RotatePointsZ(Points, Succ(High(Points)), Pi / 100.0);
     end;
    if (ch='+')or(ch='-') then ch:=prev;
  until ch = #27;
  while KeyPressed do ReadKey;
  CloseGraph;
end.
