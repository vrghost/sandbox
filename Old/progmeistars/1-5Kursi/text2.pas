program choose;
uses CRT;
var i,z:integer;
f1,f2:text;
name1,name2:string;
 procedure choose2(name1,name2:string);
 var move:array [0..30]of char;
 b1,b2:text;
 begin
  assign(b1,name1);
  assign(b2,name2);
  rewrite(b2);
  reset(b1);
  for i:=0 to 30 do move[i]:=#32;
  repeat
  i:=0; z:=0;
   repeat
   if (not eoln(b1))and(not eof(b1)) then begin read(b1,move[i]);end else readln(b1);
  { if (ord(move[i])=64)or(ord(move[i])>179)then  z:=1;        }
   i:=i+1;
   until (not eoln(b1))or (eof(b1));
   repeat
   if (ord(move[z])=64)or(ord(move[z])<179)then  begin
   write(b2,move[z]);
   write(move[z]);
   write(' =',ord(move[z]),' ');
   end;
   z:=z+1;
   until z<>i;
  until eof(b1);

  close(b1);
  close(b2);
 end;
label 1;
begin
randomize;
textcolor(random(10)+1);
clrscr;
writeln('File 1 name is...');
readln(name1);
writeln('file 2 name is...');
readln(name2);
clrscr;
assign(f1,name1);
assign(f2,name2);
{$I-}
reset(f1);
if ioresult<>0 then begin writeln('file ',name1,' was not found and program will be terminated');goto 1;end;
{$I+}
choose2(name1,name2);
{$I-}
1: close(f1);
close(f2);
readkey;
end.