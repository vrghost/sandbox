program _21;
type Pdvojka=^dvojka;
     dvojka=record
     pc:^real;
     next:pdvojka;
end;
var s:pdvojka;
begin
writeln(memavail);
new(s);
new(s^.pc);
s^.pc^:=3.14;
new(s^.next);

s^.next^.pc:=s^.pc;

new(s^.next^.next);

s^.next^.next^.pc:=s^.pc;

s^.next^.next^.next:=s;

dispose(s^.next^.next^.pc);
dispose(s^.next^.next);
dispose(s^.next);
dispose(s);
writeln(memavail,'%');
end.