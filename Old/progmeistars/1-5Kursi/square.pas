program square;
uses
  Graph, Crt;

type
  TPoint = record
    x, y : Real;
  end;


procedure RotatePoint(var pt : TPoint; fi : Real);
var
  workPt : TPoint;
  cosfi, sinfi : Real;
begin
  cosfi := cos(fi); sinfi := sin(fi);

  with workPt do
  begin
    x := pt.x * cosfi - pt.y * sinfi;
    y := pt.x * sinfi + pt.y * cosfi;
  end;

  pt := workPt;
end;

procedure CreateFigure(var fig : array of TPoint);
var
  cnt : Integer;
begin
  with fig[0] do
  begin
    x := -50.0; y := 50.0;
  end;

  for cnt := 1 to 4 do
  begin
    fig[cnt] := fig[cnt - 1];
    RotatePoint(fig[cnt], Pi / 2.0);
  end;
end;

procedure DrawFigure(const fig : array of TPoint);

begin

end;

procedure RotatePoints(var pts : array of TPoint; items : Integer; fi : Real);
var
  i : Integer;
begin
  for i := 0 to Pred(items) do
    RotatePoint(pts[i], fi);
end;

var
  S          : array [1..4] of TPoint;
  grDr, grMd : Integer;
  ap         : Word;
begin
  CreateFigure(S);

  grDr := VGA; grMd := VGAMed;
  InitGraph(grDr, grMd, 'c:\bp\bgi');

  ap := 1;
  SetActivePage(1); SetVisualPage(0);
  repeat
    DrawFigure(S);
    RotatePoints(S, 4, Pi / 20.0);

    ap := ap xor 1;
    SetActivePage(ap); SetVisualPage(ap xor 1);
    ClearViewPort;
  until KeyPressed;
  while KeyPressed do ReadKey;

  CloseGraph;
end.
