program res2name;
uses CRT;
var i:integer;
f1,f2:text;
name1,name2:string;
 procedure resname(name1,name2:string);
 var move,m2:char;
 b1,b2:text;
 begin
  assign(b1,name1);
  assign(b2,name2);
  rewrite(b2);
  reset(b1);
  repeat
  read(b1,move);
  if (move<>' ') or (seekeoln(b1)) then begin
   write(b2,move);
   write(move);
   end
   else
   begin
   writeln(b2);
   writeln;
   end;
  until eof(b1);
  close(b1);
  close(b2);
 end;
label 1;
begin
clrscr;
writeln('File 1 name is...');
readln(name1);
writeln('file 2 name is...');
readln(name2);
clrscr;
assign(f1,name1);
assign(f2,name2);
{$I-}
reset(f1);
if ioresult<>0 then begin writeln('file ',name1,' was not found and program will be terminated');goto 1;end;
{$I+}
resname(name1,name2);
{$I-}
1: close(f1);
close(f2);
readkey;
end.