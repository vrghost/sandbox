PROGRAM otrez;
{$N+}
uses graph,crt;
type point=record
 x,y:real;
 end;
 otrezok=record
 p1,p2:point;
 end;
     procedure egavga;external;
   {$L egavga.obj}

     procedure draw(o1,o2:otrezok);
     begin
     SetColor(Green);
     line(trunc(o1.p1.x),trunc(o1.p1.y),trunc(o1.p2.x),trunc(o1.p2.y));
     OutTextXY(trunc(o1.p1.x),trunc(o1.p1.y),'otrezok 1');
     OutTextXY(trunc(o1.p2.x),trunc(o1.p2.y),'otrezok 1');

     SetColor(Red);
     line(trunc(o2.p1.x),trunc(o2.p1.y),trunc(o2.p2.x),trunc(o2.p2.y));
     OutTextXY(trunc(o2.p1.x),trunc(o2.p1.y),'otrezok 2');
     OutTextXY(trunc(o2.p2.x),trunc(o2.p2.y),'otrezok 2');

     SetColor(white);
     end;

  var GrDr,GrMd :integer;
   grError   :integer;
   t,dlina1,dlina2:real;
   o1,o2     : otrezok;
   ch        :char;
   SAG1,sag2,t1,t2:comp;
   text      :string;
   r1,r2,on,tw     :point;
   xi,yi:integer;
   b:boolean;
   num       :longint;
begin
{inicilization}
randomize;
RegisterBGIDriver(@EGAVGA);
GrDr := detect;
InitGraph(grDr,grMd,'');
grError := GraphResult;
if(grError <> grOK)then
 begin
 writeln('Error: ',GraphErrorMsg(grError));
 halt(1);
 end;
{body}
SetWriteMode(XorPut);

repeat
 {Line generator}
 o1.p1.x:=random(GetMaxX);
 o1.p1.y:=random(GetMaxY);
 o1.p2.x:=random(GetMaxX);
 o1.p2.y:=random(GetMaxY);
 o2.p1.x:=random(GetMaxX);
 o2.p1.y:=random(GetMaxY);
 o2.p2.x:=random(GetMaxX);
 o2.p2.y:=random(GetMaxY);
 {-----end of generator----}
 {vi4eslenije dlini}
 dlina1:=sqrt(sqr(o1.p1.x-o1.p2.x)+sqr(o1.p1.y-o1.p2.y));
 dlina2:=sqrt(sqr(o2.p1.x-o2.p2.x)+sqr(o2.p1.y-o2.p2.y));

 {---=end=---}
 saG1:=0.0001*dlina1;
 sag2:=0.0001*dlina2;
 r1.x:=o1.p1.x;
 r1.y:=o1.p1.y;
 r2.x:=o1.p1.x;
 r2.y:=o1.p1.y;
 {output}
 draw(o1,o2);
 str(dlina1,text);
 outtextXY(10,10,text);
 str(dlina2,text);
 outtextXY(10,20,text);
 t1:=0;
 on.x:=0;
 on.y:=0;

 t2:=0;
 b:=False;
  repeat
  tw.x:=0;
  tw.y:=0;
  on.x:=r1.x+t1*on.x*o1.p2.x;
  on.y:=r1.y+t1*on.y*o1.p2.y;
  repeat
  {eto 2ja prjamaja}
  tw.x:=r2.x+t2*tw.x*o2.p2.x;
  tw.y:=r2.y+t2*tw.y*o2.p2.y;
  if (tw.x=on.x)and(tw.y=on.y)then num:=num+1;
  t2:=t2+sag2;
  until (t2>=1);
  t1:=t1+sag1;
  until (t1>=1);

 { for xi:=0 to getMaxX do begin
   for yi:=0 to GetMaxY do begin
    if(GetPixel(xi,yi)=6) then begin
     str(xi,text);
     OutTextXY(10,30,text);
    end;
   end;}
   str(num,text);
    OutTextXY(10,30,text);
    end;
 ch:=readkey;
 ClearViewPort;
until (ch='e')or(ch='E');



{ubirajem hvosti... (finishing}
closeGraph;
end.