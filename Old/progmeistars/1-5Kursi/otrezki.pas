uses graph,crt;

const maxzv=9{kolvo otrezkov - 1};
sagi=0.001;

type
 point=record
 x,y:integer;
 end;
 otrezok =array[0..1]of point;


procedure egavga;external;
 {$L egavga.obj}

  procedure steret(otrezki :array of otrezok);
  var a,b,c,i:integer;
  begin
   setColor(Black);
   for i:=0 to maxzv do begin
    line(otrezki[i,0].x,otrezki[i,0].y,otrezki[i,1].x,otrezki[i,1].y);
   end;
   setColor(GetMaxColor);
  end;

  procedure narisovat(otrezki :array of otrezok);
  var i:integer;
  begin
   setColor(White);
   for i:=0 to maxzv do begin
    line(otrezki[i,0].x,otrezki[i,0].y,otrezki[i,1].x,otrezki[i,1].y);
   end;
   setColor(GetMaxColor);
  end;

  procedure dvigat(otrezki : array of otrezok);
   var i:integer;
   begin

    for i:=maxzv downto 1 do begin
     otrezki[i,0].x:=otrezki[i-1,0].x;
     otrezki[i,0].y:=otrezki[i-1,0].y;
     otrezki[i,1].x:=otrezki[i-1,1].x;
     otrezki[i,1].y:=otrezki[i-1,1].y;
    end;

   end;


var
  GrDr,GrMd :integer;
  grError   :integer;
  uf,bg,mas :pointer;
  ap        :word;
  i,u   :integer;
  otrezki   :array[0..maxzv]of otrezok;
  t         :array[0..1]of point;
  x,y,x1,y1 :real;
  kuda,otkuda:otrezok;
begin
  randomize;
  RegisterBGIDriver(@EGAVGA);
  GrDr := VGA;
  GrMd:=VgaMed;
  InitGraph(grDr,grMd,'');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
    writeln('Error: ',GraphErrorMsg(grError));
    halt(1);
  end;

  for i:=0 to maxzv do begin
   otrezki[i,0].x:=100;
   otrezki[i,0].y:=100;
   otrezki[i,1].x:=100;
   otrezki[i,1].y:=200;
  end;
  x:=0;
  y:=0;
  x1:=0;
  y1:=0;
  i:=0;
  repeat

   if(x=0)and(y=0)and(x1=0)and(y1=0) then begin

   otkuda[0].x := otrezki[0,0].x;
   otkuda[0].y := otrezki[0,0].y;
   otkuda[1].x := otrezki[0,1].x;
   otkuda[1].y := otrezki[0,1].y;

   kuda[0].x := random(GetMaxX);
   kuda[0].y := random(GetMaxY);
   kuda[1].x := random(getMaxX);
   kuda[1].y := random(GetMaxY);


   end;

   steret(otrezki);
   dvigat(otrezki);

   otrezki[0,0].x:=trunc(x);
   otrezki[0,0].y:=trunc(y);
   otrezki[0,1].x:=trunc(x1);
   otrezki[0,1].y:=trunc(y1);

    x:=x+(kuda[0].x-otkuda[0].x)*sagi;
    y:=y+(kuda[0].y-otkuda[0].y)*sagi;
    x1:=x+(kuda[1].x-otkuda[1].x)*sagi;
    y1:=y+(kuda[1].y-otkuda[1].y)*sagi;

    if(kuda[0].x=otkuda[0].x)and(kuda[0].y=otkuda[0].y)and(kuda[1].x=otkuda[1].x)and(kuda[1].y=otkuda[1].y)then begin
    x1:=0;
    y1:=0;
    x:=0;
    y:=0;
    end;

   narisovat(otrezki);
   delay(20);
  until keypressed;

  CloseGraph;
end.
