program _21;
type Pdvojka=^dvojka;
     up=^boolean;
     dvojka=record
     pc:^up;
     next:pdvojka;
end;
var s:pdvojka;
begin
writeln(memavail);
new(s);
new(s^.pc);
new(s^.pc^);
s^.pc^^:=False;
new(s^.next);
new(s^.next^.pc);
s^.next^.pc^:=s^.pc^;

new(s^.next^.next);

new(s^.next^.next^.pc);
new(s^.next^.next^.pc^);

s^.next^.next^.pc^^:=True;

dispose(s^.next^.next^.pc^);
dispose(s^.next^.next^.pc);

dispose(s^.next^.next);
dispose(s^.next^.pc);
dispose(s^.next);
dispose(s^.pc^);
dispose(s^.pc);
dispose(s);
writeln(memavail,'*');
writeln;
end.