program kvadrat;
uses graph,crt;

const
  polustor=30;
  ugol=0.1;

type
  point=record
    x,y:real;
  end;

procedure egavga;external;
{$L egavga.obj}

procedure draw(kvadr : array of point;c:integer);
begin
  SetColor(c);
  line(trunc(kvadr[0].x),trunc(kvadr[0].y),trunc(kvadr[1].x),trunc(kvadr[1].y));
  line(trunc(kvadr[1].x),trunc(kvadr[1].y),trunc(kvadr[2].x),trunc(kvadr[2].y));
  line(trunc(kvadr[2].x),trunc(kvadr[2].y),trunc(kvadr[3].x),trunc(kvadr[3].y));
  line(trunc(kvadr[3].x),trunc(kvadr[3].y),trunc(kvadr[0].x),trunc(kvadr[0].y));
end;


var GrDr,GrMd :integer;
    grError,i   :integer;
    center   :point;
    kvadr  :array[0..3]of point;
begin
  randomize;
  RegisterBGIDriver(@EGAVGA);
  GrDr := vga;
  grMd := vgaHi;
  InitGraph(grDr,grMd,'c:\bp\bgi');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
  writeln('Error: ',GraphErrorMsg(grError));
  halt(1);
  end;
  center.x:=GetMaxX div 2;
  center.y:=GetMaxY div 2;
  setViewPort(trunc(center.x),trunc(center.y),GetMaxX,GetMaxY,ClipOff);
  kvadr[0].x:=-polustor;
  kvadr[0].y:=-polustor;
  kvadr[1].x:=+polustor;
  kvadr[1].y:=-polustor;
  kvadr[2].x:=+polustor;
  kvadr[2].y:=+polustor;
  kvadr[3].x:=-polustor;
  kvadr[3].y:=+polustor;
   repeat

    draw(kvadr,White);
     delay(200);
     draw(kvadr,0);
      for i:=0 to 3 do begin
       kvadr[i].x:=kvadr[i].x*cos(ugol)-kvadr[i].y*sin(ugol);
       kvadr[i].y:=kvadr[i].x*sin(ugol)+kvadr[i].y*cos(ugol);
      end;
until keypressed;

CloseGraph;
end.
