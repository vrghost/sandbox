{$N+}
uses graph,crt;
Type Tfloat=Extended;
 point=record
  x,y:TFloat;
  end;


 function FindCross(p1,p2:point;lx,rx:TFloat):TFloat;
  var xl1,yl1,xl2,yl2,treug,t,trt,xr1,xr2,yr1,yr2:TFloat;

begin
     xl1:=p1.x-p2.x;
     yl1:=p1.y-p2.y;
     xl2:=rx-lx;
     yl2:=0;
     xr1:=p1.x;
     xr2:=lx;
     yr1:=p1.y;
     yr2:=0;

     treug:=-xl1*yl2+xl2*yl1;

     trt:=-yl2*xr2+xr1*yl2+xl2*yr2-xl2*yr1;
     t:=trt/treug;

     FindCross:=xr1+xl1*t;

end;


procedure l(a,b,c,d : Tfloat);
begin
  line(trunc(a),trunc(b),trunc(c),trunc(d));
end;

function FYget(x:TFloat):TFloat;
 var y : TFloat;
 begin
  y:=sin(0.02*x+1)*100;
  FYget:=y;
 end;

procedure drawfunc(xf,xt:TFloat);
var temp,y   :Tfloat;
    i,hx,hy  :longint;
    p        :point;
begin
  if xf>xt then begin
    temp:=xf;
    xf:=xt;
    xt:=temp;
  end;
  hx:=GetMaxX div 2;
  hy:=GetMaxY div 2;
  p.x:=hx+trunc(xf);
  p.y:=hy+trunc(0);

  for i:=trunc(xf) to trunc(xt) do begin
    y:=FYget(i);
    PutPixel(hx+i,hy+trunc(y),Green);

    if i<> trunc(xf) then  line(trunc(hx+i),trunc(hy+y),trunc(p.x),trunc(p.y));

    p.x:=hx+i;
    p.y:=hy+trunc(y);
  end;

  SetColor(Red);

  line(trunc(xt+hx+10),trunc(hy),trunc(xf+hx-10),trunc(hy));

  SetColor(White);

end;



var grDr,grMd,grError         :integer;
    i                         :integer;
    lx,rx,x,tmp,cross,cx,cy   :TFloat;
    pl,pr                     :point;
    text                      :string;

begin
clrscr;
randomize;
  GrDr := VGA;
  GrMd:=VgaMed;
  InitGraph(grDr,grMd,'c:\bp\bgi');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
    writeln('Error: ',GraphErrorMsg(grError));
    halt(1);
  end;


  lx:=-100;
  rx:=100;

  if lx>rx then begin
    tmp:=lx;
    lx:=rx;
    rx:=tmp;
  end;

 drawfunc(lx,rx);
 pl.x:=lx;
 pl.y:=FYGet(pl.x);

 cx:=GetMaxX div 2;
 cy:=GetMaxY div 2;

 pr.x:=rx;
 pr.y:=FYget(pr.x);

  repeat
  cross:=FindCross(pl,pr,lx,rx);

  SetColor(Blue);

   l(pr.x+cx,cy,pr.x+cx,FYget(pr.x)+cy);
   l(pl.x+cx,cy,pl.x+cx,FYget(pl.x)+cy);
   l(pr.x+cx,Fyget(pr.x)+cy,pl.x+cx,FYget(pl.x)+cy);


  SetColor(White);


  if FYget(cross+cx)>0 then begin
    pr.x:=cross;
    pr.y:=FYget(pr.x);
  end
  else begin
    pl.x:=cross;
    pl.y:=FYGet(pl.x);
  end;

  until (keyPressed)or(abs(Fyget(cross))<0.0001);

  str(cross,text);
  text:='X= '+text;
  OutTextXY(100,100,text);

  str(FYget(cross),text);
  text:='y= '+text;
  OutTextXY(100,120,text);

 readkey;
CloseGraph;
end.