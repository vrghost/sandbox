{$N+}
uses graph,crt;
type
  TFloat = extended;

 point=record
     x,y:TFloat;
     end;

otrezok=array[0..1]of point;


procedure egavga;external;
{$L egavga.obj}


 procedure drline(o:otrezok;c:integer);
  begin
   SetColor(c);
   line(trunc(o[0].x),trunc(o[0].y),trunc(o[1].x),trunc(o[1].y));
   SetColor(White);
  end;

 procedure ppix(p:point;c:integer);
  begin
   PutPixel(trunc(p.x),trunc(p.y),c);
  end;

 function pomnozit(a,b:otrezok):TFloat;
  var ab,cos,otvet:double;
   aa,bb : point;
  begin
  aa.x:=a[1].x-a[0].x;
  aa.y:=a[1].y-a[0].y;
  bb.x:=b[1].x-b[0].x;
  bb.y:=b[1].y-b[0].y;
  ab:=aa.x*bb.x+aa.y*bb.y;
  cos:=ab/( sqrt(sqr(aa.x)+sqr(aa.y))*sqrt(sqr(bb.x)+sqr(bb.y)) );
  otvet:=ab*cos;
  pomnozit:=otvet;
  end;

  function dlina(l:otrezok):TFloat;
  var d:double;
  begin
   d:=sqrt(sqr(l[1].x-l[0].x)+sqr(l[1].y-l[0].y));
   dlina:=d;
  end;

var grDr,grMd,grError:integer;

    l,x,temp,oc1: otrezok;
    i:integer;
    ch    :char;
    p,lnorm,tau,n,oc  :point;
    dtau,xl,yl:TFloat;

begin
clrscr;
randomize;
  RegisterBGIDriver(@EGAVGA);
  GrDr := VGA;
  GrMd:=VgaMed;
  InitGraph(grDr,grMd,'');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
    writeln('Error: ',GraphErrorMsg(grError));
    halt(1);
  end;
 SetWriteMode(1);


{ for i:=0 to 1 do begin
 l[i].x:=random(GetMaxX);
 l[i].y:=random(GetMaxY);
 end;
 p.x:=random(GetMaxX);
 p.y:=random(GetMaxY); }
  l[0].x:=100;
  l[0].y:=100;
  l[1].x:=200;
  l[1].y:=100;
  p.x:=150;
  p.y:=50;


 drline(l,White);
 ppix(p,white);

 x[0]:=p;
 x[1]:=l[1];

 drline(x,1);

 dtau:=(pomnozit(x,l)/dlina(l));

 xl:=l[1].x-l[0].x;
 yl:=l[1].y-l[0].y;

 lnorm.x:=xl/dlina(l);
 lnorm.y:=yl/dlina(l);

 tau.x:=dtau*lnorm.x;
 tau.y:=dtau*lnorm.y;

 n.x:=xl-tau.x;
 n.y:=yl-tau.y;

 oc:=l[0];
 oc1[1].x:=oc.x+n.x;
 oc1[1].y:=oc.y+n.y;
 oc1[0]:=p;

 drline(oc1,Green);

  readkey;
  CloseGraph;
end.


