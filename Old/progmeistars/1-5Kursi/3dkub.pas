program Kub;
 uses graph,crt;
 const rebro=20;
   ugol=pi/100;
 type Tfloat=real;
 Tpoint3D=record
   x,y,z:Tfloat;
 end;
 soed=record
 pt1,pt2 : word;
 end;
procedure egavga;external;
{$L egavga.obj}
function rotate(tocki:Tpoint3d): Tpoint3D;
 var i:integer;
 begin

 tocki.x:=tocki.x*cos(ugol)-tocki.z*sin(ugol);
 tocki.z:=tocki.x*sin(ugol)+tocki.z*cos(ugol);
 rotate:=tocki;
end;

procedure narisovat(a,b:Tpoint3d);
   var x,y,x2,y2:integer;
   begin
    x:=trunc(-a.x/a.z);
    y:=trunc(-a.y/a.z);
    x2:=trunc(-b.x/b.z);
    y2:=trunc(-b.y/b.z);
    line(x,y,x2,y2);
end;

procedure draw(to4ki:array of Tpoint3d;liniji:array of soed);
   var i,ap,bp:integer;
   begin
   i:=0;
    for i:=0 to 11 do begin
     ap:=liniji[i].pt1;
     bp:=liniji[i].pt2;
     narisovat(to4ki[ap],to4ki[bp]);
    end;
end;


var grDr,grMd,grError:integer;
 to4ki:array[0..8]of Tpoint3d;
 liniji:array[0..11] of soed;
begin
randomize;
  RegisterBGIDriver(@EGAVGA);
  GrDr := VGA;
  GrMd:=VgaMed;
  InitGraph(grDr,grMd,'');
  grError := GraphResult;
  if(grError <> grOK)then
  begin
    writeln('Error: ',GraphErrorMsg(grError));
    halt(1);
  end;
   SetColor(GreeN);
   setViewPort(getMaxX div 2,getMaxY div 2,GetMaxX,GetMaxY,False);
  to4ki[0].x:=0;
  to4ki[0].y:=0;
  to4ki[0].z:=1;

  to4ki[1].x:=0;
  to4ki[1].y:=rebro;
  to4ki[1].z:=1;

  to4ki[2].x:=rebro;
  to4ki[2].y:=rebro;
  to4ki[2].z:=1;

  to4ki[3].x:=rebro;
  to4ki[3].y:=0;
  to4ki[3].z:=1;

  to4ki[4].x:=rebro;
  to4ki[4].y:=0;
  to4ki[4].z:=rebro;

  to4ki[5].x:=rebro;
  to4ki[5].y:=rebro;
  to4ki[5].z:=rebro;

  to4ki[6].x:=0;
  to4ki[6].y:=rebro;
  to4ki[6].z:=rebro;

  to4ki[7].x:=0;
  to4ki[7].y:=0;
  to4ki[7].z:=rebro;


   liniji[0].pt1:=0;
   liniji[0].pt2:=1;

 liniji[1].pt1:=1;
 liniji[1].pt2:=2;

 liniji[2].pt1:=2;
 liniji[2].pt2:=3;

 liniji[3].pt1:=3;
 liniji[3].pt2:=0;

 liniji[4].pt1:=0;
 liniji[4].pt2:=7;

 liniji[5].pt1:=3;
 liniji[5].pt2:=4;

 liniji[6].pt1:=2;
 liniji[6].pt2:=5;

 liniji[7].pt1:=1;
 liniji[7].pt2:=6;

 liniji[8].pt1:=7;
 liniji[8].pt2:=4;

 liniji[9].pt1:=4;
 liniji[9].pt2:=5;

 liniji[10].pt1:=5;
 liniji[10].pt2:=6;

  liniji[11].pt1:=6;
  liniji[11].pt2:=7;

  repeat
  draw(to4ki,liniji);
  delay(10);
  to4ki:=rotate(to4ki);
  ClearViewPort;

  until keypressed;
  readkey;
 closeGraph;
 end.