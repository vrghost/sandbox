
/*
 * base length: 15 cm
 * motor left: A
 * motor right: B
 * multiplicator: 1000
 * wheel diameter: 5.5 cm
 */

#ifndef __MOVE_STAT_HEADER_INCLUDED
#define __MOVE_STAT_HEADER_INCLUDED

#define BASE_DEG_LEN 131
#define DEG_MULTIPLICATOR 1000
#define MOTORS_BOTH OUT_AB
#define MOTOR_LEFT OUT_A
#define MOTOR_RIGHT OUT_B
#define WHEEL_DEG_LEN 48

#endif
