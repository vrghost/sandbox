// moving low-level math

#ifndef __LOW_MOV_INCL
#define __LOW_MOV_INCL

#include "sys.nxc"

#include "./calc/movingBaseStats.h"

#define DEG_TO_LEN(deg, base) ((deg * base) / DEG_MULTIPLICATOR)
#define LEN_TO_DEG(len, base) ((len * DEG_MULTIPLICATOR) / base)

#define BASE_DEG_TO_LEN(deg)  DEG_TO_LEN(deg, BASE_DEG_LEN)
#define BASE_LEN_TO_DEG(len)  LEN_TO_DEG(len, BASE_DEG_LEN)

#define WHEEL_DEG_TO_LEN(deg)  DEG_TO_LEN(deg, WHEEL_DEG_LEN)
#define WHEEL_LEN_TO_DEG(len)  LEN_TO_DEG(len, WHEEL_DEG_LEN)

mutex __mov_motor_control_mutex;
mutex __mov_motor_read_mutex;

inline void __powerCheck(int power, string msg){
     if(power <= 0 || power > 100){
         logMsg(msg);
         terminate();
     }
}

void resetMovTachos(){
     Acquire(__mov_motor_control_mutex);
     Acquire(__mov_motor_read_mutex);
     
     ResetTachoCount(MOTORS_BOTH);
     
     Release(__mov_motor_read_mutex);
     Release(__mov_motor_control_mutex);
}

void getTachos(long &right, long &left){
     Acquire(__mov_motor_read_mutex);
     
     right = MotorTachoCount(MOTOR_RIGHT);
     left = MotorTachoCount(MOTOR_LEFT);
     
     Release(__mov_motor_read_mutex);
}

long getStraightTacho(){
     long _motor1, _motor2;
     
     getTachos(_motor1, _motor2);
       
     return (_motor1 + _motor2) / 2;
}

long goStraight(long deg, int power){
     Acquire(__mov_motor_control_mutex);
     long _rv;
     
     __powerCheck(power, "!Pow_straight!");
     if(deg < 0){
            power = -power;
     }
     
     OnFwdRegEx(MOTORS_BOTH, power, OUT_REGMODE_SYNC, RESET_ALL);
     
     if(power > 0){
          while(getStraightTacho() < deg){}
     }else{
          while(getStraightTacho() > deg){}
     }
     
     _rv = getStraightTacho() - deg;
     
     Release(__mov_motor_control_mutex);
     return _rv;
}

long goTurn(long deg, int power){
     Acquire(__mov_motor_control_mutex);
     long _rv, _left, _right;
     
     __powerCheck(power, "!Pow_Turn!");
     if(deg < 0){
            power = -power;
            deg = abs(deg);
     }
     
     OnFwdEx(MOTOR_LEFT, power, RESET_ALL);
     OnRevEx(MOTOR_RIGHT, power, RESET_ALL);
     
     getTachos(_right, _left);
     while(abs(_right) < deg || abs(_left) < deg){
          getTachos(_right, _left);
          logNumMsg("r", _right);
          logNumMsg("l", _left);
          logNumMsg("l", deg);
     }
     
     _rv = (abs(_right)  + abs(_left) - 2*deg);
     
     Release(__mov_motor_control_mutex);
     return _rv / 2;
}

#endif

