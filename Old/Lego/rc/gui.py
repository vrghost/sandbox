import wx
import math
import itertools

import window_xrc
import talker

class Message(dict):

	def toStr(self):
		return ",".join("%s=%s" % _d for _d in self.iteritems())

	@classmethod
	def fromStr(cls, data):
		_rv = cls()
		for _e in data.split(","):
			if not _e:
				continue
			(_k, _v) = _e.split("=", 1)
			_rv[_k] = _v
		return _rv

class Frame(window_xrc.xrcMain):

	_serial = None
	points = None
	_readbuf = ""
	_readTimer = None
	_redrawTimer = None
	_sensAsker = None

	def __init__(self, pre):
		window_xrc.xrcMain.__init__(self, pre)
		self._serial = talker.NxtNeogatiator(); #file("C:\\log.txt", "rb")
		self._readTimer = wx.Timer(self)
		self._readTimer.Start(150)
		self._redrawTimer = wx.Timer(self)
		self._redrawTimer.Start(400)
		self._sensAsker = wx.Timer(self)
		#self._sensAsker.Start(800)
		self.points = []
		self.Bind(wx.EVT_TIMER, self.OnTimer)
	        self.drawPanel.Bind(wx.EVT_PAINT, self.OnPaint_drawPanel)
		self.drawPanel.Bind(wx.EVT_RIGHT_DOWN, self.OnRight_down_drawPanel)
		self.drawPanel.Bind(wx.EVT_LEFT_DOWN, self.OnLeft_down_drawPanel, self.drawPanel)

	def OnTimer(self, evt):
		if evt.EventObject == self._readTimer:
			self.checkData()
		elif evt.EventObject == self._redrawTimer:
			self.Refresh()
		elif evt.EventObject == self._sensAsker:
			self.askSensors()

	def askSensors(self):
		self._serial.send(2, "typ=sens,what=Locate");

	def checkData(self):
		_msg = self._serial.getMessage(0)
		if _msg:
			self._inputMsg(_msg)

	def _inputMsg(self, msg):
		_msg = Message.fromStr(msg)
		print msg
		if _msg:
			if _msg["typ"] == "sens" and _msg["nam"] == "Locate":
				self._calcPoint(_msg)
			else:
				print "unknown msg:", _msg

	def _calcPoint(self, msg):
		_dist = int(msg["dist"])
		_deg = int(msg["deg"])
		if _dist == 0 or _dist > 255:
			return
		_x = math.cos(math.radians(_deg)) * _dist
		_y = math.sin(math.radians(_deg)) * _dist
		self.points.append((_x, _y))

	def OnPaint_drawPanel(self, evt):
		(_w, _h) = self.drawPanel.GetClientSizeTuple()
	        _buffer = wx.EmptyBitmap(_w, _h)
        	_dc = wx.BufferedPaintDC(self.drawPanel, _buffer)
		_dc.Clear()
		_dc.SetPen(wx.Pen("black"))
		_dc.SetBrush(wx.Brush("black"))
		for (_x, _y) in self.points:
			_dc.DrawCircle(_x + _w/2, _y + _h/2, 2)

	def OnRight_down_drawPanel(self, evt):
		self.points = []
		self.Refresh()

	def OnLeft_down_drawPanel(self, evt):
		_x = evt.X
		_y = evt.Y
		(_cx, _cy) = self.drawPanel.GetClientSizeTuple()
		_cx /= 2.0
		_cy /= 2.0
		_dist = math.sqrt((_x - _cx) ** 2 + (_y - _cy) ** 2)
		_sin = (_y - _cy) / _dist
		_cos = (_x - _cx) / _dist
		_deg = math.degrees(math.acos(_cos))
		if _sin > 0:
			_deg = 360 - _deg
		_msg = Message(
			typ="moto",
			nam="Head",
			what="setDeg",
			arg1=str(int(_deg)),
		)
		print _msg.toStr()
		self._serial.send(1, _msg.toStr())


def showForm():
    _form = Frame(None)
    assert _form
    _form.Show()
    return _form

def run():
	_app = wx.App()
	_app.RestoreStdio()
	window_xrc.get_resources()
	showForm()
	_app.MainLoop()
