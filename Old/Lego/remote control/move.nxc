#define LEFT_MOTOR_PORT OUT_A
#define RIGHT_MOTOR_PORT OUT_B
#define BOTH_MOVE_MOTORS OUT_AB
#define DYNAMIC_STEP_COUNT  3
#define MAX_MOVE_POWER     90
#define DYNAMIC_PRECENTAGE 15
#define MAX_RPM            160    // rpm count @ full power

#define BASE_DISTANCE_DEG_LEN    131  // <distance detween wheels> * pi / 360 * 1000

#define WHEEL_DEG_LEN    48     // length of one wheel's thousandh degree in milimeters.
                                // (actually it's ~ 0.047996554 cm)

#define CM_TO_DEG(x, deg_len)     (((x) * 1000) / (deg_len))
#define DEG_TO_CM(x, deg_len)     (((x) * (deg_len)) / 1000)

int getPassedDistance(){
    int rot1, rot2;
    rot1 = MotorTachoCount(LEFT_MOTOR_PORT);
    rot2 = MotorTachoCount(RIGHT_MOTOR_PORT);
    rot1 = rot1 + rot2 ;
    return DEG_TO_CM(rot1 / 2, WHEEL_DEG_LEN);
}

long getRotPassedDistance(){
    long rot1, rot2;
    rot1 = MotorTachoCount(LEFT_MOTOR_PORT);
    rot2 = MotorTachoCount(RIGHT_MOTOR_PORT);
    rot1 = abs(rot1) + abs(rot2);
    return DEG_TO_CM(rot1 / 2, BASE_DISTANCE_DEG_LEN);
}

int moveRatio(){
    int mp1, mp2;
    mp1 = MotorPower(LEFT_MOTOR_PORT);
    mp2 = MotorPower(RIGHT_MOTOR_PORT);
    return (mp1 + mp2) / 2;
}

int holdFwdPower(int power, int len){
    if(power <= 0){
        return 0;
    }
    if(power > MAX_MOVE_POWER){
        power = MAX_MOVE_POWER;
    }
    OnFwdRegEx(BOTH_MOVE_MOTORS, power, OUT_REGMODE_SYNC, RESET_ALL);
    while(getPassedDistance()  < len ){}
    return len - getPassedDistance();
}

int holdRevPower(int power, int len){
    if(power <= 0){
        return 0;
    }
    if(power > MAX_MOVE_POWER){
        power = MAX_MOVE_POWER;
    }
    OnRevRegEx(BOTH_MOVE_MOTORS, power, OUT_REGMODE_SYNC, RESET_ALL);
    while(-getPassedDistance()  < len ){}
    return len + getPassedDistance();
}

long lenPerSecond(int power){
    return DEG_TO_CM((power * MAX_RPM * WHEEL_DEG_LEN) * 6 / 50000, WHEEL_DEG_LEN);
}

int getMaxSpeed(int len){
    int rv;
    rv = MAX_MOVE_POWER;
    while(lenPerSecond(rv) * 10 > len){
        rv -= 5;
    }
    return rv;
}

void goForward(int len){
     // length is given in centimetres, since no one waits robot to
     // perform with milimmeter precision

     int full_power_len, dynamic_len, len_delta;
     int deserved_speed, max_speed;
     int power_step, step_length;
     int ii, dist_left;

     dynamic_len = (len * DYNAMIC_PRECENTAGE) / 100;
     full_power_len = len - dynamic_len;
     max_speed = getMaxSpeed(full_power_len);
     deserved_speed = -moveRatio();
     power_step = (max_speed - deserved_speed) / DYNAMIC_STEP_COUNT;
     step_length = dynamic_len / DYNAMIC_STEP_COUNT;
     len_delta = 0;

     // speedup
     for(ii = 0; ii < DYNAMIC_STEP_COUNT; ii++){
            deserved_speed += power_step;
            len_delta = holdFwdPower(deserved_speed, step_length - len_delta);
     }
     // max power
     len_delta = holdFwdPower(max_speed, full_power_len - len_delta);
     // slow down
     deserved_speed = max_speed;
     for(ii = 0; ii < DYNAMIC_STEP_COUNT; ii++){
            deserved_speed -= power_step;
            len_delta = holdFwdPower(deserved_speed, step_length - len_delta);
     }
     CoastEx(BOTH_MOVE_MOTORS, RESET_ALL);
}

void goBack(int len){
     // length is given in centimetres, since no one waits robot to
     // perform with milimmeter precision

     int full_power_len, dynamic_len, len_delta;
     int deserved_speed, max_speed;
     int power_step, step_length;
     int ii, dist_left;

     dynamic_len = (len * DYNAMIC_PRECENTAGE) / 100;
     full_power_len = len - dynamic_len;
     max_speed = getMaxSpeed(full_power_len);
     deserved_speed = moveRatio();
     power_step = (max_speed - deserved_speed) / DYNAMIC_STEP_COUNT;
     step_length = dynamic_len / DYNAMIC_STEP_COUNT;
     len_delta = 0;

     // speedup
     for(ii = 0; ii < DYNAMIC_STEP_COUNT; ii++){
            deserved_speed += power_step;
            len_delta = holdRevPower(deserved_speed, step_length - len_delta);
     }
     // max power
     len_delta = holdRevPower(max_speed, full_power_len - len_delta);
     // slow down
     deserved_speed = max_speed;
     for(ii = 0; ii < DYNAMIC_STEP_COUNT; ii++){
            deserved_speed -= power_step;
            len_delta = holdRevPower(deserved_speed, step_length - len_delta);
     }
     CoastEx(BOTH_MOVE_MOTORS, RESET_ALL);
}

int turnGo(int power, long len){
    if(power < 0){
             return len;
    }
    if (power > MAX_MOVE_POWER){
       power = MAX_MOVE_POWER;
    }
    if(len < 0){
           power = -power;
           len = -len;
    }
    OnFwdEx(LEFT_MOTOR_PORT, power, RESET_ALL);
    OnRevEx(RIGHT_MOTOR_PORT, power, RESET_ALL);
    while(getRotPassedDistance() < len *2 ){}
    CoastEx(BOTH_MOVE_MOTORS, RESET_ALL);
    return getRotPassedDistance() - (len*2);
}

void rotate(int degrees){
     // turn robot X degrees right
     long len;
     len = DEG_TO_CM(degrees*3/2, BASE_DISTANCE_DEG_LEN); // each motor must go this distance

     turnGo(70, len);
}
