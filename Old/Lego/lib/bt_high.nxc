#ifndef __HIGH_BT_INCL
#define __HIGH_BT_INCL

#include "sys.nxc"
#include "bt_low.nxc"

#define PUSHER_BUFFER_SIZE          16
#define PUSHER_QUEUE_SIZE           10
#define PUSHER_DATA_END_MARK        "push ended"
#define RCVR_DATA_ACK               "ACK"
#define RCVR_PUSHED_DATA_MBOX       1
#define LOCAL_ANSWER_QUEUE          1
#define MAX_ACK_WAIT_ITERS          10
#define RECEIVE_TIMEOUT		        500
#define TRANSMIT_TOKEN              "You now transmit"


mutex __bt_usage_mutex;
mutex __bt_rcvr_mutex;

struct __pusherQeueElement{
       string data;
       string header;
       int dataLen;
       bool busy;
};

struct __receivedQeueElement{
	string data;
	int mbox;
	bool busy;
};

__pusherQeueElement pusherQueue[];
__receivedQeueElement receiveQueue[];

string __sensorDataHeader(int port){
     string msg;
     msg = "Please fetch:SENS:";
     msg += NumToStr(port);
     return msg;
}

string __motorDataHeader(int port){
     string msg;
     msg = "Please fetch:MOTOR:";
     msg += NumToStr(port);
     return msg;
}

bool __waitForAck(){
     bool acked;
     int iterCount;
     string msg;

     acked = false;
     iterCount = 0;
     while(!acked && iterCount < MAX_ACK_WAIT_ITERS){
         ReceiveMessage(LOCAL_ANSWER_QUEUE, true, msg);
         acked = msg == RCVR_DATA_ACK;
         iterCount++;
         if(!acked){
             if(StrLen(msg) > 0){
                 logMsg("??ACK??");
                 logMsg(msg);
             }
             NOOP;
         }
     }
     
     return acked;
}

void __sendSysMsg(string msg){
     Acquire(__bt_usage_mutex);
     pushToPc(RCVR_PUSHED_DATA_MBOX, msg);
     Release(__bt_usage_mutex);
}

void __btSendElements(){
     Acquire(__bt_usage_mutex);
     bool found;
     __pusherQeueElement el;
     int ii, bufferLen, sndFrom, sndLen;
     string sendBuffer, str;
     
     for(ii=0;ii<PUSHER_QUEUE_SIZE;ii++){
	     el = pusherQueue[ii];
	     if(el.busy){
	         sendBuffer = el.header;
	         pushToPc(RCVR_PUSHED_DATA_MBOX, sendBuffer);
	         __waitForAck();
	         sendBuffer = el.data;
	         bufferLen = el.dataLen;
	         sndFrom = 0;
	         sndLen = PUSHER_BUFFER_SIZE;
	         while(bufferLen > sndFrom){
		         sndLen = PUSHER_BUFFER_SIZE;
		         if(sndFrom + sndLen > bufferLen){
		            sndLen = bufferLen - sndFrom;
  		         }
	  	         str = "DATA:";
		         str += SubStr(sendBuffer, sndFrom, sndLen);
	 	         pushToPc(RCVR_PUSHED_DATA_MBOX, str);
		         __waitForAck();
		         sndFrom += sndLen;
	         }
	         pushToPc(
	  	         RCVR_PUSHED_DATA_MBOX, PUSHER_DATA_END_MARK);
	         __waitForAck();
	         el.busy = false;
	         pusherQueue[ii] = el;
	     }
     }
     Release(__bt_usage_mutex);
}

void __appendReceivedMsg(int mbox, string msg){
     __receivedQeueElement el;
     Acquire(__bt_rcvr_mutex);
     int ii;
     for(ii=0;ii<PUSHER_QUEUE_SIZE;ii++){
         el = receiveQueue[ii];
         if(!el.busy){
             el.mbox = mbox;
             el.data = msg;
             el.busy = true;
             receiveQueue[ii] = el;
             Release(__bt_rcvr_mutex);
             return;
         }
     }
     Release(__bt_rcvr_mutex);
     logMsg("Rcvd msg lost");
}

void __btReceiveElements(){
     int mbox;
     string msg;
     bool finished, noData;
     TIME_T idleTime;
     
     finished = false;
     idleTime = 0;
     
     while(!finished){
         // check mailboxes...
         noData = true;
         for(mbox=1;mbox<10;mbox++){
             if(mbox == LOCAL_ANSWER_QUEUE){
                 continue;
             }
             ReceiveMessage(mbox, true, msg);
             while(StrLen(msg) > 0){
                 noData = false;
                 idleTime = 0;
                 __appendReceivedMsg(mbox, msg);
                 __sendSysMsg(RCVR_DATA_ACK);
                 ReceiveMessage(mbox, true, msg);
             }
         }
         if(noData && idleTime == 0){
             idleTime = CurrentTick();
         }
         // check finish condition;
         ReceiveMessage(LOCAL_ANSWER_QUEUE, true, msg);
         finished = msg == TRANSMIT_TOKEN;
         if(finished){
             __sendSysMsg(RCVR_DATA_ACK);
         }else if(noData && CurrentTick() - idleTime > RECEIVE_TIMEOUT){
             __sendSysMsg("Transmit acquire forced");
             finished = true;
         }
     }
}

task BtPusher(){
     bool found, acked;
     int ii, sndFrom, sndLen, bufferLen;
     string sendBuffer, str;
     __pusherQeueElement el;
     
     while(1){
         __btSendElements();
         __sendSysMsg(TRANSMIT_TOKEN);
         __waitForAck();
         logMsg("Tok: him");
         NOOP_X(2);
         __btReceiveElements();
         NOOP_X(2);
         logMsg("Tok: me");
     }
}

safecall bool __btSendMeas(string header, string data, int len){
    __pusherQeueElement el;
    int ii, waitIter;
    bool found;
    
    found = false;
    waitIter = 0;
    while(!found && waitIter < 15){
        Acquire(__bt_usage_mutex);
        for(ii=0;ii<PUSHER_QUEUE_SIZE && !found;ii++){
            el = pusherQueue[ii];
            if(!el.busy){
                el.data = data;
                el.dataLen = len;
                el.header = header;
                el.busy = true;
                pusherQueue[ii] = el;
                found = true;
            }
        }
        Release(__bt_usage_mutex);
        if(!found){
            NOOP;
            waitIter += 1;
        }
    }
    return waitIter < 15;
}

safecall void btSensMeas(MEAS_TYPE src[], int len, int port){
    string data;
    
    data = ByteArrayToStr(src);
    data = SubStr(data, 0, len);
    if(!__btSendMeas(__sensorDataHeader(port), data, len)){
        logMsg("Sens data dropped");
    }
}

safecall void btMotorStr(string data, int port){
    if(!__btSendMeas(__motorDataHeader(port), data, StrLen(data))){
        logMsg("Motor data dropped");
    }
}

safecall void btMotorMeas(MEAS_TYPE src[], int len, int port){
    string data;

    data = ByteArrayToStr(src);
    data = SubStr(data, 0, len);
    if(!__btSendMeas(__motorDataHeader(port), data, len)){
        logMsg("Motor data dropped");
    }
}

safecall string getReceivedMessage(int mbox){
     Acquire(__bt_rcvr_mutex);
     int ii;
     __receivedQeueElement el;
     string msg;
     
     for(ii=0;ii<PUSHER_QUEUE_SIZE;ii++){
         el = receiveQueue[ii];
         if(el.busy && el.mbox == mbox){
             msg = el.data;
             el.busy = false;
             receiveQueue[ii] = el;
             Release(__bt_rcvr_mutex);
             return msg;
         }
     }
     Release(__bt_rcvr_mutex);
     return "";
}

void initBtPusher(){
     Acquire(__bt_usage_mutex);
     Acquire(__bt_rcvr_mutex);
     __pusherQeueElement el;
     __receivedQeueElement recEl;
     
     el.data = "";
     el.header = "";
     el.busy = false;
     el.dataLen = 0;
     ArrayInit(pusherQueue, el, PUSHER_QUEUE_SIZE);

     recEl.data = "";
     recEl.busy = false;
     recEl.mbox = 0;
     ArrayInit(receiveQueue, recEl, PUSHER_QUEUE_SIZE);
     
     StartTask(BtPusher);
     Release(__bt_rcvr_mutex);
     Release(__bt_usage_mutex);
}

#endif
