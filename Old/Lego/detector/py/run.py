import os
import sys
from datetime import datetime, timedelta
import time

from nxt_talker import Talker

outDir = os.path.join(os.path.dirname(__file__), "out")

class StateMachine(object):

    _curState = startState = None

    def __init__(self):
        self._curState = self.startState

    def setState(self, state):
        self._curState = state

    def callState(self):
        self._curState()


class MyBrickProtocol(StateMachine):

    _talker = None
    _transmitToken = "You now transmit"
    _ack = "ACK"
    _localMbox = 1
    _remoteSysMbox = 1
    _sndBuffer = None

    def __init__(self, talker):
        super(MyBrickProtocol, self).__init__()
        self._talker = talker
        self._sndBuffer = []

    def _sendSysMsg(self, msg):
        self._talker.send(self._remoteSysMbox, msg)

    def _getMsg(self):
        return self._talker.getMessage(self._localMbox)

    def _waitForAck(self):
        _acked = False
        while not _acked:
            _msg = self._getMsg()
            _acked = _msg == self._ack
            if not _acked:
                if _msg:
                    print "Not an ack", repr(_msg)
                time.sleep(0.1)

    def _sendAck(self):
        self._sendSysMsg(self._ack)

    def stateRecv(self):
        _rcv = True
        while _rcv:
            _msg = self._getMsg()
            if not _msg:
                time.sleep(0.1)
                continue
            print repr(_msg)
            self._sendAck()
            if _msg == self._transmitToken:
                _rcv = False
                self.setState(self.stateSnd)
                continue

    def stateSnd(self):
        while self._sndBuffer:
            _msg = self._sndBuffer.pop(0)
            _mbox = _msg["mbox"]
            _data = _msg["data"]
            self._talker.send(_mbox, _data)
            self._waitForAck()
        self._sendSysMsg(self._transmitToken)
        self._waitForAck()
        self.setState(self.stateRecv)

    startState = stateRecv

def run(dev):
    print "using dev=", dev
    _t = Talker(dev, 115200)
    # give time to initialize
    time.sleep(2)
    _proto = MyBrickProtocol(_t)
    while True:
        _proto.callState()
        time.sleep(0.1)

if __name__ == "__main__":
    run(sys.argv[1])

# vim: set sts=4 sw=4 et :
