from threading import Thread
import serial
import time

class CommunicationError(Exception): pass
class ReplyError(CommunicationError): pass
class SendError(CommunicationError): pass
class InputError(CommunicationError): pass

class NxtNeogatiator(Thread):

	_inBuf = ""
	_serial =  _inputMailboxes = _msgRequestBuffer = _outputMailboxes = None
	maxMsgSize = 58

	msgStatuses = (
		(0x00, "Success"),
		(0x20, "Pending communication transaction in progress"),
		(0x40, "Specified mailbox queue is empty"),
		(0x81, "No more handles"),
		(0x82, "No space"),
		(0x83, "No more files"),
		(0x84, "End of file expected"),
		(0x85, "End of file"),
		(0x86, "Not a linear file"),
		(0x87, "File not found"),
		(0x88, "Handle allready closed"),
		(0x89, "No linear space"),
		(0x8A, "Undefined error"),
		(0x8B, "File is busy"),
		(0x8C, "No write buffers"),
		(0x8D, "Append not possible"),
		(0x8E, "File is full"),
		(0x8F, "File exists"),
		(0x90, "Module not found"),
		(0x91, "Out of boundary"),
		(0x92, "Illegal file name"),
		(0x93, "Illegal handle"),
		(0xBD, "Request failed (i.e. specified file not found)"),
		(0xBE, "Unknown command opcode"),
		(0xBF, "Insane packet"),
		(0xC0, "Data contains out-of-range values"),
		(0xDD, "Communication bus error"),
		(0xDE, "No free memory in communication buffer"),
		(0xDF, "Specified channel/connection is not valid"),
		(0xE0, "Specified channel/connection not configured or busy"),
		(0xEC, "No active program"),
		(0xED, "Illegal size specified"),
		(0xEE, "Illegal mailbox queue ID specified"),
		(0xEF, "Attempted to access invalid field of a structure"),
		(0xF0, "Bad input or output specified"),
		(0xFB, "Insufficient memory available"),
		(0xFF, "Bad arguments"),
	)

	def __init__(self, port, speed):
		super(NxtNeogatiator, self).__init__()
		self._serial = serial.Serial(port, speed,
			parity=serial.PARITY_NONE, stopbits=1, bytesize=8)
		self._serial.setTimeout(1)
		self.setDaemon(True)
		self._of = file("C:\\log.txt", "wb")
		self._requestBuffer = []
		self._inputMailboxes = [list() for _id in xrange(10)]
		self._msgRequestBuffer = [False for _id in xrange(10)]
		self._outputMailboxes = [list() for _id in xrange(10)]
		# go!
		self.start()

	def run(self):
	       while True:
			while self._getInputCount() > 0:
				# there is data waiting!
				# message pushed?
				try:
					self._processPushedMessage()
				except Exception, _err:
					print _err
					self._syncInputBuffer()
                                if self._getInputCount():
                                    print "Unparsed data: %r", \
                                        self._getFromInBuf(-1)
			for _pos in xrange(len(self._outputMailboxes)):
				_mbox = self._outputMailboxes[_pos]
				if not _mbox:
					continue
				while _mbox:
					try:
						self._sendData(_pos, _mbox.pop())
					except Exception, _err:
						print _err
						self._syncInputBuffer()
			while True in self._msgRequestBuffer:
				_pos = self._msgRequestBuffer.index(True)
				try:
					self._getMessage(_pos)
				except Exception, _err:
					print _err
					self._syncInputBuffer()
				self._msgRequestBuffer[_pos] = False
			time.sleep(0.1)

	def send(self, mbox, what):
		assert isinstance(what, str)
		self._outputMailboxes[mbox].append(what)

	def requestMessage(self, mbox):
		self._msgRequestBuffer[mbox] = True

	def getMessage(self, mbox):
		_mbox = self._inputMailboxes[mbox]
		return _mbox.pop() if _mbox else None


	def _composeMsg(self, mbox, data):
		if len(data) > self.maxMsgSize:
			raise SendError("Message %s is too big" % data)
		_rv = "\x00\x09"
		_rv += chr(mbox)
		_rv += chr(len(data) + 1)
		_rv += data + "\x00"
		return _rv

	def _composeBtLength(self, num):
		return chr(num) + "\x00"

	def _getMessage(self, mbox):
		self._requestMessage(mbox)
		_reply = self._readReply()
		if _reply["bytes"][1] != 0x13:
			raise InputError("Second byte not 0x13 (%i)" % _reply["byte1"])
		if _reply["status"] == 0x40:
			# mailbox empty
			return None
		elif _reply["status"] != 0:
			raise InputError("Status not zero (%i). Status says '%s'." % \
				(_reply["status"], self._statusCodeToName(_reply["status"])))
		if _reply["mbox"] != mbox:
			raise Warning(
				"Mailbox not same as requested %i:%i" % (_reply["mbox"], mbox))
		return _reply["message"]

	def _requestMessage(self, mbox):
		_msg = "\x00\x13%s%s\x01" % (ord(mbox) + 10, ord(mbox))
		self._sendIntBtLen(5)
		self._rawSend(_msg)

	def _syncInputBuffer(self):
		if "\x00" in self._inBuf:
			_pos = self._inBuf.index("\x00")
			self._inBuf = self._inBuf[_pos+1:]
		else:
			self._inBuf = ""

	def _rawSend(self, data):
		self._serial.write(data)

	def _getInputCount(self):
		_size = self._serial.inWaiting()
		if _size > 0:
			self._inBuf += self._serial.read(_size)
		return len(self._inBuf)

	def _getFromInBuf(self, bytes):
		_size = self._serial.inWaiting()
		if _size > 0:
			self._inBuf += self._serial.read(_size)
		if bytes > len(_inBuf):
			self._inBuf += self._serial.read(
                            bytes - len(self._inBuf))
                if bytes > 0:
        	    _rv = self._inBuf[:bytes]
	            self._inBuf = self._inBuf[bytes:]
                else:
                    _rv = self._inBuf
                    self._inBuf = ""
		return _rv

	def _rawRead(self, desiredLen):
		_rv = ""
		_left = desiredLen
		while _left > 0:
			_el = self._serial.read(_left)
			if len(_el) == 0:
				raise Warning("Read timeout.")
				break
			_left -= len(_el)
			_rv += _el
		return _rv

	def _sendIntBtLen(self, num):
		_btLen = self._composeBtLength(num)
		self._rawSend(_btLen)

	def _sendData(self, mbox, what):
		_msg = self._composeMsg(mbox, what)
		self._sendIntBtLen(len(_msg))
		self._rawSend(_msg)
		self._readSendReply(mbox, what)

	def readBtLen(self, prefix=""):
		assert len(prefix) < 3
		_str = prefix + self._rawRead(2-len(prefix))
		_vals = reversed([ord(_el) for _el in _str])
		_rv = 0
		for _el in _vals:
			_rv = _rv * 256 + _el
		if _rv > 64:
			raise InputError("defined message length is too long (%i:%r)" % (_rv, _str))
		return _rv

	def _parseGeneralData(self, msg):
		_rv = {}
		_bytes = [ord(_el) for _el in msg]
		if _bytes[0] & 1:
			_rv["type"] = "System"
		elif _bytes[0] == 0x02:
			_rv["type"] = "Reply"
		else:
			assert not _bytes[0] & 1
			_rv["type"] = "Direct"
		_rv["bytes"] = _bytes
		_rv["replyRequired"] = (not _bytes[0] & 0x80) and (_rv["type"] != "Reply")
		_rv["str"] = msg
		return _rv

	def _statusCodeToName(self, code):
		for (_code, _name) in self.msgStatuses:
			if _code == code:
				return _name
		raise Warning("Status code %i not found" % code)
		#...and just to be explicit...
		return None

	def _readRawReply(self):
		_msgLen = self.readBtLen()
		_strMsg = self._rawRead(_msgLen)
		_rv = self._parseGeneralData(_strMsg)
		_bytes = list(_rv["bytes"])
		_bytes.extend([-1] * 5)
		_rv.update({
			"status":	_bytes[2],
			"mbox":		_bytes[3],
			"length":	_bytes[4],
			"message":	_strMsg[5:],
		})
		if _rv["type"] != "Reply":
			raise InputError(
				"First byte not raply command code %s." % _rv)
		return _rv

	def _readSendReply(self, mbox, data):
		_reply = self._readRawReply()
		if _reply["bytes"][1] != 0x09:
			raise ReplyError("Second byte not 0x09. (%i)" % _reply["byte1"])
		if _reply["status"] != 0:
			raise ReplyError("Message status not zero (%i). Status says '%s'." % \
				(_reply["status"], self._statusCodeToName(_reply["status"])))

	def _processPushedMessage(self):
		_len = self.readBtLen()
		_strMsg = self._rawRead(_len)
		_msg = self._parseGeneralData(_strMsg)
		if _msg["bytes"][1] == 0x09:
			# messagewrite
			self._processMessageWriteRequest(_msg)
		elif _msg["bytes"][1] == 0x13:
			# messageread
			self._processMessageReadRequest(_msg)

	def _processMessageWriteRequest(self, msg):
		if len(msg["str"]) < 4:
			_str = msg["str"] + self._rawRead(2)
			msg = self._parseGeneralData(_str)
		msg["mbox"] = msg["bytes"][2]
		msg["size"] = msg["bytes"][3]
		msg["message"] = msg["str"][4:]
		if msg["mbox"] > 10:
			msg["mbox"] -= 10
		if len(msg["message"]) < msg["size"]:
			msg["message"] += self._rawRead(msg["size"] - len(msg["message"]))
		if len(msg["message"]) == msg["size"]:
			_status = 0 # OK
			_txt = msg["message"]
			assert _txt.endswith("\x00")
			_txt = _txt[:-1]
			self._inputMailboxes[msg["mbox"]].append(msg["message"][:-1])
		else:
			_status = 0x91 # out of boundary
		if msg["replyRequired"]:
			self._sendErrorMessage(msg["bytes"][1], _status)

	def _processMessageReadRequest(self, msg):
		if len(msg["str"]) < 5:
			_str = msg["str"] + self._rawRead(5 - len(msg["str"]))
			msg = self._parseGeneralData(_str)
		msg["remoteInbox"] = msg["bytes"][2]
		msg["localInbox"] = msg["bytes"][3]
		msg["remove"] = bool(msg["bytes"][4])
		if msg["remoteInbox"] > 10:
			msg["remoteInbox"] -= 10
		# assambling response
		_targetBox = self._outputMailboxes[msg["remoteInbox"]]
		_ans = "\x02\x13"
		if _targetBox:
			_msg = _targetBox.pop()
			_ans += "\x00"	# status OK
		else:
			if msg["replyRequired"]:
				_ans += "\x40"	# mailbox is empty
				_msg = ""
			else:
				return
		_ans += chr(msg["localInbox"])
		_len = len(_msg) + 1
		_ans += chr(_len) + _msg + "\x00"
		self._sendIntBtLen(len(_ans))
		self._rawSend(_ans)

	def _sendErrorMessage(self, command, code):
		_msg = "\x02" + chr(command) + chr(code)
		self._sendIntBtLen(len(_msg))
		self._rawSend(_msg)
