import re
import sys
import math

configReProto = r"\*(?P<line>\s*(?P<name>%s)\s*:\s*(?P<value>\S+)\s+?(?P<units>cm)?)"

wheelDiam = re.compile(configReProto % "wheel diameter")
baseLen = re.compile(configReProto % "base length")
multiRe = re.compile(configReProto % "multiplicator")
lMotorRe = re.compile(configReProto % "motor left")
rMotorRe = re.compile(configReProto % "motor right")

configRes = (wheelDiam, baseLen, multiRe, lMotorRe, rMotorRe)

headerProto = """
/*
%(config)s
 */

#ifndef __MOVE_STAT_HEADER_INCLUDED
#define __MOVE_STAT_HEADER_INCLUDED

%(defines)s

#endif
"""

def getDegLen(diam):
    _c = math.pi * diam
    return _c / 360.0

def getHeaderDegLen(diam, multi):
    return multi * getDegLen(diam)

def getIntDegLen(diam, multi):
    return round(getHeaderDegLen(diam, multi))

def recount(fname):
    _text = file(fname, "r").read()
    _configData = {}
    for _re in configRes:
        _match = _re.search(_text)
        if not _match:
            raise Exception("Needed data not found")
        _data = _match.groupdict()
        _configData[_data["name"]] = _data

    _lines = [_el["line"] for _el in _configData.itervalues()]
    _lines = "\n".join(" *" + _l.rstrip() for _l in sorted(_lines))

    _mult = int(_configData["multiplicator"]["value"])
    _lMoto = _configData["motor left"]["value"].upper()
    _rMoto = _configData["motor right"]["value"].upper()
    _defines = [
        "#define WHEEL_DEG_LEN %i" % getIntDegLen(
            float(_configData["wheel diameter"]["value"]), _mult),
        "#define BASE_DEG_LEN %i" % getIntDegLen(
            float(_configData["base length"]["value"]), _mult),
        "#define MOTOR_LEFT OUT_%s" % _lMoto,
        "#define MOTOR_RIGHT OUT_%s" % _rMoto,
        "#define MOTORS_BOTH OUT_" + "".join(sorted((_lMoto, _rMoto))),
        "#define DEG_MULTIPLICATOR %s" % _mult,
    ]

    _defines.sort()

    _text = headerProto % {
        "config": _lines,
        "defines": "\n".join(_defines),
    }
    file(fname, "w").write(_text)

if __name__ == "__main__":
    recount(sys.argv[1])

# vim: set sts=4 sw=4 et :
