class Require(object):
    """Placeholder for required attributes"""

    def __init__(self, doc=""):
        self.__doc__ = doc

class BaseObject(object):

    def __init__(self, **kwargs):
        for (_name, _value) in kwargs.iteritems():
            assert hasattr(self, _name), "Object must have field %s." % _name
            setattr(self, _name, _value)
        if __debug__:
            for _name in dir(self):
                _val = getattr(self, _name)
                if isinstance(_val, Require):
                    raise AttributeError(
                        "Required attribute '%s' not set." % _name)
        self.afterInit()

    def afterInit(self):
        """Method for overriding in child classes"""

# vim: set sts=4 sw=4 et :
