import os
from geneticRisc import breeder

if __name__ == "__main__":
    os.nice(10)
    _br = breeder.Breeder()
    _br.go()

# vim: set sts=4 sw=4 et :
