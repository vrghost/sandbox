from geneticRisc import base

class ProgramMemory(base.BaseObject):
    """Main program memory.

    Structure:
        <zero> -- cell at address 0 is always 0
        <arguments>
        <output cells>
        <free memory>

    """
    startValues = base.Require(
        doc="List of star values (will be placed at start")
    outputCellCount = base.Require(
        doc="Count of cells, that will be counted as output cells")
    size = base.Require(doc="Size of memory")

    _memory = None
    _outputCellsSet = None
    _readedDataSet = None

    def afterInit(self):
        super(ProgramMemory, self).afterInit()
        _start = self.startValues
        _memory = [0]
        _memory.extend(_start)
        _freeCells = self.size - len(_start)
        assert _freeCells > 0
        _memory.extend([0] * _freeCells)
        self._memory = _memory
        _outCells = {}
        _outputStart = len(_start) + 1
        for _pos in xrange(_outputStart, _outputStart + self.outputCellCount):
            _outCells[_pos] = False
        self._outputCellsSet = _outCells
        self._readedDataSet = [
            _el for _el in xrange(1, len(self.startValues)+1)]

    def __getitem__(self, key):
        # i'll allow only non-negative keys
        if key < 0:
            raise AttributeError("negative offset not allowed")
        if key in self._readedDataSet:
            self._readedDataSet.remove(key)
        return self._memory[key]

    def __setitem__(self, key, value):
        if key < 0:
            raise AttributeError("negative offset not allowed")
        if key == 0:
            # do not touch zero cell
            return
        self._memory[key] = value
        if key in self._outputCellsSet:
            self._outputCellsSet[key] = True

    def outputReady(self):
        if self._readedDataSet:
            # Hey! you even haven't used all data!
            return False
        for _value in self._outputCellsSet.itervalues():
            if not _value:
                return False
        else:
            return True

    def getOutput(self):
        """Return output dictionary (<pos>:<value>)"""
        assert self.outputReady(), \
            "Why you want to know output of unfinished program?"
        _out = []
        for _pos in self._outputCellsSet.iterkeys():
            _out.append(self._memory[_pos])
        return _out

    def __repr__(self):
        return "\n".join(self.memory)

# vim: set sts=4 sw=4 et :
