import random

from geneticRisc import base, programMemory, codeElements

def randTrue(prob):
    """Return `True` in 1/`prob` cases"""
    return random.randint(1, prob) == 1

class Program(base.BaseObject):

    memSize = base.Require("Memory size")
    outputCellCnt = base.Require("output cell count")
    args = base.Require("Start arguments")
    maxStepCnt = base.Require("Maximum step count")

    _memory = None
    _code = None
    _currentPtr = 0
    _steps = None
    _forcedFinish = False

    def afterInit(self):
        super(Program, self).afterInit()
        self._memory = programMemory.ProgramMemory(
            size=self.memSize,
            startValues=self.args,
            outputCellCount=self.outputCellCnt
        )

    def setCode(self, code):
        self._code = code

    def codeLen(self):
        return len(self._code)

    def run(self):
        if not self._code:
            return None
        _step = 0
        _maxStep = self.maxStepCnt
        _memory = self._memory
        while _step <= _maxStep and \
            not _memory.outputReady() and \
            not self._forcedFinish:
            self._performStep()
            _step += 1
        if _memory.outputReady():
            self._steps = _step
            return _memory.getOutput()
        else:
            # execution failed
            return None

    def getTotalSteps(self):
        assert self._steps is not None, \
            "None means that program is not finished"
        return self._steps

    def _performStep(self):
        _method = self._code[self._currentPtr]
        self._currentPtr += 1
        _method()

    def alter(self):
        """Randomly alter program code"""
        for _line in self._code:
            # 10% chance to alter line
            if randTrue(5):
                _line.alter()
        if randTrue(7):
            # add line
            self._addRandLine()
        if randTrue(7):
            # remove line
            self._delRandLine()

    def genChild(self, args, alter=True):
        """Generate child (altered copy of us)"""
        _prog = self.copy(args)
        if alter:
            _prog.alter()
        return _prog

    def copy(self, args):
        """Create duplicate of self (except of `args`)"""
        _cls = self.__class__
        _prog = _cls(
            args=args,
            memSize=self.memSize,
            outputCellCnt=self.outputCellCnt,
            maxStepCnt=self.maxStepCnt
        )
        _copyCode = []
        for _st in self._code:
            _copyCode.append(_st.copy(_prog))
        _prog.setCode(_copyCode)
        return _prog

    def _delRandLine(self):
        if self._code:
            _pos = random.randint(0, len(self._code)-1)
            del self._code[_pos]

    def _addRandLine(self):
        _pos = random.randint(0, len(self._code)) # from 0 to end-of-code
        _elPos = random.randint(0, len(codeElements.elements) - 1)
        _obj = codeElements.elements[_elPos]
        self._code.insert(_pos, _obj.fromRandom(self, len(self._code)))


    def __repr__(self):
        return "\n".join((str(_el) for _el in self._code))

    @classmethod
    def withRandCode(cls, **kwargs):
        _prog = cls(**kwargs)
        _code = []
        for _iter in xrange(random.randint(1, 7)):
            _elPos = random.randint(0, len(codeElements.elements) - 1)
            _obj = codeElements.elements[_elPos]
            _code.append(_obj.fromRandom(_prog, 100))
        _prog.setCode(_code)
        return _prog

    ### program elements callbacks
    def getmem(self, pos):
        return self._memory[pos]

    def setmem(self, pos, value):
        self._memory[pos] = value

    def getPos(self):
        return self._currentPtr

    def goto(self, pos):
        self._currentPtr = pos

    def alterPos(self, delta):
        self._currentPtr += delta

### debug

if __name__ == "__main__":
    _pr = Program(
        memSize=1000, outputCellCnt=1, args=(-32,42), maxStepCnt=1000)
    _code = [
        codeElements.Add(arg1=3, arg2=2, arg3=4, program=_pr),
    ] # ans = -32 + 42
    _pr.setCode(_code)
    _pr.alter()
    #print _pr.run()
    print _pr

# vim: set sts=4 sw=4 et :
