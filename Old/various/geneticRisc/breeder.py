import random

from geneticRisc import base, program

class Breeder(base.BaseObject):
    """Breeder"""

    maxSteps = 100
    maxMem = 20
    memCnt = 1
    _childrenCnt = 30

    def func(self, a, b):
        return a / b

    def getDelta(self, args, outcome):
        return abs(self.func(*args) - outcome[0])

    def _calcBest(self, children, args):
        _out = None
        _maxRating = 0xFFFFFF
        _minLen = 0xFFFFFF
        for (_child, _ans) in children:
            _rating = self.getDelta(args, _ans)*100 - _child.codeLen()
            # lower rating is better
            if _rating < _maxRating:
                _out = _child
                _maxRating = _rating
            elif _rating == _maxRating and _minLen > _child.codeLen() \
            and random.randint(1, 5) == 3:
                _out = _child
                _minLen = _child.codeLen()
        return _out

    def _breed(self, program, args):
        """Breed & return best program"""
        _children = [
            program.genChild(args)
            for _tmp in xrange(self._childrenCnt)
        ]
        _children.append(program.genChild(args, False))
        _goodChildren = []
        _badChildren = []
        for _child in _children:
            try:
                _ans = _child.run()
            except:
                # silently discsard element
                pass
            else:
                if _ans is None:
                    _badChildren.append(_child)
                else:
                    _goodChildren.append((_child, _ans))
        if not _goodChildren:
            # if no good children, I'll choose longest bad child
            _max = -1
            _out = None
            for _child in _badChildren:
                if _child.codeLen() > _max:
                    _max = _child.codeLen()
                    _out = _child
            if not _out:
                print "Oh my... It's bad"
                return self.createProgram()
        else:
            # there are good children!
            _out = self._calcBest(_goodChildren, args)
            ## ...but eventually weak can breed too...
            #if random.randint(1, 50) == 42:
            #    _pos = random.randint(0, len(_goodChildren)-1)
            #    (_out, _ans) = _goodChildren[_pos]
        return _out

    def go(self):
        _prog = self.createProgram()
        _i = 0
        while True:
            _prog = self._breed(_prog, self.genArgs())
            if _i >= 100:
                print _prog
                print "--" * 20
                _i = 0
            _i += 1

    def createProgram(self):
        """Create random program"""
        return program.Program.withRandCode(
            memSize = self.maxMem,
            outputCellCnt = self.memCnt,
            args = self.genArgs(),
            maxStepCnt = self.maxSteps
        )


    def genArgs(self):
        """Return random program arguments"""
        _a = random.randint(-10000, 10000)
        _b = 0
        while _b == 0:
            _b = random.randint(-10000, 10000)
        return (_a, _b)

### debug

if __name__ == "__main__":
    _br = Breeder()
    print _br.go()

# vim: set sts=4 sw=4 et :
