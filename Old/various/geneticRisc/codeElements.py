import random

from geneticRisc import base

class _BaseEl(base.BaseObject):
    """Base code element"""
    program = base.Require(doc="program object")

    arg1 = base.Require()
    arg2 = base.Require()
    arg3 = base.Require()

    def copy(self, program):
        """Copy element (it will be bound to provided `program`)"""
        _cls = self.__class__
        _obj = _cls(
            program=program, arg1=self.arg1, arg2=self.arg2, arg3=self.arg3)
        if random.randint(1, 30) == 22:
            # copy error :)
            _obj.alter()
        return _obj

    @classmethod
    def fromRandom(cls, program, maxVal):
        return cls(
            program=program,
            arg1=random.randint(0, maxVal),
            arg2=random.randint(0, maxVal),
            arg3=random.randint(0, maxVal),
        )

    def correct(self):
        raise NotImplementedError()

    def alter(self):
        self.arg1 += random.randint(-10, 10)
        self.arg2 += random.randint(-10, 10)
        self.arg3 += random.randint(-10, 10)
        self.correct()

class Add(_BaseEl):

    def __call__(self):
        self.program.setmem(self.arg1,
            self.program.getmem(self.arg2) + self.program.getmem(self.arg3))

    def correct(self):
        if self.arg1 < 0:
            self.arg1 = 0
        if self.arg2 < 0:
            self.arg2 = 0
        if self.arg3 < 0:
            self.arg3 = 0

    def __repr__(self):
        return "mem[%i] = mem[%i] + mem[%i]" % \
            (self.arg1, self.arg2, self.arg3)

class Addi(_BaseEl):

    def __call__(self):
        self.program.setmem(self.arg1,
            self.program.getmem(self.arg2) + self.arg3)

    def __repr__(self):
        return "mem[%i] = mem[%i] + %i" % \
            (self.arg1, self.arg2, self.arg3)

    def correct(self):
        if self.arg1 < 0:
            self.arg1 = 0
        if self.arg2 < 0:
            self.arg2 = 0

class Nand(_BaseEl):

    def __call__(self):
        _val1 = self.program.getmem(self.arg2)
        _val2 = self.program.getmem(self.arg3)
        self.program.setmem(self.arg1, ~(_val1 & _val2))

    def __repr__(self):
        return "mem[%i] = nand(mem[%i], mem[%i])" % \
            (self.arg1, self.arg2, self.arg3)

    def correct(self):
        if self.arg1 < 0:
            self.arg1 = 0
        if self.arg2 < 0:
            self.arg2 = 0
        if self.arg3 < 0:
            self.arg3 = 0

class Beq(_BaseEl):

    def __call__(self):
        if self.program.getmem(self.arg1) == self.program.getmem(self.arg2):
            self.program.alterPos(self.program.getmem(self.arg3))

    def __repr__(self):
        return "if mem[%i] == mem[%i]: goto_rel(%i)" % \
            (self.arg1, self.arg2, self.arg3)

    def correct(self):
        if self.arg1 < 0:
            self.arg1 = 0
        if self.arg2 < 0:
            self.arg2 = 0

    @classmethod
    def fromRandom(cls, program, maxVal):
        # it's pointless to compare same memory positions
        _arg2=random.randint(0, maxVal)
        _arg3=random.randint(0, maxVal)
        while _arg2 == _arg3:
            _arg3=random.randint(0, maxVal)
        return cls(
            program=program,
            arg1=random.randint(0, maxVal),
            arg2=_arg2,
            arg3=_arg3
        )

class Jalr(_BaseEl):
    arg3 = None # 3rd argument not needed

    def __call__(self):
        self.program.setmem(self.arg1, self.program.getPos())
        self.program.goto(self.program.getmem(self.arg2))

    def __repr__(self):
        return "mem[%i] = <cur_pos>; goto %i" % (self.arg1, self.arg2)

    def alter(self):
        self.arg1 += random.randint(-10, 10)
        self.arg2 += random.randint(-10, 10)
        self.correct()

    def correct(self):
        if self.arg1 < 0:
            self.arg1 = 0
        if self.arg2 < 0:
            self.arg2 = 0

elements = (Add, Addi, Nand, Beq, Jalr)

# vim: set sts=4 sw=4 et :
