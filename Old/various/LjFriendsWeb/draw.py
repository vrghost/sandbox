import time
import itertools
import pickle
import os
from networkx import *
from pylab import *

def others_friends(dict, user):
    _cpy = dict.copy()
    del _cpy[user]
    for _line in _cpy.values():
        for _it in _line:
            yield _it

def leave_cycles(data):
    _altered = True
    _cur = data.copy()
    while _altered:
        _keys = _cur.keys()
        _vals = [_val for _val in itertools.chain(*_cur.values())]
        _oldCur = _cur
        _cur = {}
        for _key in tuple(set(_keys).intersection(_vals)) + ("_vr_ghost", ):
            _cur[_key] = _oldCur[_key]
        _altered = len(_cur.keys()) != len(_oldCur.keys())
    return _cur

_dir = "friends"

def buildGraph(user="_vr_ghost"):
    graph = XGraph()
    _file = file("finally.txt")
    _data = pickle.load(_file)
    _real = leave_cycles(_data)
    #_users = _real.keys()
    _users = set(["_vr_ghost"] + _real["_vr_ghost"])
    #_iterator = _users.copy()
    #for _u in _iterator:
    #    if _u != "_vr_ghost" and _u in _real:
    #for _us in _real[_u]:
    #            _users.add(_us)
    _filtered = {}
    for _user in _users:
        if _user not in _real:
            continue
        for _friend in _real[_user]:
            if _friend in _users and _friend in others_friends(_real, _user):
                if _user not in _filtered:
                    _filtered[_user] = []
                _filtered[_user].append(_friend)
    for (_user, _friends) in _filtered.iteritems():
        for _friend in _friends:
                graph.add_edge(_user, _friend)
    _f = file("graph.pcl", "w")
    pickle.dump(graph, _f)
    return graph

if __name__ == "__main__":
    G = buildGraph()
    print "~" * 20, help(spring_layout)
    figure(figsize=(10,10))
    pos = spring_layout(G, node_pos=spectral_layout(G), iterations=60)
    draw_networkx_nodes(G,pos,node_size=60)
    draw_networkx_edges(G,pos, width=1)
    draw_networkx_labels(G,pos,font_style="bold", font_size=12,font_family='serif', font_color=(0.5,0.7,0.7))
    savefig("gr.png")
    show()
    print "end"


# vim: set sts=4 sw=4 et :
