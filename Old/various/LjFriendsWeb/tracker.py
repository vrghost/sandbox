from urllib import urlopen
import re
from pickle import dump
import time

class Tracker(object):

    _wwwPath = r"http://%s.livejournal.com/profile"
    _intMark = r'Friends</a>:'
    _re = re.compile(r"http://[^.]+.livejournal.com/profile")

    def _getPageOf(self, uname):
        _path = self._wwwPath % uname
        print repr(_path)
        while True:
            try:
                _file = urlopen(_path)
                _out = _file.read()
                break
            except:
                print "error, retry"
                time.sleep(1)
                pass
        _file.close()
        return _out

    def _getInterestingPiece(self, uname):
        _data = self._getPageOf(uname)
        for _line in _data.splitlines():
            if self._intMark in _line:
                return _line

    def _getFriends(self, uname):
        _piece = self._getInterestingPiece(uname)
        _out = []
        try:
            for _line in self._re.findall(_piece):
                _part = _line.split(".livejournal.", 1)[0]
                _out.append(_part.split("http://", 1)[1])
        except:
            print "error processing %s, skipping" % uname
            return []
        return _out

class Processor(object):

    _userStack = None
    _oldList = None
    tracker = None
    _outF = None
    left = 0

    def __init__(self, startU):
        self._userStack = [startU]
        self._oldList = []
        self.tracker = Tracker()
        file("track.txt", "wb").write("")

    def run(self):
        _checkpoint = len(self._userStack)
        _iter = 0
        self.left = 2
        while self.left > 0:
            if _iter >= _checkpoint:
                self.checkpoint()
                _checkpoint += len(self._userStack)
            _user = self._userStack.pop(0)
            _friends = self.tracker._getFriends(_user)
            self._oldList.append(_user)
            self.addToHub(_user, _friends)
            self.extendStack(_friends)
            _iter += 1

    def checkpoint(self):
        print "CHECKPOINT"
        self.left -= 1

    def addToHub(self, user, friends):
        _data = "%s: %s\n" % (repr(user), friends)
        _f = file(r"friends\%s.txt" % user, "w")
        _f.write(_data)

    def extendStack(self, friends):
        for _fr in friends:
            if _fr not in self._oldList and _fr not in self._userStack:
                self._userStack.append(_fr)


###  debug

if __name__ == "__main__":
    _o = Processor("_vr_ghost")
    print _o.run()

# vim: set sts=4 sw=4 et :
