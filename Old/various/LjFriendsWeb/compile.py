import os
import pickle
import glob

_dir = "friends"

def buildMain(user="_vr_ghost"):
    _stack = [user]
    _owerall = {}
    _files = glob.glob(os.path.join(_dir, "*.txt"))
    _files.sort()
    _foundUsers = [_f[8:-4] for _f in _files]
    _seen = []
    while _stack:
        _user = _stack.pop(0)
        _fname = "%s.txt" % os.path.join(_dir, _user)
        _data = file(_fname, "r").read()
        _dict = eval("{%s}" % _data)
        _owerall.update(_dict)
        _seen.append(_user)
        _stack.extend([_u for _u in _dict[_user] if _u in _foundUsers and _u not in _seen])
        print len(_stack)
    _file = file("finally.txt", "w")
    pickle.dump(_owerall, _file)

if __name__ == "__main__":
    buildMain()


# vim: set sts=4 sw=4 et :
