from weakref import ref
from pickle import dump

class FreindHub(object):

    list = None

    def __init__(self, startN):
        self.list = {
            startN: [],
        }

    def addFriend(self, whom, who):
        if who not in self.list:
            self.list[who] = []
        self.list[whom].append(who)

    def pickle(self):
        print "dumping..."
        _file = file("%s_dump.pic" % self.__class__.__name__, "w")
        dump(self.list, _file)
        _file.close()
        print "dump finished..."

# vim: set sts=4 sw=4 et :
