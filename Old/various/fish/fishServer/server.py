import SocketServer

import handler

PORT = 80

def run():
    httpd = SocketServer.TCPServer(("", PORT), handler.FishHandler)
    print "serving at port", PORT
    httpd.serve_forever()

if __name__ == "__main__":
    run()

# vim: set et sts=4 sw=4 :
