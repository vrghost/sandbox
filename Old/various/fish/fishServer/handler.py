import libxml2
import libxslt
import BaseHTTPServer

class FishHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def _getHandler(self, type, id):
        def _handler(fobj):
            return self.getObjectPage(self._root, type, id)
        return _handler

    def getPathHandlers(self):
        _out = {
            "/": self.handleRoot,
            "/fishes": self.handleFishes,
        }
        for (_id, _handle) in self._getFishHandlers().iteritems():
            _path = "/".join(("", "fishes", _id))
            _out[_path] = _handle
        return _out

    def handleFishes(self, fobj):
        fobj.write("<html><head><title>Root.</title></head>" \
            "<body>")
        _k = [int(_key) for _key in self._getFishHandlers().keys()]
        _k.sort()
        for _id in _k:
            fobj.write(
                "<a href=/fishes/%(id)s>browse fish %(id)s.</a><br/>" % {
                    "id": _id,
            })
        fobj.write("</body>")

    def getObjectPage(self, root, type, id):
        _root = os.path.join(root, type)
        _xsl = os.path.join(_root, "transform.xsl")
        _xml = os.path.join(_root, "%s.xml" % id)
        _styleDoc = libxml2.parseFile(_xsl)
        _style = libxslt.parseStylesheetDoc(_styleDoc)
        _doc = libxml2.parseFile(_xml)
        _result = _style.applyStylesheet(_doc, None)
        print dir(_result)
        _style.freeStylesheet()
        _doc.freeDoc()
        _result.freeDoc()

    def handleRoot(self, fobj):
        fobj.write(
            "<html><head><title>Root.</title></head>" \
            "<body>" \
            "<a href=/fishes>browse fishes.</a><br />" \
            "<a href=/plants>browse plants.</a><br />" \
            "</body>"
        )

    # `do` methods

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.getPathHandlers()[self.path](self.wfile)

# vim: set et sts=4 sw=4 :
