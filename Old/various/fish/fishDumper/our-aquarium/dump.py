import urllib
import re
import os

import elementtree.ElementTree as ET

class Fish(object):

    id = None
    _urlRoot = "http://our-aquarium.ru/sprav/data/"
    _html = "%s.php"
    _remove = (
        re.compile("\r"),
        re.compile("</?b>"),
    )

    def __init__(self, id):
        self.id = id
        _url = self._urlRoot + (self._html % self.id)
        _text = urllib.urlopen(_url).read()
        _text = _text.decode("cp1251").encode("utf-8")
        file("got.html", "w").write(_text)
        for _re in self._remove:
            _match = _re.search(_text)
            while _match:
                _text = _text[:_match.start()] + _text[_match.end():]
                _match = _re.search(_text)
        _from = _text.find("../photo")
        _to = _text.find("</center>", _from)
        _text = "<img src=\"" + _text[_from:_to]
        file("stripped.html", "w").write(_text)
        self.parse(_text.decode("utf-8"))

    def parse(self, text):
        _part = [_el.strip() for _el in text.split("<br>") if _el.strip()]
        _imgL = _part[0]
        _imgStart = _imgL.find("src=\"") + len("src=\"")
        _imgEnd = _imgL.find('"', _imgStart)
        _imgUrl = self._urlRoot + _imgL[_imgStart:_imgEnd]
        assert _imgUrl.lower().endswith("jpg")
        self.image = urllib.urlopen(_imgUrl).read()
        self.commonName = _part[1]
        self.latinName = _part[2]
        self.sumusRu = _part[3]
        self.sumusLat = _part[4]
        self.origin = _part[5][_part[5].find(":")+1:].strip()
        _params = _part[6]
        _params = _params[_params.find(":")+1:].strip()
        _parts = []
        _t = _params.lower().find("t")
        _ph = _params.lower().find("ph")
        _dh = _params.lower().find("dh")
        _parts.append(_params[_t:_ph].lower().strip("tph ,."))
        _parts.append(_params[_ph:_dh].lower().strip("phd ,."))
        _parts.append(_params[_dh:].lower().strip("dh ,."))
        print _parts
        _vals = dict(zip(("t", "pH", "dH"), [_p.strip(" ,.") for _p in _parts]))
        for _key in _vals.keys():
            _val = _vals[_key].replace(",", ".").split("-")
            if len(_val) < 2:
                assert len(_val) == 1
                _val.append(_val[0])
            _vals[_key] = _val
        (self.maxTemp, self.lowTemp) = _vals["t"]
        (self.maxPh, self.lowPh) = _vals["pH"]
        (self.maxDh, self.lowDh) = _vals["dH"]
        # size
        _digits = []
        _digit = ""
        for _ch in _part[7]:
            if _ch.isdigit():
                _digit += _ch
            elif _digit:
                _digits.append(_digit)
                _digit = ""
        if _digit:
            _digits.append(_digit)
        _digits = [float(_d) for _d in _digits]
        _digits.sort()
        self.maxSize = _digits[-1]
        self.place = _part[8]

    def write(self, root):
        _imgP = os.path.join(root, "img", "%s.jpg" % self.id)
        file(_imgP, "wb").write(self.image)
        _root = ET.Element("fish", id=str(self.id))
        _name = ET.SubElement(_root, "name")
        ET.SubElement(_name, "common").text = self.commonName
        ET.SubElement(_name, "latin").text = self.latinName
        _sum = ET.SubElement(_root, "sumus")
        ET.SubElement(_sum, "ru").text = self.sumusRu
        ET.SubElement(_sum, "lat").text = self.sumusLat
        ET.SubElement(_root, "origin").text = self.origin
        _tmp = ET.SubElement(_root, "temperature")
        ET.SubElement(_tmp, "min").text = self.lowTemp
        ET.SubElement(_tmp, "max").text = self.maxTemp
        _tmp = ET.SubElement(_root, "ph")
        ET.SubElement(_tmp, "min").text = self.lowPh
        ET.SubElement(_tmp, "max").text = self.maxPh
        _tmp = ET.SubElement(_root, "gDh")
        ET.SubElement(_tmp, "min").text = self.lowDh
        ET.SubElement(_tmp, "max").text = self.maxDh
        ET.ElementTree(_root).write(os.path.join(root, "%s.xml" % self.id))

class Plant(object):

    _urlRoot = "http://our-aquarium.ru/content/enc/blag/rast/"
    _html = "e%02i.php"
    _remove = (
        re.compile("\r"),
        re.compile("</?(b|B|i|I)>"),
        re.compile("<P>"),
    )

    def __init__(self, id):
        self.id = id
        _url = self._urlRoot + (self._html % self.id)
        _text = urllib.urlopen(_url).read()
        _text = _text.decode("cp1251").encode("utf-8")
        file("got.html", "w").write(_text)
        for _re in self._remove:
            _match = _re.search(_text)
            while _match:
                _text = _text[:_match.start()] + _text[_match.end():]
                _match = _re.search(_text)
        _from = _text.find("=\"e")
        _to = _text.lower().find("</center>", _from)
        _to = _text.lower().find("</p>", _to)
        _text = "<img src" + _text[_from:_to]
        _text = _text.replace("</center>", "")
        file("stripped.html", "w").write(_text)
        self.parse(_text.decode("utf-8"))

    def parse(self, text):
        _parts = [_p.strip() for _p in text.split("</P>")]
        _imgL = _parts[0]
        _imgStart = _imgL.find("src=\"") + len("src=\"")
        _imgEnd = _imgL.find('"', _imgStart)
        _imgUrl = self._urlRoot + _imgL[_imgStart:_imgEnd]
        assert _imgUrl.lower().endswith("jpg")
        self.image = urllib.urlopen(_imgUrl).read()
        self.locName = _parts[1]
        self.latName = _parts[2]
        self.desc = _parts[3]

    def write(self, root):
        _imgP = os.path.join(root, "img", "%s.jpg" % self.id)
        file(_imgP, "wb").write(self.image)
        _root = ET.Element("plant", id=str(self.id))
        _name = ET.SubElement(_root, "name")
        ET.SubElement(_name, "common").text = self.locName
        ET.SubElement(_name, "latin").text = self.latName
        ET.SubElement(_root, "desc").text = self.desc
        ET.ElementTree(_root).write(os.path.join(root, "%s.xml" % self.id))

if __name__ == "__main__":
    for _id in xrange(1, 100):
        print _id
        if os.path.exists(os.path.join("plants", "%s.xml" % _id)):
            continue
        _f = Plant(_id)
        _f.write("plants")

# vim: set sts=4 sw=4 et :
