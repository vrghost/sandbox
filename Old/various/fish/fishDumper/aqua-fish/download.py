import urllib
import os
from collections import defaultdict
import re

import xml.etree.ElementTree as ET


def HTML(text):
    parser = ET.XMLTreeBuilder(html=True)
    parser.feed(text)
    return parser.close()

class CommonAncsestor(object):

    id = None
    related = None
    image = None
    _dropStrings = (
        "&copy;",
        "xmlns=\"http://www.w3.org/1999/xhtml\"",
        "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"",
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" >",
    )
    _re = re.compile("alt=[^\"]")
    _dropRe = (
        re.compile("<meta[^>]*>"),
    )
    _retUrl = None
    _relatedMark = None

    def __init__(self, id):
        _text = urllib.urlopen(self._retUrl % id).read().decode("cp1250")
        self.id = id
        file("dump.xml", "w").write(_text.encode("utf-8"))
        self.parse(_text.encode("utf-8"))

    def parse(self, text):
        for _txt in self._dropStrings:
            text = text.replace(_txt, "")
        for _re in self._dropRe:
            _match = _re.search(text)
            while _match:
                _start = text[:_match.start()]
                _end = text[_match.end():]
                text = _start + _end
                _match = _re.search(text)
        _match = self._re.search(text)
        while _match:
            _start = text[:_match.start()]
            _end = text[_match.end():]
            text = _start + "alt=\"" + _end
            _match = self._re.search(text)
        file("dump2.xml", "w").write(text)
        _tree = ET.XML(text)
        _classes = self._classDict(_tree)
        file('text.txt', 'w').write(str(_classes))
        self.obtainData(_classes, _tree)

    def obtainData(self, classes, tree):
        raise NotImplementedError

    def _getImageUrl(self, element):
        if element.tag.lower() == 'div' and element.attrib["class"] == 'ext_show_image':
            for _el in element:
                if _el.tag.lower() ==  "img":
                    _path = _el.attrib["src"]
                    return "http://www.aqua-fish.net/" + _path
        else:
            for _el in element:
                _url = self._getImageUrl(_el)
                if _url:
                    return _url

    def _getImage(self, tree):
        _url = self._getImageUrl(tree)
        return urllib.urlopen(_url).read()


    def parseRange(self, str):
        str = str.strip()
        str = str.split("(", 1)[0]
        _parts = [_t.strip() for _t in str.split("-") if _t.strip()]
        _low = _parts[0]
        _high = _parts[1].split(" ", 1)[0]
        return {"low": _low, "high": _high}

    def _classDict(self, element):
        _out = defaultdict(lambda: [])
        if "class" in element.attrib:
            _key = element.attrib["class"]
            _text = self._getFullText(element)
            _out[_key].append(_text)
        for _el in element:
            _d = self._classDict(_el)
            for (_key, _value) in _d.iteritems():
                _out[_key].extend(_value)
        return _out

    def _getFullText(self, el):
        _text = el.text or ""
        for _ch in el:
            _text += (self._getFullText(_ch) or "")
            _text += (_ch.tail or "")
        return _text

    def _findRelated(self, element):
        _out = []
        if element.tag.lower() == "a":
            _href = element.attrib["href"]
            if self._relatedMark in _href:
                _href = _href.split("?", 1)[1]
                _parts = _href.split("&")
                _parts = dict([_p.split("=", 1) for _p in _parts])
                _out.append(_parts["id"])
        for _el in element:
            _out.extend(self._findRelated(_el))
        return _out


    def _xmlRange(self, name, range):
        _main = ET.Element(name)
        ET.SubElement(_main, "low").text = range["low"]
        ET.SubElement(_main, "high").text = range["high"]
        return _main

class Fish(CommonAncsestor):

    commonName = None
    latinName = None
    size = None
    ph = None
    dGH = None
    temperature = None
    famTemperament = None
    othTemperament = None
    place = None
    breeding = None
    origin = None
    shortDesc = None
    _retUrl = "http://www.aqua-fish.net/show.php?what=fish&cur_lang=2&id=%s"
    _relatedMark = "what=fish"

    def __str__(self):
        return "<%s:\n\
        id: %s\n\
        latin name: %s\n\
        common name: %s\n\
        size: %s\n\
        ph: %s\n\
        gh: %s\n\
        temp: %s\n\
        fam: %s\n\
        oth: %s\n\
        place: %s\n\
        breeding: %s\n\
        origin: %s\n\
        desc: %s\n\
        related: %s\n>" % (
            self.__class__.__name__,
            self.id,
            self.commonName,
            self.latinName,
            self.size,
            self.ph,
            self.dGH,
            self.temperature,
            self.famTemperament,
            self.othTemperament,
            self.place,
            self.breeding,
            self.origin,
            self.shortDesc,
            self.related,
        )

    def write(self, dir):
        _root = ET.Element("fish", id=str(self.id))
        _name = ET.SubElement(_root, "name")
        ET.SubElement(_name, "common").text = self.commonName
        ET.SubElement(_name, "latin").text = self.latinName
        _root.append(self._xmlRange("size", self.size))
        _root.append(self._xmlRange("ph", self.ph))
        _root.append(self._xmlRange("dGh", self.dGH))
        _root.append(self._xmlRange("temperature", self.temperature))
        _temper = ET.SubElement(_root, "temperament")
        ET.SubElement(_temper, "family").text = self.famTemperament
        ET.SubElement(_temper, "others").text = self.othTemperament
        ET.SubElement(_root, "place").text = self.place
        ET.SubElement(_root, "breeding").text = self.breeding
        ET.SubElement(_root, "origin").text = self.origin
        ET.SubElement(_root, "desc").text = self.shortDesc
        _rel = ET.SubElement(_root, "related")
        for _id in self.related:
            ET.SubElement(_rel, "id").text = _id
        ET.ElementTree(_root).write(os.path.join(dir, "%s.xml" % self.id), "utf-8")
        file(os.path.join(dir, "img", "%i.jpg" % self.id), "wb").write(self.image)
    def obtainData(self, classes, tree):
        self.commonName = classes["ext_popis"][0]
        self.latinName = classes["ext_popis"][1]
        self.size = self.parseRange(classes["ext_popis"][2])
        self.ph = self.parseRange(classes["ext_popis"][3])
        self.dGH = self.parseRange(classes["ext_popis"][4].strip("dGH"))
        self.temperature = self.parseRange(classes["ext_popis"][5])
        self.famTemperament = classes["ext_popis"][6]
        self.othTemperament = classes["ext_popis"][7]
        self.place = classes["ext_popis"][8]
        self.breeding = classes["ext_popis"][9]
        self.origin = classes["ext_popis"][10]
        self.shortDesc = classes["ext_popis"][11]
        self.related = self._findRelated(tree)
        self.image = self._getImage(tree)


class Plant(CommonAncsestor):

    name = None
    size = None
    ph = None
    dh = None
    temp = None
    subst = None
    light = None
    place = None
    prop = None
    origin = None
    growth = None
    desc = None
    related = None

    _retUrl = "http://www.aqua-fish.net/show.php?what=plant&cur_lang=2&id=%s"
    _relatedMark = "what=plant"

    def __str__(self):
        return "<%s:\n\
        id: %s\n\
        name: %s\n\
        size: %s\n\
        ph: %s\n\
        gh: %s\n\
        temp: %s\n\
        subst: %s\n\
        light: %s\n\
        place: %s\n\
        prop: %s\n\
        origin: %s\n\
        growth: %s\n\
        desc: %s\n\
        related: %s\n>" % (
            self.__class__.__name__,
            self.id,
            self.name,
            self.size,
            self.ph,
            self.dh,
            self.temp,
            self.subst,
            self.light,
            self.place,
            self.prop,
            self.origin,
            self.growth,
            self.desc,
            self.related,
        )

    def obtainData(self, classes, tree):
        self.name = classes["ext_popis"][0]
        self.size =  self.parseRange(classes["ext_popis"][1])
        self.ph =  self.parseRange(classes["ext_popis"][2])
        self.dh =  self.parseRange(classes["ext_popis"][3].strip("dGH"))
        self.temp =  self.parseRange(classes["ext_popis"][4])
        self.subst = classes["ext_popis"][5]
        self.light = classes["ext_popis"][6]
        self.place = classes["ext_popis"][7]
        self.prop = classes["ext_popis"][8]
        self.origin = classes["ext_popis"][9]
        self.growth = classes["ext_popis"][10]
        self.desc = classes["ext_popis"][11]
        self.related = self._findRelated(tree)
        self.image = self._getImage(tree)

    def write(self, dir):
        _root = ET.Element("plant", id=str(self.id))
        _name = ET.SubElement(_root, "name")
        ET.SubElement(_name, "latin").text = self.name
        _root.append(self._xmlRange("size", self.size))
        _root.append(self._xmlRange("ph", self.ph))
        _root.append(self._xmlRange("dGh", self.dh))
        _root.append(self._xmlRange("temperature", self.temp))
        ET.SubElement(_root, "substrate").text = self.subst
        ET.SubElement(_root, "light").text = self.light
        ET.SubElement(_root, "place").text = self.place
        ET.SubElement(_root, "propagation").text = self.prop
        ET.SubElement(_root, "origin").text = self.origin
        ET.SubElement(_root, "growth").text = self.growth
        ET.SubElement(_root, "desc").text = self.desc
        _rel = ET.SubElement(_root, "related")
        for _id in self.related:
            ET.SubElement(_rel, "id").text = _id
        ET.ElementTree(_root).write(os.path.join(dir, "%s.xml" % self.id), "utf-8")
        file(os.path.join(dir, "img", "%i.jpg" % self.id), "wb").write(self.image)

def getFish():
    for _id in xrange(1, 1000):
        print _id
        #if os.path.exists(os.path.join("fishes", "%s.xml" % _id)):
        #    print "skipping."
        #    continue
        try:
            _f = Fish(_id)
        except IndexError, _err:
            print "[ERROR] retreiving %s: %s. skipping" % (_id, _err)
            continue
        _f.write(os.path.join("orig", "fishes"))

def getPlants():
    for _id in xrange(1, 1000):
        print _id
        if os.path.exists(os.path.join("plants", "%s.xml" % _id)):
            print "skipping."
            continue
        try:
            _p = Plant(_id)
        except IndexError, _err:
            print "[ERROR] retreiving %s: %s. skipping" % (_id, _err)
            continue
        _p.write(os.path.join("orig", "plants"))

if __name__ == "__main__":
    getFish()
    #getPlants()


# vim: set et sts=4 sw=4 :
