import os
import glob
import fish

def exportFish(obj):
    def _addRange(src, trg):
        _min = src.low[0].text
        _max = src.high[0].text
        _mn = XmlObject()
        _mn.text = _min
        _mx = XmlObject()
        _mx.text = _max
        trg.max = [_mx]
        trg.min = [_mn]
    _out = XmlObject()
    _out.tag = "object"
    _out.type = "fish"
    _hard = XmlObject()
    _hard.units = "dGH"
    _addRange(obj.dGh[0], _hard)
    _temp = XmlObject()
    _temp.units = "C"
    _addRange(obj.temperature[0], _temp)
    _ac = XmlObject()
    _ac.units = "pH"
    _addRange(obj.ph[0], _ac)
    _wather = XmlObject()
    _wather.temperature = [_temp]
    _wather.hardness = [_hard]
    _wather.acidity = [_ac]
    _env = XmlObject()
    _env.wather = [_wather]
    _out.environment = [_env]
    # names
    _names = XmlObject()
    _gr1 = XmlObject()
    _gr1.lang = "LAT"
    _txt = XmlObject()
    _txt.text = obj.name[0].latin[0].text
    _gr1.text = [_txt]
    _gr2 = XmlObject()
    _gr2.lang = "EN"
    _txt = XmlObject()
    _txt.text = obj.name[0].common[0].text
    _gr2.text = [_txt]
    _names.group = [_gr1, _gr2]
    _out.name = [_names]
    _char = XmlObject()
    _out.character = [_char]
    _gr = XmlObject()
    _char.group = [_gr]
    _gr.lang = "EN"
    _l = XmlObject()
    _l.text = obj.origin[0].text
    _gr.origin = [_l]
    _br = XmlObject()
    _gr.breeding = [_br]
    _t = XmlObject()
    _br.type = [_t]
    _t.text = obj.breeding[0].text
    _gr.origin = [_l]
    _pl = XmlObject()
    _pl.text = obj.place[0].text
    _gr.place = [_pl]
    _temp = XmlObject()
    _gr.temperament = [_temp]
    _toS = XmlObject()
    _toO = XmlObject()
    _toS.type = "own"
    _toO.type = "others"
    _toS.text = obj.temperament[0].family[0].text
    _toO.text = obj.temperament[0].others[0].text
    _temp.to = [_toS, _toO]
    _i = XmlObject()
    _out.images = [_i]
    _img = XmlObject()
    _img.alt = "Photo of a %s" % obj.name[0].latin[0].text
    _img.src = "%s.jpg" % obj.id
    _i.img = [_img]
    _dsc = XmlObject()
    _out.description = [_dsc]
    if obj.desc[0].text and obj.desc[0].text != "N/A":
        _gr = XmlObject()
        _gr.lang = "EN"
        _dsc.group = [_gr]
        _t = XmlObject()
        _gr.topic = [_t]
        _t.text = obj.desc[0].text
    return _out

if __name__ == "__main__":
    database.setRoot(r"F:\hoopoe\various\trunc\fish\resources")
    _root = os.path.split(__file__)[0]
    _fishes = os.path.join(_root, "orig", "fishes")
    for _file in glob.glob(os.path.join(_fishes, "*.xml")):
        _obj = exportFish(XmlObject.fromFile(_file))
        def _call(_o):
            for _gr in _o.name.group:
                if _gr.lang == "LAT":
                    print _gr.text
                    _text = _gr.text
                    break
            print _text
            raise "a"
        database.getObjects(_call)
        database.addObject(_obj)
    database.saveAll()

# vim: set et sts=4 sw=4 :
