import os

import fishDB as database

databasePath = os.path.split(__file__)[0]
databasePath = os.path.join(databasePath, "resources", "database.lite")

database.initSqlite(databasePath)


# vim: set et sts=4 sw=4 :
