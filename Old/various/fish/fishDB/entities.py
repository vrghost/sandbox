from sqlobject.sqlbuilder import AND

import tables

class _DBList(tuple):

    def __init__(self, *args, **kwargs):
        self._callable = kwargs.copy().pop("callable")
        super(_DBList, self).__init__(*args, **kwargs)

    def append(self, obj):
        self._callable(obj)

    def extend(self, lst):
        for _item in lst:
            self.append(_item)

class _ObjDict(dict):

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        self[name] = value


# vim: set et sts=4 sw=4 :
