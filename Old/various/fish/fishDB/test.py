import os

import database

_path = os.path.split(os.path.realpath(__file__))[0]
_path = os.path.join(_path, "test.sqlite")
database.init.sqlite(_path)
import entities
#print entities.Fish.fetchall()

# vim: set et sts=4 sw=4 :
