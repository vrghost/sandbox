
class ObjDict(dict):

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        self[name] = value

# vim: set et sts=4 sw=4 :
