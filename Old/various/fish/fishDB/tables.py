from collections import defaultdict
import sqlobject

from . import col, config

class _BaseTable(sqlobject.SQLObject):

    def __classinit__(cls, newAttrs):
	sqlobject.SQLObject.__classinit__(cls, newAttrs)
        if not cls.__name__.startswith("_"):
    	    cls.createTable(ifNotExists=True)

    @classmethod
    def fetchall(cls, *args, **kwargs):
        return tuple(cls.select(*args, **kwargs))

class Object(_BaseTable):

    type = sqlobject.StringCol(length=50, notNull=True)
    descriptions = sqlobject.MultipleJoin("Description")
    images = sqlobject.MultipleJoin("Images")
    names = sqlobject.MultipleJoin("Names")
    environement = sqlobject.MultipleJoin("Environement")

    def getNames(self, language):
        return self._getItems(language, self.names)

    def getDescriptions(self, language):
        return self._getItems(language, self.descriptions)

    def _getItems(self, language, iter):
        _code = config.languages.get(language, language)
        return [_obj for _obj in iter if _obj.language == _code]

class _Attribute(_BaseTable):

    object = sqlobject.ForeignKey("Object", notNull=True)

class Environement(_Attribute):

    temperature = sqlobject.MultipleJoin("Temperature")
    acidity = sqlobject.MultipleJoin("Acidity")
    hardness = sqlobject.MultipleJoin("Hardness")

class Description(_Attribute):

    text = sqlobject.BLOBCol(length=2**16)
    language = col.NamedEnumCol(enumValues=config.languages, notNull=True)

class Images(_Attribute):

    image = sqlobject.BLOBCol(length=2**24, notNull=True)

class Names(_Attribute):

    language = col.NamedEnumCol(enumValues=config.languages, notNull=True)
    name = sqlobject.UnicodeCol(length=255, notNull=True)

class _Range(_Attribute):

    min = sqlobject.DecimalCol(size=10, precision=4, notNull=True)
    max = sqlobject.DecimalCol(size=10, precision=4, notNull=True)

class Temperature(_Range):

    units = sqlobject.StringCol(length=10, default="C", notNull=True)

class Acidity(_Range):

    units = sqlobject.StringCol(length=10, default="ph", notNull=True)

class Hardness(_Range):

    units = sqlobject.StringCol(length=10, default="dgh", notNull=True)

class Character(_Attribute):

    toOthers = col.NamedEnumCol(enumValues={
        "peacifull": "peacifull",
        "aggressive": "aggressive",
    }, notNull=True)

    toSelf = col.NamedEnumCol(enumValues={
        "peacifull": "peacifull",
        "aggressive": "aggressive",
    }, notNull=True)

# vim: set et sts=4 sw=4 :
