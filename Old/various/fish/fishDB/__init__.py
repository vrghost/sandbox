import database

def initSqlite(path):
    database.init.sqlite(path)
    _loadTables()

def _loadTables():
    import tables
    for (_name, _value) in tables.__dict__.iteritems():
        globals[_name] = _value


# vim: set et sts=4 sw=4 :
