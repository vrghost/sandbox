import sqlobject

from . import util

class _SOGreedyEnumCol(sqlobject.SOEnumCol):

    def _getlength(self, obj):
        # I want to have some spare length.
        # just in case.
        # the size increasing rate was just guessed.
        _out = super(_SOGreedyEnumCol, self)._getlength(obj)
        return (_out * 2) + 3

class NamedEnumCol(sqlobject.EnumCol):

    baseClass = _SOGreedyEnumCol

    def __init__(self, **kwargs):
        _vals = kwargs.pop("enumValues")
        kwargs["enumValues"] = _vals.values()
        super(NamedEnumCol, self).__init__(**kwargs)
        self.values = util.ObjDict(_vals)


# vim: set et sts=4 sw=4 :
