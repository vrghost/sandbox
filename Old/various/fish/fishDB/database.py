import logging
import sqlobject

class Init(object):

    _connection = None

    @property
    def connected(self):
        return bool(_connection)

    def sqlite(self, dbPath):
        _builder = sqlobject.sqlite.builder()
        self._connect(_builder, dbPath)

    def _connect(self, builder, *args, **kwargs):
        kwargs["debug"] = True
        _connection = builder(*args, **kwargs)
        self._connection = _connection
        sqlobject.sqlhub.processConnection = _connection

    def getConnection(self):
        return self._connection


init = Init()

# vim: set et sts=4 sw=4 :
