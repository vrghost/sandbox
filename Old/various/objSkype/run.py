import event

import server

import time

if __name__ == "__main__":
    _server = server.getServer(True)
    _server.start()
    while True:
        time.sleep(1)

# vim: set sts=4 sw=4 et :
