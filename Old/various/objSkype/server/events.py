import threading
import event
import time

import weakref

from . import base

class EventPumper(base.Daemon):

    ready = False

    def run(self):
        try:
            self.ready = True
            while True:
                event.dispatch()
                time.sleep(0.1)
        finally:
            self.ready = False

class EventCaster(base.Base):

    _msgBuffer = _handlers = None
    maxBufferedMsg = 1000

    def __init__(self):
        self._handlers = []
        self._msgBuffer = []

    def subscribe(self, handler):
        assert handler not in self._handlers
        _ref = weakref.proxy(handler, self._removeRef)
        self._handlers.append(_ref)
        self._sendBufferedMessages()
        return lambda: self._removeRef(_ref) # canceller

    def _removeRef(self, weakref):
        self._handlers.remove(weakref)

    def unsubscribe(self, handler):
        self._removeRef(_handler)

    def send(self, msg, timeout=0):
        if not self._handlers:
            self._buffer(msg, timeout)
        else:
            self._send(msg, timeout)

    _keeper = []

    def _send(self, msg, timeout):
        assert self._handlers
        for _handle in self._handlers:
            event.event(_handle, msg).add(timeout)

    def _buffer(self, *sendArgs):
        if len(self._msgBuffer) > self.maxBufferedMsg:
            print "Max buffered msg cound readhed.\n"\
                "Erasing lasto one (%s)." % self._msgBuffer.pop()
        self._msgBuffer.append(sendArgs)

    def _sendBufferedMessages(self):
        while self._msgBuffer:
            self._send(*self._msgBuffer.pop(0))

class Handle(object):

    def __init__(self, meth):
        self._meth = meth

    def __get__(self, inst, klass):
        self._inst = weakref.ref(inst)
        return self

    def __call__(self, evt, handle, type, arg):
        self._meth(self._inst(), arg)

def handler(method):
    return Handle(method)

# vim: set sts=4 sw=4 et :
