import threading

class Base(object):
    pass

class Daemon(Base, threading.Thread):

    def __init__(self, *args, **kwargs):
        super(Daemon, self).__init__(*args, **kwargs)
        self.setDaemon(True)

class BaseException(Exception):
    pass

# vim: set sts=4 sw=4 et :
