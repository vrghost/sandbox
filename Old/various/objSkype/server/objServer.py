import time
import threading

import re

from . import messages, base, events, raw

class SkypeObjServer(base.Base):

    toSkype = None
    eventCaster = None
    _handlers = None
    _collectedData = None

    def __init__(self):
        super(SkypeObjServer, self).__init__()
        self.eventCaster = events.EventCaster()
        self.toSkype = events.EventCaster()
        self._handlers = {
            "CURRENTUSERHANDLE": self.currentUserHandle,
            "PROFILE": self.profileHandle,
            "USER": self.userHandle,
            "USERS": self.handleFriendList,
            "ERROR": self.errorHandle,
            "CHATMESSAGE": self.chatmessageHandle,
            "CHAT": self.chatHandle,
        }
        self._collectedData = {
            "messages": {},
            "users": {},
            "client": {},
            "chats": {},
        }
        self.send("PROTOCOL 7")
        self.send("GET CURRENTUSERHANDLE")
        self.send("SEARCH FRIENDS")

    def send(self, message):
        self.toSkype.send(message)

    @events.handler
    def fromSkype(self, message):
        assert isinstance(message, basestring)
        (_cmd, _data) = message.split(" ", 1)
        if _cmd in self._handlers:
            self._handlers[_cmd](_data)
        else:
            print "Unknown message: %r" % message

    def log_info(self, msg):
        self.eventCaster.send(msg)

    def emit(self, msg):
        self.eventCaster.send(msg)

    @events.handler
    def onProfileChange(self, msg):
        _name = msg["name"]
        _value = msg["object"].getSkypeValue(_name)
        self.send("SET PROFILE %s %s" % (_name, _value))

    @events.handler
    def onMessageChange(self, msg):
        _name = msg["name"]
        _value = msg["object"].getSkypeValue(_name)
        _id = msg["object"].getSkypeValue("id")
        self.send("SET CHATMESSAGE %s %s %s" % (_id, _name, _value))

    @events.handler
    def onChatChange(self, msg):
        _name = msg["name"]
        _value = msg["object"].getSkypeValue(_name)
        _id = msg["object"].getSkypeValue("id")
        self.send("ALTER CHAT %s set%s %s" % (_id, _name, _value))

    # handlers

    def chatmessageHandle(self, data):
        (_id, _name, _value) = data.split(" ", 2)
        if _id in self._collectedData["messages"]:
            _msg = self._collectedData["messages"][_id]
        else:
            _msg = self._collectedData["messages"][_id] = \
                messages.Chatmessage(_id)
            _msg.onChange.subscribe(self.onMessageChange)
            for _attr in _msg.attrs:
                self.send("GET CHATMESSAGE %s %s" % (_id, _attr))
        _msg.setValue(_name, _value)
        if _msg.complete:
            #self.emit(_msg)
            pass

    def currentUserHandle(self, username):
        _profileObj = messages.Profile(username.strip())
        _profileObj.onChange.subscribe(self.onProfileChange)
        self._collectedData["profile"] = _profileObj
        for _name in _profileObj.attrs:
            self.send("GET PROFILE " + _name)

    def profileHandle(self, data):
        (_name, _value) = data.split(" ", 1)
        self._collectedData["profile"].setValue(_name, _value)
        if self._collectedData["profile"].complete:
            pass

    def handleFriendList(self, data):
        _friends = [_el.strip() for _el in data.split(",")]
        for _friend in _friends:
            _frObj = \
                self._collectedData["users"][_friend] = messages.User()
            for _attr in _frObj.attrs:
                self.send("GET USER %s %s" % (_friend, _attr))

    def userHandle(self, data):
        (_username, _name, _value) = data.split(" ", 2)
        if _username in self._collectedData["users"]:
            _user = self._collectedData["users"][_username]
        else:
            _user = self._collectedData["users"][_username] = messages.User()
            for _attr in _user.attrs:
                self.send("GET USER %s %s" % (_username, _attr))
        _user.setValue(_name, _value)
        if _user.complete:
            #self.emit(_user)
            pass

    def errorHandle(self, data):
        (_code, _text) = data.split(" ", 1)
        print "Error %s: %s" % (_code, _text)

    def chatHandle(self, data):
        (_id, _prop, _val) = data.split(" ", 2)
        if _id in self._collectedData["chats"]:
            _obj = self._collectedData["chats"][_id]
        else:
            _obj = self._collectedData["chats"][_id] = messages.Chat(_id)
            _obj.onChange.subscribe(self.onChatChange)
            for _attr in _obj.attrs:
                self.send("GET CHAT %s %s" % (_id, _attr))
        _obj.setValue(_prop, _val)
        if _obj.complete:
            self.emit(_obj)

class TestObserver(base.Base):

    @events.handler
    def handler(self, msg):
        #msg.topic = "topic!"
        #print "msg", msg
        pass

class _Animation(object):

    msg = None
    frame = 0

    def __init__(self, film, msg):
        self.film = film
        self.msg = msg

    def nextFrame(self):
        self.msg.topic = self.film[self.frame]
        self.frame = (self.frame + 1) % len(self.film)

class Animator(base.Daemon):

    def animBanner(text, width):
        _out = []
        _text = " ".join((text, text, text))
        for _pos in xrange(len(text)):
            _out.append(_text[_pos:_pos+width])
        return _out

    films = {
        "simple": (
            "++++\n++++",
            "----\n----",
            "||||\xc2\xaa||||".decode("utf-8"),
        ),
        "banner": animBanner("This is marqueeeeeeeeeeeee", 15),
    }

    _animations = _newMessages = None

    def __init__(self):
        super(Animator, self).__init__()
        self._animations = {}
        self._newMessages = []

    @events.handler
    def handler(self, msg):
        _val = getattr(msg, "topic", None)
        if hasattr(_val, "startswith") and _val.startswith("!play animation"):
            self._newMessages.append(msg)

    def run(self):
        while True:
            self._deployAnimations()
            self._playAnimations()
            #time.sleep(0.3)

    def _deployAnimations(self):
        while self._newMessages:
            _msg = self._newMessages.pop()
            if _msg.id not in self._animations:
                _animName = _msg.topic.rsplit(" ", 1)[-1].strip()
                self._animations[_msg.id] = _Animation(
                    self.films[_animName], _msg)

    def _playAnimations(self):
        for _anim in self._animations.itervalues():
            _anim.nextFrame()

class ServerEncapsulator(object):

    _controlledThreads = None

    def __init__(self, startEventPumper):
        self.startEventPumper = startEventPumper
        self._controlledThreads = {
            "eventPumper": events.EventPumper(),
            "rawServer": raw.rawServer(),
        }
        self._highServer = SkypeObjServer()
        _rawServer = self._controlledThreads["rawServer"]
        _rawServer.fromSkype.subscribe(self._highServer.fromSkype)
        self._highServer.toSkype.subscribe(_rawServer.toSkype)
        assert __debug__, "debug only!"
        self._test = TestObserver()
        self._highServer.eventCaster.subscribe(self._test.handler)
        self._animator = Animator()
        self._highServer.eventCaster.subscribe(self._animator.handler)
        self._animator.start()

    def start(self):
        self._controlledThreads["rawServer"].start()
        if self.startEventPumper:
            self._controlledThreads["eventPumper"].start()

def getServer(startEventPumper):
    return ServerEncapsulator(startEventPumper)

# vim: set sts=4 sw=4 et :
