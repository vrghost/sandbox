import itertools

from . import events

# just wide-used handlers
class _handle(object):

    def __init__(self, fromSkype=None, toSkype=None):
        self._from = fromSkype
        self._to = toSkype

    def decode(self, value):
        if self._from:
            _out = self._from(value)
        else:
            _out = value
        return _out

    def encode(self, value):
        if self._to:
            _out = self._to(value)
        else:
            _out = value
        return _out

skypeBoolean = _handle(
    lambda val: val.lower() == "true",
    lambda val: "TRUE" if val else "FALSE",
)

skypeInt = _handle(
    lambda val: int(val),
    lambda val: str(val),
)

def _processHandle(value):
    if isinstance(value, basestring):
        return (value, None)
    else:
        return value

class SkypeObject(object):

    server = None
    attrs = None
    roAttrs = rwAttrs = ()
    _handlers = None
    _data = None
    _handlersInitialized = False
    onChange = None

    def __new__(cls, *args, **kwargs):
        def _constructProperty(name, setter=None):
            return property(lambda self: self._data[name], setter)
        def _getSetter(name):
            def _setter(self, value):
                self._data[name] = value
                self.onChange.send({
                    "name": name,
                    "object": self,
                })
            return _setter
        if not cls._handlersInitialized:
            for _value in cls.roAttrs:
                _name = _processHandle(_value)[0]
                setattr(cls, _name, _constructProperty(_name))
            for _value in cls.rwAttrs:
                _name = _processHandle(_value)[0]
                setattr(
                    cls, _name, _constructProperty(_name, _getSetter(_name)))
            cls._handlersInitialized = True
        return super(SkypeObject, cls).__new__(cls, *args, **kwargs)

    def __init__(self):
        self.onChange = events.EventCaster()
        self._data = {}
        _attrs = []
        _handlers = self._handlers = {}
        for _value in itertools.chain(self.roAttrs, self.rwAttrs):
            (_name, _handle) = _processHandle(_value)
            if _handle:
                _handlers[_name] = _handle
            _attrs.append(_name)
        self.attrs = set(_attrs)

    @property
    def complete(self):
        return self.attrs.issubset(self._data.iterkeys())

    def setValue(self, name, value):
        name = name.lower()
        _handle = self._handlers.get(name)
        if _handle:
            value = _handle.decode(value)
        self._data[name] = value

    def getSkypeValue(self, name):
        name = name.lower()
        _value = self._data[name]
        _handle = self._handlers.get(name)
        if _handle:
            _value = _handle.encode(_value)
        return _value

    def __repr__(self):
        return "<%s:%s>" % (self.__class__.__name__, self._data)

class Profile(SkypeObject):

    rwAttrs = (
        "pstn_balance",
        "pstn_balance_currency",
        "fullname",
        "birthday",
        "sex",
        "languages",
        "country",
        "ipcountry",
        "province",
        "city",
        "phone_home",
        "phone_office",
        "phone_mobile",
        "homepage",
        "about",
        "mood_text",
        "rich_mood_text",
        "timezone",
        "call_apply_cf",
        "call_noanswer_timeout",
        "call_forward_rules",
        "call_send_to_vm",
        "sms_validated_numbers",
    )

    def __init__(self, accountName):
        super(Profile, self).__init__()
        self.setValue("account_name", accountName)

class Chatmessage(SkypeObject):

    roAttrs = (
        "id",
        "timestamp",
        "from_handle",
        "from_dispname",
        "type",
        "status",
        "chatname",
        "users",
        ("is_editable", skypeBoolean),
        "edited_by",
        "edited_timestamp",
        "options",
        "role",
    )

    rwAttrs = (
        "body",
    )

    def __init__(self, id):
        super(Chatmessage, self).__init__()
        self.setValue("id", id)

class User(SkypeObject):

    roAttrs = (
        "handle",
        "fullname",
        "birthday",
        "sex",
        "language",
        "country",
        "province",
        "city",
        "phone_home",
        "phone_office",
        "phone_mobile",
        "homepage",
        "about",
        ("hascallequipment", skypeBoolean),
        ("is_video_capable", skypeBoolean),
        ("is_voicemail_capable", skypeBoolean),
        "buddystatus",
        ("isauthorized", skypeBoolean),
        ("isblocked", skypeBoolean),
        "onlinestatus",
        "lastonlinetimestamp",
        ("can_leave_vm", skypeBoolean),
        "speeddial",
        "receivedauthrequest",
        "mood_text",
        "rich_mood_text",
        "timezone",
        ("is_cf_active", skypeBoolean),
        ("nrof_authed_buddies", skypeInt),
        "buddystatus",
        "isblocked",
        ("isauthorized", skypeBoolean),
        "speeddial",
        "displayname",
        # do not work: "skypeme", "skypeout",
    )

class Chat(SkypeObject):

    roAttrs = (
        "id",
        "name",
        "timestamp",
        "adder",
        "status",
        "posters",
        "members",
        "chatmessages",
        "activemembers",
        "friendlyname",
        "recentchatmessages",
        "bookmarked",
    )

    rwAttrs = (
        "topic",
        "topicxml",
    )

    def __init__(self, id):
        super(Chat, self).__init__()
        self.setValue("id", id)


# vim: set sts=4 sw=4 et :
