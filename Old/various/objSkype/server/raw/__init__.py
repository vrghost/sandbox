import os

from . import exceptions

def getRawServer():
    if os.name == "nt":
        from .win32 import WinAdaptor as _rawServer
    else:
        raise RuntimeError("OS '%s' is not supported" % os.name)
    return _rawServer

# __init__
rawServer = getRawServer()

# vim: set sts=4 sw=4 et :
