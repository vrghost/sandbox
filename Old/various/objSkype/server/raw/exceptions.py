from .. import base

class SkypeError(base.BaseException):
    """Skype Error class."""

class SkypeNotConnectedError(SkypeError):
    """Not connected to skype."""

class SkypeNotAuthorizationError(SkypeError):
    """Failed to autorize."""

class SkypeRefusedError(SkypeError):
    """Connection refused."""

# vim: set sts=4 sw=4 et :
