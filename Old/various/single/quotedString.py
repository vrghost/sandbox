# this module implements some experiments in regexp auto-generation.
# This should generate such re, that would split string by given separator(s)
# and omit the escaped seps.

def _fixMagics(str):
    # \ -> \\ must be first!
    _out = str.replace("\\", "\\" * 2)
    for _magic in ".^$*+?{}[]()|":
        _out = _out.replace(_magic, "\\" + _magic)
    return _out

def getSingleSplit(sep, esc="\\"):
    _nSep = _fixMagics(sep)
    _nEsc = _fixMagics(esc)
    return "(?<!%s)%s" % (_nEsc, _nSep)

def getMultiSplit(seps, esc="\\"):
    _nSeps = _fixMagics(seps)
    _nEsc = _fixMagics(esc)
    return "(?<!%s)[%s]" % (_nEsc, _nSeps)

import re
_re = re.compile(getMultiSplit(":*", "|"))

def _reSplit(line):
    return _re.split(line)

_sp = ":*"
_sep = "|"
def _forSplit(line):
    _out = []
    _skipNext = False
    _collect = ""
    for _ch in line:
        if _skipNext:
            _collect += _ch
            _skipNext = False
        else:
            if _ch == _sep:
                _skipNext = True
                _collect += _ch
            elif _ch in _sp:
                _out.append(_collect)
                _collect = ""
    else:
        if _collect:
            _out.append(_collect)
    return _out

if __name__ == "__main__":
    _line = "aaa:bbb:ddd|:dd:ee*dddd|*dd*fff||fff"
    from timeit import Timer
    t = Timer("_reSplit(%r)" % _line, "from __main__ import _reSplit")
    print t.timeit()
    t = Timer("_forSplit(%r)" % _line, "from __main__ import _forSplit")
    print t.timeit()


# vim: set et sts=4 sw=4 :
