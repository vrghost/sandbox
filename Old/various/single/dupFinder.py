""" This is small program used for duplicate file finding."""

import os
import binascii
import sys

# Note that i'm currently using py2.4, so ElementTree is not in batteries.
import elementtree.ElementTree as ET

class DupeFinder(object):
    _fileCache = None

    def checksum(self, text):
        return binascii.crc32(text)

    def reset(self):
        self._fileCache = {}

    def processFile(self, fname):
        try:
            _data = file(fname, "rb").read()
        except IOError:
            print "file %r not found" % fname
            return
        _hash = self.checksum(_data)
        if _hash in self._fileCache:
            self._fileCache[_hash].append(fname)
        else:
            self._fileCache[_hash] = [fname]

    def find(self, root):
        self.reset()
        for (_root, _dirs, _files) in os.walk(root):
            print "In dir %r" % _root
            for _file in _files:
                self.processFile(
                    os.path.realpath(os.path.join(_root, _file)))

    def getDupes(self):
        _out = []
        for _files in self._fileCache.itervalues():
            if len(_files) > 1:
                _out.append(_files)
        return _out

    def dupesToXml(self):
        _dupes = self.getDupes()
        _root = ET.Element("DupeList")
        for _files in _dupes:
            _listEl = ET.SubElement(_root, "dupes")
            for _file in _files:
                _dupe = ET.SubElement(_listEl, "file", path=_file)
        return ET.ElementTree(_root)

    def writeToXml(self, fname):
        _et = self.dupesToXml()
        _et.write(fname)

if __name__ == "__main__":
    (_dir, _xmlName) = sys.argv[1:3]
    _finder = DupeFinder()
    _finder.find(_dir)
    _finder.writeToXml("test.xml")

# vim: set et sts=4 sw=4 :
