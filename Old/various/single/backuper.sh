#! /bin/bash

SRC=$1
TARGET=$2

function toRealPath(){
	pushd $1
	OUT=`pwd`
	popd
}

function fname(){
	OUT=`echo "$1" | tr "//" "\n" | tail -1`
}

function archName(){
	OUT=$1_`date +%Y%m%d`.7z
}


toRealPath $TARGET
TARGET=$OUT
toRealPath $SRC
SRC=$OUT
fname $SRC
archName $OUT
TARGET=$TARGET/$OUT
echo "($SRC)->($TARGET)"
7za a -pisdn20 $TARGET $SRC
