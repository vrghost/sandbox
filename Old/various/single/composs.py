# $Id:$
# $Date:$
# $Rev:$
# $Revision:$

curAvg = None

def countAvg(vals):
    global curAvg
    _sum = 0
    for _val in vals:
        _diff = abs(_val - curAvg)
        assert _diff != 180
        if _diff > 180:
            _val += 360
        _sum += _val
    curAvg = (_sum / len(vals)) % 360

if __name__ == "__main__":
    _meanings = (0.0, 120.0, 240.0)
    ####
    curAvg = _meanings[0]
    for _cnt in xrange(1, len(_meanings)+1):
        print curAvg
        countAvg(_meanings[:_cnt])
    print curAvg


# vim: set et sts=4 sw=4 :
