""" This is small program used for duplicate file deleting.

(It's input file format is dupFinder's output file format)
"""
import sys
import os

# Note that i'm currently using py2.4, so ElementTree is not in batteries.
import elementtree.ElementTree as ET

def processDupes(root):
    _files = []
    for _dup in root:
        _files.append(_dup.attrib["path"])
    _files.sort(cmp=lambda x, y: cmp(len(x), len(y)))
    # removing one thet will be kept (smallest)
    del _files[0]
    for _file in _files:
        os.remove(_file)

def processRoot(root):
    for _elem in root:
        if _elem.tag == "dupes":
            processDupes(_elem)

if __name__ == "__main__":
    _xmlName = sys.argv[1]
    _root = ET.parse(_xmlName).getroot()
    processRoot(_root)

# vim: set et sts=4 sw=4 :
