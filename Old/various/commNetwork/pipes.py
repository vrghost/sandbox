import time
import socket
from threading import Thread
from select import select

class _PipeGen(object):

    server = None
    port = 56482
    host = '127.0.0.1'
    socketList = None

    def __init__(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.host, self.port))
        self.socketList = []

    def _serverRun(self, socket):
        socket.listen(1)
        (_conn, _addr) = socket.accept()
        self.socketList.append(_conn)

    def _clientRun(self):
        _socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        _socket.connect((self.host, self.port))
        self.socketList.append(_socket)

    def getSockets(self):
        _srvTh = Thread(target=self._serverRun, args=(self.server, ))
        _clTh = Thread(target=self._clientRun)
        _srvTh.start()
        _clTh.start()
        while _srvTh.isAlive() or _clTh.isAlive():
            time.sleep(0.1)
        assert len(self.socketList) == 2, \
            "We must heve two connected sockets here"
        _out = tuple(self.socketList)
        self.socketList = []
        return _out

_pipeGen = _PipeGen()

class _StringPipe(object):

    _socket = None

    def __init__(self, socket):
        self._socket = socket

    def read(self, size):
        return self._socket.recv(size)

    def write(self, data):
        self._socket.sendall(data)

    def canRead(self):
        return select((self._socket, ), (), (), 0)[0]

    def canWrite(self):
        return select((), (self._socket, ), (), 0)[1]

def getPipes():
    return [_StringPipe(_sock) for _sock in _pipeGen.getSockets()]

if __name__ == "__main__":
    # tests
    _s = []
    for _x in xrange(10):
        _s.append(_pipeGen.getSockets())
    print _s
# vim: set sts=4 sw=4 et :
