import threading
import os
from select import select
import time
import random

import symbols, pipes

class ProcessorPipe(object):
    """Processor pipe object.

    Can compose messages.
    And support message communication protocol.
    Protocol ('self' sending message):
    -->  <STX>
    <--  <ACK>
    -->  <ENQ><receiver><sender><msg_len><msg><checksum><ETX>  <-+
     +-> <NAK>                                      -----+
    -|
     +-> <ACK>

    `receiver` is a receiver ID (4 hex digits),
    `sender` is a sender ID (4 hex digits),
    `msg_len` is message lenth (4 HEX digits);
    `<msg>` is a message string;
    `checksum` is two HEX numbers (XOR of all char codes in 'msg')
    """

    state = None
    _outMsgQueue = None
    _pipe = None

    def __init__(self, pipe, gotDataCb=None):
        self._outMsgQueue = []
        self._pipe = pipe
        self._gotDataCb = gotDataCb

    def setDataCb(self, cb):
        self._gotDataCb = cb

    def getChecksum(self, data):
        return reduce(lambda a,b: a ^ ord(b), data, 0)

    _msgMask = symbols.ENQ + \
        "%(receiver)04x%(sender)04x%(length)04x%(msg)s%(checksum)02x" \
        + symbols.ETX

    def composeMsg(self, receiver, sender, data):
        if not isinstance(data, str):
            data = str(data)
        return self._msgMask % {
            "receiver":     receiver,
            "sender":       sender,
            "length":       len(data),
            "msg":          data,
            "checksum":     self.getChecksum(data),
        }

    def canWrite(self):
        return self._pipe.canWrite()

    def canRead(self):
        return self._pipe.canRead()

    def randomIdle(self):
        _value = 1
        for _i in xrange(4):
            _value *= random.random()
        time.sleep(_value)

    def _sendMessageI(self, sender, receiver, data):
        while not self.canWrite():
            yield
        self._pipe.write(symbols.STX)
        while not self.canRead():
            yield
        _msg = self._pipe.read(1)
        if _msg != symbols.ACK:
            if _msg in (symbols.STX, ):
                # error, re-init transfer
                self.randomIdle()
                self.sendMessage(sender, receiver, data, oob=True)
                raise StopIteration
            else:
                raise ValueError(
                    "Incorrect message 'READY' answer: %r" % _msg)
        _data = self.composeMsg(receiver, sender, data)
        while not self.canWrite():
            yield
        self._pipe.write(_data)
        _allOk = False
        while not _allOk:
            while not self.canRead():
                yield
            _ans = self._pipe.read(1)
            if _ans == symbols.ACK:
                _allOk = True
            elif _ans == symbols.NAK:
                while not self.canWrite():
                    yield
                _writePipe.write(_data)
            else:
                raise ValueError("Unknown ans; %r" % _ans)

    def _getMessageI(self):
        _data = None
        while _data != symbols.STX:
            while not self.canRead():
                yield
            _data = self._pipe.read(1)
            if _data not in (symbols.STX, symbols.NAK):
                raise ValueError("Unknown incoming data code: %r" % _data)
        while not self.canWrite():
            yield
        self._pipe.write(symbols.ACK)
        _finishIter = False
        while not _finishIter:
            while not self.canRead():
                yield
            # ENQ + receiver + sender + length
            _msgStart = self._pipe.read(1 + 4 + 4 + 4)
            _start = _msgStart[0]
            _error = False
            try:
                _receiver = int(_msgStart[1:5], 16)
                _sender = int(_msgStart[5:9], 16)
                _length = int(_msgStart[9:13], 16)
            except ValueError:
                _error = True
            if _start != symbols.ENQ or _error:
                raise ValueError("Incorrect message start: %r" % _msgStart)
            # length + checksum + ETX
            _msg = self._pipe.read(_length + 2 + 1)
            _data = _msg[:_length]
            _msg = _msg[_length:]
            try:
                _checksum = int(_msg[:2], 16)
            except ValueError:
                raise ValueError("Failed to parse checksum %r" % _msg[:2])
            _etx = _msg[2]
            if _etx != symbols.ETX:
                raise ValueError("%r symbol is not ETX)" % _etx)
            if _checksum == self.getChecksum(_data):
                self._gotDataCb(pipe=self, sender=_sender, receiver=_receiver,
                    data=eval(_data))
                _finishIter = True
            else:
                _finishIter = False
                self._pipe.write(symbols.NAK)
        self._pipe.write(symbols.ACK)

    def sendMessage(self, sender, receiver, data, oob=False):
        assert None not in (sender, receiver)
        if not isinstance(data, dict) or "type" not in data:
            raise ValueError(
                "You must provide dict with 'type' field at least")
        _data = (sender, receiver, data)
        if oob:
            self._outMsgQueue.insert(0, _data)
        else:
            self._outMsgQueue.append(_data)

    def ioStep(self):
        if self.state:
            # we were doing smth
            try:
                self.state.next()
            except StopIteration:
                self.state = None
        # we weren't doing anything or StopIteration error was raised
        # check for readable socket (may be someone is sending smth?)
        if not self.state:
            if self.canRead():
                self.state = self._getMessageI()
            elif self._outMsgQueue:
                (_sender, _receiver, _data) = self._outMsgQueue.pop(0)
                self.state = self._sendMessageI(_sender, _receiver, _data)

class _BaseNode(threading.Thread):

    query = "Qry"
    data = "data"
    structure = "structure"
    structureAns = "structureResult"
    idQry = "idQuery"
    system = "system"
    idMsg = "idMsg"
    routes = "routeList"
    id = None
    multicast = 0

    def __init__(self, id):
        super(_BaseNode, self).__init__()
        if id <= 0:
            raise ValueError("Id must be greater that 0!")
        self.id = id

    def run(self):
        raise NotmplementedError

    def inDataCb(self, pipe, sender, receiver, data):
        """Input data callback"""
        if receiver == self.multicast:
            self.processMulticastData(pipe, sender, data)
        elif receiver == self.id:
            self.processIncomingData(pipe, sender, data)
        else:
            print ("ERROR MESSAGE", data)

    def processIncomingData(self, pipe, sender, data):
        print ("GOT", data, "FROM", sender)

    def processMulticastData(self, pipe, sender, data):
        _out = False
        if data["type"] == self.query:
            if data["class"] == self.idQry:
                _msg = {
                    "type":     self.system,
                    "class":    self.idMsg,
                    "id":       self.id,
                }
                pipe.sendMessage(self.id, sender, _msg)
                _out = True
        return _out

class ProcessorNode(_BaseNode):
    """`Processor` node (has one I/O)"""

    _pipe = None

    def __init__(self, pipe, id):
        super(ProcessorNode, self).__init__(id)
        pipe.setDataCb(self.inDataCb)
        self._pipe = pipe

    def sendMessage(self, receiver, data, oob=False):
        self._pipe.sendMessage(self.id, receiver, data, oob)

    def run(self):
        _step = 0
        while True:
            self._pipe.ioStep()
            if _step > 10:
                self.doWork()
                _step = 0
            _step += 1

    def doWork(self):
        # do work
        raise NotImplementedError

class RouterPipe(object):

    __slots__ = ("pipe", "othId", "parent", "msgBuffer")

    def __init__(self, **kwargs):
        self.msgBuffer = []
        self.othId = None
        for (_name, _value) in kwargs.iteritems():
            assert _name in self.__slots__
            setattr(self, _name, _value)

    def sendMessage(self, target, data, source=None):
        if source is None:
            source = self.parent.id
        return self.pipe.sendMessage(source, target, data)

class _Routes(list):

    timestamp = 0

    def reset(self, newVal, time):
        while self:
            self.pop()
        self.extend(newVal)
        self.timestamp = time

class RouterNode(_BaseNode):

    _pipes = None
    _lastStructureQry = None
    _routes = None

    def __init__(self, id, pipes=()):
        super(RouterNode, self).__init__(id)
        self._pipes = {}
        self._routes = _Routes()
        for _pipe in pipes:
            self.addPipe(pipe)

    def addPipe(self, pipe):
        pipe.setDataCb(self.inDataCb)
        self._pipes[pipe] = RouterPipe(pipe=pipe, parent=self)

    def knowAllId(self):
        return reduce(
            lambda a,b: a and b.othId, self._pipes.itervalues(), True)

    def inDataCb(self, pipe, sender, receiver, data):
        if not self.knowAllId():
            if receiver == self.multicast:
                _pass = True
            elif data["type"] == self.system and data["class"] == self.idMsg:
                _pass = True
            else:
                _pass = False
        else:
            _pass = True
        if _pass:
            super(RouterNode, self).inDataCb(pipe, sender, receiver, data)
        else:
            self._pipes[pipe].msgBuffer.append((pipe, sender, receiver, data))

    def processIncomingData(self, pipe, sender, data):
        if data["type"] == self.system:
            if data["class"] == self.idMsg:
                _pipe = self._pipes[pipe]
                _pipe.othId = data["id"]
                if self.knowAllId:
                    for _pipe in self._pipes.itervalues():
                        for _data in _pipe.msgBuffer:
                            self.inDataCb(*_data)
                _pipe.msgBuffer = []
            elif data["class"] == self.structure:
                if self._lastStructureQry and \
                    self._lastStructureQry["data"]["id"] == data["id"]:
                    return
                self._lastStructureQry = {
                    "data":     data.copy(),
                    "pipe":     pipe,
                }
                for _pipe in self._pipes.itervalues():
                    # send request further
                    _pipe.sendMessage(_pipe.othId, data)
                # send my answer
                _ans = (
                    self.id, [_p.othId for _p in self._pipes.itervalues()])
                _ans = {
                    "type":     self.system,
                    "class":    self.structureAns,
                    "value":    _ans,
                    "id":       data["id"],
                }
                pipe.sendMessage(self.id, sender, _ans)
            elif data["class"] == self.structureAns:
                if not self._lastStructureQry or \
                    self._lastStructureQry["data"]["id"] != data["id"]:
                    return
                _pipe = self._pipes[self._lastStructureQry["pipe"]]
                _pipe.sendMessage(_pipe.othId, data)
            elif data["class"] == self.routes:
                self.updateRoutes(data["routes"], data["time"])

    def updateRoutes(self, newVal, time):
        if self._routes.timestamp >= time:
            return
        print "Got routes!", self.id
        self._routes.reset(newVal, time)
        _msg = {
            "type":     self.system,
            "class":    self.routes,
            "time":     time,
            "routes":   newVal,
        }
        for _pipe in self._pipes.itervalues():
            _pipe.sendMessage(_pipe.othId, _msg)

    def askId(self, pipe, force=False):
        if pipe.othId and not force:
            return
        _msg = {
            "type":     self.query,
            "class":    self.idQry,
        }
        pipe.sendMessage(self.multicast, _msg)

    def run(self):
        for _pipe in self._pipes.itervalues():
            self.askId(_pipe)
        _knowAllId = False
        while not _knowAllId:
            _knowAllId = True
            for _pipe in self._pipes.itervalues():
                _pipe.pipe.ioStep()
                _knowAllId = _knowAllId and _pipe.othId
        while True:
            for _pipe in self._pipes:
                _pipe.ioStep()
            self.doWork()
            time.sleep(0.01)

    def doWork(self):
        pass

class RootNode(RouterNode):

    _asked = False
    processorCount = None
    _connections = None

    def __init__(self, id, processorcount):
        super(RootNode, self).__init__(id)
        self.processorcount = processorcount
        self._connections = {}

    def askStructure(self):
        _msg = {
            "type":     self.system,
            "class":    self.structure,
            "id":       int(time.time())
        }
        for _pipe in self._pipes.itervalues():
            _pipe.pipe.sendMessage(self.id, _pipe.othId, _msg)

    def doWork(self):
        if not self._asked:
            self._asked = True
            self.askStructure()

    def processIncomingData(self, pipe, sender, data):
        if data["type"] == self.system and data["class"] == self.structureAns:
            (_src, _targets) = data["value"]
            self._connections[_src] = _targets
            if len(self._connections) >= self.processorcount:
                _conn = []
                for (_src, _targets) in self._connections.iteritems():
                    for _target in _targets:
                        _val = [_target, _src]
                        _val.sort()
                        if _val not in _conn:
                            _conn.append(_val)
                self.updateRoutes(_conn, time.time())
        else:
            super(RootNode, self).processIncomingData(pipe, sender, data)

def getPipes():
    return [ProcessorPipe(_pipe) for _pipe in pipes.getPipes()]

if __name__ == "__main__":
    _rootId = 1
    _routerIds = (2, 3, 4, 5, 6)
    _connections = (
        (1, 2),
        (1, 6),
        (1, 5),
        (2, 3),
        (2, 6),
        (3, 4),
        (4, 5),
        (4, 6),
    )
    #####
    _routers = {}
    for _id in _routerIds:
        _routers[_id] = RouterNode(_id)
    _routers[_rootId] = RootNode(_rootId, len(_routerIds) + 1)
    for (_from, _to) in _connections:
        _from = _routers[_from]
        _to = _routers[_to]
        (_pipeA, _pipeB) = getPipes()
        _from.addPipe(_pipeA)
        _to.addPipe(_pipeB)
    for _router in _routers.itervalues():
        _router.start()

# vim: set sts=4 sw=4 et :
