__all__ = []

_symbols = (
    ("NUL", "\x00"),
    ("SOH", "\x01"),
    ("STX", "\x02"),
    ("ETX", "\x03"),
    ("EOT", "\x04"),
    ("ENQ", "\x05"),
    ("ACK", "\x06"),
    ("BEL", "\x07"),
    ("BS",  "\x08"),
    ("HT",  "\x09"),
    ("LF",  "\x0A"),
    ("VT",  "\x0B"),
    ("FF",  "\x0C"),
    ("CR",  "\x0D"),
    ("SO",  "\x0E"),
    ("SI",  "\x0F"),
    ("DLE", "\x10"),
    ("DC1", "\x11"),
    ("DC2", "\x12"),
    ("DC3", "\x13"),
    ("DC4", "\x14"),
    ("NAK", "\x15"),
    ("SYN", "\x16"),
    ("ETB", "\x17"),
    ("CAN", "\x18"),
    ("EM",  "\x19"),
    ("SUB", "\x1A"),
    ("ESC", "\x1B"),
    ("FS",  "\x1C"),
    ("GS",  "\x1D"),
    ("RS",  "\x1E"),
    ("US",  "\x1F"),
)

globals().update(dict(_symbols))

# vim: set ts=4 sts=4 sw=4 et :
