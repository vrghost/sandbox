from pygame import display
from pygame.locals import DOUBLEBUF

class Display(object):
    """Games' display object """
    _screen = None
    _needFlip = False
    _objects = None

    DEFAULT_MODES = 0 & DOUBLEBUF

    def __init__(self, resolution, modes=DEFAULT_MODES):
        """Init display.

        Resolution - tuple of (Width, Height);
        modes - modes (see pygame.locals.display.set_mode for description)
        """
        self._screen = display.set_mode(resolution, modes)
        (self.maxX, self.maxY) = resolution
        self._needFlip = DOUBLEBUF & modes
        self._objects = []

    def flip(self):
        display.flip()

    def addObject(self, obj):
        self._objects.append(obj)

    def setObjects(self, objects):
        self._objects = list(objects)

    def act(self, timeD):
        for _obj in self._objects:
            _obj.act(timeD, self, self._objects)

    def draw(self, timeD):
        _scr = self._screen
        _scr.fill((0, 0, 0))
        for _obj in self._objects:
            _obj.draw(timeD, _scr)
        display.update()
        if self._needFlip:
            self.flip()

# vim: set sts=4 sw=4 et :
