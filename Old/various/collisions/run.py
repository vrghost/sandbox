from pygame import time, event
import os
import pygame
import sys

from collisions.display import Display

from collisions import drawObj, objLoader

FRAMES_PER_SECOND = 30

if __name__ == "__main__":
    _objRoot = sys.argv[1]
    (_H, _W) = (800, 600)
    _getter = objLoader.ObjLoader(maxH=_H, maxW=_W)
    _screen = Display((_H, _W))
    _objects = _getter.getAll(_objRoot)
    _screen.setObjects(_objects)
    _clock = time.Clock()
    while True:
        _timeD = _clock.tick(FRAMES_PER_SECOND)
        for event in pygame.event.get():
            if not hasattr(event, 'key'):
                continue
            if event.key == pygame.K_ESCAPE:
                sys.exit(0) # quit the game
        _screen.act(1.0/_timeD)
        _screen.draw(_timeD)

# vim: set sts=4 sw=4 et :
