# Physical objects
import math

from collisions import objBase

epsilon = 0.1
Infinity = 10e+6
friction = 0.9
gravity = 800

def sign(num):
    if num >= 0:
        _out = 1
    else:
        _out = -1
    return _out

class PhysPoint(objBase.BasePoint):

    def __init__(self, x, y, mass, velX=0, velY=0):
        self.x = x
        self.y = y
        self.mass = float(mass)
        self.velX = velX
        self.velY = velY

    def act(self, timeD, screen, objects):
        if self.mass < Infinity:
            self.x += self.velX * timeD
            self.y += self.velY * timeD
        if self.x < 0:
            self.x = 0
        if self.y < 0:
            self.y = 0
        if self.x > screen.maxX:
            self.x = screen.maxX
        if self.y > screen.maxY:
            self.y = screen.maxY
        self.applyForce(0, gravity)
        self.velX *= friction
        self.velY *= friction

    def applyForce(self, fx, fy):
        self.velX += (fx / self.mass)
        self.velY += (fy / self.mass)

class PhysSpring(objBase.BaseSpring):

    def __init__(self, pointA, pointB, length, harshness=10):
        self.points = (pointA, pointB)
        self.length = float(length)
        self.harshness = float(harshness)

    def getActualLen(self):
        _dx = self.points[0].x - self.points[1].x
        _dy = self.points[0].y - self.points[1].y
        return math.sqrt(_dx**2 + _dy**2)

    def act(self, timeD, screen, objects):
        _actLen = self.getActualLen()
        _delta = self.length - _actLen
        if abs(_delta) <= epsilon:
            _force = 0.1
        else:
            _force = _delta * self.harshness
        _height = self.points[0].y - self.points[1].y
        _width = self.points[0].x - self.points[1].x
        if not _force:
            _forceRatioY = _forceRatioX = 0
        elif _height == 0:
            _forceRatioY = 0
            _forceRatioX = 0.8
        elif _width == 0:
            _forceRatioY = 0.8
            _forceRatioX = 0
        else:
            _forceRatioY = _height / _actLen
            _forceRatioX = _width / _actLen
        _fx = _force * _forceRatioX
        _fy = _force * _forceRatioY
        self.points[0].applyForce(_fx, _fy)
        self.points[1].applyForce(-_fx, -_fy)

class PhysObj(object):

    def __init__(self, points, springs):
        self._points = points
        self._springs = springs

    def act(self, timeD, screen, objects):
        for _obj in self._springs:
            _obj.act(timeD, screen, objects)
        for _obj in self._points:
            _obj.act(timeD, screen, objects)

# vim: set sts=4 sw=4 et :
