# drawable objects
from pygame import draw

from collisions import physObj

class DrawPoint(physObj.PhysPoint):

    RADIUS = 5
    COLOR = (0, 0, 255)

    def draw(self, timeD, surface):
        _coords = [int(_coord) for _coord in (self.x, self.y)]
        draw.circle(surface, self.COLOR, _coords, self.RADIUS)

class DrawSpring(physObj.PhysSpring):

    COLOR = (0, 255, 0)

    def getColor(self):
        _actLen = self.getActualLen()
        _delta = self.length - _actLen
        if _delta > 0:
            _ratio = int(255 * (_actLen / self.length))
            _out = (_ratio, 100, 0)
        elif _delta < 0:
            _ratio = int(255 * ( self.length / _actLen))
            _out = (0, 100, _ratio)
        else:
            _out = (0, 255, 0)
        return _out

    def draw(self, timeD, surface):
        _getPos = lambda point: (point.x, point.y)
        draw.line(surface, self.getColor(), _getPos(self.points[0]),
            _getPos(self.points[1]))

class DrawObj(physObj.PhysObj):

    def draw(self, timeD, screen):
        for _obj in self._springs:
            _obj.draw(timeD, screen)
        for _obj in self._points:
            _obj.draw(timeD, screen)


# vim: set sts=4 sw=4 et :
