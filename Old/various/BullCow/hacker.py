
class Hacker(object):

    usedNumbers = set() # numbers, that we know exist in key, but we don't
                        # know their position (if we know their pos,
                        # than it won't be found here)
    knownKey = [ None, None, None, None ]   # Known key sequence
                                            #will be stored here
    triedKeys = [ [], [], [], [] ]  # Keys that we have tried, but the're wrong

    inputCallable = None
    outputCallable = None

    # all possible number set
    possibleNumbers = frozenset(range(10))

    itemCallbacks = {
        # binds processor callback names to input signals
        "+":    "processCorrect",
        "-":    "processFaied",
        "?":    "processKnown",
    }

    def __init__(self, inputCallable, outputCallable):
        """ Initialization.
        inputCallable is func, that will provide information for us.
        outStream is stream, to whic we're going to write out output ^_^

        """
        self.inputCallable = inputCallable
        self.outputCallable = outputCallable


    def requestData(self):
        """returns next data string"""
        return self.inputCallable()

    def writeToOut(self, string, *args):
        self.outputCallable(string % args)

    @property
    def hacked(self):
        # If there are None's in known key, than we haven't finished yet
        return not None in self.knownKey

    def getAnswer(self):
        """Reads & parses answer to an array"""
        _raw = ""
        while len(_raw) != 4:
            _raw = self.requestData()
        # the fastest way of splitting string to an char arr
        _possible = set(self.itemCallbacks.keys())
        _arr = [ _char for _char in _raw ]
        if not _possible.issuperset(_arr):
            # unknown symbols are in input stream!
            # XXX: We'll try to return our recursive call
            print ":(", _possible, set(_arr)
            return self.getAnswer()
        return _arr

    def getState(self, lastKey):
        return "".join([ str(_k) for _k in lastKey])

    def run(self):
        _lastKey = None
        while not self.hacked:
            _lastKey = self.getNextKey()
            self.writeToOut("%s\n", self.getState(_lastKey))
            _data = self.getAnswer()
            for _pos in xrange(len(_data)):
                _val =  _data[_pos]
                _name = self.itemCallbacks[_val]
                _callback = getattr(self, _name)
                _callback(_pos, _lastKey)

    def _addNotTestedNumbers(self, unknownKey):
        """Method that adds not tested numbers"""
        if len(unknownKey) == 4:
            # we have lot of work to do
            return list(unknownKey)
            return
        # we have some free slots! they're could be usefull ^_^
        # So we'll try to add not tested numbers to free slot.
        _out = [ None ] * 4
        _copyArr = list(unknownKey)
        for _pos in xrange(4):
            if self.knownKey[_pos] is None and _copyArr:
                _out[_pos] = _copyArr.pop()
        # if there is still not used numbers, let's add them only
        if _copyArr:
            for _el in _copyArr:
                for _pos in xrange(4):
                    if _out[_pos] is None and _el not in self.triedKeys[_pos]:
                        _out[_pos] = _el
        return _out

    _maxKey = None
    _minNones = 999
    _testedNumbers = []
    def getNextKey(self):
        # WARNING: this function uses obj-level `_testedNumbers`
        # variable for it's state saving
        _out = [ None ] *4
        if len(self.usedNumbers) < 4:
            # we do not know all keys yet
            _notTestedNumbers = self.possibleNumbers.difference(
                    self._testedNumbers)
            _notTestedNumbers = list(_notTestedNumbers)[:4]
            self._testedNumbers.extend(_notTestedNumbers[::-1])
            _out = self._addNotTestedNumbers(_notTestedNumbers)
        self._maxKey = None
        self._minNones = 999
        try:
            _out = self.fillPossibleKeys(_out)
        except Exception:
            _out = list(self._maxKey)
            for _pos in xrange(4):
                print _pos, _out
                if _out[_pos] is None:
                    _out[_pos] = 1
        return _out

    def fillPossibleKeys(self, initArr):
        _out = list(initArr)
        for _pos in xrange(4):
            if _out[_pos] is None:
                try:
                    _key = self.getPossibleKey(_pos, _out)
                except:
                    continue
                _saved = _out[_pos]
                _out[_pos] = _key
                if _out.count(None) < self._minNones:
                    self._minNones = _out.count(None)
                    self._maxKey = list(_out)
                try:
                    return self.fillPossibleKeys(_out)
                except:
                    _out[_pos] = _saved
                    continue
        if None in _out:
            raise Exception
        return _out

    def getPossibleKey(self, pos, bannedKeys):
        """Returns one possible key @ pos"""
        if self.knownKey[pos] is not None:
            return self.knownKey[pos]
        _tried = self.triedKeys[pos]
        _possible = self.usedNumbers.intersection(
                self.possibleNumbers.difference(_tried))
        # We won't use known keys (they all will be placed
        # on their corresponding places. Sooner ol later.)
        _possible = _possible.difference(self.knownKey)
        if len(_possible):
            # difference with bannedKeys is performed
            # so program would try not to use same numbers in
            # one call (seems better for me).
            _lastDiff = _possible.difference(bannedKeys)
            if len(_lastDiff) > 0:
                return _lastDiff.pop()
            else:
                raise "NO!"
        else:
            raise "NO!"


    ### Callbacks

    def processFaied(self, pos, lastKey):
        # Do nothing.
        pass

    def processCorrect(self, pos, lastKey):
        _key = lastKey[pos]
        self.usedNumbers.add(_key)
        self.knownKey[pos] = _key

    def processKnown(self, pos, lastKey):
        _key = lastKey[pos]
        self.usedNumbers.add(_key)
        self.triedKeys[pos].append(_key)


    def reset(self):
        """Resets all memory of a hacker"""
        self._testedNumbers = []
        self.knownKey = [ None, None, None, None ]
        self.triedKeys = [ [0], [], [], [] ]
        self.usedNumbers = set()

import sys

if __name__ == "__main__":
    hacker = Hacker(lambda: raw_input("answer screen: "), sys.stdout)
    hacker.run()

# vim: set sts=4 sw=4 et :
