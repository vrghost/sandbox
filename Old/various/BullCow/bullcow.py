from boolMatrix import BoolMatrix

class Solver(object):

    lastAnswer = ""

    memoryMatrix = None
    codeLength = None
    codeMaxNumber = None

    # callback definitions
    _callbackDefs = {
        "+": "processCorrect",
        "-": "processWrong",
        "?": "processYellow",
    }

    # I/O
    inputCallback = None
    outputCallback = None

    ## callbacks

    def processCorrect(self, pos, askedValue):
        self.memoryMatrix.setRow(askedValue, False)
        self.memoryMatrix.setCol(pos, False)
        self.memoryMatrix.setCell(pos, askedValue, True)

    def processYellow(self, pos, askedValue):
        self.memoryMatrix.setCell(pos, askedValue, False)

    def processWrong(self, pos, askedValue):
        self.memoryMatrix.setRow(askedValue, False)

    ## main body

    def __init__(self,
        inputCallback, outputCallback,
        codeLength=4, codeMaxNumber=9
    ):
        self.memoryMatrix = BoolMatrix(codeLength, codeMaxNumber+1) # +1
                                                        # is because of 0
        # saving prefs
        self.codeLength = codeLength
        self.codeMaxNumber = codeMaxNumber
        self.inputCallback = inputCallback
        self.outputCallback = outputCallback

    def _toOut(self, string, *args):
        self.outputCallback(string % args)

    def _getInput(self):
        _ans = self.inputCallback()
        self.lastAnswer = _ans
        return _ans

    def parseString(self, str):
        """Parses string

        (Mainly parsing consists of error checking)

        If error happend function returns 'None'

        """
        _chars = [
            _ch
            for _ch in str
            if _ch in self._callbackDefs
        ]
        if len(_chars) != self.codeLength:
            # incorrect/too short string entered
            return None
        return _chars

    def askAndProcess(self, data):
        """Writes `data` to output,
            waits for correct answer and than processes it
        """
        self._toOut("%s\n",
            "".join([str(_part) for _part in data]))
        _ans = ""
        while not _ans:
            _ans = self.parseString(self._getInput())
        for _pos in xrange(self.codeLength):
            _cbName = self._callbackDefs[_ans[_pos]]
            _callback = getattr(self, _cbName)
            _callback(_pos, data[_pos])

    def hacked(self):
        return self.lastAnswer == "+" * self.codeLength

    def doStartupChat(self):
        """Preforms startup chat (01234 & 4567 requests)"""
        _first = (0,1,2,3)
        _second = (4,5,6,7)
        self.askAndProcess(_first)
        self.askAndProcess(_second)

    def _getFirstTrue(self, pos, bannedKeys=[]):
        """Returns first `True` in col @ pos"""
        # It would try not to use vals in `bannedKeys`
        # bout if it's impossible, it'll use them.
        _col = self.memoryMatrix.getCol(pos)
        _lastGood = None
        for _pos in xrange(self.codeMaxNumber + 1): # + 1 => zero!
            if _col[_pos]:
                if _pos in bannedKeys:
                    _lastGood = _pos
                else:
                    return _pos
        if _lastGood is not None:
            return _lastGood
        raise Exception("Haven't found it! %s" % self.memoryMatrix)

    def getNextMove(self):
        _out = [ None ] * self.codeLength
        for _pos in xrange(self.codeLength):
            _ans = self._getFirstTrue(_pos, _out)
            _out[_pos] = _ans
        return _out

    def solve(self):
        self.doStartupChat()
        #self._toOut("%s\n", self.memoryMatrix)
        while not self.hacked():
            _move = self.getNextMove()
            self.askAndProcess(_move)

import sys

if __name__ == "__main__":
    _solver = Solver(
        lambda: raw_input("answer:  "),
        lambda d: sys.stdout.write(d)
    )
    _solver.solve()


# vim: set et sts=4 sw=4 :
