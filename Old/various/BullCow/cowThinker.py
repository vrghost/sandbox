# Quick'n'dirty tester

class Thinker(object):

    thought = None

    def correct(self, th):
        _str = str(th).zfill(4)
        if len(_str) != 4:
            return False
        _set = frozenset([_ch for _ch in _str])
        return len(_set) == 4

    def thinkOf(self, minInt=122):
        for _thought in xrange(minInt+1, 9999):
            if self.correct(str(_thought)):
                self.thought = _thought
                return _thought
        raise Exception("Finish!")

    def process(self, strVal):
        _sTh = str(self.thought).zfill(4)
        if len(strVal) != 4:
            return "-" * 4
        _out = ""
        for _pos in xrange(4):
            if strVal[_pos] == _sTh[_pos]:
                _out += "+"
            elif strVal[_pos] in _sTh:
                _out += "?"
            else:
                _out += "-"
        return _out


# vim: set et sts=4 sw=4 :
