import hacker
import cowThinker

solver = None
thinker = None

thinkerAnswer = ""

moves = 0

def solverInput():
    print "Thinker:", thinkerAnswer
    return thinkerAnswer


def solverOutput(data):
    print "Solver: ", data.strip()
    _a = thinker.process(data.strip())
    global moves
    moves += 1
    global thinkerAnswer
    thinkerAnswer = _a

import time

if __name__ == "__main__":
    thinker = cowThinker.Thinker()
    thinker.thinkOf(1000)
    solver = hacker.Hacker(solverInput, solverOutput)
    solver.run()
    _longMoves = 0
    _failed = []
    _total = 0
    try:
        while True:
            print "=" * 25
            _lastMoves = moves
            moves = 0
            thinker.thinkOf(thinker.thought)
            solver.reset()
            solver.run()
            _total += 1
            if moves > 5:
                _longMoves += 1
                _failed.append((thinker.thought, moves))
    except Exception, _e:
        print _e
    print "long: ", _longMoves, _failed
    print "total: ", _total

# vim: set et sts=4 sw=4 :
