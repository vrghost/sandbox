class _BoolList(list):

    def __init__(self, length, defValue=True):
        _initArr = [ defValue ] * length
        self.extend(_initArr)

class BoolMatrix(list):
    """Matrix structure is
    [ BoolList, BoolList, BoolList.... ]
        <---- width ------->

    """


    width = None
    height = None

    def __init__(self, width, height, defValue=True):
        for _col in xrange(height):
            _line = _BoolList(width, defValue)
            self.append(_line)
        # note that theese properties should be read-only
        self.height = height
        self.width = width

    def getCol(self, pos, copy=True):
        assert(pos >= 0 and pos < self.width)
        # `copy` parameter is added just for
        # similarity with `getRow` method
        _out = []
        for _currRow in xrange(self.height):
            _row = self[_currRow]
            _out.append(_row[pos])
        return _out

    def getRow(self, pos, copy=True):
        assert(pos >= 0 and pos < self.height)
        if copy:
            _out = list(self[pos])
        else:
            _out = self[pos]
        return _out

    def setCol(self, pos, value):
        assert(pos >= 0 and pos < self.width)
        for _list in self:
            _list[pos] = value


    def setRow(self, pos, value):
        assert(pos >= 0 and pos < self.height)
        _list = self[pos]
        for _pos in xrange(self.width):
            _list[_pos] = value

    def setCell(self, x, y, value):
        assert(x >= 0 and x < self.width)
        assert(y >= 0 and y < self.height)
        self[y][x] = value


    def __str__(self):
        _out = "%s: \n" % self.__class__.__name__
        for _pos in xrange(self.height):
            _row = self.getRow(_pos, False)
            # True * 1 = 1
            # False * 1 = 0
            _outRow = [ str(_part * 1) for _part in _row]
            _out += "%s\n" % _outRow
        return _out

# vim: set et sts=4 sw=4 :
