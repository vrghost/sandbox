from distutils import file_util
import os
import sys

__version__ = "$Id:$"


def walk(path):
    _out = []
    for (_root, _dirs, _files) in os.walk(path):
        if _files:
            for _file in _files:
                _out.append(os.path.realpath(os.path.join(_root, _file)))
    return _out

def sortByNames(files):
    _out = {}
    for _file in files:
        _fname = os.path.split(_file)[1]
        _fname = os.path.splitext(_fname)[0].strip(".")
        if _fname:
            _name = _fname[0]
            if _name in _out:
                _out[_name].append(_file)
            else:
                _out[_name] = [_file]
    return _out

def performSort(root, files):
    for (_dir, _files) in files.iteritems():
        _path = os.path.join(root, _dir)
        if not os.path.exists(_path):
            os.mkdir(_path)
        for _file in _files:
            file_util.copy_file(_file, _path)


usage = """<executable> <root path> <output path>"""

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print usage
        exit(0)
    (_root, _out) = sys.argv[1:]
    _files = walk(os.path.realpath(_root))
    _files = sortByNames(_files)
    performSort(os.path.realpath(_out), _files)


# vim: set sts=4 sw=4 et :
