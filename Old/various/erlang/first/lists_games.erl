-module(lists_games).
-export([firstEl/1]).
-export([compr/1]).

firstEl([]) -> [];
firstEl([{El, _}|Tail]) -> [El | firstEl(Tail)].

compr([]) -> [];
compr(Lst) -> [2*X || X <- Lst].