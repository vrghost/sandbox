-module(sbox).
-export([g/1]).

g(X) when X > 10 -> 10;
g(_) -> false.
