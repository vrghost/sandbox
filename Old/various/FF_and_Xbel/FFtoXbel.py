import sys
from xml.etree import ElementTree
from HTMLParser import HTMLParser

class HtmlElement(object):
    tag = None
    attrib = None
    text = None

    def __init__(self, tag, attrib, text=""):
        self.tag = tag
        self.attrib = dict(attrib)
        self.text = text

    def __repr__(self):
        return "<%s %s %r>" % (self.tag, self.attrib, self.text)

class FFParser(HTMLParser):

    _newElement = None
    processor = None

    def reset(self):
        HTMLParser.reset(self)
        self.processor = XbelBuilder()

    def handle_starttag(self, tag, attrs):
        if self._newElement:
            self.processor.newElement(self._newElement)
        self._newElement = HtmlElement(tag, attrs)

    def handle_endtag(self, tag):
        if self._newElement:
            self.processor.newElement(self._newElement)
            if self._newElement.tag != tag:
                self.processor.endOf(tag)
            self._newElement = None
        else:
            self.processor.endOf(tag)

    def handle_data(self, data):
        if self._newElement:
            self._newElement.text += data

    def getResult(self):
        return self.processor.getResult()

class XbelBuilder(object):

    xmlRoot = None
    elementStack = None
    ignoredTags = ("meta", "title", "h1", "p", "dt")
    _collectedHeader = None
    _lastElementAdded = None

    def __init__(self):
        self.reset()

    def reset(self):
        self.xmlRoot = ElementTree.Element("xbel")
        self.xmlRoot.attrib["version"] = "1.0"
        self.xmlRoot.htmlTag = None
        self.elementStack = []

    def newElement(self, element):
        if element.tag in self.ignoredTags:
            return
        if element.tag in ("h2", "h3"):
            self.processHeader(element)
        elif element.tag == "dd":
            self.processDesc(element)
        elif element.tag == "dl":
            self.dirListStart(element)
        elif element.tag == "a":
            self.newBookmark(element)
        elif element.tag == "hr":
            self.addSeparator(element)
        else:
            raise Exception("Can't process: %s" % element)

    def endOf(self, tag):
        if tag in self.ignoredTags:
            return
        if tag == "dl":
            self.elementStack.pop()
        else:
            raise Exception("This shouldn't happen")

    def getResult(self):
        assert not self.elementStack, "Stack should be empty"
        return self.xmlRoot

    @property
    def topElement(self):
        if self.elementStack:
            _out = self.elementStack[-1]
        else:
            _out = self.xmlRoot
        return _out

    def createElement(self, name):
        _child = ElementTree.SubElement(self.topElement, name)
        self._lastElementAdded = _child
        return _child

    def newStackElement(self, name):
        _child = self.createElement(name)
        self.elementStack.append(_child)
        return _child

    ######

    def processHeader(self, element):
        assert not self._collectedHeader
        self._collectedHeader = element

    def processDesc(self, element):
        if self._collectedHeader:
            _el = self._collectedHeader
            if hasattr(_el, "desc"):
                _el.desc += element.text
            else:
                _el.desc = element.text
        else:
            assert self._lastElementAdded.tag == "bookmark"
            _el = ElementTree.SubElement(self._lastElementAdded, "desc")
            _el.text = element.text

    def dirListStart(self, element):
        self.newStackElement("folder")
        if self._collectedHeader:
            _title = self.createElement("title")
            _title.text = self._collectedHeader.text
            if hasattr(self._collectedHeader, "desc"):
                _desc = self.createElement("desc")
                _desc.text = self._collectedHeader.desc
        self._collectedHeader = None

    def newBookmark(self, element):
        _child = self.createElement("bookmark")
        for _name in ("id", "href", "shortcuturl"):
            if _name in element.attrib:
                _child.attrib[_name] = element.attrib[_name]
        _title = ElementTree.SubElement(_child, "title")
        _title.text = element.text

    def addSeparator(self, element):
        self.createElement("separator")

if __name__ == "__main__":
    (_inpFile, _outFile) = sys.argv[1:3]
    print "input file: %s" % _inpFile
    _text = file(_inpFile).read()
    _parser = FFParser()
    _parser.feed(_text)
    _xml = _parser.getResult()
    ElementTree.ElementTree(_xml).write(_outFile)
# vim: set sts=4 sw=4 et :
