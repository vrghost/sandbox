import sys
from xml.etree.ElementTree import XML

def getHeader():
    return """
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <TITLE>Bookmarks</TITLE>
    <H1>Bookmarks</H1>
    """

def processFolder(element):
    for _child in element:
        if _child.tag == "title":
            _title = _child.text
            break
    else:
        raise Exception("No directory name found")
    _id = element.attrib["id"]
    _head = '<DT><H3 ID="%s">%s</H3>\n' % (_id, _title)
    _body = processTree(element)
    return "%s\n<DL><p>\n%s\n</DL><p>\n" % (_head, _body)

def processBookmark(element):
    _title = None
    _desc = None
    for _child in element:
        if _child.tag == "title":
            _title = _child.text
        elif _child.tag == "desc":
            _desc = _child.text
        else:
            raise NotImplementedError(
                "Unknown bookmark child - %s" % _child.tag)
    _id = element.attrib["id"]
    _href = element.attrib["href"]
    _out = '<DT><A HREF="%s" ID="%s">%s</A>\n' % (_href, _id, _title)
    if _desc:
        _out += "<DD>%s\n" % _desc
    return _out

def processTree(tree):
    _out = ""
    for _child in tree:
        if _child.tag == "folder":
            _out += processFolder(_child)
        elif _child.tag == "bookmark":
            _out += processBookmark(_child)
        elif _child.tag in ("title", "info", "separator"):
            # FIXME: separator
            # just skip title
            pass
        else:
            raise NotImplementedError("Unknown tag : %s" % _child.tag)
    return _out

def processXbelTree(root):
    _head = getHeader()
    _body = processTree(root)
    return _head + _body

if __name__ == "__main__":
    (_inpFile, _outFile) = sys.argv[1:3]
    print "input file: %s" % _inpFile
    _tree = XML(file(_inpFile).read())
    _out = processXbelTree(_tree)
    file(_outFile, "w").write(_out.encode("utf-8"))
# vim: set sts=4 sw=4 et :
