import os

import _fileList, pageBase

class CssFiles(_fileList.FileList):

    fileDir = os.path.join(_fileList.FileList.public, "css")
    _excludeFiles = ("Thumbs.db", ".svn", )

    @pageBase.pageHandler
    def refresh(self):
        return self.refreshTableContents()

    @pageBase.simpleHandler
    def getFiles(self):
        return [_el.path for _el in self._tableCls.fetchall()]

pageBase.updateModuleHandlers(globals(), CssFiles)
# vim: set et sts=4 sw=4 :
