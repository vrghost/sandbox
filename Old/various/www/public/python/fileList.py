# -*- coding: utf8 -*-
import cgi
import os
import urllib
from difflib import SequenceMatcher
from StringIO import StringIO
import elementtree.ElementTree as ET
from pytils.translit import slugify

import _fileList

def osCall(meth, *args):
    _newArgs = []
    for _arg in args:
        _newArgs.append(_arg.encode("utf-8"))
    return meth(*_newArgs)

class PageBuilder(object):

    request = None
    args = None
    basePath = None
    path = "."
    headers = property(lambda self: self.request.headers_out)
    encoding = "utf-8"
    fileCache = None

    def __init__(self, basePath, request):
        self.basePath = basePath
        self.request = request
        self.args = self._extractGetArgs(
            request.subprocess_env["QUERY_STRING"])
        self.path = self._fixPath(self.args.get("path", "."))
	_tblName = "".join((_p.title() for _p in basePath.split(os.sep)))
	self.fileCache = _fileList.getTableClass(_tblName)

    def _extractGetArgs(self, query):
        _out = {}
        for (_name, _value) in cgi.parse_qsl(query):
            _out[_name] = unicode(_value, self.encoding)
        return _out

    def _fixPath(self, path):
        _path = os.path.normpath(os.path.join(self.basePath, path))
        if not _path.startswith(self.basePath) or not osCall(
            os.path.exists, _path):
            _path = self.basePath
        return _path

    def getPage(self):
    	if "update" in self.args:
	    _out = self.updateDatabase()
        elif "search" in self.args:
            _out = self.genPage(self.doSearch(self.args["search"]))
        elif osCall(os.path.isdir, self.path):
            _out = self.genPage(self.listDir(self.path))
        else:
            _out = self.sendFile(self.path)
        return _out

    def updateDatabase(self):
    	_written = 0
        self.fileCache.clearTable()
	_needToVisit = [self.basePath]
	_visitedDirs = [self.basePath]
	while _needToVisit:
		_path = _needToVisit.pop()
		for (_root, _dirs, _files) in os.walk(_path):
			for _file in _files:
		    		_fullPath = os.path.join(_root, _file).decode(
					"utf-8")
		    		self.fileCache(path=_fullPath)
		    		_written += 1
			for _dir in _dirs:
		    		_fullPath = os.path.join(_root, _dir)
                                _size = os.stat(_fullPath).st_size
                                _obj = self.fileCache(path=_fullPath.decode("utf-8"), attributes={"size": _size})
		    		_written += 1
				if os.path.islink(_fullPath):
				    _realPath = os.path.realpath(_fullPath)
				    if _realPath in _visitedDirs:
				    	continue
				    else:
				        _visitedDirs.append(_realPath)
				    _needToVisit.append(_fullPath)
	return "%i files and directories added to database.\n" \
	    "Visited dirs: %s" % (_written, _visitedDirs)

    def sendFile(self, path):
        self.request.content_type = "application/octet-stream"
        self.headers["Content-Length"] = str(osCall(os.stat, path).st_size)
        self.headers["Content-Disposition"] = 'attachment; filename="%s"' % \
            osCall(os.path.split, path)[1]
        self.headers["Content-Transfer-Encoding"] = "binary"
        return osCall(file, path, "rb").read()

    def genPage(self, contents):
        self.request.content_type = "text/html"
        _root = ET.Element("html")
        _root.append(self._getHead())
        _root.append(self._getBody(contents))
        # output
        _io = StringIO()
        ET.ElementTree(_root).write(_io, self.encoding)
        _io.seek(0)
        return _io.read()

    def listDir(self, path):
        (_dirs, _files) = self.ls(self.path)
        _dirs.insert(0, "..")
        _table = ET.Element("table", border="1")
        for _dir in _dirs:
            _table.append(
                self._tableRow("DIR", lambda: self._fileLink(_dir), "0", "b"))
        for _file in _files:
            _call = lambda: self._fileLink(_file["name"])
            (_size, _name) = self.fixSize(_file["size"])
            _table.append(
                    self._tableRow("file", _call, _size, _name))
        return (self._getSearchMenu(), _table)

    def fixSize(self, size):
        if not isinstance(size, float):
            size = float(size)
        _sizes = ("kb", "mb", "gb", "tb")
        _size = "b"
        _pos = 0
        while size > 1024:
            _size = _sizes[_pos]
            size = size / 1024
            _pos += 1
        return (("%.2f" % size), _size)

    def doSearch(self, pattern):
        _pattern = pattern.lower()
        _pattern2 = slugify(_pattern)
        self.path = self.basePath
        _list = []
        for _filePath in self.fileCache.select():
	    (_root, _file) = os.path.split(_filePath.path)
            _fileCmp = _file.lower()
            _fileCmp2 = slugify(_fileCmp)
            if (_pattern in _fileCmp) or (_pattern2 in _fileCmp2):
                _likeness = 1.0
            else:
                _likeness1 = SequenceMatcher(
                    None, _pattern, _fileCmp).ratio()
                _likeness2 = SequenceMatcher(
                    None, _pattern2, _fileCmp2).ratio()
                _likeness = max(_likeness1, _likeness2)
            _list.append((_likeness, _file, _root))
        def _cmpList(s1, s2):
            _out = cmp(s2[0], s1[0])
            if _out == 0:
                _out = cmp(len(s2[1].lower()), len(s1[1].lower()))
            return _out
        _list.sort(cmp=_cmpList)
        _list = _list[:80]
        _table = ET.Element("table", border="1")
        for (_ratio, _file, _root) in _list:
            _dir = _root[len(self.basePath)+1:]
            _table.append(
                self._tableRow("%i%%" % (_ratio * 100),
                    lambda: self._fileLink(_file, _root), _dir
                ))
        _linkToRoot = ET.Element("a", href="?path=")
        _linkToRoot.text = "<ROOT>"
        return (_linkToRoot, self._getSearchMenu(), _table)

    def _getHead(self):
        _head = ET.Element("head")
        _meta = ET.SubElement(
            _head, "meta", content="text/html", charset=self.encoding)
        _meta.attrib["http-equiv"] = "content-type"
        _title = ET.SubElement(_head, "title")
        _path = self.path[len(self.basePath)+1:]
        if not _path:
            _path = "<root>"
        _title.text = "Library: %r" % _path
        return _head

    def ls(self, path):
        _list = osCall(os.listdir, path)
        _dirs = []
        _files = []
        for _name in _list:
            _name = _name.decode("utf-8")
            _fullPath = os.path.join(path, _name)
            if osCall(os.path.isdir, _fullPath):
                _dirs.append(_name)
            else:
                _size = os.stat(_fullPath.encode("utf-8")).st_size
                _files.append({"name": _name, "size": _size})
        _dirs.sort()
        _files.sort()
        return (_dirs, _files)

    def _getBody(self, contents):
        _body = ET.Element("body", bgcolor="#ffffff")
        for _element in contents:
            _body.append(_element)
        return _body

    def _getSearchMenu(self):
        _form = ET.Element("form", method="GET")
        ET.SubElement(_form, "input", type="text", name="search")
        ET.SubElement(_form, "input", type="submit", value="Search!")
        _up = ET.SubElement(_form, "a", href="?update=now")
	_up.text = "Update database (slow!)"
        return _form

    def _tableRow(self, *args):
        _row = ET.Element("tr")
        for _arg in args:
            _child = ET.SubElement(_row, "td")
            if callable(_arg):
                _child.append(_arg())
            else:
                if not isinstance(_arg, basestring):
                    _arg = str(_arg)
                _child.text = _arg
        return _row

    def _fileLink(self, name, path=None):
        if not path:
            path = self.path
        if name == "..":
            _relPath = os.path.split(path)[0]
        else:
            _relPath = os.path.join(path, name)
	if _relPath.startswith(self.basePath):
	    _relPath = _relPath[len(self.basePath)+1:]
	else:
	    _relPath = "/"
        _ancor = ET.Element(
            "a", href="?path=%s" % _relPath)
        _ancor.text = name
        return _ancor

# vim: set et sts=4 sw=4 :
