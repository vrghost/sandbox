import os
import urllib

class simpleHandler(object):

    method = None
    instance = None

    def __init__(self, method):
        if not callable(method):
            raise ValueError
        self.method = method

    def __call__(self, *args, **kwargs):
        kwargs["self"] = self.instance
        return self.method(*args, **kwargs)

    def __get__(self, instance, owner):
        if instance is not None:
            self.instance = instance
        return self

class pageHandler(simpleHandler):

    def __call__(self, *args, **kwargs):
        _out = super(pageHandler, self).__call__(*args, **kwargs)
        if not isinstance(_out, basestring):
            _out = str(_out)
        return _out

class BasePage(object):

    req = None
    base = "/var/www"
    sep = "/"
    public = os.path.join(base, "public")

    def __init__(self, req):
        self.req = req

    @classmethod
    def getHandlers(cls):
        _out = []
        for _name in dir(cls):
            _val = getattr(cls, _name)
            if isinstance(_val, simpleHandler):
                _out.append(_name)
        return _out

    def getUrl(self, path):
        if os.path.isabs(path):
            # srtip 'base' from it
            assert path.startswith(self.base)
            _pathParts = path.split(os.sep)
            _baseParts = self.base.split(os.sep)
            _relPath = _pathParts[len(_baseParts):]
            # this is abs. path for www client
            path = self.sep + self.sep.join(_relPath)
        return urllib.pathname2url(path)

def _getHandler(klass, name):
    def _handler(req):
        _obj = klass(req)
        return getattr(_obj, name)()
    return _handler

def updateModuleHandlers(glob, *cls):
    for _klass in cls:
        for _name in _klass.getHandlers():
            glob[_name] = _getHandler(_klass, _name)

# vim: set et sts=4 sw=4 :
