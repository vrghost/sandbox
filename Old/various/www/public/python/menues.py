import os

import _fileList, pageBase

class MenuList(_fileList.FileList):

    fileDir = _fileList.FileList.base

    def shouldIndex(self, fname):
        _path = os.path.join(self.fileDir, fname)
        return os.path.isdir(_path) and fname not in (".svn", "public", "phpmyadmin")

    def getFileList(self):
    	_out = super(MenuList, self).getFileList()
        _addFiles = ("viewsvn", "LU_wiki", )
	_out.extend((os.path.join(self.fileDir, _file) 
	    for _file in _addFiles))
        return _out

    @pageBase.pageHandler
    def refresh(self):
        return self.refreshTableContents()

    @pageBase.simpleHandler
    def getMenus(self):
        return [_el.path for _el in self._tableCls.fetchall()]

pageBase.updateModuleHandlers(globals(), MenuList)
# vim: set et sts=4 sw=4 :
