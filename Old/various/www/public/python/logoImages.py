import os

import _fileList, pageBase

class LogoImage(_fileList.FileList):

    fileDir = os.path.join(_fileList.FileList.public, "images", "__random")

    def shouldIndex(self, fname):
        return fname not in ("Thumbs.db", ".svn")

    @pageBase.pageHandler
    def refresh(self):
        return self.refreshTableContents()

    @pageBase.pageHandler
    def getRandomLogo(self):
        return self._tableCls.getRandom().path

pageBase.updateModuleHandlers(globals(), LogoImage)
# vim: set et sts=4 sw=4 :
