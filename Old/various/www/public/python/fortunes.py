import os

from database import Fortunes
import pageBase

class FortuneHolder(pageBase.BasePage):

    fortuneRoot = "/usr/share/games/fortunes/"

    @pageBase.pageHandler
    def refresh(self):
        """Sync system fortunes with database-stored ones."""
        _files = (
            open(os.path.join(self.fortuneRoot, _name))
            for _name in os.listdir(self.fortuneRoot)
            if os.path.splitext(_name)[1] == ""
        )
        _fortunes = []
        _fortune = u""
        for _file in _files:
            for _line in _file.readlines():
                _line = _line.decode("latin1")
                if _line.strip() == "%":
                    if _fortune:
                        _fortunes.append(_fortune)
                        _fortune = ""
                else:
                    _fortune += _line
        else:
            if _fortune:
                    _fortunes.append(_fortune)
        Fortunes.clearTable()
        for _fortune in _fortunes:
            Fortunes(fortune=_fortune)
        return "%i fortunes written to database" % len(_fortunes)

    @pageBase.pageHandler
    def getRandomFortune(self):
        return Fortunes.getRandom().fortune

pageBase.updateModuleHandlers(globals(), FortuneHolder)
# vim: set et sts=4 sw=4 :
