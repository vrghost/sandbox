import os
from sqlobject import classregistry

from database import _FileList as DbRootCls
import pageBase

def getTableClass(name):
    _classLabel = "_%sTableClass" % name
    _globals = globals()
    if _classLabel in _globals:
        _out = _globals[_classLabel]
    else:
        class _Cls(DbRootCls):

	    def __classinit__(cls, newAttrs):
	    	cls.__name__ = name
		DbRootCls.__classinit__(cls, newAttrs)
        _globals[_classLabel] = _Cls
        _out = _Cls
    return _out

class FileList(pageBase.BasePage):

    fileDir = None
    tableCls = None

    def __init__(self, *args, **kwargs):
        super(FileList, self).__init__(*args, **kwargs)
        _name = self.__class__.__name__
        _cls = getTableClass(_name)
        self._tableCls = _cls

    def refreshTableContents(self):
        """Sync system fortunes with database-stored ones."""
	_files = self.getFileList()
        self._tableCls.clearTable()
        for _file in _files:
            assert _file.startswith(self.base)
            self._tableCls(path=self.getUrl(_file))
        return "%i files added." % len(_files)

    def shouldIndex(self, fname):
        return True

    def getFileList(self):
        return [
            os.path.join(self.fileDir, _file)
            for _file in os.listdir(self.fileDir)
            if self.shouldIndex(_file)
        ]

# vim: set et sts=4 sw=4 :
