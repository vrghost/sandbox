import os

from database import Animals
import pageBase

class AnimalHolder(pageBase.BasePage):

    animalFile = "/var/www/public/ascii_animals.txt"

    @pageBase.pageHandler
    def refresh(self):
        """Sync system fortunes with database-stored ones."""
        _animals = []
        _animal = u""
        for _line in open(self.animalFile):
            _line = _line.decode("latin1")
            if _line.startswith("#_#!"):
                if _animal:
                    _animals.append(_animal)
                    _animal = ""
            else:
                _animal += _line
        else:
            if _animal:
                    _animals.append(_animal)
        Animals.clearTable()
        for _animal in _animals:
            Animals(animal=_animal)
        return "%i animals written to database" % len(_animals)

    @pageBase.pageHandler
    def getRandomAnimal(self):
        return Animals.getRandom().animal

pageBase.updateModuleHandlers(globals(), AnimalHolder)
# vim: set et sts=4 sw=4 :
