import sqlobject

MySQLConnection = sqlobject.mysql.builder()
connection = MySQLConnection(user="www", password="XXX", db="www", sqlobject_encoding="utf8", charset="utf8")
sqlobject.sqlhub.processConnection = connection

class _BaseTable(sqlobject.SQLObject):

    def __classinit__(cls, newAttrs):
	sqlobject.SQLObject.__classinit__(cls, newAttrs)
    	cls.createTable(ifNotExists=True)

    @classmethod
    def getRandom(cls):
        return cls.fetchall(limit=1, orderBy=sqlobject.func.RAND())[0]

    @classmethod
    def fetchall(cls, *args, **kwargs):
        return tuple(cls.select(*args, **kwargs))

class Fortunes(_BaseTable):

    fortune = sqlobject.UnicodeCol()

class Animals(_BaseTable):

    animal = sqlobject.UnicodeCol()

# file lists

class _FileList(_BaseTable):

    path = sqlobject.UnicodeCol()
    attributes = sqlobject.PickleCol()

# vim: set et sts=4 sw=4 :
