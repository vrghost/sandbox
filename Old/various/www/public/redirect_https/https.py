from apache import apache
from cgi import parse_qsl
from urllib import quote

def index(req):
    _url = [req.hostname]
    _args = dict(parse_qsl(req.args))
    if "url" in _args:
        _url.append(quote(_args["url"]))
    _url = "https://" + "/".join(_url)
    req.headers_out['location'] = _url
    req.status = apache.HTTP_MOVED_PERMANENTLY
    return "You are being redirected to %s" % _url

# vim: set et sts=4 sw=4 :
