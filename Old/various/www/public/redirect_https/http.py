from apache import apache

def index(req):
    _url = [req.hostname]
    _path = req.uri.strip("/")
    _url.extend(_path.split("/")[:-1])
    _url.append("https.py")
    _url = "https://" + "/".join(_url)
    if req.args:
        _url += "?" + req.args
    req.headers_out['location'] = _url
    req.status = apache.HTTP_MOVED_PERMANENTLY
    return "You are being redirected to %s" % _url

# vim: set et sts=4 sw=4 :
