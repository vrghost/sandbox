# -*- coding: utf8 -*-

from www.public.python import fileList

def index(req):
    _pb = fileList.PageBuilder("/mnt/sda2/Books", req)
    _out = _pb.getPage()
    if not isinstance(_out, basestring):
        _out = str(_out)
    return _out

# vim: set et sts=4 sw=4 :
