import sys

# backup original modules
_origSysModules = sys.modules.copy()

import elementtree.ElementTree as ET
from StringIO import StringIO

from www.public.python import fortunes, animals, logoImages, cssFiles, menues

def index(req):
    _ib = IndexBuilder(req, "/var/www")
    _out = _ib.getPage()
    if not isinstance(_out, basestring):
        _out = str(out)
    return _out

def refresh(req):
    _modules = (fortunes, animals, logoImages, cssFiles, menues)
    _out = ""
    for _mod in _modules:
        _meth = getattr(_mod, "refresh")
        _out += "%s: %r\n" % (_mod.__name__, _meth(req))
    return _out

def test(req):
    return menues.refresh(req)

def reload(req):
    # reload modules
    sys.modules = _origSysModules.copy()
    return "Modules reloaded.\n" \
    	"\tCurrently loaded modules:\n%s" % "\n".join(sys.modules.keys())

class IndexBuilder(object):

    encoding = "utf-8"
    root = None
    request = None

    def __init__(self, request, root):
        request.content_type = "text/html"
        self.request = request
        self.root = root

    def getPage(self):
        _root = ET.Element("html")
        _root.append(self.getHead())
        _root.append(self.getBody())
        _io = StringIO()
        ET.ElementTree(_root).write(_io, self.encoding)
        _io.seek(0)
        return _io.read()

    def getHead(self):
        _head = ET.Element("head")
        _meta = ET.SubElement(_head, "meta",
            content="text/html", charset=self.encoding)
        _meta.attrib["http-equiv"] = "content-type"
        _meta = ET.SubElement(_head, "meta",
            content="no-cache")
        _meta.attrib["http-equiv"] = "cache-control"
        _title = ET.SubElement(_head, "title")
        _title.text = "Hoopoe server"
        for _css in cssFiles.getFiles(self.request):
            ET.SubElement(_head, "link",
                rel="stylesheet", type="text/css", href=_css)
        return _head

    def getBody(self):
        _body = ET.Element("body", bgcolor="#ffffff")
        _table = ET.SubElement(_body, "table", width="100%")
        _table.append(self._addRow(self.getImage(), self.getMenu()))
	ET.SubElement(_body, "script", type="text/javascript", src="https://ssl.google-analytics.com/ga.js")
	_googleAn2 = ET.SubElement(_body, "script", type="text/javascript")
	_googleAn2.text = r"""
		var pageTracker = _gat._getTracker("UA-3344125-1");
		pageTracker._initData();
		pageTracker._trackPageview();
	"""
        return _body

    def _addRow(self, *args):
        _tr = ET.Element("tr")
        for _arg in args:
            _elem = ET.SubElement(_tr, "td")
            _elem.append(_arg)
        _elem.attrib["width"] = "100%"
        return _tr

    def getImage(self):
        _imgRelPath = ("public", "images", "__random")
        _image = logoImages.getRandomLogo(self.request)
        return ET.Element("img", src=_image)

    def getMenu(self):
        _div = ET.Element("div")
        ET.SubElement(_div, "div").append(self.getCow())
        ET.SubElement(_div, "div").text = "What are you doing here?"
        ET.SubElement(_div, "div").append(self.getList())
        return _div

    def getCow(self):
        _tt = ET.Element("tt")
        _pre = ET.SubElement(_tt, "pre")
        _pre.text = self.applyAnimal(self.getFortune())
        return _tt

    def getList(self):
        _correctList = menues.getMenus(self.request)
        _correctList.sort()
        _root = ET.Element("div")
        _hrefMask = "https://%s%%s" % self.request.hostname
        for _el in _correctList:
            _a = ET.SubElement(_root, "a", href=_hrefMask % _el)
            _a.text = _el.strip("/")
            ET.SubElement(_root, "br")
        return _root

    def getBalloon(self, text):
        _maxLen = max((len(_line) for _line in text.splitlines()))
        _len = _maxLen + 2
        _balloon = " " + ("-" * _len) + "\n"
        for _line in text.splitlines():
            _balloon += "(%s)\n" % _line.expandtabs(1).center(_len)
        _balloon += " " + ("-" * _len)
        return _balloon

    def getAnimal(self):
        return animals.getRandomAnimal(self.request)

    def applyAnimal(self, text):
        _animal = self.getAnimal()
        _balloon = self.getBalloon(text)
        _out = _balloon + "\n"
        _indent = 11
        _spaces = " " * _indent
        _animLines = [
            _spaces + _line
            for _line in _animal.splitlines()
        ]
        _animLines[0] = _animLines[0][:7] + "O" + _animLines[0][8:]
        _animLines[1] = _animLines[1][:8] + "o" + _animLines[1][9:]
        _out += "\n".join(_animLines)
        return _out

    def getFortune(self):
        return fortunes.getRandomFortune(self.request)


# vim: set et sts=4 sw=4 :
