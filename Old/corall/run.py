import os
import random
import math

from PIL import Image, ImageDraw, ImageColor, ImageEnhance

class StateObj(object):

    _data = None

    def __init__(self, fname, empty=False):
        self._fname = fname
        if empty:
            self._data = {}
        else:
            self._parseFile()

    def _parseFile(self):
        _data = {}
        for _line in file(self._fname):
            (_name, _value) = _line.strip().split("=", 1)
            _data[_name] = eval(_value)
        self._data = _data

    def save(self):
        _text = "\n".join(
            "%s=%r" % (_name, _value)
            for (_name, _value) in self._data.iteritems())
        file(self._fname, "w").write(_text)

    def __getattr__(self, name):
        return self._data[name]

    def __setattr__(self, name, value):
        if name.startswith("_"):
            self.__dict__[name] = value
        else:
            self._data[name] = value


class CorallBowl(object):

    imgPath = property(
        lambda s: os.path.join(
            s.sysConfig.image.dir, s.sysConfig.image.filename
    ))
    statePath = property(
        lambda s: os.path.join(
            s.sysConfig.image.dir, s.sysConfig.system.curState
    ))

    sysConfig = pilImg = state = None

    def __init__(self, config, empty=False, baseImg=None):
        self.sysConfig = config
        self._readImage(empty, baseImg)

    def _readImage(self, isEmpty, baseImg):
        if os.path.isfile(self.imgPath) and not isEmpty:
            self.pilImg = Image.open(self.imgPath)
            self.state = StateObj(self.statePath)
        else:
            self._createImage(baseImg)

    def _createImage(self, base):
        _w = self.sysConfig.image.width
        _h = self.sysConfig.image.height
        if not base:
            self.pilImg = Image.new("RGB", (_w, _h), self.sysConfig.draw.bgColour)
        else:
            self.pilImg = base
        _draw = ImageDraw.Draw(self.pilImg)
        # set boundary box
        _draw.ellipse(
            (0, 0, _w-1, _h-1), outline=self.sysConfig.draw.frameColour)
        # set root point
        _draw.point((_w/2, _h/2), fill=self.sysConfig.draw.plantColour)
        # set empty state
        self.state = StateObj(self.statePath, empty=True)
        self.state.lastGrow = (_w/2, _h/2)

    def _randomStartPoint(self):
        _r = min((
            self.sysConfig.image.width,
            self.sysConfig.image.height,
        )) / 2
        _deg = random.randint(0, 360) + random.random()
        _rad = math.radians(_deg)
        _x = math.sin(_rad) * _r
        _y = math.cos(_rad) * _r
        return (_r + _x, _r + _y)


    def growPixel(self):
        (_x1, _y1) = map(int, self._randomStartPoint())
        (_x2, _y2) = map(int, self.state.lastGrow)
        _iterLen = max(abs(_x1 - _x2), abs(_y1 - _y2))
        _xStep = (_x2 - _x1) / (1.0 * _iterLen)
        _yStep = (_y2 - _y1) / (1.0 * _iterLen)
        # now search for first growen pixel
        _prev = None
        _growCol = ImageColor.getrgb(self.sysConfig.draw.plantColour)
        for _id in xrange(0, _iterLen+1):
            _x = _x1 + _xStep * _id
            _y = _y1 + _yStep * _id
            _pixel = self.pilImg.getpixel((_x, _y))
            if _pixel == _growCol:
                # found growable pixel
                if not _prev:
                    raise Exception("No place to grow to.")
                else:
                    self.pilImg.putpixel(_prev, _growCol)
                    self.state.lastGrow = _prev
                    break
            _prev = map(int, (_x, _y))

    def save(self):
        self.pilImg.save(self.imgPath)
        self.state.save()

    @classmethod
    def fromBowl(cls, otherBowl):
        _img = otherBowl.pilImg
        _enh = ImageEnhance.Brightness(_img)
        _img = _enh.enhance(0.4)
        _obj = cls(otherBowl.sysConfig, empty=True, baseImg=_img)
        return _obj

if __name__ == "__main__":
    import config
    _bowl = CorallBowl(config.root)
    for _x in xrange(200):
        try:
            _bowl.growPixel()
        except Exception, _err:
            print _err
            _bowl = CorallBowl.fromBowl(_bowl)
    _bowl.save()

# vim: set sts=4 sw=4 et :
