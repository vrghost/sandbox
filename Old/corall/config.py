import os

class _AttrDict(dict):

    def __getattr__(self, name):
        return self[name]

root = _AttrDict({
    "image": _AttrDict({
        "filename": "grow.png",
        "width": 200,
        "height": 200,
        "dir": os.path.split(__file__)[0]
    }),
    "draw": _AttrDict({
        "bgColour": "#000000",
        "plantColour": "#FF002E",
        "frameColour": "#0000FF",
    }),
    "system": _AttrDict({
        "curState": "grow.txt",
    }),
})

# vim: set sts=4 sw=4 et :
