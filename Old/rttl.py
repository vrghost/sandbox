import re
import subprocess

class _RttlNote(object):

    _player = None
    _duration = _note = _sharp = _dots = _octave = None

    _freqs = (
        ("p", 1), # pause
        ("c", 261.6),
        ("d", 293.7),
        ("e", 329.6),
        ("f", 349.2),
        ("g", 392.0),
        ("a", 440.0),
        ("b", 493.9),
        ("c5", 523.2),
    )

    def __init__(self, player,
        duration, note, sharp,
        dots, octave
    ):
        self._player = player
        if duration:
            self._duration = int(duration)
        self._note = note
        self._sharp = bool(sharp)
        self._dots = len(dots) if dots else 0
        if octave:
            self._octave = int(octave)

    @property
    def duration(self):
        if self._duration:
            _rv = self._duration
        else:
            _rv = self._player.getNoteDefault("duration")
        return _rv

    @property
    def octave(self):
        if self._octave:
            _rv = self._octave
        else:
            _rv = self._player.getNoteDefault("octave")
        return _rv

    note = property(lambda s: s._note)
    sharp = property(lambda s: "#" if s._sharp else "")
    dots = property(lambda s: "." * s._dots)


    def __repr__(self):
        _note = "".join(map(str, (
                self.duration,
                self._note,
                self.sharp,
                self.dots,
                self.octave,
        )))
        return "<%s:%s>" % (self.__class__.__name__, _note)

    @staticmethod
    def _beep(hz, msec):
        _proc = subprocess.call((
            "beep",
            "-f", "%.2f" % hz,
            "-l", "%i" % msec,
        ))

    def getBeep(self):
        _beatTime = (1000. * 60.) / self._player.bpm
        _actualBeats = 1.0 / self.duration
        _dotSize = _actualBeats / 2.0
        for _ii in xrange(self._dots):
            _actualBeats += _dotSize
            _dotSize /= 2.0
        _msec = _actualBeats * _beatTime
        for (_pos, (_name, _freq)) in enumerate(self._freqs):
            if _name == self.note:
                if self.sharp:
                    _hz = _freq + \
                        (self._freqs[_pos + 1][1] - _freq) / 2.
                else:
                    _hz = _freq
                break
        else:
            raise ValueError("Unknown note %s" % self)
        return (_hz, _msec)

    def play(self):
        self._beep(*self.getBeep())

class RttlMelodyPlayer(object):

    _melody = _name = _settings = _notes = None

    def __init__(self, melody):
        self._melody = melody
        self._parseMelody()

    def __repr__(self):
        return "<%s:melody=%r>" % (
            self.__class__.__name__, self._melody)

    def getNoteDefault(self, name):
        return self._settings[name.lower()]

    bpm = property(lambda s: s.getNoteDefault("bpm"))

    def _parseMelody(self):
        try:
            (_name, _settings, _notes) = self._melody.split(":")
        except ValueError:
            raise ValueError(
                "Failed to extract melody parts from %r" % \
                self.melody
            )
        self._name = _name.strip()
        # Parse settings
        _parsedSettings = {}
        _settingNames = {
            "d": "duration",
            "o": "octave",
            "b": "bpm",
        }
        try:
            for _el in _settings.lower().split(","):
                    (_name, _value) = _el.strip().split("=")
                    _parsedSettings[_settingNames[_name]] = \
                        int(_value)
        except ValueError:
            raise ValueError(
                "Failed to parse settings %r" % _settings)
        self._settings = _parsedSettings
        # Parse notes
        _parsedNotes = []
        for _el in _notes.lower().split(","):
            _re = re.match(r"^\s*"
                r"(\d+)?" # duration
                r"([pabcdefg])" # note
                r"(#)?" # sharp
                r"(\.+)?" # dots
                r"(\d+)?" # octave
                r"\s*$", _el)
            _note = dict(zip(
                (
                    "duration",
                    "note",
                    "sharp",
                    "dots",
                    "octave",
                ),
                _re.groups()
            ))
            _parsedNotes.append(_RttlNote(player=self, **_note))
        self._notes = tuple(_parsedNotes)
        print (
            self._name,
            self._settings,
            self._notes
        )

    def play(self):
        for _note in self._notes:
            _note.play()


if __name__ == "__main__":
    _p = RttlMelodyPlayer(r"HauntedHouse: d=4,o=5,b=108: 2a4, 2e, 2d#, 2b4, 2a4, 2c, 2d, 2a#4, 2e., e, 1f4, 1a4, 1d#, 2e., d, 2c., b4, 1a4, 1p, 2a4, 2e, 2d#, 2b4, 2a4, 2c, 2d, 2a#4, 2e., e, 1f4, 1a4, 1d#, 2e., d, 2c., b4, 1a4")
    print _p
    _p.play()


# vim: set sts=4 sw=4 et :
