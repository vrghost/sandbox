import random
import math

from log import log

class PhysObj(object):

    _x = _y = 0
    _vx = _vy = 0

    def __init__(self, x, y):
        self.setPos(x, y)

    def setPos(self, x, y):
        self._x = x
        self._y = y

    def setSpeed(self, vx, vy):
        self._vx = vx
        self._vy = vy

    def getSpeed(self):
        return (self._vx, self._vy)

    def act(self):
        self._x += self._vx
        self._y += self._vy

    def posChanged(self):
        return self._vx != 0 or self._vy != 0

    def getPos(self):
        return (self._x, self._y)

    def toDict(self):
        return {
            "x": self._x,
            "y": self._y,
            "vx": self._vx,
            "vy": self._vy,
        }

class Bat(PhysObj):

    id = None
    _config = None
    _perpDeg = math.radians(90)
    _negDeg = math.radians(180)
    _horizDir = ("left", "right")
    _vertDir = ("up", "down")
    _origX = None

    def __init__(self, x, y, id, config):
        super(Bat, self).__init__(x, y)
        self._origX = x
        self._config = config
        self.id = id

    def _halfH(self):
        return self._config["height"] / 2

    def getRect(self):
        return (self._config["width"], self._config["height"])

    def act(self, world):
        super(Bat, self).act()
        (_w, _h) = world.getSizeTuple()
        if self._y < self._halfH():
            self._y = self._halfH()
        if self._y > _h - self._halfH():
            self._y = _h - self._halfH()
        _win = self._config["horizWindow"]
        if self._x > self._origX + _win:
            self._x = self._origX + _win
        elif self._x < self._origX - _win:
            self._x = self._origX - _win

    def toDict(self):
        _rv = super(Bat, self).toDict()
        _rv["id"] = self.id
        return _rv

    def processData(self, data):
        (_vx, _vy) = self.getSpeed()
        _dir = data["direction"]
        if data["action"] == "stop":
            if _dir in self._vertDir:
                _vy = 0
            elif _dir in self._horizDir:
                _vx = 0
        elif data["action"] == "move":
            if _dir in self._vertDir:
                _vy = self._config["speedY"]
                if _dir == "up":
                    _vy = -_vy
            elif _dir in self._horizDir:
                _vx = self._config["speedX"]
                if _dir == "left":
                    _vx = -_vx
        else:
            log("Unknown data: %r", data)
        self.setSpeed(_vx, _vy)

    def onCollision(self, ball):
        (_bx, _by) = ball.getPos()
        (_mx, _my) = self.getPos()
        _speed = ball.getTotalSpeed()
        _x = _mx - _bx
        _y = _my - _by
        if _y == 0:
            _deg = 0
        else:
            if _x == 0:
                _deg = self._perpDeg
                if _y > 0:
                    _deg += self._negDeg
            else:
                _tan = float(_y) / _x
                _deg = math.atan(_tan)
        if _x > 0:
            assert _x != 0
            _deg += self._negDeg
        ball.setSpeedVec(_speed*self._config["speedCoef"], _deg)

class Ball(PhysObj):

    _r = None
    _config = None
    _two = math.sqrt(2.0)

    def __init__(self, x, y, config):
        super(Ball, self).__init__(x, y)
        self._config = config
        self.setDefSpeed()

    def setDefSpeed(self):
        # give random speed to ball
        _total = self._config["speed"]
        _disp = random.random() * 2 * math.pi
        self.setSpeedVec(_total, _disp)
        self._vx = random.choice((self._vx, -self._vx))
        self._vy = random.choice((self._vy, -self._vy))

    def setSpeedVec(self, speed, deg):
        _vx = speed * math.cos(deg)
        _vy = speed * math.sin(deg)
        self.setSpeed(_vx, _vy)

    def getTotalSpeed(self):
        return math.sqrt(self._vx**2 + self._vy**2)

    def getR(self):
        return self._config["radius"]

    def act(self, world):
        super(Ball, self).act()
        (_w, _h) = world.getSizeTuple()
        # tob & bottom collisions
        if self._y < self.getR():
            self._y = self.getR()
            self._vy = -self._vy
        elif self._y > _h - self.getR():
            self._y = _h - self.getR()
            self._vy = -self._vy
        # left & right
        if self._x < self.getR() or self._x > _w - self.getR():
            world.onBallEscape(self, self._x)

    def toDict(self):
        _rv = super(Ball, self).toDict()
        _rv["radius"] = self._r
        return _rv

class Engine(object):

    _width = _height = None
    _bats = _ball = _app = None
    _enabled = False
    _justCollided = None

    def __init__(self, app, config):
        self._app = app
        self._width = config["width"]
        self._height = config["height"]
        self._enabled = app.physEnabled()
        _off = config["offset"]
        # init bats
        _bat1 = Bat(_off, self._height/2, 1, config["bat"])
        _bat2 = Bat(self._width - _off, self._height/2, 2, config["bat"])
        self._bats = (_bat1, _bat2)
        self._ball = Ball(self._width/2, self._height/2, config["ball"])

    def getSizeTuple(self):
        return (self._width, self._height)

    def getBatList(self):
        return [_bat.toDict() for _bat in self._bats]

    def act(self, event):
        if not self._enabled:
            return
        for _bat in self._bats:
            _bat.act(self)
            if _bat.posChanged():
                self._app.recvCommand({
                    "address": "app.evt.phys",
                    "type": "moved",
                    "object": "bat",
                    "data": _bat.toDict()
                })
        # act ball
        self._ball.act(self)
        self._app.recvCommand({
            "address": "app.evt.phys",
            "type": "moved",
            "object": "ball",
            "data": self._ball.toDict(),
        })
        self._detectCollisions()

    def _detectCollisions(self):
        _r = self._ball.getR()
        (_ballX, _ballY) = self._ball.getPos()
        for _bat in self._bats:
            (_w, _h) = _bat.getRect()
            (_batX, _batY) = _bat.getPos()
            _xDist = _w/2 + _r
            _yDist = _h/2 + _r
            if abs(_batX - _ballX) <= _xDist and abs(_batY - _ballY) <= _yDist:
                if self._justCollided == _bat.id:
                    return
                _bat.onCollision(self._ball)
                self._justCollided = _bat.id
                break
        else:
            self._justCollided = None

    def onBallEscape(self, ball, x):
        ball.setPos(self._width/2, self._height/2)
        ball.setDefSpeed()
        _diffs = {}
        for _bat in self._bats:
            _ballX = _bat.getPos()[0]
            _diffs[abs(_ballX - x)] = _bat
        _min = min(_diffs)
        _lostOne = _diffs[_min]
        self._justCollided = None
        self._app.recvCommand({
            "address": "app.evt.phys",
            "type": "oneLost",
            "object": ("ball", "bat"),
            "ball": ball.toDict(),
            "bat": _lostOne.toDict(),
        })

    def recvCommand(self, command):
        if "object" in command:
            _obj = command["object"]
            if _obj == "bat":
                self._onBatCommand(command)
            else:
                log("Unknown command %s", command)
        else:
            log("Unknown command %s", command)

    def _onBatCommand(self, command):
        if "action" not in command or "id" not in command:
            log("Unknown command %s", command)
            return
        _action = command["action"]
        _id = command["id"]
        for _bat in self._bats:
            if _bat.id == _id:
                _bat.processData(command)
                break
        else:
            log("Couldn't find bat %s", command)

def construct(app, config):
    return Engine(app, config)

# vim: set sts=4 sw=4 et :
