import sys
import getopt
import random
import time

from log import log

class App(object):

    _elements = _config = None
    _options = ("client", "server", "host=")
    usage = """
        Use --server option to initialize server.
        Use --client to initialize client.
        Use --host=x.x.x.x to give connection host.
    """
    _args = {}
    _routes = _hashedRoutes = None
    _srandSet = 0

    networkEnabled = lambda s: s.getArg("server") or s.getArg("client")
    isServer = lambda s: s.getArg("server")
    physEnabled = lambda s: not s.networkEnabled() or s.isServer()


    def __init__(self, routes, config):
        self._elements = {}
        self._config = config
        self._routes = routes
        (_args, _unparsed) = getopt.getopt(sys.argv[1:], (), self._options)
        if _unparsed:
            print self.usage
            raise Exception("terminated")
        self._args = dict(
            (_k.lstrip("-"), _v if _v else True) for (_k, _v) in _args)

    def getSeed(self):
        return random.randint(1, 2**20)

    def onConstructed(self):
        if self.networkEnabled():
            self.call("app.net", "connect")
            if self.isServer():
                self._initSrand()
            else:
                self._acquireSrand()

    def _acquireSrand(self):
        while not self._srandSet:
            self.call("app.net", "act")
            time.sleep(0.1)

    def _initSrand(self):
        self.recvCommand({
            "address": "app.srand",
            "seed": self.getSeed(),
        })

    def terminate(self):
        self.call("app.net", "terminate")

    def _hashRoutes(self):
        _hashed = {}
        for (_key, _targets) in self._routes.iteritems():
            _methods = []
            for (_member, _method) in _targets:
                _obj = self._getDest(_member)
                if not _obj:
                    continue
                _methods.append(getattr(_obj, _method))
            _hashed[_key] = _methods
        self._hashedRoutes = _hashed

    def getArg(self, name):
        return self._args.get(name, None)

    def setSrand(self, evt):
        self._srandSet += 1
        random.seed(evt["seed"])

    def addElement(self, obj, name):
        self._elements[name] = obj
        self._hashRoutes()

    def call(self, addr, method, args=(), kwargs={}):
        _obj = self._getDest(addr)
        return getattr(_obj, method)(*args, **kwargs)

    def recvCommand(self, command):
        _addr = command["address"]
        if _addr in self._hashedRoutes:
            # process logical address
            [
                _handle(command.copy())
                for _handle in self._hashedRoutes[_addr]
            ]
        else:
            _dst = self._getDest(_addr)
            if _dst:
                _dst.recvCommand(command)
            else:
                log("Failed to deliver %s", command)
                raise Ecxception

    def _getDest(self, addr):
        _parts = addr.split(".", 2)
        assert _parts[0] == "app"
        if len(_parts) == 1:
            return self
        (_app, _dest) = _parts[:2]
        return self._elements.get(_dest, None)


def construct(routes, config):
    return App(routes, config)

# vim: set sts=4 sw=4 et :
