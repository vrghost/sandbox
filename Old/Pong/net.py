import socket
import pickle
from select import select
import time

from log import log

class NetworkManager(object):

    _pickleProtocol = 2
    _port = None
    _host = "0.0.0.0"
    _socket = None
    _isServer = True
    _connected = False
    _socket = None
    _sndBuf = _rcvBuf = ""
    _sep = "\x00"
    _active = False

    _doStat = True
    _statData = None

    def __init__(self, app, config):
        self._pickleProtocol = config.get("pickleProtocol", 2)
        self._app = app
        self._port = config["port"]
        self._socket = None
        self._active = app.networkEnabled()
        if self._active:
            self._isServer = app.isServer()
            if self._isServer:
                self._host = config.get("host", "0.0.0.0")
            else:
                self._host = app.getArg("host")
        if self._doStat:
            self._statData = {}

    def pickle(self, object):
        return pickle.dumps(object, self._pickleProtocol)

    def unpickle(self, string):
        return pickle.loads(string)

    def prepare(self, object):
        return "".join(self.pickle(object).encode("base64").splitlines())

    def parse(self, string):
        return self.unpickle(string.decode("base64"))

    def terminate(self):
        if self._connected:
            self._socket.shutdown(socket.SHUT_RDWR)
            self._socket.close()

    def send(self, obj):
        if not self._active:
            return
        self._sndBuf += self.prepare(obj) + self._sep

    def connect(self):
        assert not self._connected
        if self._isServer:
            _srvSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            _srvSock.bind((self._host, self._port))
            _srvSock.listen(1)
            (self._socket, _addr) = _srvSock.accept()
            log("Connection from %s", _addr)
        else:
            _sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            _sock.connect((self._host, self._port))
            self._socket = _sock
        self._socket.setblocking(False)
        self._connected = True
        if self._doStat:
            self._statData.update({
                "startTime": time.time(),
                "inBytes": 0,
                "outBytes": 0,
                "lastPrinted": 0,
            })

    def act(self, event=None):
        if not self._active:
            return
        assert self._connected
        self._doSend()
        self._doReceive()
        _timeS = int(time.time())
        if self._doStat and \
            _timeS > self._statData["lastPrinted"] \
             and _timeS % 10 == 0:
            _tDiff = time.time() - self._statData["startTime"]
            _inKb = self._statData["inBytes"] / 1024.
            _outKb = self._statData["outBytes"] / 1024.
            print "Avg in:", _inKb / _tDiff, "kb/sec"
            print "Avg out:", _outKb / _tDiff, "kb/sec"
            self._statData["lastPrinted"] = _timeS

    def _doSend(self):
        if not self._sndBuf:
            return
        while self._sndBuf:
            _sent = self._socket.send(self._sndBuf)
            self._sndBuf = self._sndBuf[_sent:]
            if self._doStat:
                self._statData["outBytes"] += _sent

    def _doReceive(self):
        while True:
            try:
                _data = self._socket.recv(1)
            except socket.error:
                return
            if _data:
                self._rcvBuf += _data
                if self._doStat:
                    self._statData["inBytes"] += len(_data)
            if self._sep in self._rcvBuf:
                _parts = self._rcvBuf.split(self._sep)
                self._rcvBuf = _parts.pop()
                for _part in _parts:
                    if not _part:
                        continue
                    _obj = self.parse(_part)
                    _obj["address"] += ".transferred"
                    self._app.recvCommand(_obj)

def construct(app, config):
    return NetworkManager(app, config)

# vim: set sts=4 sw=4 et :
