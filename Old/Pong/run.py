import app
import gui
import phys
import net

routes = {
    "app.evt.phys": (
        ("app.gui", "recvCommand"),
        ("app.net", "send"),
    ),
    "app.evt.phys.transferred": (
        ("app.gui", "recvCommand"),
    ),
    "app.keyaction": (
        ("app.net", "send"),
        ("app.phys", "recvCommand"),
    ),
    "app.tick": (
        ("app.phys", "act"),
        ("app.net", "act"),
    ),
    "app.keyaction.transferred": (
        ("app.phys", "recvCommand"),
    ),
    "app.srand": (
        ("app", "setSrand"),
        ("app.net", "send"),
    ),
    "app.srand.transferred": (
        ("app", "setSrand"),
    ),
}

def run():
    import config
    _app = app.construct(routes, config.app)
    _phys = phys.construct(_app, config.phys)
    _app.addElement(_phys, "phys")
    _net = net.construct(_app, config.net)
    _app.addElement(_net, "net")
    _gui = gui.construct(_app, config.gui)
    _app.addElement(_gui, "gui")
    _app.onConstructed()
    try:
        _gui.mainLoop()
    finally:
        _app.terminate()

if __name__ == "__main__":
    run()


# vim: set sts=4 sw=4 et :
