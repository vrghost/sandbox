import pygame

from log import log

class GuiElement(object):

    _image = _rect = None

    def __init__(self, imgFname):
        self._image = pygame.image.load(imgFname).convert()
        self._rect = self._image.get_rect()

    def draw(self, screen):
        screen.blit(self._image, self._rect)

    def applyData(self, data):
        self._rect.centerx = data["x"]
        self._rect.centery = data["y"]

class Ball(GuiElement):
    pass

class Bat(GuiElement):

    id = None
    score = 0

    def __init__(self, imgFname, id):
        super(Bat, self).__init__(imgFname)
        self.id = id

class Gui(object):

    _keys = _app = _screen = None
    _timerid = pygame.USEREVENT + 1
    _redrawid = _timerid + 1
    _colors = None
    _ball = _bats = None
    _scoreFont = None

    def __init__(self, app, config):
        pygame.init()
        self._keys = config["keys"]
        self._colors = config["colors"]
        self._app = app
        _size = app.call("app.phys", "getSizeTuple")
        self._screen = pygame.display.set_mode(_size)
        self._scoreFont = pygame.font.Font(None, 72)
        pygame.time.set_timer(self._timerid, config["tick"])
        pygame.time.set_timer(self._redrawid, config["redraw"])
        self._ball = Ball(config["img"]["ball"])
        _bats = []
        for _data in app.call("app.phys", "getBatList"):
            _bat = Bat(config["img"]["bat"][_data["id"]], _data["id"])
            _bat.applyData(_data)
            _bats.append(_bat)
        self._bats = _bats

    def mainLoop(self):
        _working = True
        while _working:
            for _event in pygame.event.get():
                if _event.type == pygame.QUIT:
                    _working = False
                    continue
                elif _event.type == pygame.KEYDOWN:
                    self.onKeydown(_event)
                elif _event.type == pygame.KEYUP:
                    self.onKeyup(_event)
                elif _event.type == self._timerid:
                    self._app.recvCommand({"address": "app.tick"})
                elif _event.type == self._redrawid:
                    self._redraw()
            pygame.time.delay(10)

    def _redraw(self):
        self._screen.fill(self._colors["background"])
        self._drawScore()
        self._ball.draw(self._screen)
        for _bat in self._bats:
            _bat.draw(self._screen)
        pygame.display.flip()

    def _drawScore(self):
        _msg = ":".join(map(str, (_bat.score for _bat in self._bats)))
        _text = self._scoreFont.render(
            _msg, True, self._colors["score"], self._colors["background"])
        _rect = _text.get_rect()
        _scr = self._screen.get_rect()
        _rect.centerx = _scr.centerx
        _rect.centery = _scr.centery
        self._screen.blit(_text, _rect)

    def getKeyStat(self, code):
        for (_player, _keys) in self._keys.iteritems():
            _dir = _keys.get(code, None)
            if _dir:
                return (_player, _dir)
        return None

    def onKeydown(self, event):
        _stat = self.getKeyStat(event.key)
        if not _stat:
            log("Unknown key %s", event)
            return
        self._app.recvCommand({
            "address": "app.keyaction",
            "object": "bat",
            "action": "move",
            "direction": _stat[1],
            "id": _stat[0],
        })

    def onKeyup(self, event):
        _stat = self.getKeyStat(event.key)
        if not _stat:
            log("Unknown key %s", event)
            return
        self._app.recvCommand({
            "address": "app.keyaction",
            "object": "bat",
            "action": "stop",
            "direction": _stat[1],
            "id": _stat[0],
        })

    def recvCommand(self, command):
        if command["type"] == "moved":
            self._onMoved(command)
        elif command["type"] == "oneLost":
            self._onScore(command)
        else:
            log("Unknown command %s", command)

    def _onScore(self, command):
        _bat = self._getBat(command["bat"]["id"])
        if _bat:
            _bat.score += 1
        else:
            log("Unknown bat %s", command)

    def _onMoved(self, command):
        if command["object"] == "ball":
            self._ball.applyData(command["data"])
        elif command["object"]:
            _bat = self._getBat(command["data"]["id"])
            if _bat:
                _bat.applyData(command["data"])
            else:
                log("Unknown bat %s", command)
        else:
            log("Unknown command %r", command)

    def _getBat(self, id):
        for _bat in self._bats:
            if _bat.id == id:
                return _bat

def construct(app, config):
    return Gui(app, config)

# vim: set sts=4 sw=4 et :
