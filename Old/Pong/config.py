net = {
    "pickleProtocol": 2,
    "port": 4242,
}

phys = {
    "width": 640,
    "height": 480,
    "ball": {
        "radius": 23,
        "speed": 10,
    },
    "offset": 50,
    "bat":{
        "width": 20,
        "height": 100,
        "speedY": 10,
        "speedX": 5,
        "speedCoef": 1.05,
        "horizWindow": 30,
    },
}

app = {
}

import pygame

gui = {
    "keys": {
        1: {
            pygame.K_DOWN: "down",
            pygame.K_UP: "up",
            # ;-)
            #pygame.K_LEFT: "left",
            #pygame.K_RIGHT: "right",
        },
        2: {
            pygame.K_s: "down",
            pygame.K_w: "up",
            pygame.K_d: "right",
            pygame.K_a: "left",
        },
    },
    "img": {
        "ball": "ball.png",
        "bat": {
            1: "bat1.png",
            2: "bat2.png",
        }
    },
    "tick": 1000 / 25,
    "redraw": 1000 / 25,
    "colors": {
        "background": (255, 255, 255),
        "score": (0, 0, 0),
    }
}
# vim: set sts=4 sw=4 et :
