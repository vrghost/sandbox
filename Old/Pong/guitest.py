import sys, pygame
import config
import log

pygame.init()
#size = width, height = 320, 240
width, height = config.phys["width"], config.phys["height"]
size = (width, height)
speed = [2, 2]
black = 0, 0, 0

screen = pygame.display.set_mode(size)
# convert not nesscesary
ball = pygame.image.load("ball.png").convert()
ballrect = ball.get_rect()

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == config.gui["downkey"]:
                print "down key pressed"
            elif event.key == config.gui["upkey"]:
                print "up key pressed"

    ballrect = ballrect.move(speed)
    if ballrect.left < 0 or ballrect.right > width:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > height:
        speed[1] = -speed[1]

    screen.fill(black)
    screen.blit(ball, ballrect)
    pygame.display.flip()
    pygame.time.delay(10)
