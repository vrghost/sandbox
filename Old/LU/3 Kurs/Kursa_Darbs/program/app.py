import networkx as NX
import syck

from DB import db

import actors, logger

class AppRoot(logger.LoggerMixin):

    activeGraph = None
    nxGraph = None
    graphLayout = None

    def listGraphs(self):
        return actors.Graph.listAll()

    def saveActiveGraph(self):
        if not self.activeGraph:
            return
        self.activeGraph.save()

    def terminate(self):
        self.saveActiveGraph()
        db.syncToFile()

    def activateGraph(self, graph):
        self.saveActiveGraph()
        self.activeGraph = graph
        self._createNxGraph()

    def _createNxGraph(self):
        _nxGraph = NX.Graph()
        for _node in self.activeGraph.nodes:
            _nxGraph.add_node(_node)
        for _edge in self.activeGraph.edges:
            _nxGraph.add_edge(*_edge.nodes)
        self.nxGraph = _nxGraph
        self.graphLayout = NX.drawing.layout.spring_layout(_nxGraph)

    def getEvaporation(self):
        return self.activeGraph.evaporation

    def setEvaporation(self, value):
        assert value > 0 and value <= 1
        self.activeGraph.setEvaporation(value)

    def getEdges(self):
        return self.activeGraph.edges

    def getNodes(self):
        return self.activeGraph.nodes

    def importGraph(self, name, fname):
        _def = syck.load(file(fname))
        _def['name'] = name
        _gr = actors.Graph.fromDef(**_def)
        _gr.save()

    def getGraphLayout(self):
        return self.graphLayout

    def getNodeLayout(self, node):
        return self.graphLayout[node]

    def tick(self):
        if not self.activeGraph:
            return
        self.activeGraph.act()

    def clearPheromone(self):
        if not self.activeGraph:
            return
        for _obj in self.activeGraph.edges:
            _obj.setPheromone(0)

    def getPheromoneInfluence(self):
        if not self.activeGraph:
            return
        return self.activeGraph.pheromoneInfluence

    def setPheromoneInfluence(self, val):
        if not self.activeGraph:
            return
        self.activeGraph.setPheromoneInfluence(val)

    def getDesirabilityInfluence(self):
        if not self.activeGraph:
            return
        return self.activeGraph.desirabilityInfluence

    def setDesirabilityInfluence(self, val):
        if not self.activeGraph:
            return
        self.activeGraph.setDesirabilityInfluence(val)


# vim: set et sts=4 sw=4 :
