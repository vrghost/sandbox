import inspect
import random

from DB import memoryShadows
from executorQueue import ExecurorQueue

class BaseActor(object):

    data = None
    _dataCls = None
    _bindings = ()
    id = property(lambda s: s.data.id)

    @classmethod
    def _findDataCls(cls):
        cls._dataCls = getattr(memoryShadows, cls.__name__, object())

    @classmethod
    def fromDataObject(cls, data):
        _obj = cls()
        _obj._setData(data)
        return _obj

    @classmethod
    def fromDef(cls, **kwargs):
        raise NotImplementedError

    def _setData(self, data):
        assert isinstance(data, self._dataCls)
        self.data = data
        self.data.actor = self

    def save(self):
        self.data.sync()

    def __cmp__(self, other):
        return cmp(self.id, other.id)

class Graph(BaseActor):

    edges = property(lambda s: [_obj.actor for _obj in s.data.edges])
    nodes = property(lambda s: [_obj.actor for _obj in s.data.nodes])
    ants = property(lambda s: [_obj.actor for _obj in s.data.ants])
    foods = property(lambda s: [_obj.actor for _obj in s.data.foods])
    lairs = property(lambda s: [_obj.actor for _obj in s.data.lairs])
    evaporation = property(lambda s: s.data.evaporation)
    maxAntCount = property(lambda s: s.data.maxAntCount)
    pheromoneInfluence = property(lambda s: s.data.pheromoneInfluence)
    desirabilityInfluence = property(lambda s: s.data.desirabilityInfluence)
    name = property(lambda s: s.data.name)

    _executor = None

    def __init__(self, *args, **kwargs):
        super(Graph, self).__init__(*args, **kwargs)
        self._executor = ExecurorQueue(10)

    @classmethod
    def listAll(cls):
        return tuple(_obj.actor for _obj in cls._dataCls.getAll())

    @classmethod
    def fromDef(cls,
        name, evaporation, maxAntCount, pheromoneInfluence,
        desirabilityInfluence,
        nodes, edges, lairs, foods
        ):
        _cnt = cls._dataCls.selectBy(name=name).count()
        if _cnt > 0:
            raise Exception("Graph '%s' already exists." % name)
        _grData = cls._dataCls(
            name=name,
            evaporation=evaporation,
            maxAntCount=maxAntCount,
            pheromoneInfluence=pheromoneInfluence,
            desirabilityInfluence=desirabilityInfluence
        )
        _gr = cls.fromDataObject(_grData)
        # base graph structure
        for _node in nodes:
            Node.fromDef(graph=_gr, **_node)
        for _edge in edges:
            Edge.fromDef(graph=_gr, **_edge)
        # additional: layers & foods
        for _lair in lairs:
            Lair.fromDef(graph=_gr, **_lair)
        for _food in foods:
            Food.fromDef(graph=_gr, **_food)
        _gr.save()
        return _gr

    def getNodeByName(self, name):
        for _node in self.nodes:
            if _node.name == name:
                return _node
        raise Exception("Node with name '%s' not found." % name)

    def save(self):
        super(Graph, self).save()
        for _obj in self.edges:
            _obj.save()
        for _obj in self.nodes:
            _obj.save()
        for _obj in self.ants:
            _obj.save()

    def _spawnAnt(self):
        assert self.lairs
        _spawnNode = self.lairs[0].node
        Ant.fromDef(graph=self, node=_spawnNode)

    def act(self):
        while len(self.ants) < self.maxAntCount:
            self._spawnAnt()
        ############# call succeeding actors
        _executor = self._executor
        _executor.putlist(self.ants)
        _executor.join()
        _executor.putlist(self.edges)
        _executor.join()

    def setEvaporation(self, value):
        assert value > 0 and value <= 1
        self.data.evaporation = value

    def setPheromoneInfluence(self, val):
        assert val > 0 and val <= 2
        self.data.pheromoneInfluence = val

    def setDesirabilityInfluence(self, val):
        assert val > 0 and val <= 2
        self.data.desirabilityInfluence = val

class GraphElement(BaseActor):

   graph = property(lambda s: s.data.graph.actor)

class Node(GraphElement):

    name = property(lambda s: s.data.name)
    edges = property(lambda s: [_obj.actor for _obj in s.data.edges])
    ants = property(lambda s: [_obj.actor for _obj in s.data.ants])
    lair = property(lambda s: s.data.lair.actor if s.data.lair else None)
    food = property(lambda s: s.data.food.actor if s.data.food else None)

    @classmethod
    def fromDef(cls, graph, name):
        _count = cls._dataCls.selectBy(name=name, graph=graph.data).count()
        if _count > 0:
            raise Exception(
                "Node '%s' for graph '%s' exists." % (name, graph.data))
        _data = cls._dataCls(name=name, graph=graph.data)
        _obj = cls.fromDataObject(_data)
        _obj.save()
        return _obj

class Edge(GraphElement):

    nodes = property(lambda s: [_obj.actor for _obj in s.data.nodes])
    pheromone = property(lambda s: s.data.pheromone)
    price = property(lambda s: s.data.price)

    @classmethod
    def fromDef(cls, graph, nodes, price):
        assert len(nodes) == 2 and price != 0
        _data = cls._dataCls(
            graph=graph.data,
            price=price,
            pheromone=0,
            traveled=0
        )
        _obj = cls.fromDataObject(_data)
        _obj._setNodes(*[graph.getNodeByName(_node) for _node in nodes])
        _obj.save()
        return _obj

    def setPheromone(self, val):
        self.data.pheromone = val

    def _setNodes(self, nodeA, nodeB):
        assert len(self.data.nodes) == 0
        self.data.addNode(nodeA.data)
        self.data.addNode(nodeB.data)

    def contains(self, obj):
        return obj in self.nodes

    def otherNode(self, one):
        if one not in self.nodes:
            raise Exception("Incorrect node %s. (%s)" % (one.id, self))
        _oth = [_node for _node in self.nodes if _node != one]
        assert len(_oth) == 1
        return _oth[0]

    def travel(self):
        self.data.traveled += 1

    def act(self):
        _newPher = self.data.pheromone * self.graph.evaporation
        _newPher += self.data.traveled * (1.0/self.data.price)
        self.data.pheromone = _newPher
        self.data.traveled = 0

    def __repr__(self):
        _way =  "<->".join(str(_n.id) for _n in self.nodes)
        return "<%s:%s:%s>" % (self.__class__.__name__, self.id, _way)


class NodeElement(GraphElement):

    node = property(lambda s: s.data.node.actor)

class Lair(NodeElement):

    @classmethod
    def fromDef(cls, graph, node):
        _node = graph.getNodeByName(node)
        _data = cls._dataCls(
            graph=graph.data,
            node=_node.data
        )
        _obj = cls.fromDataObject(_data)
        _obj.save()
        return _obj

class Food(NodeElement):

    @classmethod
    def fromDef(cls, graph, node, amount):
        _node = graph.getNodeByName(node)
        _data = cls._dataCls(
            graph=graph.data,
            node=_node.data,
            amount=amount
        )
        _obj = cls.fromDataObject(_data)
        _obj.save()
        return _obj

class Ant(NodeElement):

    carryingFood = property(lambda s: s.data.carryingFood)
    path = property(lambda s: tuple(_e.actor for _e in s.data.path))

    def setPath(self, val):
        self.data.path = (_e.data for _e in val)
        assert self.path == tuple(val)

    @classmethod
    def fromDef(cls, graph, node):
        _data = cls._dataCls(
            graph=graph.data,
            node=node.data,
            carryingFood=False,
        )
        _obj = cls.fromDataObject(_data)
        _obj.save()
        return _obj

    def _chose(self, objs):
        _total = sum(objs.itervalues())
        _minSum = random.random() * _total
        for (_obj, _prob) in objs.iteritems():
            _minSum -= _prob
            if _minSum <= 0:
                return _obj
        raise Exception("Chose function contains an error")

    def act(self):
        if self.carryingFood:
            self._returnToLair()
        else:
            self._randomlyWander()

    def _randomlyWander(self):
        _node = self.node
        _graph = self.graph
        _probs = {}
        _epsilon = 0.0001
        for _edge in _node.edges:
            _ph = _edge.pheromone ** _graph.pheromoneInfluence
            _desirability = (1.0/_edge.price) ** \
                    _graph.desirabilityInfluence
            _desirability = 1
            _prob = _ph + _desirability + _epsilon
            _probs[_edge] = _prob
        self._travel(self._chose(_probs))
        if self.node.food:
            self.onFoundFood()

    def _travel(self, edge):
        if self.carryingFood:
            edge.travel()
        else:
            # loop prevention
            _path = self.path
            _node = self.node
            if _node in _path:
                _newPath = []
                for _obj in _path:
                    _newPath.append(_obj)
                    if _obj == _node:
                        break
                _path = _newPath
            else:
                _path += (self.node, )
            self.setPath(_path)
        self.data.node = edge.otherNode(self.node).data
        self.save()

    def _returnToLair(self):
        _path = self.path
        _target = _path[-1]
        _edge = None
        for _ed in self.node.edges:
            if _ed.contains(_target):
                _edge = _ed
                break
        assert _edge is not None
        self._travel(_edge)
        self.setPath(_path[:-1])
        if self.node.lair:
            self.onFoundLair()

    def setCarryingFood(self, val):
        self.data.carryingFood = val

    def onFoundFood(self):
        self.setCarryingFood(True)

    def onFoundLair(self):
        self.setCarryingFood(False)
        self.setPath([])

### actor initiation
_dummy = lambda: 42
for _obj in locals().values():
    _meth = getattr(_obj, "_findDataCls", _dummy)
    _meth()
del _meth, _dummy
## end initiation

def findClass(data):
    if inspect.isclass(data):
        dataCls = data
    else:
        dataCls = data.__class__
    _name = dataCls.__name__
    try:
        assert issubclass(dataCls, memoryShadows.DataObject)
    except:
        print dataCls
        raise
    for _cls in globals().values():
        if inspect.isclass(_cls) and issubclass(_cls, BaseActor):
            if _cls.__name__ == _name:
                return _cls
    raise Exception("'%s' not found." % _name)

# vim: set et sts=4 sw=4 :
