class LazyImport(object):

    _target = None
    _lib = None

    def __init__(self, target):
        self._target = target

    def __getattr__(self, name):
        if self._lib is None:
            self._lib = __import__(self._target)
        _attr = getattr(self._lib, name)
        if callable(_attr):
            setattr(self, name, _attr)
        return _attr

# vim: set et sts=4 sw=4 :
