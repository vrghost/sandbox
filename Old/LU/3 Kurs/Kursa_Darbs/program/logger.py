import sys
import traceback
from cStringIO import StringIO

def _writerHandler(level):
    def _handle(self, msg, *args):
        self._logMsg(level, msg, *args)
    return _handle

class LogWriter(object):

    _outputObjs = None
    log_debug = _writerHandler("debug")
    log_info = _writerHandler("info")
    log_error = _writerHandler("error")

    def __init__(self, objs):
        self._outputObjs = list(objs)

    def _write(self, msg):
        for _fobj in self._outputObjs:
            _fobj.write(msg)

    def _logMsg(self, level, msg, *args):
        if args:
            msg = msg % args
        _prefix = "[%s] " % level.upper()
        _msg = "\n".join(_prefix + _line for _line in msg.splitlines())
        _msg += "\n"
        self._write(_msg)

    def log_exception(self, msg, *args):
        if args:
            msg = msg % args
        (_typ, _val, _tb) = sys.exc_info()
        _sio = StringIO()
        traceback.print_exception(_typ, _val, _tb, None, _sio)
        _sio.seek(0, 0)
        _msg = _sio.read()
        _msg += "\n" + "Message: " + msg
        self._logMsg("exception", _msg)

def _loggerCall(funcName):
    def _handle(self, msg, *args):
        if self._logWriter:
            getattr(self._logWriter, funcName)(msg, *args)
        else:
            raise Exception("Logger not found. Message %s:%s not written" % \
                (msg, args))
    return _handle

class LoggerMixin(object):

    _logWriter = None
    log_debug = _loggerCall("log_debug")
    log_info = _loggerCall("log_info")
    log_error = _loggerCall("log_error")
    log_exception = _loggerCall("log_exception")


logWriter = None
def initLogging(fobjs):
    _obj = LogWriter(fobjs)
    globals()["logWriter"] = _obj
    LoggerMixin._logWriter = _obj

# vim: set et sts=4 sw=4 :
