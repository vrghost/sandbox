import wx

import main

class Gui(wx.App):

    _appObj = None

    def MainLoop(self):
        super(Gui, self).MainLoop()

    def guiInit(self, appObj):
        self._appObj = appObj
        main.showForm(appObj, self)

    def terminate(self):
        self._appObj.terminate()
        self.ExitMainLoop()

def initGui(appObj):
    global _appGui
    _appGui = Gui()
    _appGui.RestoreStdio()
    _appGui.guiInit(appObj)
    return _appGui

# vim: set et sts=4 sw=4 :
