import wx
import os

import xrc.import_xrc

xrc.initModule("import")

class ImportMenu(xrc.import_xrc.xrcGraphImport):

    _app = None
    _parent = None
    file_picker = None

    def __init__(self, parent, parentForm, app):
        super(ImportMenu, self).__init__(parent)
        self.file_picker = wx.xrc.XRCCTRL(self, "file_picker")
        self._parent = parentForm
        self._app = app

    def OnButton_import_button(self, evt):
        _fpath = self.file_picker.GetPath()
        _grname = self.name_imput.GetValue().strip()
        if os.path.isfile(_fpath) and _grname:
            _newGr = self._app.importGraph(_grname, _fpath)
            self._parent.OnGraphImport()
        self.Close()


def showForm(app, parent):
    _form = ImportMenu(None, parent, app)
    assert _form
    _form.Show()
    return _form

# vim: set et sts=4 sw=4 :
