import wx

import xrc.main_xrc
import importDialog

xrc.initModule("main")

class ObjectChoice(object):

    _src = None
    _name = None
    _objects = ()

    def __init__(self, srcObj, nameFunc, objList=()):
        self._src = srcObj
        self._name = nameFunc
        self.setObjects(objList)

    def setObjects(self, objList):
        self._objects = tuple(objList)
        self._src.Clear()
        for _obj in self._objects:
            self._src.Append(self._name(_obj))

    def getSelection(self):
        return self._objects[self._src.GetSelection()]

class AntSim(xrc.main_xrc.xrcSimRoot):

    _app = None
    _gui = None
    worldName = None
    draw_panel = None
    evaporation_text = None
    pheromone_text = None
    desirability_text = None
    run_timer = None

    nodes = None
    edges = None

    def __init__(self, parent, gui, app):
        super(AntSim, self).__init__(parent)
        self._initDrawing()
        self.draw_panel = wx.xrc.XRCCTRL(self, "draw_panel")
        self.evaporation_text = wx.xrc.XRCCTRL(self, "evaporation_text")
        self.pheromone_text = wx.xrc.XRCCTRL(self, "pheromone_text")
        self.desirability_text = wx.xrc.XRCCTRL(self, "desirability_text")
        self.run_timer = wx.Timer(self, wx.ID_ANY)
        self.draw_panel.Bind(wx.EVT_PAINT, self.OnDraw)
        self.Bind(wx.EVT_TIMER, self.OnTimer)
        self._app = app
        self._gui = gui
        self.worldName = ObjectChoice(
            self.world_name_choice,
            lambda obj: obj.name,
        )
        self.OnGraphImport()

    def _initDrawing(self):
        self.nodes = {
            'empty': {'pen': wx.Pen('black', 1), 'brush': wx.Brush('gray')},
            'food': {'pen': wx.Pen('black', 1), 'brush': wx.Brush('yellow')},
            'lair': {'pen': wx.Pen('black', 1), 'brush': wx.Brush('green')},
        }
        self.edges = {
            'blue': wx.Pen('blue', 3),
            'green1': wx.Pen((0, 255, 0), 3),
            'green2': wx.Pen((100, 200, 100), 3),
        }

    def OnGraphImport(self):
        self.worldName.setObjects(self._app.listGraphs())


    def OnClose(self, evt):
        self.run_timer.Stop()
        self._gui.terminate()

    def OnMenu_menu_import(self, evt):
        importDialog.showForm(self._app, self)

    def OnChoice_world_name_choice(self, evt):
        _selected = self.worldName.getSelection()
        self._app.activateGraph(_selected)
        self._enableGraphControls()
        self.Refresh()

    def repaintGraph(self):
        self.draw_panel.Refresh()

    def _enableGraphControls(self):
        self.world_evaporation.Enable()
        self.world_run.Enable()
        self.run_speed.Enable()
        self.step_btn.Enable()
        self._updateControls()

    def _updateControls(self):
        _ev = self._app.getEvaporation()
        self.world_evaporation.SetValue(_ev*100)
        self.evaporation_text.SetLabel(str(_ev))
        _ph = self._app.getPheromoneInfluence()
        self.pheromone_slider.SetValue(_ph*100)
        self.pheromone_text.SetLabel(str(_ph))
        _des = self._app.getDesirabilityInfluence()
        self.desirability_slider.SetValue(_des*100)
        self.desirability_text.SetLabel(str(_des))

    def OnScroll_changed_world_evaporation(self, evt):
        _val = self.world_evaporation.GetValue() / 100.0
        self._app.setEvaporation(_val)
        self._updateControls()

    def OnButton_step_btn(self, evt):
        self._app.tick()
        self.repaintGraph()

    def _getAbsNodePos(self, dc):
        (_w, _h) = dc.GetSize()
        _border = 25
        _usableW = _w - _border * 2
        _usableH = _h - _border * 2
        _xList = []
        _yList = []
        for _node in self._app.getNodes():
            (_x, _y) = self._app.getNodeLayout(_node)
            _xList.append(_x)
            _yList.append(_y)
        _minX = min(_xList)
        _maxX = max(_xList)
        _minY = min(_yList)
        _maxY = max(_yList)
        _xLen = _maxX - _minX
        _yLen = _maxY - _minY
        _out = {}
        for _node in self._app.getNodes():
            (_rx, _ry) = self._app.getNodeLayout(_node)
            _rx -= _minX
            _ry -= _minY
            _x = _usableW * (_rx / _xLen) + _border
            _y = _usableH * (_ry / _yLen) + _border
            _out[_node] = (_x, _y)
        return _out

    def drawNode(self, dc, node, nodePos):
        _r = 10
        (_x, _y) = nodePos[node]
        if node.lair:
            _type = 'lair'
        elif node.food:
            _type = 'food'
        else:
            _type = 'empty'
        dc.SetPen(self.nodes[_type]['pen'])
        dc.SetBrush(self.nodes[_type]['brush'])
        dc.DrawCircle(_x, _y, _r)
        dc.DrawText(str(len(node.ants)), _x+_r, _y+_r)

    def _drawEdge(self, dc, edge, nodePos, maxPheromone):
        _scale = edge.pheromone / maxPheromone
        _price = edge.price
        _delta = 150 * _scale
        _name = "pen_%s" % int(_delta)
        if _name not in self.edges:
            self.edges[_name] = wx.Pen((100+_delta,100,100), 3)
        dc.SetPen(self.edges[_name])
        (_from, _to) = edge.nodes
        (_x1, _y1) = nodePos[_from]
        (_x2, _y2) = nodePos[_to]
        dc.DrawLine(_x1, _y1, _x2, _y2)
        # draw pheromone meter
        dc.SetPen(self.edges['blue'])
        _posX = (_x2 - _x1) / 2 + _x1
        _posY = (_y2 - _y1) / 2 + _y1
        _posX += 10
        _posY += 10
        _len = 40 * _scale
        dc.DrawLine(_posX, _posY, _posX + _len, _posY)
        # draw length meter
        _posY += 5
        _lengthPerOne = 10
        dc.SetPen(self.edges['green1'])
        dc.DrawLine(_posX, _posY, _posX + (_price * _lengthPerOne), _posY)
        dc.SetPen(self.edges['green2'])
        for _pos in xrange(int(_price)+1):
            _x = _posX + (_pos*_lengthPerOne)
            dc.DrawLine(_x, _posY, _x, _posY+3)

    def OnDraw(self, evt):
        _buffer = wx.EmptyBitmap(*self.draw_panel.GetClientSizeTuple())
        _dc = wx.BufferedPaintDC(self.draw_panel, _buffer)
        _dc.Clear()
        if not self._app.graphLayout:
            return
        _absNodes = self._getAbsNodePos(_dc)
        # draw edges
        _maxPheromone = \
            max(_edge.pheromone for _edge in self._app.getEdges()) + 1
        for _edge in self._app.getEdges():
            self._drawEdge(_dc, _edge, _absNodes, _maxPheromone)
        # draw nodes
        for _node in self._app.getNodes():
            self.drawNode(_dc, _node, _absNodes)

    def OnButton_pheromone_reset(self, evt):
        self._app.clearPheromone()
        self.repaintGraph()

    def OnScroll_changed_pheromone_slider(self, evt):
        _val = self.pheromone_slider.GetValue() / 100.0
        self._app.setPheromoneInfluence(_val)
        self._updateControls()

    def OnScroll_changed_desirability_slider(self, evt):
        _val = self.desirability_slider.GetValue() / 100.0
        self._app.setDesirabilityInfluence(_val)
        self._updateControls()

    def OnTogglebutton_world_run(self, evt):
        _pressed = self.world_run.GetValue()
        if _pressed:
            self.run_timer.Start(100)
        else:
            self.run_timer.Stop()

    def OnRunTimer(self, evt):
        self._app.tick()
        self.repaintGraph()

    def OnTimer(self, evt):
        if evt.GetEventObject() == self.run_timer:
            self.OnRunTimer(evt)

def showForm(app, gui):
    _form = AntSim(None, gui, app)
    assert _form
    _form.Show()
    return _form

# vim: set et sts=4 sw=4 :
