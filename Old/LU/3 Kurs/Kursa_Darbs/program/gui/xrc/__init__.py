import os
import wx.xrc as xrc

_xrcDir = os.path.split(os.path.realpath(__file__))[0]

def _getCorrectXrc(name):
    _res = xrc.EmptyXmlResource()
    _res.Load(os.path.join(_xrcDir, '%s.xrc' % name))
    return _res

def initModule(name):
    globals()["%s_xrc" % name].__res = _getCorrectXrc(name)

# vim: set et sts=4 sw=4 :
