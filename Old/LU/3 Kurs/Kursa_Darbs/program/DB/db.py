import sqlobject

class DbOperator(object):

    _dbConn = None
    fname = None

    def __init__(self, fname):
        self.fname = fname
        builder = sqlobject.sqlite.builder()
        self._dbConn = builder(fname)
        sqlobject.sqlhub.processConnection = self._dbConn
        self.copyFileToMem()

    def copyFileToMem(self):
        pass

    def copyMemToFile(self):
        pass


_operator = None
def initDB(fname):
    global _operator
    _operator = DbOperator(fname)

def syncToFile():
    _operator.copyMemToFile()

# vim: set et sts=4 sw=4 :
