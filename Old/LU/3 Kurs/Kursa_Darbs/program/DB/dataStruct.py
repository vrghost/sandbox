import inspect
from sqlobject import *

class DataObject(SQLObject, object):

    class sqlmeta:
        lazyUpdate = True
        cacheValues = True


class Graph(DataObject):

    name = StringCol(length=255, alternateID=True)
    edges = MultipleJoin('Edge')
    nodes = MultipleJoin('Node')
    ants = MultipleJoin('Ant')
    foods = MultipleJoin('Food')
    lairs = MultipleJoin('Lair')

    evaporation = FloatCol()
    pheromoneInfluence = FloatCol()
    desirabilityInfluence = FloatCol()
    maxAntCount = IntCol()

    @classmethod
    def getAll(cls):
        return tuple(cls.select())

class GraphElement(DataObject):

    graph = ForeignKey('Graph')

class Edge(GraphElement):

    price = IntCol()
    traveled = IntCol()
    pheromone = FloatCol()
    nodes = RelatedJoin('Node')


class Node(GraphElement):

    ants = MultipleJoin('Ant')
    edges =  RelatedJoin('Edge')
    food = SingleJoin('Food')
    lair = SingleJoin('Lair')
    name = StringCol(length=50)

class NodeData(GraphElement):

    node = ForeignKey('Node')

class Food(NodeData):

    amount = FloatCol()

class Lair(NodeData):
    pass

class Ant(NodeData):

    carryingFood = BoolCol()
    pathIds = StringCol(default="", length=2**32)

    def getPath(self):
        if not self.pathIds:
            return ()
        _list = map(int, self.pathIds.split(","))
        _dat = tuple(Node.select(IN(Node.q.id, _list)))
        _dict = dict((_obj.id, _obj) for _obj in _dat)
        _out = []
        for _id in _list:
            _out.append(_dict[_id])
        return tuple(_out)

    def setPath(self, edges):
        _list = ",".join(str(_e.id) for _e in edges)
        self.pathIds = _list

    path = property(getPath, setPath)

def getTableClasses():
    _out = []
    for _cls in globals().values():
        if inspect.isclass(_cls) and issubclass(_cls, DataObject):
            _out.append(_cls)
    return _out

def createTables():
    for _cls in getTableClasses():
        _cls.createTable(ifNotExists=True)

# vim: set et sts=4 sw=4 :
