#!/usr/bin/python

import os

curDir = os.path.split(__file__)[0]
dataDir = os.path.join(curDir, "data")
_files = os.listdir(dataDir)
_names = set()
_hasStat = False
for _file in _files:
	if _file == "stat":
		assert os.path.isdir(os.path.join(dataDir, _file))
		_hasStat = True
		# skip 'stat' dir
		continue
	_name = os.path.splitext(_file)[0]
	if _name.endswith(".stat"):
		_name = os.path.splitext(_name)[0]
	_names.add(_name)

os.system(
	"./processors/spawn.sh %s" % " ".join(
		'"%s"' % _n for _n in _names))
if _hasStat:
	os.system("./processors/spawn_stat.sh")
