\contentsline {chapter}{\numberline {1.}Ievads}{1}
\contentsline {section}{\numberline {1.1.}Visp\IeC {\=a}r\IeC {\=e}js}{1}
\contentsline {section}{\numberline {1.2.}Swarm inteligence}{2}
\contentsline {section}{\numberline {1.3.}Skudru kolonija}{2}
\contentsline {chapter}{\numberline {2.}Skudru kolonijas algoritms}{3}
\contentsline {section}{\numberline {2.1.}Apk\IeC {\=a}rtnes vides defin\IeC {\=\i }cija}{3}
\contentsline {section}{\numberline {2.2.}Skudras izv\IeC {\=e}les algoritms}{3}
\contentsline {section}{\numberline {2.3.}Feromona p\IeC {\=a}rjaunojums}{4}
\contentsline {section}{\numberline {2.4.}Skudru kolonijas optimiz\IeC {\=a}cija}{4}
\contentsline {section}{\numberline {2.5.}Vai t\IeC {\=a}s visp\IeC {\=a}r str\IeC {\=a}d\IeC {\=a}?}{5}
\contentsline {section}{\numberline {2.6.}K\IeC {\=a}p\IeC {\=e}c tas ir lab\IeC {\=a}ks p\IeC {\=a}r citiem?}{7}
\contentsline {chapter}{\numberline {3.}Algoritma efektivit\IeC {\=a}tes nov\IeC {\=e}rt\IeC {\=e}jums}{10}
\contentsline {section}{\numberline {3.1.}Algoritma parametri}{10}
\contentsline {section}{\numberline {3.2.}Vides parametri}{12}
\contentsline {chapter}{\numberline {4.}Koeficientu uzska\IeC {\c n}o\IeC {\v s}ana}{13}
\contentsline {section}{\numberline {4.1.}Izv\IeC {\=e}les algoritma uzska\IeC {\c n}o\IeC {\v s}ana}{13}
\contentsline {subsection}{\numberline {4.1.1.}Feromona ietekm\IeC {\=e}\IeC {\v s}anas koeficients}{13}
\contentsline {subsection}{\numberline {4.1.2.}Heiristiska koeficienta ietekm\IeC {\=e}\IeC {\v s}anas koeficients}{13}
\contentsline {section}{\numberline {4.2.}Feromona p\IeC {\=a}rjaunojuma uzska\IeC {\c n}o\IeC {\v s}ana}{15}
\contentsline {subsection}{\numberline {4.2.1.}Feromona iztvaiko\IeC {\v s}anas koeficients}{15}
\contentsline {section}{\numberline {4.3.}Citi algoritma parametri}{17}
\contentsline {chapter}{\numberline {5.}Funkciju izv\IeC {\=e}le}{19}
\contentsline {section}{\numberline {5.1.}Feromona iztvaiko\IeC {\v s}ana}{19}
\contentsline {chapter}{\numberline {6.}Secin\IeC {\=a}jumi}{22}
