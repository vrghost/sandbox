def ACO():
	while(not metTerminationCondition()):
		for ant in getAllAnts():
			ant.constructSolution()
		pheromoneUpdate()
		if savedBestSolution > currentBestSolution():
			savedBestSolution = currentBestSolution()
