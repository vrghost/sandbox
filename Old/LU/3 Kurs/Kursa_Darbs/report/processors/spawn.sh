#!/bin/bash

children=()

function graph {
    python ./processors/graph.py "$1" &
    children[${#children[*]}]=$!
}

function plot {
    python ./processors/plot.py "$1" &
    children[${#children[*]}]=$!
}

for name in $*
do
    graph $name
    plot $name
done

for pid in "${children[@]}"
do
    wait $pid
done
