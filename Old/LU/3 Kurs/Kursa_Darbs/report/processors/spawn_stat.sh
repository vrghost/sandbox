#!/bin/bash

children=()

function plot_stat {
    python ./processors/plot_stat.py "$1" &
    children[${#children[*]}]=$!
}

for name in `ls ./data/stat`
do
    plot_stat $name
done

for pid in "${children[@]}"
do
    wait $pid
done
