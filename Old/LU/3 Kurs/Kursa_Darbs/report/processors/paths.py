import os

curDir = os.path.split(os.path.realpath(__file__))[0]
reportDir = os.path.split(curDir)[0]
imgDir = os.path.join(reportDir, "img")
dataDir = os.path.join(reportDir, "data")


# vim: set et sts=4 sw=4 :
