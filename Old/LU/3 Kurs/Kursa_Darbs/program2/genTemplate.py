import random

import networkx as NX

_headMask = """
evaporation: %(evaporation)s
maxAntCount: %(maxAntCount)s
pheromoneInfluence: %(phInf)s
desirabilityInfluence: %(desInf)s
"""

def getPrice():
    return random.randint(1, 100)

def getHead(maxAntCount, evaporation, phInf, desInf):
	return _headMask % {
		"evaporation": evaporation,
		"maxAntCount": maxAntCount,
		"phInf": phInf,
		"desInf": desInf,
	}

def pathPrice(graph, path):
	_pairs = zip(path, path[1:])
	_out = 0
	for (_from, _to) in _pairs:
		_out += graph.get_edge(_from, _to)
	return _out
	

def constructGraph(size):
	_g = NX.XGraph()
	assert not _g.multiedges
	assert not _g.selfloops
	for _id in range(size):
		_g.add_node("NODE_%i" % _id)
	_freeNodes = _g.nodes()
	_usedNodes = [random.choice(_freeNodes)]
	for _node in _usedNodes:
		_freeNodes.remove(_node)
	_start = random.choice(_usedNodes)
	# get tree
	while _freeNodes:
		_n1 = random.choice(_freeNodes)
		_n2 = random.choice(_usedNodes)
		_g.add_edge(_n1, _n2, getPrice())
		_usedNodes.append(_n1)
		_freeNodes.remove(_n1)
	_end = None
	_notCheckedNodes = list(_usedNodes)
	assert _usedNodes
	while not _end or _end == _start or _g.has_edge(_start, _end):
		_end = random.choice(_notCheckedNodes)
		_notCheckedNodes.remove(_end)
		_minPath = NX.dijkstra_path(_g, _start, _end)
		if _notCheckedNodes and len(_minPath) < 3:
			_end = None
	assert _minPath[0] == _start, _minPath[-1] == _end
	_curPrice = pathPrice(_g, _minPath)
	_prev = _minPath[-2]
	for _node in reversed(_minPath[1:-2]):
		_g.add_edge(_end, _node, _curPrice)
		_curPrice += _g.get_edge(_prev, _node) 
		_prev = _node
	_m2 = NX.dijkstra_path(_g, _start, _end)
	assert _minPath ==  _m2, (_minPath, _m2)
	return (_g, _start, _end)

def graphToYaml(graph, start, end):
	_out = ""
	_nodes = ""
	_space = " " * 4
	_space2 = " " * 6
	for _node in graph.nodes():
		_nodes += "\n" + _space + "- name: " + _node
	_out += "nodes:" + _nodes
	_edges = ""
	for (_from, _to, _data) in graph.edges():
		_edge = _space + "-\n"
		_edge += _space2 + "nodes: [" + _from + ", " + _to + "]\n"
		_edge += _space2 + "price: %s\n" % _data
		_edges += _edge
	_out += "\nedges:\n" + _edges
	_out += "lairs:\n" + _space + "- node: %s\n" % start
	_out += "foods:\n" + _space + "-\n" + _space2 + "node: " + end + "\n" + _space2 + "amount: 100\n"
	return _out
	

def genGraph(nodeCount, maxAntCount, evaporation=0.85, phInf=1, desInf=1):
	return "\n".join((
		getHead(maxAntCount, evaporation, phInf, desInf),
		graphToYaml(*constructGraph(nodeCount))
	))

if __name__ == "__main__":
	print genGraph(10, 3)

# vim: set et sts=4 sw=4 :
