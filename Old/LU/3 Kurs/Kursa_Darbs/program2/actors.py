from random import random
from time import time

import algorythms

class BaseActor(object):

    _holders = ()
    _holderNames = None

    def __init__(self, **kwargs):
        _vals = []
        self._holderNames = _holderNames = []
        for _val in self._holders:
            if isinstance(_val, (tuple, list)):
                assert len(_val) == 2
                _vals.append(_val)
                _holderNames.append(_val[0])
            else:
                _vals.append((_val, None))
                _holderNames.append(_val)
        _data = dict(_vals)
        _data.update(kwargs)
        _complex = {}
        for (_name, _value) in _data.iteritems():
            if _name in _holderNames:
                setattr(self, _name, _value)
            else:
                _complex[_name] = _value
        if _complex:
            self._setComplexTypes(**_complex)

    def _setComplexTypes(self, **kwargs):
        raise NotImplementedError(str(kwargs))

    def act(self):
        raise NotImplementedError

    def serialize(self):
        _out = {}
        for _name in self._holderNames:
            _out[_name] = getattr(self, _name)
        self.updateSerial(_out)
        return _out


class Graph(BaseActor):

    _holders = ("maxAntCount", "evaporation", "desirabilityInfluence",
        "pheromoneInfluence", ("minPheromone", 1e-8), ("minPathLeng", None))

    nodes = edges = lairs = foods = ants = minPath = ()
    _queue = _edgeList = _minPathFoundOn = None
    iterLeft = _iterCount = _lastSnapshot = _solutionsFound = 0
    _phHistory = None
    _startTime = _runTime = None
    antAlg = None
    statPointCnt = 500

    def _setComplexTypes(self, nodes, edges, lairs, foods, antAlg):
    	self._phHistory = []
	print antAlg
	self.antAlg = algorythms.ants[antAlg]
        self.nodes = dict(
            (_kw["name"], Node(graph=self, **_kw))
            for _kw in nodes
        )
        self.edges = [Edge(graph=self, **_kw) for _kw in edges]
        _edgeList = {}
        for _edge in self.edges:
            (_n1, _n2) = _edge.nodes
            _edgeList[(_n1, _n2)] = _edge
	    _edgeList[(_n2, _n1)] = _edge
	    self._edgeList = _edgeList
        self.lairs = [Lair(graph=self, **_kw) for _kw in lairs]
        self.foods = [Food(graph=self, **_kw) for _kw in foods]
        self.ants = []
        _parentNode = self.lairs[0].node # XXX quick hack!
        while len(self.ants) < self.maxAntCount:
            self.ants.append(Ant(graph=self, node=_parentNode.name))

    def updateSerial(self, out):
        out["nodes"] = [_obj.serialize() for _obj in self.nodes.itervalues()]
        out["edges"] = [_obj.serialize() for _obj in self.edges]
        out["lairs"] = [_obj.serialize() for _obj in self.lairs]
        out["foods"] = [_obj.serialize() for _obj in self.foods]
        out["ants"] = [_obj.serialize() for _obj in self.ants]
	out["minPath"] = [_node.name for _node in self.minPath]

    def _getPathStat(self, path):
    	_price = 0
	_totalPh = 0
	_prev = None
	for _node in path:
		if _prev is not None:
		    _edge = self._edgeList[(_node, _prev)]
		    _price += _edge.price
		    _totalPh += _edge.pheromone
		_prev = _node
	return (_price, _totalPh)
	

    def foundPath(self, ant):
    	self._solutionsFound += 1
    	_path = list(ant.nodeMemory)
	_path.append(ant.node)
	(_curPr, _curPh) = self._getPathStat(_path)
	(_bestPr, _bestPh) = self._getPathStat(self.minPath)
	if (not self.minPath) or _bestPr > _curPr or (_bestPr == _curPr and _curPh > _bestPh):
		if (not self.minPath) or _bestPr > _curPr:
			self._minPathFoundOn = {
				"worldIter": self._iterCount,
				"runTime": time() - self._startTime,
				"solutionsFound": self._solutionsFound,
			}
		self.minPath = _path
		(_bestPr, _bestPh) = (_curPr, _curPh)

	if self._iterCount - self._lastSnapshot > 2: # take stat every 100 iters at least
		_worldPh = sum(_e.pheromone for _e in self._edgeList.itervalues())
		self._phHistory.append({
			"worldAmount": _worldPh,
			"curBestPheromone": _bestPh,
			"curBestPrice": _bestPr,
			"worldIterNo": self._iterCount,
			"solutionsFound": self._solutionsFound,
		})
		self._lastSnapshot = self._iterCount
		
    def getNode(self, name):
        return self.nodes[name]

    def act(self, iterCount):
	self._iterCount = 0
	self._solutionsFound = 0
	self._lastSnapshot = -0xfff # take snapshot on next solution found
	self._startTime = time()
	while self._iterCount <= iterCount:
	    [_an.act() for _an in self.ants]
	    [_ed.act() for _ed in self.edges]
	    self._iterCount += 1
	self._runTime = time() - self._startTime

    def  getStatistics(self):
    	_hist = self._phHistory
	while len(_hist) / 2 > self.statPointCnt:
		_newHist = []
		_pos = 0
		while _pos < len(_hist):
			_newHist.append(_hist[_pos])
			_pos += 2
		_hist = _newHist
        return {
		"pheromoneHistory": _hist,
		"definedMinPathLeng" : self.minPathLeng,
		"foundMinPath": self._getPathStat(self.minPath)[0],
		"minPath": self._minPathFoundOn,
		"totalSolutionsFound": self._solutionsFound,
		"totalIterCount": self._iterCount,
		"runTime": self._runTime,
	}


class Node(BaseActor):

    _holders = ("graph", "name")
    edges = lair = food = None

    def addEdge(self, edge):
        if self.edges is None:
            self.edges = []
        self.edges.append(edge)

    def setLair(self, lair):
        self.lair = lair

    def setFood(self, food):
        self.food = food

    def updateSerial(self, out):
        del out["graph"]

class Edge(BaseActor):

    _holders = ("graph", "price", ("pheromone", 0))
    nodes = ()
    passCount = 0

    def _setComplexTypes(self, nodes):
        assert len(nodes) == 2
        self.nodes = [self.graph.getNode(_name) for _name in nodes]
        for _node in self.nodes:
            _node.addEdge(self)

    def act(self):
    	self.graph.antAlg[1](self)

    def walk(self, ant):
        self.graph.antAlg[0](self, ant)

    def updateSerial(self, out):
        del out["graph"]
        out["nodes"] = [_obj.name for _obj in self.nodes]

    def otherNode(self, node):
        _out = [_nd for _nd in self.nodes if _nd != node]
        assert len(_out) == 1
        return _out[0]

class Lair(BaseActor):

    _holders = ("graph", )
    node = None

    def _setComplexTypes(self, node):
        self.node = self.graph.getNode(node)
        self.node.setLair(self)

    def updateSerial(self, out):
        del out["graph"]
        out["node"] = self.node.name

class Food(BaseActor):

    _holders = ("graph", "amount")
    node = None

    def _setComplexTypes(self, node):
        self.node = self.graph.getNode(node)
        self.node.setFood(self)

    def updateSerial(self, out):
        del out["graph"]
        out["node"] = self.node.name

class Ant(BaseActor):

    _holders = ("graph", )
    node = None
    carryingFood = False
    nodeMemory = ()
    lastSolPrice = None

    def _setComplexTypes(self, node):
        self.node = self.graph.getNode(node)
        self.nodeMemory = []

    def _chose(self, objs):
        _total = sum(objs.itervalues())
        _minSum = random() * _total
        for (_obj, _prob) in objs.iteritems():
            _minSum -= _prob
            if _minSum <= 0:
                return _obj
        raise Exception("Chose function contains an error")

    def getProb(self, edge):
        _ph = max(edge.pheromone, self.graph.minPheromone)
        _heur = 1.0 / edge.price
        return (_ph**self.graph.pheromoneInfluence) * \
            (_heur**self.graph.desirabilityInfluence)

    def act(self):
        if self.carryingFood:
            self._walkBack()
            if self.node.lair:
                self.carryingFood = False
                self.nodeMemory = []
        else:
            self._wander()
	    if self.node.food:
	    	self.carryingFood = True
		(_curPr, _curPh) = self.graph._getPathStat(self.nodeMemory + [self.node])
		self.lastSolPrice = _curPr
		self.graph.foundPath(self)

    def _wander(self):
        _sel = {}
        for _edge in self.node.edges:
            _sel[_edge] = self.getProb(_edge)
        _edge = self._chose(_sel)
        _target = _edge.otherNode(self.node)
        self.nodeMemory.append(self.node)
        self.node = _target

    def _walkBack(self):
        _target = self.nodeMemory.pop()
        _path = [_ed
            for _ed in self.node.edges
            if _ed.otherNode(self.node) == _target
        ]
        assert len(_path)
        _edge = _path[0]
        _edge.walk(self)
        self.node = _target

    def updateSerial(self, out):
        del out["graph"]
        out["node"] = self.node.name
        out["carryingFood"] = bool(self.carryingFood)

def constructGraph(data, minPath, antAlg):
    return Graph(minPathLeng=minPath, antAlg=antAlg, **data)

# vim: set et sts=4 sw=4 :
