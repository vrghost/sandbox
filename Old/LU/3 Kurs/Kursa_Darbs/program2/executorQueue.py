import Queue
import threading

class _WorkerThread(threading.Thread):

    _queue = None

    def __init__(self, queue):
        super(_WorkerThread, self).__init__()
        self.setDaemon(True)
        self._queue = queue
        self.start()

    def run(self):
        while True:
            _item = self._queue.get()
            try:
                _item.act()
            except Exception, _err:
                print "Error %s"  % _err
            self._queue.task_done()

class ExecurorQueue(object):

    _workers = ()
    _queue = None

    def __init__(self, workerCnt):
        self._queue = Queue.Queue()
        self._workers = [
            _WorkerThread(self._queue)
            for _ii in xrange(workerCnt)
        ]

    def put(self, item):
        self._queue.put(item)

    def putlist(self, items):
        for _item in items:
            self._queue.put(_item)

    def join(self):
        self._queue.join()

# vim: set et sts=4 sw=4 :
