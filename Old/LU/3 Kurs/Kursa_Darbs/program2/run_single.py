import os
import sys
import time
import yaml

import actors

def saveDict(fname, val):
    _outDir = os.path.split(__file__)[0]
    _outDir = os.path.join(_outDir, "out")
    _f = file(os.path.join(_outDir, fname + ".yaml"), "w")
    yaml.dump(val, _f)
    _f.close()

def run(data, iterCount, minPath, outFname, antAlg):
    _graph = actors.constructGraph(data, minPath, antAlg)
    _graph.act(iterCount)
    saveDict(outFname, _graph.serialize())
    _statDat = outFname + ".stat"
    saveDict(_statDat, _graph.getStatistics())

if __name__ == "__main__":
    _inFname = sys.argv[1]
    assert os.path.isfile(_inFname), \
        "Must give graph structure as argument"
    _data = yaml.load(file(_inFname))
    _iterCount = int(sys.argv[2])
    _minPath = int(sys.argv[3])
    _outFname = sys.argv[4]
    if len(sys.argv) > 5:
    	assert sys.argv[5]
	_antAlg = sys.argv[5]
    else:
    	_antAlg = "reversePrice"
    run(_data, _iterCount, _minPath, _outFname, _antAlg)



# vim: set et sts=4 sw=4 :
