def reversePrice_act(edge):
        _passes = edge.passCount * 1.0
        edge.pheromone = edge.pheromone * (1-edge.graph.evaporation) + \
            (_passes / edge.price)
        edge.passCount = 0


def reversePrice_walk(edge, ant):
	edge.passCount += 1

def constMul_walk(edge, ant):
	edge.passCount += 1

def constMul_act(edge):
        edge.pheromone = edge.pheromone * (1-edge.graph.evaporation) + \
            (edge.passCount * 1.0)
        edge.passCount = 0

def revSol_walk(edge, ant):
	edge.passCount += 1.0 / ant.lastSolPrice

def revSol_act(edge):
        edge.pheromone = edge.pheromone * (1-edge.graph.evaporation) + \
            edge.passCount
        edge.passCount = 0

ants = {
	"reversePrice" : (reversePrice_walk, reversePrice_act),
	"constMul": (constMul_walk, constMul_act),
	"reverseSolPrice": (revSol_walk, revSol_act),
}

# vim: set et sts=4 sw=4 :
