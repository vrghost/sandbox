import os
import sys
import time
import yaml

import networkx as NX
import pygraphviz

def fixName(name):
    if name.startswith("NODE_"):
    	_rv = name[len("NODE_"):]
    else:
    	_rv = name
    return _rv

def drawGraph(data, outFname):
    _graph = pygraphviz.AGraph()
    _foodNodes = [_obj["node"] for _obj in data["foods"]]
    _lairNodes = [_obj["node"] for _obj in data["lairs"]]
    for _node in data["nodes"]:
	if _node["name"] in _foodNodes:
		_color = "green"
		_shape = "egg"
	elif _node["name"] in _lairNodes:
		_color = "red"
		_shape = "house"
	else:
		_color = "black"
		_shape = "ellipse"
        _graph.add_node(fixName(_node["name"]), color=_color, shape=_shape)
    for _edge in data["edges"]:
        (_from, _to) = (fixName(_nm) for _nm in _edge["nodes"])
	(_f1, _f2) = _edge["nodes"]
	_color = ["black"]
	_label = str(_edge["price"]) + r"\n" + "%.2f" % _edge["pheromone"]
	if _f1 in data["minPath"] and _f2 in data["minPath"]:
		_color.append("red")
	_graph.add_edge(_from, _to, color=":".join(_color), label=_label)
    _graph.layout(prog="dot")
    _graph.draw(outFname)
    

if __name__ == "__main__":
    _inFname = sys.argv[1]
    assert os.path.isfile(_inFname)
    _outDir = os.path.split(os.path.realpath(__file__))[0]
    _outDir = os.path.split(_outDir)[0]
    _outDir = os.path.join(_outDir, "report", "img", "graphs")
    _fname = os.path.splitext(os.path.split(_inFname)[1])[0]
    _outFile = os.path.join(_outDir, _fname + ".png")
    if os.path.isfile(_outFile):
	# if output file exists and is younger than input, do nothing
	_inS = os.stat(_inFname)
	_outS = os.stat(_outFile)
	_inTime = max(_inS.st_mtime, _inS.st_ctime)
	_outTime = min(_outS.st_mtime, _outS.st_ctime)
	if _inTime < _outTime:
	    exit(1)
    _data = yaml.load(file(_inFname))
    drawGraph(_data, _outFile)
 


# vim: set et sts=4 sw=4 :
