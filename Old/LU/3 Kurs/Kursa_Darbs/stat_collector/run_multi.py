import Queue
import threading

import time
import sys
import os

class WT(threading.Thread):

    _orders = None

    def __init__(self):
        super(WT, self).__init__()
        self.setDaemon(True)
	self._orders = []

    def addOrder(self, ord):
    	self._orders.append(ord)

    def run(self):
        while self._orders:
	    	_e = self._orders.pop()
		os.system(_e)


if __name__ == "__main__":
	_command = 'python run.py "%s"'
	_w = [WT() for _x in xrange(4)]
	_pos = 0
	for _dir in sys.argv[1:]:
		if not os.path.isdir(_dir):
			print "%s skip" % _dir
		_w[_pos].addOrder(_command % _dir)
		_pos += 1
		if _pos > 3:
			_pos = 0
	[_t.start() for _t in _w]
	time.sleep(1)
	print _w
	_ended = False
	while not _ended:
		_ended = True
		for _t in _w:
			if _t.isAlive():
				_ended = False
				_t.join(1)

# vim: set et sts=4 sw=4 :
