import os
import sys

from itertools import chain

import stats
import syck
import yaml

outDir = os.path.join(
	os.path.split(os.path.realpath(__file__))[0],
	"out"
)

def getStat(data):
	_names = set()
	_avgs = {}
	if not all(hasattr(_o, "iterkeys") for _o in data):
		return _avgs
	for _keys in (_o.iterkeys() for _o in data):
		_names.update(_keys)
	for _name in _names:
		_data = [_o[_name] for _o in data]
		_avgs[_name + "_average"] = stats.mean(_data)
		_avgs[_name + "_error"] = stats.sterr(_data)
	return _avgs
	

def processStat(data):
	if all("graph_" in _o["name"] for _o in data):
		# group by graph
		_getNum = lambda n: int(n.split("graph_", 1)[1].split(".", 1)[0])
		_nums = set(_getNum(_o["name"]) for _o in data)
		_groupDict = dict((_id, [_o for _o in data if _getNum(_o["name"])==_id]) for _id in _nums)
		_graphGroup = _groupDict
	else:
		# encapsulate all data as single graph group
		_graphGroup = {"all": data}
	_groupStat = []
	for (_grName, _group) in _graphGroup.iteritems():
		_stats = [_o["stat"]["pheromoneHistory"] for _o in _group]
		_stat = zip(*_stats)
		#_stat = _stats
		assert _stat, _grName
		_topValues = (
			("stat", "definedMinPathLeng"),
			("stat", "foundMinPath"),
		)
		_topDicts = []
		for _dat in _group:
			_vals = dict((_name[1], _dat[_name[0]].get(_name[1])) for _name in _topValues)
			_topDicts.append(_vals)
		for _val in _topDicts:
			if _val["foundMinPath"] == 0:
				_val["quality"] = 0
			else:
				_val["quality"] = (1.0*_val["definedMinPathLeng"]) / _val["foundMinPath"] 
		_groupStat.append({
			"pheromoneHistory": [getStat(_o) for _o in _stat],
			"stepStat": getStat([_o["stat"]["minPath"] for _o in _group]),
			"topDicts": getStat(_topDicts),
		})
	# we've got groups. now need to find average scores between them.
	_out = {}
	_gPathStats = zip(*[_o["pheromoneHistory"] for _o in _groupStat])
	_avgGMinPath = []
	for _obj in _gPathStats:
		_keys = set()
		_iterData = {}
		for _item in _obj:
			[_keys.add(_k) for _k in _item.keys()]
		for _name in _keys:
			_iterData[_name] = stats.mean([_o[_name] for _o in _obj])
		_avgGMinPath.append(_iterData)
	_out["pheromoneHistory"] = _avgGMinPath
	for _key in ("stepStat", "topDicts"):
		_data = [_o[_key] for _o in _groupStat]
		_keys = set()
		for _o in _data:
			[_keys.add(_k) for _k in _o.keys()]
		_iterData = {}
		for _ikey in _keys:
			_iterData[_ikey] = stats.mean([_o[_ikey] for _o in _data if _ikey in _o])
		_out[_key] = _iterData
	return _out

def generateStat(srcDir):
	print srcDir
	_statData = []
	print "reading..."
	_files = [_f for _f in os.listdir(srcDir) if os.path.splitext(_f)[1] == ".yaml"]
	_names = set()
	for _f in _files:
		_name = os.path.splitext(_f)[0]
		if _name.endswith(".stat"):
			_name = os.path.splitext(_name)[0]
		_names.add(_name)
	for _name in _names:
		_file = os.path.join(srcDir, _name + ".yaml")
		_items = {"name": _name}
		#if os.path.isfile(_file):
		#	_items["graph"] = syck.load(file(_file))
		_file = os.path.join(srcDir, _name + ".stat.yaml")
		if os.path.isfile(_file):
			_items["stat"] = syck.load(file(_file))
		_statData.append(_items)
	print "processing..."
	_stat = processStat(_statData)
	print "saving..."
	_outFname = os.path.split(srcDir)[1]
	assert _outFname
	_outStat = os.path.join(
		outDir,
		_outFname + ".stat.yaml",
	)
	_f = file(_outStat, "w")
	yaml.dump(_stat, _f)
	_f.close()

if __name__ == "__main__":
	assert len(sys.argv) == 2
	_srcDir = sys.argv[1]
	assert os.path.isdir(_srcDir)
	generateStat(_srcDir)

# vim: set et sts=4 sw=4 :
