# -*- coding: utf-8 -*-

axioms = ("a", "b")

## defining rules

def reverseRule1(str):
    if str.startswith("a") and str.endswith("c"):
        return str[1:-1]
    else:
        return None

def reverseRule2(str):
    if str.startswith("d") and str.endswith("b"):
        return str[1:-1] + "c"
    else:
        return None

reverseRules = (reverseRule1, reverseRule2)

def check(theorem):
    if theorem in axioms:
        return True
    for _rule in reverseRules:
        _reversed = _rule(theorem)
        if _reversed is not None:
            return check(_reversed)
    return False

print check("aaabccc")

# vim: set et sts=4 sw=4 :
