# -*- coding: utf-8 -*-

from operator import *

def OR(*args):
    _f = lambda a, b: a or b
    return reduce(_f, args)

def AND(*args):
    _f = lambda a, b: a and b
    return reduce(_f, args)

def NOT(a):
    return not a

def IMP(a, b):
    return not a or b


fo = lambda B, C, D: IMP(AND(B, C), NOT(IMP(OR(B, D), AND(C, D))))
f1 = lambda B, C, D: OR(NOT(B), NOT(C), NOT(D))
f2 = lambda B, C, D: OR(NOT(B), NOT(C), AND(B, NOT(C)), AND(D, NOT(C)), AND(B, NOT(D)))

_bool = (True, False)

for b in _bool:
    for c in _bool:
        for d in _bool:
            assert fo(b,c,d) == f1(b,c,d)
            assert fo(b,c,d) == f2(b,c,d)

print "all ok"


# vim: set et sts=4 sw=4 :
