# -*- coding: utf-8 -*-

class Operator(object):
    "Abstract operator"

    name = None
    values = None

    def __repr__(self):
        return "%s(%s)" % (self.name, ", ".join(map(str, self.values)))

    @classmethod
    def createOp(cls, _name):
        class _clsOp(cls):
            name = _name
        return _clsOp

    @classmethod
    def fromParser(cls, str, count, data):
        print data
        return cls(*data)

class SingleOp(Operator):


    def __init__(self, value):
        self.values = (value, )


class DoubleOp(Operator):

    def __init__(self, v1, v2):
        self.values = (v1, v2)

Not = SingleOp.createOp("NOT")
And = DoubleOp.createOp("AND")
Or = DoubleOp.createOp("AND")
Imp = DoubleOp.createOp("IMP")
Forall = SingleOp.createOp("FORALL")
#Exists = SingleOp("Exists")

#usableOps = [Not, And, Or, Imp, Forall]

# vim: set et sts=4 sw=4 :
