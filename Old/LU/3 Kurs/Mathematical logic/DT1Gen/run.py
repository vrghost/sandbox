# -*- coding: utf-8 -*-

IMP = "->"
B = "B"
C = "C"
D = "D"

def L1(B, C):
    return (B, IMP, (D, IMP, B))

def L2(B, C, D):
    return ((B, IMP, (C, IMP, D)), IMP, ((B, IMP, C), IMP, (B, IMP, D)))

def GenDt1(hipoteses, prove):
    B = hipoteses[-1]
    _prove = []
    for _F in prove:
        if _F in hipoteses:
            _app = (
                _F,
                L1(_F, B),
                (B, IMP, _F),
            )
        elif _F == B:
            _app = (
                L2(B, D, B),
                L1(B, (D, IMP, B)),
                ((B, IMP, (D, IMP, B)), IMP, (B, IMP, B)),
                L1(B, D, B),
                (B, IMP, B),
            )
        else:
            #isinstance(_F, (L1, L2)):
            _app = (
                _F,
                L1(_F, B),
                (B, IMP, _F),
            )
        _prove.extend(_app)
    return _prove

def toStr(tuple):
    _out = ""
    for _el in tuple:
        if _el == IMP:
            _out += IMP
        elif isinstance(_el, basestring):
            _out += _el
        else:
            _out += "(" + toStr(_el) + ")"
        _out += " "
    return _out

def StrDt1(hip, theor):
    _str = ""
    for (_num, _line) in enumerate(GenDt1(hip, theor)):
        _str += "%i) %s\n" % (_num+1, toStr(_line))
    return _str

print StrDt1(("~A", "A"), (L1("~A", "A"), ("A", IMP, "~A"), ))

# vim: set et sts=4 sw=4 :
