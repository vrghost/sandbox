# -*- coding: utf-8 -*-

from pyparsing import *

from operators import *

word = Word("".join(set(alphas.upper())))

expr = Forward()
sExpr = Suppress("(") + expr + Suppress(")")
notE = Suppress("~") + expr
notE.setParseAction(Not.fromParser)
andE = expr + Suppress("&") + expr
andE.setParseAction(And.fromParser)

expr << (notE | andE | sExpr | word)

def parse(text):
    return expr.parseString(text)

if __name__ == "__main__":
    #_text = "(((~A)) -> ((A -> (~A -> (B -> (~A)))) -> ((A -> (~A)) -> (A -> (B -> (~A)))))) -> ((((~A)) -> (A -> (~A -> (B -> (~A))))) -> (((~A)) -> ((A -> (~A)) -> (A -> (B -> (~A))))))"
    _text = "(A&B)&C"
    _res = parse(_text)[0]
    print "%r => %r" % (_text, _res)

# vim: set et sts=4 sw=4 :
