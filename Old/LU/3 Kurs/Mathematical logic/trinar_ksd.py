# -*- coding: utf-8 -*-

def OR(a, b):
    return max(a, b)

def AND(a, b):
    return min(a, b)

def NOT(a):
    return {
        0:  2,
        1:  0,
        2:  1,
    }[a]

def IMP(a, b):
    return {
        (0, 0): 2,
        (0, 1): 0,
        (0, 2): 0,
        (1, 0): 1,
        (1, 1): 2,
        (1, 2): 1,
        (2, 0): 0,
        (2, 1): 1,
        (2, 2): 2,
    }[(a, b)]

def formula(a, b):
    return IMP(OR(IMP(a,b),IMP(b,a)), NOT(a))


for _a in xrange(3):
    for _b in xrange(3):
        print _a, _b, "-->", formula(_a, _b)


# vim: set et sts=4 sw=4 :
