# -*- coding: utf-8 -*-

axioms = ("a", "b")

## defining rules

def rule1(str):
    return "a" + str + "c"

def rule2(str):
    if str.endswith("c"):
        return "d" + str[:-1] + "b"
    else:
        return None

rules = (rule1, rule2)

#### checking routines

def _checkPair(src, targ):
    for _rule in rules:
        if _rule(src) == targ:
            return True
    return False

def check(seq):
    if len(seq) > 2:
        _out = check(seq[:-1])
        _out &= _checkPair(seq[-2], seq[-1])
    else:
        assert len(seq) == 2
        _out = seq[0] in axioms and _checkPair(seq[0], seq[1])
    return _out

def humanReadableCheck(seq):
    _mask = "\t* Proof '%s' is %%s for current theory." % " |- ".join(seq)
    if check(seq):
        _ans = _mask % "correct"
    else:
        _ans = _mask % "incorrect"
    print _ans

humanReadableCheck(["a", "aac", "aaacc"])
humanReadableCheck(["a", "aac", "aaacc", "aaaaccc", "daaaaccb"])
humanReadableCheck(["b", "abc", "dabb"])
humanReadableCheck(["b", "abc", "adabb"])

# vim: set et sts=4 sw=4 :
