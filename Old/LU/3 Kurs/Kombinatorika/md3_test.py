# -*- coding: utf-8 -*-

from itertools import *

def nRange(n):
    return range(n)

def genPerm(seq):
    assert isinstance(seq, tuple)
    if len(seq) == 1:
        yield seq
        raise StopIteration
    _el = seq[0]
    _tail = seq[1:]
    for _t in genPerm(_tail):
        for _pos in xrange(len(_t)+1):
            yield _t[:_pos] + (_el, ) + _t[_pos:]

def isNonDiv(seq):
    for _j in xrange(1, len(seq)):
        _head = seq[:_j]
        if set(_head) == set(range(1, _j+1)):
            return False
    return True

def fac(n):
    if n <= 1:
        return 1
    else:
        return n * fac(n-1)

def form(len):
    return fac(len) - sum(fac(_l) for _l in xrange(1, len))

if __name__ == "__main__":
    _sum = 0
    for _len in xrange(1, 15):
        _total = _good = 0
        for _p in genPerm(tuple(range(1, _len+1))):
            _total += 1
            if isNonDiv(_p):
                _good += 1
        print "For %i total: %i good: %i formula: %i" % \
            (_len, _total, _good, _total - _sum)
        _sum += fac(_len)

# vim: set et sts=4 sw=4 :
