# -*- coding: utf-8 -*-

from itertools import *

class HashDict(object):

    def __init__(self):
        self._items = []

    def __getitem__(self, key):
        for (_n, _v) in self._items:
            if _n == key:
                return _v
        raise Exception

    def __setitem__(self, key, value):
        for (_id, (_n, _v)) in enumerate(tuple(self._items)):
            if _n == key:
                del self._items[_id]
        self._items.append((key, value))

    def __repr__(self):
        return "<%s:%s>" % (self.__class__.__name__, self._items)


def getPerm(n, x, perm):
    _F = HashDict()
    _cik = []
    _C = [n]
    _cik.append(_C)
    _F[_C] = perm[n-1]+1
    for _i in reversed(xrange(n-1)):
        _a = perm[_i]
        print _a, _i
        if 0 <= _a <= x-1:
            _C = [_i+1]
            _cik.insert(0, _C)
            _F[_C] = perm[n-_i+1]
        elif _a > x-1:
            _k = x-_a
            _prev = list(chain(*_cik))[_k]
            for (_pos, _loop) in enumerate(_cik):
                try:
                    _idx = _loop.index(_prev)
                except ValueError:
                    continue
            print _prev
        else:
            raise Exception(_a)
    print _F
    return _cik

if __name__ == "__main__":
    print getPerm(9, 4, (4,8,5,0,7,5,2,4,1))

# vim: set et sts=4 sw=4 :
