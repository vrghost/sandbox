#!/bin/bash

DRIVER_NAME="charDev"
MODE="664"
GROUP="root"

insmod ./bin/$DRIVER_NAME.ko $* major=203

if [ ! $? ] ; then
    echo "insmod error!"
    exit $?
fi

major=$(awk "/^([0-9]+)[ ]+$DRIVER_NAME\$/ {print \$1;}" /proc/devices)

if [ ! $major ] ; then
    echo "Failed to determine major."
    exit 1
fi

for _i in `seq 0 9`; do
    devName=/dev/${DRIVER_NAME}$_i
    if [ -e $devName ] ; then
	rm -f $devName
    fi
    mknod $devName c $major $_i
    chgrp $GROUP $devName
    chmod $MODE $devName
done


# vim: set ff=unix
