#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>

MODULE_LICENSE("BSD");


static int hello_init(void) {
  printk(KERN_ALERT " Hello world!\n");
  return 0;
}

static void hello_exit(void) {
  printk(KERN_ALERT " Bye-Bye cruel world <Emo/> \n");
}

module_init(hello_init);
module_exit(hello_exit);
