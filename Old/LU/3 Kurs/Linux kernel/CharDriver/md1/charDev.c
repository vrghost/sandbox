#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

// Constants
#define FIRST_MINOR 0
#define DEV_CNT (unsigned int)1
#define DEFAULT_MODULE_NAME "charDev"
#define MEM_SIZE 512
#define LOG_PREFIX "[" DEFAULT_MODULE_NAME ":" TOSTRING(__LINE__) "] "
#define EOL "\n"
#define ALLOC_MEM_MODE GFP_KERNEL

// Macro
#define log_debug(x, ...) printk(KERN_DEBUG LOG_PREFIX x EOL, ##__VA_ARGS__);
#define log_error(x, ...) printk(KERN_ALERT LOG_PREFIX x EOL, ##__VA_ARGS__);

MODULE_LICENSE("MIT");

// module variables

struct device{
    char* memory;
    unsigned int size, read_pos, write_pos, used_size;
    // OS-related field
    struct rw_semaphore sem;
    struct cdev cdev;
};

struct driver_struct{
   dev_t registered_dev;

   struct device *devices;
   int device_count;

   struct proc_dir_entry *proc_dir;
};

static int major = 0;
module_param(major, int, S_IRUGO|S_IWUSR);

int do_open(struct inode *, struct file *);
int do_release(struct inode *, struct file *);
ssize_t do_read(struct file *, char __user *, size_t, loff_t *);
ssize_t do_write(struct file *, const char __user *, size_t, loff_t *);

int read_proc(char *page, char **start, off_t offset, int count,
                 int *eof, void *data);

struct file_operations my_fops = {
    .owner = THIS_MODULE,
};
// variables

struct driver_struct driver;

// init & exit

void init_driver(struct driver_struct *driver){
  int _err;
  driver->device_count=DEV_CNT;
  if(major == 0){
    log_debug("Using dynamic device allocation.");
    _err = alloc_chrdev_region(
    	&driver->registered_dev, FIRST_MINOR, driver->device_count,
    	DEFAULT_MODULE_NAME
    );
  }else{
      log_debug("Allocating device with major %i.", major);
      driver->registered_dev = MKDEV(major, FIRST_MINOR);
      _err = register_chrdev_region(
	driver->registered_dev, driver->device_count, DEFAULT_MODULE_NAME);
  }
  driver->proc_dir = create_proc_read_entry(DEFAULT_MODULE_NAME, 0,
    NULL, read_proc, NULL);
  if(_err){
  	log_error("Error code %i!!!", _err);
	return;
  }else{
      log_debug(
	"Registered device. Root: %i, %i.",
	MAJOR(driver->registered_dev), MINOR(driver->registered_dev))
  }
}

void init_device(struct device *device, dev_t chrDev){
  int _err, i;
  struct cdev *cDev;
  // module data structure init
  device->size = MEM_SIZE;
  device->read_pos = device->write_pos = device->used_size = 0;
  device->memory = (char *)kmalloc(sizeof(char) * device->size, ALLOC_MEM_MODE);
  if( !device->memory){
      log_error("Failed to allocate device memory!");
  }else{
      for(i=0; i < device->size; i++){
	  device->memory[i] = '\0';
      }
  }
  init_rwsem(&(device->sem));
  cDev = &(device->cdev);
  cdev_init(cDev, &my_fops);
  _err = cdev_add(cDev, chrDev, 1);
  if(_err){
  	log_error("Error %d adding device (major %i minor %i).",
		_err, MAJOR(chrDev), MINOR(chrDev));
	return;
  }
}

void init_driver_devices(struct driver_struct *driver, int count){
    int i, major, minor;
    driver->devices = (struct device *)kmalloc(
	   sizeof(struct device) * count, GFP_KERNEL);
    if(!driver->devices){
	log_error("Failed to allocate devices.");
	return;
    }
    major = MAJOR(driver->registered_dev);
    minor = MINOR(driver->registered_dev);
    for(i=0;i<count;i++){
	log_debug("Initializing device %i, %i", major, minor + i);
	init_device(&(driver->devices[i]), MKDEV(major, minor + i));
    }
}

static int driver_init(void) {
  log_debug("--------> Load init.");
  // file operations init
  my_fops.open = do_open;
  my_fops.release = do_release;
  my_fops.read = do_read;
  my_fops.write = do_write;
  ///
  init_driver(&driver);
  init_driver_devices(&driver, DEV_CNT);
  return 0;
}

void cleanup_devices(void){
	log_debug("Unregistering chardev region...");
	unregister_chrdev_region(driver.registered_dev, DEV_CNT);
}

void cleanup_memory(void){
    int i;
    struct device *cur_dev;
    for(i=0; i < driver.device_count; i++){
	cur_dev = &(driver.devices[i]);
    	if(cur_dev->memory){
		kfree(cur_dev->memory);
    	}else{
		log_debug("No memory was allocated.");
    	}
    }
    kfree(driver.devices);
    if(driver.proc_dir){
	remove_proc_entry(DEFAULT_MODULE_NAME, NULL);
	driver.proc_dir = NULL;
    }
}

static void driver_exit(void) {
  cleanup_devices();
  cleanup_memory();
  log_debug("--------> Module removed.");
}

module_init(driver_init);
module_exit(driver_exit);

// file operations

int do_open(struct inode *inode, struct file *file_p){
    struct device *dev = NULL;
    dev = container_of(inode->i_cdev, struct device, cdev);
    if(!dev){
	log_error("Error obtaining device!");
	return -EFAULT;
    }
    if( file_p->f_mode & FMODE_WRITE){
	log_debug("File opened for writing.");
	down_write(&(dev->sem));
    }else{
	if(! file_p->f_mode & FMODE_READ){
	    log_debug("Is it possible to open file not for writing and nor for reading?");
	}
    }
    dev->read_pos = dev->write_pos = 0;
    file_p->private_data = dev;
    log_debug("Device opened.");
    return 0;
}

int do_release(struct inode *inode, struct file *file_p){
    struct device *dev = NULL;
    if(file_p->private_data){
        dev = (struct device *)file_p->private_data;
	dev->read_pos = dev->write_pos = 0;
	if( file_p->f_mode & FMODE_WRITE){
	    up_write(&(dev->sem));
	}
	file_p->private_data = NULL;
	log_debug("Device released.");
    }else{
	log_error("Releasing non opened device?");
    }
    return 0;
}

ssize_t do_read(struct file *file, char __user *usr, size_t size, loff_t *offset){
    int err, readed=0;
    struct device *dev = NULL;
    dev = (struct device *)file->private_data;
    if(!down_read_trylock(&(dev->sem))){
	log_debug("Failed to down semaphore for reading. Seems that file was opened for writing.");
	return -EFAULT;
    }
    if(dev->read_pos < dev->used_size){
	readed = (dev->used_size - dev->read_pos > size ? size : dev->used_size - dev->read_pos);
	err = copy_to_user(usr, dev->memory + dev->read_pos, readed);
	if(err){
	    log_error("Error coping data to user! (%i)", err);
	    return -EFAULT;
	}
	dev->read_pos += readed;
    }
    up_read(&(dev->sem));
    return readed;
}

ssize_t do_write(struct file *file, const char __user *usr, size_t size, loff_t *offset){
    struct device *dev = NULL;
    int err, delta;
    dev = (struct device *)file->private_data;
    if(dev->write_pos == 0){
	dev->used_size = 0;
    }
    if(size + dev->write_pos > dev->size){
	delta = dev->size;
	while(size + dev->write_pos > delta){
	    delta *= 2;
	}
	log_debug("Reallocating memory. Old size: %i, new: %i.", dev->size, delta);
	dev->memory = (char *)krealloc(
	    dev->memory, sizeof(char) * delta, ALLOC_MEM_MODE);
	if(!dev->memory){
	    log_error("Failed to reallocate memory.");
	    return -EFAULT;
	}
	dev->size = delta;
    }
    err = copy_from_user(dev->memory + dev->write_pos, usr, size);
    if(err){
	log_error("Error copying data from user (%i).", err);
	return -EFAULT;
    }else{
	dev->write_pos += size;
	dev->used_size += size;
	log_debug("Obtained %i bytes from user.", size);
    }
    return size;
}

// PROC

int read_proc(char *page, char **start, off_t offset, int count,
                 int *eof, void *data){
    int limit = count - 64, written=0, i;
    struct device *dev;
    for(i=0;i < driver.device_count && written < limit; i++){
	dev = &(driver.devices[i]);
	if(!dev){
	    log_error("Strange device with index %i", i);
	    return -EFAULT;
	};
	written += sprintf(page + written,
	    "[Device %i]\n"
	    "\tsize: %u\n"
	    "\tused size: %u\n",
	    i, dev->size, dev->used_size);
    }
    return written;
}

// vim: set sts=4 sw=4 :
