#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

// Constants
#define FIRST_MINOR 0
#define DEV_CNT (unsigned int)5
#define DEFAULT_MODULE_NAME "charDev"
#define MEM_SIZE 1024*4
#define LOG_PREFIX "[" DEFAULT_MODULE_NAME ":" TOSTRING(__LINE__) "] "
#define EOL "\n"

// Macro
#define log_debug(x, ...) printk(KERN_DEBUG LOG_PREFIX x EOL, ##__VA_ARGS__);
#define log_error(x, ...) printk(KERN_ALERT LOG_PREFIX x EOL, ##__VA_ARGS__);

MODULE_LICENSE("MIT");

// module param definition
static char *module_name;
module_param(module_name, charp, S_IRUGO|S_IWUSR);

// module variables

struct my_dev_struct{
    char* memory;
    int size;
    int pos;

    // OS-related field
    struct semaphore sem;
    struct cdev devices;
    dev_t registered_dev;
};

int do_open(struct inode *, struct file *);
int do_release(struct inode *, struct file *);
ssize_t do_read(struct file *, char __user *, size_t, loff_t *);
ssize_t do_write(struct file *, const char __user *, size_t, loff_t *);

struct file_operations my_fops = {
    .owner = THIS_MODULE,
};

// variables

struct my_dev_struct my_dev;

// janitor functions

int sstrlen(char *str){
    // 'safe' strlen
    int len, size;
    if(!str){
	return -1;
    }
    size = 255;
    for(len=0; len<size; len++){
	if(str[len] == '\0'){
	    break;
	}
    }
    if(len == size){
	log_error("Error getting size of string. Returning -1.");
	len = -1;
    }
    return len;
}

// init & exit

void init_driver_memory(void){
  int i;
  // file operations init
  my_fops.open = do_open;
  my_fops.release = do_release;
  my_fops.read = do_read;
  my_fops.write = do_write;
  // module data structure init
  my_dev.size = MEM_SIZE;
  my_dev.pos = my_dev.registered_dev = 0;
  my_dev.memory = (char *)kmalloc(sizeof(char) * my_dev.size, GFP_KERNEL);
  if( !my_dev.memory){
      log_error("Failed to allocate device memory!");
  }else{
      for(i=0; i < my_dev.size; i++){
	  my_dev.memory[i] = '-';
      }
  }
  cdev_init(&(my_dev.devices), &my_fops);
  my_dev.devices.owner = THIS_MODULE;
  my_dev.devices.ops = &my_fops;
  init_MUTEX(&(my_dev.sem));
}

void init_driver_devices(void){
  int _err, i, major;
  dev_t registered_dev, cur_dev;
  // device registration
  if(sstrlen(module_name) < 1){
      log_error("Error obtaining device name. Using default '%s'",
	      DEFAULT_MODULE_NAME)
      // just assigning pointers (unsafe but fast & simple)
      module_name = DEFAULT_MODULE_NAME;
  }
  log_debug("Using device name '%s'.", module_name)
  log_debug("Allocating chardev.");
  _err = alloc_chrdev_region(
	&registered_dev, FIRST_MINOR, DEV_CNT, module_name);
  if(_err){
  	log_error("Error code %i!!!", _err);
	return;
  }
  // saving for freeing
  my_dev.registered_dev = registered_dev;
  major = MAJOR(registered_dev);
  for(i=0; i < DEV_CNT; i++){
      	cur_dev = MKDEV(major, FIRST_MINOR + i);
  	log_debug("Major: %i, minor: %i.", MAJOR(cur_dev), MINOR(cur_dev));
  	_err = cdev_add(&(my_dev.devices), cur_dev, 1);
  	if(_err){
      		log_error("Error %d adding device (index %i).", _err, i);
		break;
  	}
  }
}

static int driver_init(void) {
  log_debug("--------> Load init.");
  init_driver_memory();
  init_driver_devices();
  return 0;
}

void cleanup_devices(void){
    if(my_dev.registered_dev){
	log_debug("Unregistering chardev region...");
	unregister_chrdev_region(my_dev.registered_dev, DEV_CNT);
    }
}

void cleanup_memory(void){
    if(my_dev.memory){
	kfree(my_dev.memory);
    }else{
	log_debug("No memory was allocated.");
    }
}

static void driver_exit(void) {
  cleanup_devices();
  cleanup_memory();
  log_debug("--------> Module removed.");
}

module_init(driver_init);
module_exit(driver_exit);

// file operations

int do_open(struct inode *inode, struct file *file_p){
    struct my_dev_struct *dev;
    dev = container_of(inode->i_cdev, struct my_dev_struct, devices);
    log_debug("Opened.");
    file_p->private_data = dev;
    return 0;
}

int do_release(struct inode *inode, struct file *file_p){
    file_p->private_data = NULL;
    log_debug("Released.");
    return 0;
}

ssize_t do_read(struct file* filep, char __user *user, size_t size, loff_t *loff){
    struct my_dev_struct *dev;
    int pos, read_size;
    unsigned long err;

    dev = filep->private_data;
    if(!dev){
	log_error("No device specified!");
	return -EFAULT;
    }
    if(loff){
	pos = *loff;
    }else{
	log_debug("no offset??");
	return -EFAULT;
    }
    if (down_interruptible(&dev->sem)){
	log_error("Semaphore error!");
        return -ERESTARTSYS;
    }
    // Actual copy to user
    while(pos < size){
	read_size = (dev->size - pos);
	if(read_size < 0){
	    log_error("Something awful happened.");
	    return -EFAULT;
	}
	if(read_size > dev->size){
	    read_size = dev->size;
	}
    	err = copy_to_user(user, dev->memory + pos, read_size);
    	log_debug("Read (size %i)", read_size);
	pos += read_size;
    }
    up(&dev->sem);
    return pos;
}


ssize_t do_write(struct file* filep, const char __user *user, size_t size, loff_t *offset){
    int pos, out;
    struct my_dev_struct *dev;
    unsigned long err;
    dev = filep->private_data;
    if(!dev){
	log_error("No device specified!");
	return -EFAULT;
    }
    pos = dev->pos;
    log_debug("Writing %i with offset %i", size, pos);
    if (down_interruptible(&dev->sem)){
	log_error("Semaphore error!");
        return -ERESTARTSYS;
    }
    if(size + pos > dev->size){
	pos = 0;
    	err = copy_from_user(dev->memory, user, dev->size);
	out = dev->size;
    }else{
    	err = copy_from_user(dev->memory + pos, user, size);
	pos += size;
	out = size;
    }
    up(&dev->sem);
    if(err){
    	log_error("Copy error (%lu).", err);
    	return -EFAULT;
    }
    dev->pos = pos;
    return out;
}
