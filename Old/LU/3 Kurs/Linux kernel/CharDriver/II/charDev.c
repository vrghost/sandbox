#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

// Constants
#define FIRST_MINOR 0
#define DEV_CNT (unsigned int)6
#define DEFAULT_MODULE_NAME "charDev"
#define MEM_SIZE 512
#define LOG_PREFIX "[" DEFAULT_MODULE_NAME ":" TOSTRING(__LINE__) "] "
#define EOL "\n"

// Macro
#define log_debug(x, ...) printk(KERN_DEBUG LOG_PREFIX x EOL, ##__VA_ARGS__);
#define log_error(x, ...) printk(KERN_ALERT LOG_PREFIX x EOL, ##__VA_ARGS__);

MODULE_LICENSE("MIT");

// module variables

struct pipe_head{
    char* memory;
    int size;
    int read_pos, write_pos;
    struct pipe_head *other_head;
    ssize_t (*write)(struct pipe_head *, const char __user *, ssize_t);


    // OS-related field
    struct semaphore sem;
    struct cdev cdev;
};

struct driver_struct{
   dev_t registered_dev;

   struct pipe_head *devices;
   int device_count;

   struct proc_dir_entry *proc_dir;
};


int do_open(struct inode *, struct file *);
int do_release(struct inode *, struct file *);
ssize_t do_read(struct file *, char __user *, size_t, loff_t *);
ssize_t do_write(struct file *, const char __user *, size_t, loff_t *);

ssize_t pipe_write(struct pipe_head *, const char __user *, ssize_t);

int read_proc(char *page, char **start, off_t offset, int count,
                 int *eof, void *data);

struct file_operations my_fops = {
    .owner = THIS_MODULE,
};
// variables

struct driver_struct driver;

// janitor functions

int sstrlen(char *str){
    // 'safe' strlen
    int len, size;
    if(!str){
	return -1;
    }
    size = 255;
    for(len=0; len<size; len++){
	if(str[len] == '\0'){
	    break;
	}
    }
    if(len == size){
	log_error("Error getting size of string. Returning -1.");
	len = -1;
    }
    return len;
}

// init & exit

void init_driver(struct driver_struct *driver){
  int _err;
  driver->device_count=DEV_CNT;
  _err = alloc_chrdev_region(
	&driver->registered_dev, FIRST_MINOR, driver->device_count,
	DEFAULT_MODULE_NAME
  );
  driver->proc_dir = create_proc_read_entry(DEFAULT_MODULE_NAME, 0,
    NULL, read_proc, NULL);
  if(_err){
  	log_error("Error code %i!!!", _err);
	return;
  }
}

void init_device(struct pipe_head *device, dev_t chrDev){
  int _err, i;
  struct cdev *cDev;
  // module data structure init
  device->size = MEM_SIZE;
  device->read_pos = device->write_pos = 0;
  device->memory = (char *)kmalloc(sizeof(char) * device->size, GFP_KERNEL);
  device->write = pipe_write;
  if( !device->memory){
      log_error("Failed to allocate device memory!");
  }else{
      for(i=0; i < device->size; i++){
	  device->memory[i] = '\0';
      }
  }
  init_MUTEX(&(device->sem));
  log_debug("Adding device %i:%i", MAJOR(chrDev), MINOR(chrDev));
  cDev = &(device->cdev);
  cdev_init(cDev, &my_fops);
  _err = cdev_add(cDev, chrDev, 1);
  if(_err){
  	log_error("Error %d adding device (major %i minor %i).",
		_err, MAJOR(chrDev), MINOR(chrDev));
	return;
  }
}

void init_driver_devices(struct driver_struct *driver, int count){
    int i, major;
    struct pipe_head *head_a, *head_b;
    driver->devices = (struct pipe_head *)kmalloc(
	   sizeof(struct pipe_head) * count, GFP_KERNEL);
    if(!driver->devices){
	log_error("Failed to allocate devices.");
	return;
    }
    major = MAJOR(driver->registered_dev);
    for(i=0;i<count;i++){
	init_device(&(driver->devices[i]), MKDEV(major, FIRST_MINOR + i));
    }
    // interconnect pipe heads
    for(i=0;i<count;i+=2){
	log_debug("Pairing pipes %i<-->%i", i, i+1);
	head_a = &(driver->devices[i]);
	head_b = &(driver->devices[i+1]);
	head_a->other_head = head_b;
	head_b->other_head = head_a;
    }
}

static int driver_init(void) {
  log_debug("--------> Load init.");
  if(DEV_CNT & (DEV_CNT % 2)){
      log_error("Dev count must be pair and non-zero.");
      return -EFAULT;
  }
  // file operations init
  my_fops.open = do_open;
  my_fops.release = do_release;
  my_fops.read = do_read;
  my_fops.write = do_write;
  ///
  init_driver(&driver);
  init_driver_devices(&driver, DEV_CNT);
  return 0;
}

void cleanup_devices(void){
	log_debug("Unregistering chardev region...");
	unregister_chrdev_region(driver.registered_dev, DEV_CNT);
}

void cleanup_memory(void){
    int i;
    struct pipe_head *cur_dev;
    for(i=0; i < driver.device_count; i++){
	cur_dev = &(driver.devices[i]);
    	if(cur_dev->memory){
		kfree(cur_dev->memory);
    	}else{
		log_debug("No memory was allocated.");
    	}
    }
    kfree(driver.devices);
    if(driver.proc_dir){
	remove_proc_entry(DEFAULT_MODULE_NAME, NULL);
	driver.proc_dir = NULL;
    }
}

static void driver_exit(void) {
  cleanup_devices();
  cleanup_memory();
  log_debug("--------> Module removed.");
}

module_init(driver_init);
module_exit(driver_exit);

// file operations

int do_open(struct inode *inode, struct file *file_p){
    struct pipe_head *dev = NULL;
    dev = container_of(inode->i_cdev, struct pipe_head, cdev);
    if(!dev){
	log_error("Error obtaining device!");
	return -EFAULT;
    }
    file_p->private_data = dev;
    return 0;
}

int do_release(struct inode *inode, struct file *file_p){
    file_p->private_data = NULL;
    return 0;
}

ssize_t do_read(struct file *file, char __user *usr, size_t size, loff_t *offset){
    int err, readed=0;
    struct pipe_head *dev = NULL;
    dev = (struct pipe_head *)file->private_data;
    if(down_interruptible(&(dev->sem))){
	log_error("Failed accruing semaphore.");
	return -EFAULT;
    }
    while(dev->read_pos != dev->write_pos){
	err = copy_to_user(usr + readed, dev->memory + dev->read_pos, 1);
	if(err){
	    log_error("Error coping data to user! (%i)", err);
	    return -EFAULT;
	}
	readed++;
	dev->read_pos = (dev->read_pos + 1) % dev->size;
    }
    up(&(dev->sem));
    return readed;
}

ssize_t do_write(struct file *file, const char __user *usr, size_t size, loff_t *offset){
    struct pipe_head *dev = NULL;
    dev = (struct pipe_head *)file->private_data;
    return dev->other_head->write(dev->other_head, usr, size);
}

ssize_t pipe_write(struct pipe_head *self, const char __user *what, ssize_t size){
    ssize_t written = 0;
    int err, delta;
    if(down_interruptible(&(self->sem))){
	log_error("Failed accruing semaphore.");
	return -EFAULT;
    }
    delta = (self->read_pos - self->write_pos + self->size) % self->size;
    if(delta < size){
	while(delta < size){
	    delta *= 2;
	}
	log_debug("Reallocating memory. Old size: %i, new: %i.", self->size, delta);
	self->memory = (char *)krealloc(self->memory, sizeof(char) * delta);
	self->size = delta;
    }
    while(written != size){
	err = copy_from_user(self->memory + self->write_pos, what + written, 1);
	if(err){
	    log_error("Error copying data from user (%i).", err);
	    return -EFAULT;
	}
	written ++;
	self->write_pos = (self->write_pos + 1) % self->size;
    }
    up(&(self->sem));
    return written;
}

// PROC

int read_proc(char *page, char **start, off_t offset, int count,
                 int *eof, void *data){
    int limit = count - 64, written=0, i;
    struct pipe_head *dev;
    for(i=0;i < driver.device_count && written < limit; i++){
	dev = &(driver.devices[i]);
	if(!dev){
	    log_error("Strange device with index %i", i);
	    return -EFAULT;
	};
	written += sprintf(page + written,
	    "[Device %i]\n"
	    "\tsize: %i\n"
	    "\tread pos: %i\n"
	    "\twrite pos:%i\n",
	    i, dev->size, dev->read_pos, dev->write_pos);
    }
    return written;
}

// vim: set sts=4 sw=4 :
