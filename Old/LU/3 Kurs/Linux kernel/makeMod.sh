#!/bin/bash

BUILD_ROOT=/tmp/build_module
BIN_DIR=bin/
_result=0

if [ -e $BUILD_ROOT ] ; then
	rm -rf $BUILD_ROOT/*
else
	mkdir -p $BUILD_ROOT
fi

cp -r ./* $BUILD_ROOT
if [ ! $? ] ; then
    echo "Error copying files."
    exit $?
fi

pushd $BUILD_ROOT

make $*
if [ ! $? ] ; then
    echo "Error building."
    popd
    exit $?
fi

popd

if [ -e $BIN_DIR ] ; then
	rm -rf $BIN_DIR/*
else
	mkdir $BIN_DIR
fi

`cp $BUILD_ROOT/*.ko $BIN_DIR`
exit $?

# vim: set ff=unix
