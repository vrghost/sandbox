# -*- coding: utf-8 -*-

import time

import os
import sys
import math
import itertools
import operator
from random import random

_logFile = None
def log(msg):
    global _logFile
    if _logFile is None:
        _fileDir = os.path.split(os.path.realpath(sys.argv[0]))[0]
        _logName = os.path.join(_fileDir, "hmm.log")
        _logFile = file(_logName, "w")
    msg = msg.rstrip()
    _logFile.write(msg + "\n")
    print msg

def getValues(seq):
    return "".join(map(lambda _l: _l.value, seq))

def statesToStr(seq):
    return "-".join(map(str, (_el.id for _el in seq)))

def getStates(seq):
    return statesToStr(_el.state for _el in seq)

def configToStr(el, depth=1):
    assert isinstance(el, dict)
    _out = ""
    _mask = (" " * (depth * 4)) + "%s: %s\n"
    for (_name, _value) in el.iteritems():
        if isinstance(_value, dict):
            _value = "\n" + configToStr(_value, depth+1)
        _out += _mask % (_name, _value)
    return _out


class Element(object):

    def __init__(self, state, value):
        self.state = state
        self.value = value

    def __repr__(self):
        return "-%s-" % self.value

class _Chooser(object):

    def chose(self, itemProbs):
        _value = random()
        _minProb = 0
        for (_item, _prob) in itemProbs:
            _minProb += _prob
            if _value < _minProb:
                return _item
        raise RuntimeError("Failed to choose!.")

class HmmState(_Chooser):

    def __init__(self, id):
        # we need to create object first.
        # It will be filled later
        self.id = id

    def __repr__(self):
        return "<%s id=%s>" % (self.__class__.__name__, self.id)

    def setData(self, trans, emit):
        # this is a real init method
        self.trans = trans
        self.emit = dict(emit)

    def genEmission(self):
        return Element(self, self.chose(self.emit.iteritems()))

    def nextState(self):
        return self.chose(self.trans.iteritems())

    def emitProb(self, element):
        return self.emit.get(element.value, 0)

    def transProb(self, targ):
        return self.trans[targ]

class Hmm(_Chooser):

    states = curState = None

    def __init__(self, transm, emissions, initProb):
        assert len(transm) == len(emissions) == len(initProb)
        _states = dict((_it, HmmState(_it))
            for _it in xrange(1, len(transm) + 1))
        _initPr = {}
        for _stateId in xrange(1, len(transm) + 1):
            _stateTrans = {}
            _trans = transm[_stateId]
            _emit = emissions[_stateId]
            _state = _states[_stateId]
            for (_id, _prob) in _trans.iteritems():
                _target = _states[_id]
                _stateTrans[_target] = _prob
            _state.setData(_stateTrans, _emit)
            # also fixing init probs
            _initPr[_state] = initProb[_stateId]
        self.states = _states
        self.initProb = _initPr

    def reset(self):
        self.curState = self.chose(self.initProb.iteritems())

    def emit(self):
        while True:
            yield self.curState.genEmission()
            self.curState = self.curState.nextState()

    def getForward(self, line):
        # forward algorithm implementation
        (_start, _line) = (line[0], line[1:])
        _newProbs = _probs = {}
        for (_el, _prob) in self.initProb.iteritems():
            _probs[_el] = _el.emitProb(_start) * _prob
        for _el in _line:
            _newProbs = {}
            for _to in self.states.itervalues():
                _probSum = 0
                for (_from, _prob) in _probs.iteritems():
                    _probSum += _from.transProb(_to) * _prob
                _probSum *= _to.emitProb(_el)
                _newProbs[_to] = _probSum
            _probs = _newProbs
        assert _probs == _newProbs
        return _probs

    def getForwardProb(self, line):
        return sum(self.getForward(line).itervalues())


    def getBackward(self, line):
        (_start, _line) = (line[-1], line[:-1])
        _newProbs = _probs = dict(
            (_state, _state.emitProb(_start))
            for _state in self.states.itervalues()
        )
        for _el in reversed(_line):
            _newProbs = {}
            for _source in self.states.itervalues():
                _probSum = 0
                for (_res, _prob) in _probs.iteritems():
                    _probSum += _source.transProb(_res) * _prob
                _probSum *= _source.emitProb(_el)
                _newProbs[_source] = _probSum
            _probs = _newProbs
        assert _probs == _newProbs
        return _probs

    def getBackwardProb(self, line):
        _sum = 0
        for (_state, _prob) in self.getBackward(line).iteritems():
            _sum += _prob * self.initProb[_state]
        return _sum

    def getVeritabi(self, line):
        # veritabi algorithm implementation
        (_start, _line) = (line[0], line[1:])
        _probs = []
        for (_state, _prob) in self.initProb.iteritems():
            _probs.append({
                "state": _state,
                "prob": _prob * _state.emitProb(_start),
                "from": None,
            })
        _way = [_probs]
        for _el in _line:
            _lastWay = _way[-1]
            _newStep = []
            for _target in self.states.itervalues():
                _stat = []
                for _source in _lastWay:
                    _from = _source["state"]
                    _prob = _source["prob"]
                    _stat.append({
                        "from": _from,
                        "prob": _prob * _from.transProb(_target),
                    })
                _best = max(_stat, key=lambda el: el["prob"])
                _best["state"] = _target
                _best["prob"] *= _target.emitProb(_el)
                _newStep.append(_best)
            _way.append(_newStep)
        _best = max(_way.pop(), key=lambda el: el["prob"])
        _prob = _best["prob"]
        _traceback = [_best["state"], _best["from"]]
        while _way:
            _prev = _traceback[-1]
            _possibleFrom = [
                _el for _el in _way.pop() if _el["state"] == _prev]
            assert len(_possibleFrom) == 1
            _traceback.append(_possibleFrom[0]["from"])
        assert _traceback[-1] is None
        _traceback.pop()
        return {"way": reversed(_traceback), "probability": _prob}

class A_matrix(object):

    def __init__(self, sequences):
        self.seq = sequences
        self._states = set(_el.state for _el in itertools.chain(*sequences))
        self._symbs = set(_el.value for _el in itertools.chain(*sequences))
        self._calculated = {}
        self._totals = {}

    def _countTrans(self, fr, to):
        _count = 0
        for _line in self.seq:
            for (_pos, _ch) in enumerate(_line[:-1]):
                if _ch.state == fr and _line[_pos+1].state == to:
                    _count += 1
        return _count

    def count(self, _from, _to):
        if (_from, _to) in self._calculated:
            _out = self._calculated[(_from, _to)]
        else:
            _out = self._countTrans(_from, _to)
            self._calculated[(_from, _to)] = _out
        return _out

    def maxProb(self, fr, to):
        assert isinstance(fr, HmmState)
        assert isinstance(to, HmmState)
        _curCount = self.count(fr, to)
        if (fr, to) in self._totals:
            _totalCount = self._totals[(fr, to)]
        else:
            _totalCount = sum(self.count(fr, _st) for _st in self._states)
            self._totals[(fr, to)] = _totalCount
        return float(_curCount) / _totalCount

class E_matrix(object):

    def __init__(self, sequences):
        self.seq = sequences
        self._states = set(_el.state for _el in itertools.chain(*sequences))
        self._symbs = set(_el.value for _el in itertools.chain(*sequences))
        self._calc = {}
        self._totals = {}

    def _countSymb(self, state, symb):
        _count = 0
        for _ch in itertools.chain(*self.seq):
            if _ch.value == symb and _ch.state == state:
                _count += 1
        return _count

    def count(self, state, symb):
        if (state, symb) in self._calc:
            _out = self._calc[(state, symb)]
        else:
            _out = self._countSymb(state, symb)
            self._calc[(state, symb)] = _out
        return _out

    def maxProb(self, state, symb):
        assert isinstance(state, HmmState)
        assert isinstance(symb, basestring)
        _thisCount = self.count(state, symb)
        if (state, symb) in self._totals:
            _totalCount = self._totals[(state, symb)]
        else:
            _totalCount = sum(self.count(state, _sym) for _sym in self._symbs)
            self._totals[(state, symb)] = _totalCount
        return float(_thisCount) / _totalCount

class HmmOptimizer(object):

    def __init__(self, hmm, sequences):
        sequence = tuple(sequences)
        self.seq = sequences
        self.hmm = hmm
        self.a = A_matrix(sequences)
        self.e = E_matrix(sequences)
        self.states = set(hmm.states.itervalues())
        self.symbols = set(_el.value for _el in itertools.chain(*sequences))

    def aMatrix(self):
        _out = {}
        for _src in self.states:
            _out[_src.id] = _collector = {}
            for _target in self.states:
                _collector[_target.id] = \
                    self._getNewTransKoef(_src, _target)
            # normalizing values...
            _total = float(sum(_collector.itervalues()))
            for (_name, _value) in _collector.iteritems():
                _collector[_name] = _value / _total
        return _out

    def _getNewTransKoef(self, src, trg):
        _sum = 0
        for _line in self.seq:
            _divisor = float(self.hmm.getForwardProb(_line))
            _divided = 0
            for _ii in xrange(1, len(_line)-1):
                _head = _line[:_ii]
                _symb = _line[_ii]
                _tail = _line[_ii+1:]
                _divided += reduce(operator.mul, (
                    self.hmm.getForward(_head)[src],
                    self.a.maxProb(src, trg),
                    self.e.maxProb(trg, _symb.value),
                    self.hmm.getBackward(_tail)[trg],
                ))
            _sum += _divided / _divisor
        return _sum

    def eMatrix(self):
        _out = {}
        for _state in self.states:
            _collection = _out[_state.id] = {}
            for _symb in self.symbols:
                _collection[_symb] = self._getEmissionKoef(_state, _symb)
            # normalizing...
            _total = float(sum(_collection.itervalues()))
            for (_name, _value) in _collection.iteritems():
                _collection[_name] = _value / _total
        return _out

    def _getEmissionKoef(self, state, symbol):
        _out = 0
        for _seq in self.seq:
            _divisor = float(self.hmm.getForwardProb(_seq))
            _divided = 0
            for _pos in xrange(1, len(_seq)):
                if _seq[_pos].value != symbol:
                    continue
                _head = _seq[:_pos]
                _tail = _seq[_pos:]
                _divided += self.hmm.getForward(_head)[state] * \
                    self.hmm.getBackward(_tail)[state]
            _out += _divided / _divisor
        return _out

    _optimalEmissions = _optimalTrans = _optimalInit = None

    def getBetterHmm(self):
        if self._optimalEmissions is None:
            self._optimalEmissions = _optimizer.eMatrix()
        log("Got optimal emissions.")
        if self._optimalTrans is None:
            self._optimalTrans = _optimizer.aMatrix()
        log("Got optimal transmissions.")
        # seems that all init probs are equal for optimal hmm
        if self._optimalInit is None:
            self._optimalInit = {
                1: 0.25,
                2: 0.25,
                3: 0.25,
                4: 0.25,
            }
        log("Optimized HMM configuration.")
        log("Transition matrix:")
        log(configToStr(self._optimalTrans))
        log("\nEmission matrix:")
        log(configToStr(self._optimalEmissions))
        log("\nState initialization matrix:")
        log(configToStr(self._optimalInit))
        log("\n\n--------\n\n")
        return Hmm(self._optimalTrans, self._optimalEmissions, self._optimalInit)

if __name__ == "__main__":
    ##### beginning of hmm config
    transitionProbability = {
        1: {
            1: 0.180,
            2: 0.274,
            3: 0.426,
            4: 0.120,
        },
        2: {
            1: 0.171,
            2: 0.368,
            3: 0.274,
            4: 0.187,
        },
        3: {
            1: 0.161,
            2: 0.339,
            3: 0.375,
            4: 0.125,
        },
        4: {
            1: 0.079,
            2: 0.355,
            3: 0.384,
            4: 0.182,
        },
    }
    emissionMatrix = {
        1:  {
            "A": 0.8,
            "T": 0.2,
        },
        2:  {
            "C": 0.4,
            "G": 0.6,
        },
        3:  {
            "C": 0.6,
            "G": 0.4,
        },
        4:  {
            "A": 0.2,
            "T": 0.8,
        },
    }
    initProb = {
        1:  0.25,
        2:  0.25,
        3:  0.25,
        4:  0.25,
    }
    # generation options
    lineLength = 20
    lineCount = 100
    ############# end of hmm config
    log("Original HMM configuration.")
    log("Transition matrix:")
    log(configToStr(transitionProbability))
    log("\nEmission matrix:")
    log(configToStr(emissionMatrix))
    log("\nState initialization matrix:")
    log(configToStr(initProb))
    log("\n\n--------\n\n")
    _hmm = Hmm(
        transitionProbability,
        emissionMatrix,
        initProb,
    )
    _lines = []
    _total = 0
    for _seq in xrange(lineCount):
        _hmm.reset()
        _line = tuple(itertools.islice(_hmm.emit(), lineLength))
        _lines.append(_line)
        _prob = math.log(_hmm.getForwardProb(_line))
        _total += _prob
        log(" ".join((
            "%s)" % _seq,
            getValues(_line),
            "score: %s" % _prob,
        )))
    log("==============\n" + \
        "TOTAL: %s\n" % _total + \
        "------\n" + \
        "Generating optimized hmm...\n")
    _optimizer = HmmOptimizer(_hmm, _lines)
    _betterHmm = _optimizer.getBetterHmm()
    _betterScore = 0
    for (_seq, _line) in enumerate(_lines):
        _prob = math.log(_betterHmm.getForwardProb(_line))
        _veritabi = _betterHmm.getVeritabi(_line)
        log("\n".join((
            "",
            "[%s]" % _seq,
            getValues(_line),
            "score: %s" % _prob,
            "real path: %s" % getStates(_line),
            "veritabi path: %s" % statesToStr(_veritabi["way"]),
            "veritabi probability: %s" % math.log(_veritabi["probability"]),
        )))
        _betterScore += _prob
    log("=================\nBetter score: %s" % _betterScore)


# vim: set et sts=4 sw=4 :
