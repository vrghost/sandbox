# -*- coding: utf-8 -*-

import util


#wordA = " AGC"
#wordB = " AAAC"
wordA = " ACGACG"
wordB = " ACGAC"

_matr = util.Matrix.Matrix(wordA, wordB)

insert = -2

def filler(matrix, charA, charB, x, y):
    if x == 0 and y == 0:
        return 0
    _values = []
    if y > 0:
        _values.append(((matrix[y-1][x] + insert), matrix.UP))
    if x > 0:
        _values.append(((matrix[y][x-1] + insert), matrix.LEFT))
    if x > 0 and y > 0:
        if charA == charB == "-":
            _score = 0
        elif "-" in (charA, charB):
            _score = -2
        else:
            _score = 1 if charA == charB else -1
        _values.append((
            matrix[y-1][x-1] + _score, matrix.REPLACE
        ))
    (_value, _direction) = max(_values, key=lambda el: el[0])
    _value = matrix.wrap(_value)
    _value.marks.append(_direction)
    return _value

_matr.fill(filler)

print _matr

print "\n".join(_matr.restorePaths()[0])

# vim: set et sts=4 sw=4 :
