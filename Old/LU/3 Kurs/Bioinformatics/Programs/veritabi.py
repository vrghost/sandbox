# -*- coding: utf-8 -*-

import math
import itertools
from random import random

trans = {
    1: {
        1: 0.7,
        2: 0.3,
    },
    2: {
        1: 0.3,
        2: 0.7,
    },
}
emiss = {
    1: {
        "A": 0.7,
        "B": 0.1,
        "C": 0.2,
    },
    2: {
        "A": 0.1,
        "B": 0.5,
        "C": 0.4,
    },
}

# vim: set et sts=4 sw=4 :
