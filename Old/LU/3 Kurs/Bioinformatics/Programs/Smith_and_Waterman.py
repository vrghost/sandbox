# -*- coding: utf-8 -*-

import util

#wordA = " AGC"
#wordB = " AAAC"
wordA = " " "ILTAP"
wordB = " " "LPVSILT"

_matr = util.Matrix.Matrix(wordA, wordB)

insert = -8

def filler(matrix, charA, charB, x, y):
    if x == 0 or y == 0:
        return 0
    _values = [
        (0, None),
        ((matrix[y-1][x] + insert), matrix.UP),
        ((matrix[y][x-1] + insert), matrix.LEFT),
        (
            matrix[y-1][x-1] + util.matrixes.Blosum62.get(charA, charB)
        ,
            matrix.REPLACE
        ),
    ]
    (_value, _direction) = max(_values, key=lambda el: el[0])
    _value = matrix.wrap(_value)
    if _direction:
        _value.marks.append(_direction)
    return _value

_matr.fill(filler)

print _matr

print "\n".join(_matr.restorePaths()[0])

# vim: set et sts=4 sw=4 :
