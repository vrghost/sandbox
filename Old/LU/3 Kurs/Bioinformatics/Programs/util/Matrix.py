# -*- coding: utf-8 -*-

class MatrixElement(int):

    marks = None

    def __init__(self, value):
        super(MatrixElement, self).__init__(value)
        self.marks = []

class Matrix(object):

    _wordA = _wordB = _data = None

    UP = "UP"
    LEFT = "LEFT"
    REPLACE = "REPLACE"

    def __init__(self, wordA, wordB, default=0, filler=None):
        _out = []
        _w = len(wordA)
        _h = len(wordB)
        for _y in xrange(_h):
            _row = [MatrixElement(default) for _i in xrange(_w)]
            _out.append(_row)
        if filler:
            for _x in xrange(_w):
                self._setEl(_x, 0, filler(_x, 0, _out))
            for _y in xrange(_h):
                self._setEl(0, _y, filler(0, _y, _out))
        self._data = _out
        self._wordA = wordA
        self._wordB = wordB

    def wrap(self, value):
        if not isinstance(value, MatrixElement):
            value = MatrixElement(value)
        return value

    def _setEl(self, x, y, value):
        self._data[y][x] = self.wrap(value)

    def fill(self, filler):
        _w = len(self._data[0])
        _h = len(self._data)
        for _y in xrange(_h):
            assert _w == len(self._data[_y])
            _row = self._data[_y]
            for _x in xrange(_w):
                self._setEl(
                    _x, _y,
                    filler(self, self._wordA[_x], self._wordB[_y], _x, _y)
                )

    def _align(self, obj, leng):
        if len(str(obj)) > leng:
            return str(obj)[:leng]
        else:
            return str(obj).rjust(leng)

    def __str__(self):
        _out = "\n".join((
            "   |" + "  ".join((repr(_c) for _c in self._wordA)),
            "---+" + ("-----" * len(self._wordA)),
        ))
        _out += "\n"
        for _y in xrange(len(self._data)):
            _out += "%r|" % self._wordB[_y]
            _out += "".join((self._align(_el, 5) for _el in self._data[_y]))[3:]
            _out += "\n"
        return _out

    def __getitem__(self, index):
        return tuple(self._data[index])

    def _alignStr(self, x, y):
        _l = (x, y)
        _endA = self._wordA[x+1:]
        _endB = self._wordB[y+1:]
        while x or y:
            _el = self._data[y][x]
            _addA = self._wordA[x]
            _addB = self._wordB[y]
            if self.REPLACE in _el.marks:
                x -= 1
                y -= 1
            elif self.UP in _el.marks:
                y -= 1
                _addA = "-"
            elif self.LEFT in _el.marks:
                x -= 1
                _addB = "-"
            else:
                x = y = 0
            _endA = _addA + _endA
            _endB = _addB + _endB
        return (_endA, _endB)

    def restorePaths(self):
        # restore aliment
        _values = []
        for (_y, _row) in enumerate(self._data):
            _values.extend([(_el, _x, _y) for (_x, _el) in enumerate(_row)])
        _values.sort(reverse=True, key=lambda el: el[0])
        return [self._alignStr(*_v[1:]) for _v in _values]

# vim: set et sts=4 sw=4 :
