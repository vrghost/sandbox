/*
* accepred facts are:
*    const(X)
*    var(X)
*    ln(X)
*    plus(X, Y)
*    minus(X, Y)
*    mul(X, Y)
*    divide(X, Y)
*    power(X, Y)
*/

diff(const(_), _, const(0.0)).
diff(var(X), var(X), const(1.0)).
diff(var(X), var(D), var(X)):- not(X = D).
/* (u-v)' == (u+(-v))' */
diff(minus(X, Y), var(D), R):- diff(
    plus(X, mul(const(-1.0), Y)), var(D), R).

/* (u+v)' = u'+v' */
diff(plus(X, Y), var(D), plus(X1, Y1)):-
    diff(X, var(D), X1),
    diff(Y, var(D), Y1).

/* (uv)' = u'v + v'u */
diff(mul(X, Y), var(D), plus(mul(X1, Y), mul(Y1, X))):-
    diff(X, var(D), X1),
    diff(Y, var(D), Y1).

/* (u/v)' = (u'v-v'u)/(v^2); */
diff(divide(X, Y), var(D), divide(minus(mul(X1, Y), mul(Y1, X)), mul(Y,Y))):-
    diff(X, var(D), X1),
    diff(Y, var(D), Y1).

/* (ln u)' = u'/u. */
diff(ln(X), var(D), divide(X1, X)):-
    diff(X, var(D), X1).

/* (u^v)'=u^v (vu'/u+v' ln u) */
diff(power(X, Y), var(D), mul(power(X, Y), plus(div(mul(Y, X1), X), mul(Y1, ln(X))))):-
    diff(X, var(D), X1),
    diff(Y, var(D), Y1).

/* Example I/O:
*
* -- find differential on "42*x" by "x"
* {{{
* ?- diff(mul(const(42), var(x)), var(x), R).
* R = plus(mul(const(0.0), var(x)), mul(const(1.0), const(42))).
* }}}
* -- this is equivalent of R="0.0*x+1.0*42"="42"
*
* -- find differential on "x^3" by "x"
* {{{
* ?- diff(mul(var(x), mul(var(x), var(x))), var(x), R).
* R = plus(mul(const(1.0), mul(var(x), var(x))), mul(plus(mul(const(1.0), var(x)), mul(const(1.0), var(x))), var(x))) .
* }}}
* -- this is equivalent of R="1.0*x*x+(1.0*x+1.0*x)*x"="x^2+2*x^2"="3*x^2"
*
* -- find differential on "x^42" by "x" 
* {{{
* ?- diff(power(var(x), const(42)), var(x), R).
* R = mul(power(var(x), const(42)), plus(div(mul(const(42), const(1.0)), var(x)), mul(const(0.0), ln(var(x)))))
* }}}
* -- this is equivalent of R="x^42*(42/x+0.0*ln(x))"="42*x^41"
*/
