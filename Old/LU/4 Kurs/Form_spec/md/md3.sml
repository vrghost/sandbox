open Real;

(* ===== Variable management ===== *)

fun getEmptyContext() = fn name:string => 0.0;
fun setVar(name:string,what:real,tailContext) = 
    fn getName:string =>
      if name=getName then
        what
      else
        tailContext getName;

(* ===== Arithmetics ===== *)

datatype aExpr = aConst of real
  |     aVar of string
  |     plus of aExpr * aExpr
  |     minus of aExpr * aExpr
  |     mul of aExpr * aExpr
;

fun evalArithm (aConst x) context = x
  |     evalArithm (aVar x) context = context x
  |     evalArithm (plus(x:aExpr,y:aExpr)) context =
                (evalArithm x context)+(evalArithm y context)
  |     evalArithm (minus(x:aExpr,y:aExpr)) context =
                (evalArithm x context)-(evalArithm y context)
  |     evalArithm (mul(x:aExpr,y:aExpr)) context =
                (evalArithm x context)*(evalArithm y context)
;

(* ===== Booleans ===== *)

datatype bExpr = bConst of bool
  |     isEq of aExpr * aExpr
  |     isLe of aExpr * aExpr
  |     isGt of aExpr * aExpr
  |     bNot of bExpr
  |     bOr  of bExpr * bExpr
  |     bAnd of bExpr * bExpr
;

fun compareExpr(x:aExpr, y:aExpr, context) =
        let
          val x1 = (evalArithm x context)
          val y1 = (evalArithm y context)
        in
          compareReal(x1, y1)
        end

fun evalBool (bConst b) context = b
  |     evalBool (isEq(x:aExpr, y:aExpr)) context =
                compareExpr(x, y, context) = IEEEReal.EQUAL
  |     evalBool (isLe(x:aExpr, y:aExpr)) context =
                compareExpr(x, y, context) = IEEEReal.LESS
  |     evalBool (isGt(x:aExpr, y:aExpr)) context =
                compareExpr(x, y, context) = IEEEReal.GREATER
  |     evalBool (bNot(x: bExpr)) context = not(evalBool x context)
  |     evalBool (bOr(x: bExpr, y: bExpr)) context =
                (evalBool x context) orelse (evalBool y context)
  |     evalBool (bAnd(x: bExpr, y: bExpr)) context =
                (evalBool x context) andalso (evalBool y context)
;

(* ===== commands ===== *)

datatype command = skip
  |     assign of string * aExpr
  |     cWhile of bExpr * command
  |     ifThen of bExpr * command
  |     ifThenElse of bExpr * command * command
  |     commands of command list
;

(* 
* progStep takes two arguments:
*  - command stack (list of commands)
*  - variable context
*
*  Returns these two updated items
*
*)
fun progStep(nil,context) = (nil, context)
  |     progStep(skip::tail, context) = (tail, context)
  |     progStep(assign(name, what:aExpr)::tail, context) =
        (tail, setVar(name, (evalArithm what context), context))
  |     progStep(cWhile(clause, cmd)::tail, context) =
        let
           val b = (evalBool clause context)
        in
           if b then 
             (cmd::cWhile(clause, cmd)::tail, context) else (tail, context)
        end
  |     progStep(ifThen(clause, cmd)::tail, context) =
        let
           val b = (evalBool clause context)
        in
           if b then 
             (cmd::tail, context) else (tail, context)
        end
  |     progStep(ifThenElse(clause, cmd1, cmd2)::tail, context) =
        let
           val b = (evalBool clause context)
        in
           if b then 
             (cmd1::tail, context) else (cmd2::tail, context)
        end
  |     progStep(commands(nil)::tail, context) = (tail, context)
  |     progStep(commands(c::t)::tail, context) =
             (c::commands(t)::tail, context)
;


(* ===== I/O ===== *)


fun arToStr (aConst(x)) = Real.toString(x)
  |    arToStr (aVar(x)) = x
  |    arToStr (plus(x:aExpr, y:aExpr)) = "("^(arToStr x)^")+("^(arToStr y)^")"
  |    arToStr (minus(x:aExpr, y:aExpr)) = "("^(arToStr x)^")-("^(arToStr y)^")"
  |    arToStr (mul(x:aExpr, y:aExpr)) = "("^(arToStr x)^")*("^(arToStr y)^")"
;

fun bToStr (bConst b) = if b then "TRUE" else "FALSE"
  |     bToStr (isEq(x: aExpr, y:aExpr)) = arToStr(x)^" = "^(arToStr x)
  |     bToStr (isLe(x: aExpr, y:aExpr)) = arToStr(x)^" < "^(arToStr x)
  |     bToStr (isGt(x: aExpr, y:aExpr)) = arToStr(x)^" > "^(arToStr x)
  |     bToStr (bNot(x: bExpr)) = "not("^bToStr(x)^")"
  |     bToStr (bAnd(x: bExpr, y:bExpr)) = "("^bToStr(x)^")&&("^bToStr(y)^")"
  |     bToStr (bOr(x: bExpr, y:bExpr)) = "("^bToStr(x)^")||("^bToStr(y)^")"
;

fun progToStr (skip) = "skip"
  |     progToStr (assign(var:string, what: aExpr)) = var^":= "^(arToStr what)
  |     progToStr (cWhile(cas:bExpr, what:command)) = 
        "while "^(bToStr cas)^" do\n"^(progToStr what)^"\nod"
  |     progToStr (ifThen(cas:bExpr, what:command)) = 
        "if "^(bToStr cas)^" then\n"^(progToStr what)^"\nfi"
  |     progToStr (ifThenElse(cas:bExpr, w1:command, w2:command)) = 
        "if "^(bToStr cas)^" then\n"^(progToStr w1)^"\nelse\n"^(progToStr w2)^"\nfi"
  |     progToStr (commands(nil)) = ""
  |     progToStr (commands(cmd::tail)) = (progToStr cmd)^"\n"^progToStr(commands(tail))
;

fun pprog(p:command) = print("\n\n\n\n\n"^progToStr(p)^"\n\n\n\n\n");

(* ===== debug ===== *)

val con = getEmptyContext();
val con = setVar("x", 42.0, con);
val con = setVar("y", 1.0, con);

(* ===== program 1 ===== *)
val c1 = assign("y", minus(aVar("y"), aVar("x")));
val c2 = assign("x", minus(aVar("x"), aVar("y")));
val c3 = ifThenElse(isGt(aVar("y"), aVar("x")), c2, c1);
val prog1 = cWhile(bNot(isEq(aVar("x"), aVar("y"))), c3);
