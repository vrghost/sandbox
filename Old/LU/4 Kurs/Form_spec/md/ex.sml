fun fst(x,y) = x;
fun snd(x,y) = y;

datatype aexp = 
        acon of int |
        var of string |
        plus of aexp * aexp |
	minus of aexp * aexp |
        times of aexp * aexp;

datatype bexp = 
        bcon of bool |
        eq of aexp * aexp |
        ge of aexp * aexp |
	not_b of bexp |
        and_b of bexp * bexp |
        or_b of bexp * bexp;

datatype com = 
        skip | 
        assign of string * aexp |
        seq of com*com | 
	if_c of bexp * com * com |
        while_c of bexp * com;

fun 	aval (acon(x)) s = x | 
	aval (var(v)) s = s v |
	aval (plus(a, b)) s = (aval a s) + (aval b s) |
	aval (minus(a, b)) s = (aval a s) - (aval b s) |
	aval (times(a, b)) s = (aval a s) * (aval b s);

fun 	bval (bcon x) s = x |
	bval (eq (a,b)) s = (aval a s = aval b s ) |
	bval (ge (a,b)) s = (aval a s >= aval b s ) |
	bval (not_b(b)) s = not ( bval b s ) |
	bval (and_b(b1,b2)) s = (bval b1 s) andalso (bval b2 s) |
	bval (or_b(b1,b2)) s = (bval b1 s) orelse (bval b2 s);

fun do_assign x a s = 
        let
          val v = (aval a s)
        in
          fn y => if x=y then v else (s y) 
        end;

fun 	eval (skip) s = s | 
	eval (assign(x,a)) s = do_assign x a s |
	eval (seq(c1,c2)) s = eval c2 (eval c1 s) |
	eval (if_c(b,c1,c2)) s = if (bval b s) then (eval c1 s)	else (eval c2 s) |
	eval (while_c(b,c)) s = 
		if (bval b s) then (eval (while_c(b,c)) (eval c s)) else s;

fun 	mk_state nil x = 0 | 
	mk_state ((v,value)::t) x = if v=x then value else mk_state t x;

fun 	show_value s x = (x, s(x));
fun 	show_state s l = map (show_value s) l;

fun run c l = show_state (eval c (mk_state l)) (map fst l);

val gcd_program = 
  let
    val ax = var("x");
    val ay = var("y");
    val b1 = not_b(eq(ax,ay));
    val b2 = ge(ax,ay);
    val c1 = assign("x",minus(ax,ay));
    val c2 = assign("y",minus(ay,ax));
    val c3 = if_c(b2,c1,c2)
  in
    while_c(b1,c3)
  end;

