datatype exp =
        const of real |
        var of string |
        ln of exp |
        plus of exp * exp|
        minus of exp * exp|
        mul of exp * exp|
        divide of exp * exp|
        power of exp * exp;

fun diff (const x) (var d) = const 0.0 |
    diff (var x) (var d) = if x=d then (const 1.0) else (var x)|
    (* (u-v)' == (u+(-v))' *)
    diff (minus(x: exp, y: exp)) (var d) = diff (plus(
        x, mul((const ~1.0), y)
    ))(var d)|
    (* (u+v)' = u'+v' *)
    diff (plus(x: exp, y: exp)) (var d) = plus(
        (diff x (var d)),
        (diff y (var d))
    )|
    (* (uv)' = u'v + v'u *)
    diff (mul(x: exp, y: exp)) (var d) = plus(
        (mul((diff x (var d)), y)),
        (mul(x, (diff y (var d))))
    )|
    (* (u/v)' = (u'v-v'u)/(v^2); *)
    diff (divide(x: exp, y:exp)) (var d) = divide(
        minus(
            mul((diff x (var d)), y),
            mul(x, (diff y (var d)))
        ),
        mul(y, y)
    )|
    (* (ln u)' = u'/u. *)
    diff (ln(x: exp)) (var d) = divide((diff x (var d)), x)|
    (* (u^v)'=u^v (vu'/u+v' ln u) *)
    diff (power(x: exp, y:exp)) (var d) = mul(
        power(x, y),
        plus(
            divide((mul(y, (diff x (var d)))), x),
            mul((diff y (var d)), ln(x))
        ));

(* ======================= I/O =======================*)
open Real;

fun to_str (const x) = Real.toString(x) |
    to_str (var x) = x |
    to_str (ln x) = "ln("^(to_str x)^")" |
    to_str (plus(x: exp, y:exp)) = "("^(to_str x)^")+("^(to_str y)^")" |
    to_str (minus(x: exp, y:exp)) = "("^(to_str x)^")-("^(to_str y)^")" |
    to_str (mul(x: exp, y:exp)) = "("^(to_str x)^")*("^(to_str y)^")" |
    to_str (divide(x: exp, y:exp)) = "("^(to_str x)^")/("^(to_str y)^")" |
    to_str (power(x: exp, y:exp)) = "("^(to_str x)^")**("^(to_str y)^")" ;

fun print_expr (x: exp) = print ((to_str x) ^ "\n");

(* Example I/O:
*
* -- val1 = 2*x --
* {{{
*   val val1 = mul((const 2.0), (var "x"))
*   - print_expr(diff val1 (var "x"));
*   ((0.0)*(x))+((2.0)*(1.0))
*   val it = () : unit
* }}}
*
* -- val2 = x*2 --
* {{{
*   val val2 = mul((var "x"), (const 2.0))
*   - print_expr(diff val2 (var "x"));
*   ((1.0)*(2.0))+((x)*(0.0))
*   val it = () : unit
* }}}
*
* -- val3 = (x*2)+(42*x) --
* {{{
*   val val3 = plus(mul((var "x"), (const 2.0)), mul((const 42.0),(var "x")))
*   - print_expr(diff val3 (var "x"));
*   (((1.0)*(2.0))+((x)*(0.0)))+(((0.0)*(x))+((42.0)*(1.0)))
*   val it = () : unit
* }}}
*
*
* -- val4 = (x*2)*(42*x) --
* {{{
*   val val4 = mul(mul((var "x"), (const 2.0)), mul((const 42.0),(var "x")))
*   - print_expr(diff val4 (var "x"));
*   ((((1.0)*(2.0))+((x)*(0.0)))*((42.0)*(x)))+(((x)*(2.0))*(((0.0)*(x))+((42.0)*(1.0))))
*   val it = () : unit
* }}}
*
*
* -- val5 = x^42 --
* {{{
*   val val5 = power(var "x", const 42.0)
*   - print_expr(diff val5 (var "x"));
*   ((x)**(42.0))*((((42.0)*(1.0))/(x))+((0.0)*(ln(x))))
*   val it = () : unit
* }}}
* 
*)
