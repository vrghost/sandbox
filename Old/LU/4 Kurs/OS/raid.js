var _raid0 =
{
  _head: 1,
  _healthy: true,

  _reset: function()
  {
    _runner._run(['rst', 'msg/RAID masīvs ir vesels!']);
    _raid0._healthy = true;
    _raid0._head = 1;
  },

  _write: function(_value)
  {
    var _old_head = _raid0._head;
    var _prog = [];

    if (_raid0._healthy)
    {
      _prog[_prog.length] = 'fcr';
      _prog[_prog.length] = 'frh';
      _prog[_prog.length] = 'chhs';

      while (_value.length > 0)
      {
        if (_raid0._head > 16)
        {
          _raid0._head = _old_head;
          alert('Nav brīvas vietas rakstīšanai!');
          return;
        }

        while (_value.length < 16)
        {
          _value += '-';
        }

        var _cmd = 'wr/' + _raid0._head;

        for (var i = 0; i < 4; i++)
        {
          var _block = _value.substr(i * 4, 4);
          _cmd += '/' + _block;
        }

        _prog[_prog.length] = _cmd;

        _value = _value.substr(16, _value.length - 16);

        _raid0._head++;
      }

      _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';
    }
    else
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir bojāts un datu rakstīšana nav iespējama.';
    }

    _runner._run(_prog);
  },

  _kill: function(_value)
  {
    _raid0._healthy = false;

    var _prog = [];
    _prog[_prog.length] = 'chhs';
    _prog[_prog.length] = 'kill/' + _value;
    _prog[_prog.length] = 'fh/' + _value;
    _prog[_prog.length] = 'fcr';
    _prog[_prog.length] = 'msg/RAID masīvs ir bojāts un dati neatgriezeniski zaudēti!';

    _runner._run(_prog);
  },

  _heal: function()
  {
    var _prog = [];

    if (_raid0._healthy)
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';
    }
    else
    {
      _raid0._healthy = true;
      _raid0._head = 1;

      _prog[_prog.length] = 'rst';
      _prog[_prog.length] = 'msg/RAID masīva darbība atjaunota. Sākotnēji dati neatgriezeniski zaudēti!';
    }

    _runner._run(_prog);
  }
}

var _raid1 =
{
  _head: 1,
  _healthy: [1, 1, 1, 1],

  _reset: function()
  {
    _runner._run(['rst', 'msg/RAID masīvs ir vesels!']);
    _raid1._healthy = [1, 1, 1, 1];
    _raid1._head = 1;
  },

  _write: function(_value)
  {
    var _old_head = _raid1._head;
    var _prog = [];

    if (_raid1._isHealthy())
    {
      _prog[_prog.length] = 'fcr';
      _prog[_prog.length] = 'frh';
      _prog[_prog.length] = 'chhs';

      while (_value.length > 0)
      {
        if (_raid1._head > 16)
        {
          _raid1._head = _old_head;
          alert('Nav brīvas vietas rakstīšanai!');
          return;
        }

        while (_value.length < 4)
        {
          _value += '-';
        }

        var _cmd_part = '/' + _value.substr(0, 4);
        var _cmd = 'wr/' + _raid1._head;
        for (var i = 0; i < 4; i++)
        {
          _cmd += _raid1._healthy[i] ? _cmd_part : '/----';
        }

        _prog[_prog.length] = _cmd;

        _value = _value.substr(4, _value.length - 4);

        _raid1._head++;
      }

      _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';
    }
    else
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir bojāts un datu rakstīšana nav iespējama.';
    }

    _runner._run(_prog);
  },

  _kill: function(_value)
  {
    _raid1._healthy[parseInt(_value - 1)] = 0;

    var _prog = [];
    _prog[_prog.length] = 'chhs';
    _prog[_prog.length] = 'kill/' + _value;
    _prog[_prog.length] = 'fh/' + _value;
    _prog[_prog.length] = 'fcr';

    if (_raid1._isHealthy())
    {
      _prog[_prog.length] = 'msg/RAID masīvs bojāts, taču tas darbojas un dati nav zaudēti.';
    }
    else
    {
      _prog[_prog.length] = 'msg/' + 'RAID masīvs ir bojāts un dati neatgriezeniski zaudēti!';
    }

    _runner._run(_prog);
  },

  _heal: function()
  {
    var _prog = [];

    if (_raid1._healthy[0] && _raid1._healthy[1] && _raid1._healthy[2] && _raid1._healthy[3])
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';
    }
    else if (!_raid1._isHealthy())
    {
      _prog[_prog.length] = 'msg/' + 'RAID masīvs ir bojāts un dati neatgriezeniski zaudēti!';
    }
    else
    {
      _prog[_prog.length] = 'heal';
      _prog[_prog.length] = 'fcr';
      _prog[_prog.length] = 'fhr';

      var _hdds0 = '';
      var _hdds1 = '';
      for (var i = 0; i < 4; i++)
      {
        if (_raid1._healthy[i])
          _hdds1 += (i + 1);
        else
          _hdds0 += (i + 1);
      }

      for (var i = 1; i <= 16; i++)
      {
        _prog[_prog.length] = 'chhs';
        _prog[_prog.length] = 'rd/' + _hdds1 + '/' + i;
        _prog[_prog.length] = 'fh/' + _hdds1;

        var _hdd = _hdds1.substr(0, 1);
        var _data = $('cell_' + i + '_' + _hdd).innerHTML;

        _prog[_prog.length] = 'wr/' + i + '/' + _data + '/' + _data + '/' + _data + '/' + _data;
        _prog[_prog.length] = 'fh/' + _hdds0;
      }

      _prog[_prog.length] = 'chhs';
      _prog[_prog.length] = 'msg/RAID masīva darbība atjaunota bez datu zudumiem.';

      _raid1._healthy = [1, 1, 1, 1];
    }

    _runner._run(_prog);
  },

  _isHealthy: function()
  {
    for (var i = 0; i < 4; i++)
    {
      if (_raid1._healthy[i])
        return true;
    }

    return false;
  }
}

var _raid5 =
{
  _head: 1,
  _healthy: [1, 1, 1, 1],
  _data: [],

  _reset: function()
  {
    _raid5._head = 1;
    _raid5._healthy = [1, 1, 1, 1];
    _raid5._data = [];

    var _prog = [];
    _prog[_prog.length] = 'rst';

    var _datas = [];
    _datas[0] = ['/----/----/----/$----|----|----'];
    _datas[1] = ['/$----|----|----/----/----/----'];
    _datas[2] = ['/----/$----|----|----/----/----'];
    _datas[3] = ['/----/----/$----|----|----/----'];

    for (var i = 0; i < 16; i++)
    {
      _prog[_prog.length] = 'wr/' + (i + 1) + _datas[i % 4];
      _raid5._data[i] = _datas[i % 4];
    }

    _prog[_prog.length] = 'chhs';
    _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';

    _runner._run(_prog);
  },

  _write: function(_value)
  {
    var _old_head = _raid5._head;
    var _prog = [];

    if (_raid5._isHealthy())
    {
      _prog[_prog.length] = 'fcr';
      _prog[_prog.length] = 'frh';
      _prog[_prog.length] = 'chhs';

      while (_value.length > 0)
      {
        if (_raid5._head > 16)
        {
          _raid5._head = _old_head;
          alert('Nav brīvas vietas rakstīšanai!');
          return;
        }

        while (_value.length < 12)
        {
          _value += '-';
        }

        var _cmd = 'wr/' + _raid5._head;
        var _data = '';

        var _parity_block = '/$' + _value.substr(0, 4) + '|' + _value.substr(4, 4) + '|' + _value.substr(8, 4);
        var _parity_pos = (_raid5._head + 2) % 4;

        for (var i = 0; i < 4; i++)
        {
          var _pos = i + (i > _parity_pos ? -1 : 0);

          if (_raid5._healthy[i])
          {
            if (i == _parity_pos)
            {
              _data += _parity_block;
            }
            else
            {
              _data += '/' + _value.substr(_pos * 4, 4);
            }
          }
          else
          {
            _data += '/----';
          }
        }

        _prog[_prog.length] = _cmd + _data;
        _raid5._data[_raid5._head - 1] = _data;

        _value = _value.substr(12, _value.length - 12);

        _raid5._head++;
      }

      _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';
    }
    else
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir bojāts un datu rakstīšana nav iespējama.';
    }

    _runner._run(_prog);
  },

  _kill: function(_value)
  {
    _raid5._healthy[parseInt(_value) - 1] = 0;

    var _prog = [];
    _prog[_prog.length] = 'chhs';
    _prog[_prog.length] = 'kill/' + _value;
    _prog[_prog.length] = 'fh/' + _value;
    _prog[_prog.length] = 'fcr';

    if (_raid5._isHealthy())
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir bojāts, taču tas darbojas un dati nav zaudēti.';
    }
    else
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir bojāts un dati neatgriezeniski zaudēti!';
    }

    _runner._run(_prog);
  },

  _heal: function()
  {
    var _prog = [];

    if (_raid5._healthy[0] && _raid5._healthy[1] && _raid5._healthy[2] && _raid5._healthy[3])
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir vesels!';
    }
    else if (!_raid5._isHealthy())
    {
      _prog[_prog.length] = 'msg/RAID masīvs ir bojāts un dati neatgriezeniski zaudēti!';
    }
    else
    {
      _prog[_prog.length] = 'heal';
      _prog[_prog.length] = 'fcr';
      _prog[_prog.length] = 'fhr';

      var _hdd0 = '';
      var _hdds1 = '';

      for (var i = 0; i < 4; i++)
      {
        if (_raid5._healthy[i])
          _hdds1 += (i + 1);
        else
          _hdd0 = i + 1;
      }

      for (var i = 1; i <= 16; i++)
      {
        _prog[_prog.length] = 'chhs';
        _prog[_prog.length] = 'rd/' + _hdds1 + '/' + i;
        _prog[_prog.length] = 'fh/' + _hdds1;
        _prog[_prog.length] = 'wr/' + i + _raid5._data[i - 1];
        _prog[_prog.length] = 'fh/' + _hdd0;
      }

      _prog[_prog.length] = 'chhs';
      _prog[_prog.length] = 'msg/RAID masīva darbība atjaunota bez datu zudumiem.';

      _raid5._healthy = [1, 1, 1, 1];
    }

    _runner._run(_prog);
  },

  _isHealthy: function()
  {
    var _count = 0;

    for (var i = 0; i < 4; i++)
    {
      if (_raid5._healthy[i])
        _count++;
    }

    return _count >= 3;
  }
}

var _runner =
{
  _prog: [],
  _prog_pc: 0,
  _is_working: false,

  _initialize: function()
  {
    $('raid_type').value = 0;
    _raid = _raid0;
    _raid._reset();
  },

  _assignRaid: function(_value)
  {
    _raid = eval('_raid' + _value);
    _raid._reset();
  },

  _run: function(_user_prog)
  {
    _runner._prog = _runner._compileProg(_user_prog)
    _runner._prog_pc = 0;
    _runner._execute();
  },

  _compileProg: function(_prog)
  {
    var _new_prog = [];

    for (var i = 0; i < _prog.length; i++)
    {
      if (_prog[i] == 'fcr') // Flash CPU-RAID
      {
        for (var j = 0; j < 2; j++)
        {
          _new_prog[_new_prog.length] = ['fcr', 1];
          _new_prog[_new_prog.length] = ['p', 2];
          _new_prog[_new_prog.length] = ['fcr', 0];
          _new_prog[_new_prog.length] = ['p', 2];
        }
      }
      else if (_prog[i] == 'frh') // Flash RAID-HDD
      {
        for (var j = 0; j < 2; j++)
        {
          _new_prog[_new_prog.length] = ['frh', 1];
          _new_prog[_new_prog.length] = ['p', 2];
          _new_prog[_new_prog.length] = ['frh', 0];
          _new_prog[_new_prog.length] = ['p', 2];
        }
      }
      else if (_prog[i].substr(0, 3) == 'fh/') // Flash Exact HDD-RAID
      {
        var _hdds = _prog[i].substr(3, _prog[i].length - 3);

        for (var j = 0; j < 2; j++)
        {
          for (var k = 0; k < _hdds.length; k++)
          {
            _new_prog[_new_prog.length] = ['fh', _hdds.substr(k, 1), 1];
          }

          _new_prog[_new_prog.length] = ['p', 2];

          for (var k = 0; k < _hdds.length; k++)
          {
            _new_prog[_new_prog.length] = ['fh', _hdds.substr(k, 1), 0];
          }

          _new_prog[_new_prog.length] = ['p', 2];
        }
      }
      else if (_prog[i].substr(0, 5) == 'kill/') // Kill HDD
      {
        var _hdd = _prog[i].substr(5, 1);
        _new_prog[_new_prog.length] = ['kill', _hdd];
        _new_prog[_new_prog.length] = ['p', 2];

        for (var j = 1; j <= 16; j++)
        {
          _new_prog[_new_prog.length] = ['wr', _hdd, j, '----'];
          _new_prog[_new_prog.length] = ['hhs', _hdd, j];
        }
      }
      else if (_prog[i].substr(0, 4) == 'heal') // Heal HDD
      {
        _new_prog[_new_prog.length] = ['heal'];
        _new_prog[_new_prog.length] = ['p', 2];
      }
      else if (_prog[i] == 'rst') // Reset
      {
        _new_prog[_new_prog.length] = ['fcr', 1];
        _new_prog[_new_prog.length] = ['frh', 1];
        _new_prog[_new_prog.length] = ['p', 2];
        _new_prog[_new_prog.length] = ['fcr', 0];
        _new_prog[_new_prog.length] = ['frh', 0];
        _new_prog[_new_prog.length] = ['heal'];
        _new_prog[_new_prog.length] = ['p', 2];
        _new_prog[_new_prog.length] = ['rst'];
      }
      else if (_prog[i].substr(0, 3) == 'wr/') // Write HDDs
      {
        var _part = _prog[i].substr(3, _prog[i].length - 3);

        var _sep = _part.indexOf('/');
        var _cell = _part.substr(0, _sep);
        var _part = _part.substr(_sep + 1, _part.length - _sep - 1);

        for (var j = 0; j < 4; j++)
        {
          _sep = _part.indexOf('/');

          var _data = _sep >= 0 ? _part.substr(0, _sep) : _part;
          _new_prog[_new_prog.length] = ['wr', j + 1, _cell, _data];
          _new_prog[_new_prog.length] = ['hhs', j + 1, _cell];

          _part = _part.substr(_sep + 1, _part.length - _sep - 1);
        }

        _new_prog[_new_prog.length] = ['p', 2];
      }
      else if (_prog[i].substr(0, 3) == 'rd/') // Read HDDs
      {
        var _part = _prog[i].substr(3, _prog[i].length - 3);

        var _sep = _part.indexOf('/');
        var _hdds = _part.substr(0, _sep);
        var _cell = parseInt(_part.substr(_sep + 1, _part.length - _sep - 1));

        for (var k = 0; k < _hdds.length; k++)
        {
          _new_prog[_new_prog.length] = ['hhs', _hdds.substr(k, 1), _cell];
        }

        _new_prog[_new_prog.length] = ['p', 2];
      }
      else if (_prog[i].substr(0, 4) == 'msg/') // Message
      {
        var _msg = _prog[i].substr(4, _prog[i].length - 4);
        _new_prog[_new_prog.length] = ['msg', _msg];
      }
      else // Visas bezoperandu komandas
      {
        _new_prog[_new_prog.length] = [_prog[i]];
      }
    }

    return _new_prog;
  },

  _execute: function()
  {
    if (_runner._prog_pc >= _runner._prog.length)
    {
      _runner._is_working = false;
      return;
    }

    _runner._is_working = true;

    while (_runner._prog_pc < _runner._prog.length)
    {
      var _cmd = _runner._prog[_runner._prog_pc++];

      switch (_cmd[0])
      {
        case 'fcr': // Flash CPU-RAID
        case 'frh': // Flash RAID-HDD
          var _class_name = _cmd[0] == 'fcr' ? 'cpu-raid' : 'raid-hdd';
          if (_cmd[1] == 1)
          {
            $$('div.' + _class_name).each(function(_element) { _element.addClassName('highlight'); });
          }
          else
          {
            $$('div.' + _class_name).each(function(_element) { _element.removeClassName('highlight'); });
          }

          break;

        case 'fh': // Flash Exact HDD-RAID
          var _class_name = 'hdd-' + _cmd[1];
          if (_cmd[2] == 1)
          {
            $$('div.' + _class_name).each(function(_element) { _element.addClassName('highlight'); });
          }
          else
          {
            $$('div.' + _class_name).each(function(_element) { _element.removeClassName('highlight'); });
          }

          break;

        case 'wr': // Write HDD
          var _element = $('cell_' + _cmd[2] + '_' + _cmd[1]);
          if (_cmd[3].substr(0, 1) == '$')
          {
            _element.innerHTML = '<span class="parity">P<span class="small">' + _cmd[3].substr(1, _cmd[3].length - 1) + '</span></span>';
          }
          else
          {
            _element.innerHTML = _cmd[3];
          }

          break;

        case 'kill': // Kill HDD
          $('oHdd' + _cmd[1] + 'Container').addClassName('killed');
          break;

        case 'heal': // Heal HDD
          for (var i = 1; i <= 4; i++)
          {
            $('oHdd' + i + 'Container').removeClassName('killed');
          }
          break;

        case 'hhs': // Highlight HDD Sector
          $('cell_' + _cmd[2] + '_' + _cmd[1]).addClassName('highlight');
          break;

        case 'chhs': // Clear Highlight HDD Sector
          $$('div.cell_data.highlight').each(function(_element) { _element.removeClassName('highlight'); });
          break;

        case 'rst': // Reset
          $$('div.cell_data.highlight').each(function(_element) { _element.removeClassName('highlight'); });
          $$('div.cell_data').each(function(_element) { _element.innerHTML = '----'; });
          break;

        case 'msg': // Message
          $('oMessageBox').innerHTML = 'OS/BIOS: ' + _cmd[1];
          new Effect.Highlight('oMessageBox', { startcolor: '#ffff00', endcolor: '#ffffff'});
          break;

        case 'p': // Pause
          window.setTimeout('_runner._execute();', _cmd[1] * 34);
          return;
      }

    }
  }
}

Event.observe(window, 'load', _runner._initialize);
