<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>{$site_name}{if $article->header}: {$article->header}{/if}</title>
<link href="{$media_dir}/style.css" rel="stylesheet" type="text/css" />
<!-- user style -->
{if $user && $user->style}
<style type="text/css">
	{literal}.bodyText{{/literal}
	{if $user->style->text_font}
		font-family:  {$user->style->text_font};
	{/if}
	{if $user->style->text_size}
		font-size:{$user->style->text_size}{if strripos($user->style->text_size, 'px') === false }px{/if};
	{/if}
	{if $user->style->text_color}
		color: {$user->style->text_color};
	{/if}
	{literal}}{/literal}
</style>
{/if}
</head>

<body>
<div align="center">
  <table width="780" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="56" colspan="3" align="left" valign="middle" bgcolor="#DCD8C8" class="sitenameText">{$header}</td>
    </tr>
    <tr>
      <td width="74" align="left" valign="top" background="{$media_dir}/bar1.gif">&nbsp;</td>
      <td width="29" align="left" valign="top"><img src="{$media_dir}/imgtopbar1.gif" alt="" width="29" height="25" /></td>
      <td width="677" align="left" valign="middle" bgcolor="#BAB398" class="navText">
<table width="100%" height="100%" border="0px"><tr>

<td width="142px">
  <!--path here-->
{foreach from=$path item=cur_el name=pathIter}
	{$cur_el}
	{if !$smarty.foreach.pathIter.last}&gt;{/if}
{/foreach}
</td>
<td>{if $user}Welcome, {$user->name}{else}
<a href="?action=loginScreen">Login</a>
{/if}</td>
<td width="100px">
{if $user}
<a href="?action=logout">Logout</a>
{else}
<a href="?action=register">Register</a>
{/if}
</td>

</tr></table>
	</td>
    </tr>
    <tr>
      <td colspan="3" align="left" valign="top"><img src="{$media_dir}/blank.gif" alt="" width="1" height="4" /></td>
    </tr>
  </table>
  <table width="780" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="142" align="center" valign="top" class="bodyText">
{if $menu}
	<H1>menu:</H1><br>
{/if}
{foreach from=$menu item=menu_el}
<a href="?article={$menu_el.id}">{$menu_el.text}</a><br>
{/foreach}
{if $user}
<hr />
<a href="?action=writeArticle">Write article</a><br>
<a href="?action=personalize">Personalize</a><br>
<a href="?action=writeEmail">Write email</a><br>
{if $user->level <= -100}
<a href="?action=userList">Manage users</a><br>
{/if}
{/if}
{if $money}
<hr />
<table border="1px">
<tr><th>Name</th><th>Units</th><th>Rate</th></tr>
{foreach from=$money.Currencies item=m_iter}
<tr>
<td>{$m_iter->ID}</td><td>{$m_iter->Units}</td><td>{$m_iter->Rate}</td>
</tr>
{/foreach}
</table>
{/if money}
	 </td>
      <td align="left" valign="top" class="border"><table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td align="left" valign="top" class="headerText">
{if $error}ERROR!{else}
	<table border="0px" width="100%"><tr>
	<td>{$article->header}</td>
	{if $article && $user && $user->level <= $article->edit_level}
	<td width="50px">
	<a href="?action=editArticle&article={$article->id}">Edit</a>
	</td>
	<td width="50px">
	<a href="?action=deleteArticle&article={$article->id}">Delete</a>
	</td>
	{/if}
	</tr></table>
{/if}
</td>
        </tr>
        <tr>
        <td align="left" valign="top" class="bodyText">
{if $error}
[ERROR] <pre>{$error}</pre>
{else}
	{if $force_template_include}
		{include file=$force_template_include}
	{else}
		{$article->text}
	{/if}
{/if}
	</td>
        </tr>
      </table></td>
    </tr>
  </table>
  <table width="780" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="3"><img src="{$media_dir}/blank.gif" alt="" width="1" height="4" /></td>
    </tr>
    <tr>
      <td width="4" align="left" valign="top"><img src="{$media_dir}/bottom_nav_leftbar.gif" alt="" width="4" height="27" /></td>
      <td align="center" valign="middle" background="{$media_dir}/bottom_bar.gif" class="copyrightText">Copyright Ilya Orlov 2006</td>
      <td align="center" valign="middle" background="{$media_dir}/bottom_bar.gif" class="copyrightText">This template is taken from <a href='http://www.interspire.com/templates/download/337'>www.interspire.com</a></td>
      <td width="4" align="right" valign="top"><img src="{$media_dir}/bottom_nav_rightbar.gif" alt="" width="4" height="27" /></td>
    </tr>
  </table>
</div>
</body>
</html>
