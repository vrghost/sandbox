<?php
require_once('PRESENTATION\Smarty\Smarty.class.php');

class Page{

    const smartyDir = 'PRESENTATION/Smarty';
    const mediaDir = 'PRESENTATION/Smarty/templates/media';
    const siteName = 'fooSite';

    public function __construct(){
	$_smarty = new Smarty();
	$_smarty->template_dir = Page::smartyDir.'/templates';
	$_smarty->compile_dir = Page::smartyDir.'/templates_c';
	$_smarty->cache_dir = Page::smartyDir.'/cache';
	$_smarty->config_dir = Page::smartyDir.'/configs';
	$_smarty->assign('media_dir', Page::mediaDir);
	$_smarty->assign('site_name', Page::siteName);
	$this->smarty = $_smarty;
	$this->path = array();
	$this->menu = array();
	$this->loggedin = false;
    }

    public function display(){
	$this->_compile();
	$this->smarty->display('template.tpl');
    }

    public function test(){
	$this->header = 'FooHeader';
	$this->articleHeader = 'FooArticle';
	$this->articleText = 'cool! <br>an article!';
	$this->menu = 'cool! <br> menu!';
	$this->addPathEl('E1');
	$this->addPathEl('E2');
	$this->display();
    }

    private function _compile(){
	$this->smarty->assign('header', $this->header);
	$this->smarty->assign('menu', $this->menu);
	$this->smarty->assign('path', $this->path);
	$this->smarty->assign(
	    'force_template_include', $this->forceTemplateInclude);
	$this->smarty->assign('user', $this->user);
	$this->smarty->assign('error', $this->error);
	$this->smarty->assign('article', $this->article);
	$this->smarty->assign('users', $this->users);
	$this->smarty->assign('money', $this->money);
    }

    public function addPathEl($el){
	$this->path[count($this->path)] = $el;
    }

    public function addMenuEl($id, $text){
	$_el = array();
	$_el['id'] = $id;
	$_el['text'] = $text;
	$this->menu[count($this->menu)] = $_el;
    }

    public function articleWritingMode(){
	$this->forceTemplateInclude = 'article_form.tpl';
	$this->addPathEl("new article");
    }

    public function userListMode(){
	$this->_setHeader("Editing users.");
	$this->forceTemplateInclude = 'user_list.tpl';
    }

    public function registerMode(){
	$this->_setHeader("Register.");
	$this->forceTemplateInclude = 'registration_form.tpl';
    }

    public function loginMode(){
	$this->_setHeader("Login.");
	$this->forceTemplateInclude = 'login_form.tpl';
    }

    public function emailWritingMode(){
	$this->_setHeader("Send email to administration.");
	$this->forceTemplateInclude = 'email_form.tpl';
    }

    private function _setHeader($str){
	$_article = new stdClass();
	$_article->header = $str;
	$this->article = $_article;
	$this->addPathEl($str);
    }

    public function personalizeMode(){
	$this->_setHeader("Personalize");
	$this->forceTemplateInclude = 'personalize_form.tpl';
    }

}

?>
