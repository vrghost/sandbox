<?php
require_once('PRESENTATION/main.php');

require_once('LOGIC/main.php');
require_once('LOGIC/xmlProcessor.php');
require_once('LOGIC/mail.php');

require_once('site_config.php');

class Processor{

    public function __construct($useSSL=False, $sslPort=443){
        $this->users = new Users();
        $this->page = new Page();
        $this->articles = new Articles();
        $this->moneyProc = new MoneyProcessor();
        $this->mail = new AdminMailSender();
    }

    public function process($args){
        $_action = $args['action'];
        $_user = $this->tryRestoreSession();
        $this->drawLeftPart($_user);
        $this->page->addPathEl("Main");
        if($_user){
            $this->page->user = $_user;
            if($_action == 'logout'){
                $this->logoutUser();
            }elseif($_action == 'writeArticle'){
                $this->renderWriteArticle($_user);
            }elseif($_action == 'commitArticle'){
                $this->tryCommitArticle($_user, $args);
            }elseif($_action == 'editArticle'){
                $this->editArticle($_user, $args);
            }elseif($_action == 'deleteArticle'){
                $this->deleteArticle($_user, $args);
            }elseif($_action == 'userList'){
                $this->drawUserList($_user);
            }elseif($_action == 'delUser'){
                $this->delUser($_user, $args);
            }elseif($_action == 'personalize'){
                $this->drawPersonalize();
            }elseif($_action == 'applyPersonalize'){
                $this->applyPersonalize($_user, $args);
            }elseif($_action == 'writeEmail'){
                $this->displayEmailForm($args);
            }elseif($_action == 'sendEmail'){
                $this->trySendEmail($args);
            }else{
                $this->personalizedPage($_user, $args);
            }
        }else{
            if($_action == 'loginScreen'){
                $this->showLoginScreen($args);
            }elseif($_action == 'login'){
                $this->processLogin($args);
            }elseif($_action == 'register'){
                $this->showRegistrationForm($args);
            }elseif($_action == 'tryRegister'){
                $this->tryRegister($args);
            }else{
                $this->drawPublicPage($args);
            }
        }   
        $this->page->display();
    }

    public function processLogin($args){
        $this->_checkSSL($args);
        $_user = $this->users->checkUser($args['uname'], $args['password']);
        if($_user){
            $this->users->newSession($_user);
        }
        $this->relocatePage();
    }

    public function tryRestoreSession(){
        $_id = $_SESSION['id'];
        $_hash = $_SESSION['hash'];
        if(!$_id || !$_hash){
            return false;
        }
        return $this->users->continueSession($_id, $_hash);
    }

    public function displayEmailForm($args){
        $this->_initSSL($args);
        $this->page->emailWritingMode();
    }

    public function trySendEmail($args){
        $this->_checkSSL($args, True);
        $body = $args['body'];
        if($_FILES['attach']){
            $_file = $_FILES['attach'];
            $attach = array(
                'file_path'=>$_file['tmp_name'],
                'file_name'=>$_file['name']
            );
        }else{
            $attach = Null;
        }
        $this->mail->send("email from WEB page", $body, $attach);
    }

    public function personalizedPage($user, $args){
        $this->displayReadedArticle($user->level, $args['article']);
    }

    public function logoutUser(){
        $_ans = $this->tryRestoreSession();
        if(!$_ans)
            return false;
        $this->users->dropSession($_ans->id);
        $this->relocatePage();
        return true;
    }

    public function drawPublicPage($args){
        $this->displayReadedArticle(100, $args['article']);
    }

    public function fillMenu($articles){
        foreach($articles as $article){
            $this->page->addMenuEl($article->id, $article->header);
        }
    }

    public function displayReadedArticle($level, $artId){
        if(!$level || !$artId)
            return false;
        $_found = $this->articles->getArticle($artId, $level);
        if($_found){
            $this->page->article = $_found;
            $this->page->addPathEl($_found->header);
        }
    }

    public function renderWriteArticle($user){
        $this->page->articleWritingMode();
    }

    public function tryCommitArticle($user, $args){
        $_args = array();
        $_level = $args['level'];
        $_editLevel = $args['editor_level'];
        $_artId = $args['id'];
        if(!($_level<=100 && $_level >= $user->level)){
            $this->notifyAboutError('Ambitious reader level.');
            return;
        }
        if(!($_editLevel>=-100 && $_editLevel <= $user->level)){
            $this->notifyAboutError('Ambitious editor level.');
            return;
        }
        if(!$args['text'] || !$args['header']){
            $this->notifyAboutError('Text or header missing.');
            return;
        }
        if($_artId){
            if($this->articles->canUserEdit($_artId, $user->level)){
                $this->articles->editArticle(
                    $_artId,
                    $args['header'],
                    $args['text'],
                    $_level,
                    $_editLevel
                );
            }else{
                $this->notifyAboutError(
                'You have no rights to edit this article.');
                return;
            }
        }else{
            $this->articles->addArticle(
               $args['header'], $args['text'], $_level, $_editLevel);
        }
        $this->relocatePage();
    }

    public function notifyAboutError($text){
        if($this->page->error){
            $this->page->error .= "\n".$text;
        }else{
            $this->page->error = $text;
        }
    }

    public function editArticle($user, $args){
        $_artId = $args['article'];
        $_article = $this->articles->getArticle($_artId, $user->level);
        if($_article ){
            $this->page->article = $_article;
            $this->page->articleWritingMode();
        }
    }

    public function deleteArticle($user, $args){
        $_artId = $args['article'];
        if($_artId && $this->articles->canUserEdit($_artId, $user->level)){
            $this->articles->dropArticle($_artId);
        }
        $this->relocatePage();
    }

    public function drawUserList($user){
        // only 'da adminz' must be allowed to manage users
        if($user->level > -100)
            return false;
        $_users = $this->users->getAllUsers();
        foreach($_users as $_user){
            // just tiding values before sowing them;
            $_user->last_seen = TableObj::translateTime($_user->last_seen);
        }
        $this->page->users = $_users;
        $this->page->userListMode();
    }

    public function delUser($user, $args){
        if($user->level > -100){
            return false;
        }
        $userId = $args['id'];
        if($userId){
            $this->users->delUser($userId);
        }
        $this->relocatePage();
    }

    private function _checkSSL($args, $force=False){
        if(Config::useSSL){
            $_protocol = $_SERVER['SERVER_PROTOCOL'];
            $_port = $_SERVER['SERVER_PORT'];
            if(preg_match("/.*HTTPS.*/i", _protocol)){
                if($_port == Config::sslPort){
                    return True;
                }else{
                    if($force)
                       die("Wrong port!");
                    else
                        return False;
                }
            }else{
                if($force)
                    die("Use SSL!");
                else
                    return False;
            }
        }
        // if we can't use SSL, than we won't
        return True;
    }

    private function _initSSL($args){
        if($this->_checkSSL($args)){
            // SSL is already enabled
            return True;
        }else{
            // create & send `Location` header
            $_host = $_SERVER['HTTP_HOST'];
            $_uri = $_SERVER['REQUEST_URI'];
            $_location = 'https://'.$_host.':'.$this->sslPort.$_uri;
            header('Location: '.$_location);
            die('user has gone to ssl');
        }
    }

    public function showRegistrationForm(){
        $this->_initSSL($args);
        $this->page->registerMode();
    }

    public function showLoginScreen($args){
        $this->_initSSL($args);
        $this->page->loginMode();
    }

    public function tryRegister($args){
        $_user = $args['uname'];
        $_pass = $args['password'];
        if($_user && $_pass && $this->users->regUser($_user, $_pass)){
            $this->relocatePage();
        }
        $this->notifyAboutError("Registration failed");
    }

    const moneyAddr = 'http://www.bank.lv/ValutuKursi/XML/xml.cfm';

    public function drawLeftPart($user){
        if($user){
            $_level = $user->level;
        }else{
            $_level = 100;
        }
        $_articles = $this->articles->getArticles($_level);
        $this->fillMenu($_articles);
        $_money = $this->moneyProc->processXmlFile(Processor::moneyAddr);
        $this->page->money = $_money;
    }

    public function drawPersonalize(){
        $this->page->personalizeMode();
    }

    public function applyPersonalize($user, $args){
        $this->users->personalize($user->id,
            $args['text_font'], $args['text_size'], $args['text_color']);

    }

    public function relocatePage(){
        header("location: .");
    }
}

// vim: set sts=4 sw=4 et :
?>
