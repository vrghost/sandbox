<?php

require_once('DB.php');

require_once('site_config.php');

class Database{

    public function __construct(){
	$db_host = Config::db_host;
	$db_user = Config::db_user;
	$db_pass = Config::db_pass;
	$this->db_name = $db_name = Config::db_name;
	$dsn = "mysql://$db_user:$db_pass@unix+$db_host/$db_name";

	$this->db = DB::connect($dsn);
	$this->db->setFetchMode(DB_FETCHMODE_OBJECT);
    }

    public function __destruct(){
	$this->db->commit();
	$this->db->disconnect();
    }
}

class TableObj{

    public function __construct($table_name){
	$this->tbName = $table_name;
	$this->projdb = new Database();
	$_tblInfo = $this->projdb->db->tableInfo($table_name);
	$_fields = array();
	foreach($_tblInfo as $_cur){
	    $_data = array();
	    $_data['type'] = $_cur['type'];
	    $_data['len'] = $_cur['len'];
	    $_fields[$_cur['name']] = $_data;
	}
	$this->fields = $_fields;
    }

    public function getData($args=null){
	if(is_null($args)){
	    return $this->_fetchAll();
	}else{
	    return $this->_fetchByArgs($args);
	}
    }

    private function _fetchAll(){
	return $this->projdb->db->getAll('SELECT * FROM '.$this->tbName);
    }

    private function _escapeString($str){
	return $this->projdb->db->escapeSimple($str);
    }

    private function _fetchByArgs($args){
	$_query = 'SELECT * from '.$this->tbName;
	if(is_array($args)){
	    $_app = ' WHERE '.$this->joinArr($args, ' AND ');
	}else{
	    assert(is_string($args));
	    $_app = $args;
	}
	$_query .= $_app;
	$_ans = $this->projdb->db->getAll($_query);
	if (PEAR::isError($_ans)) {
	    return False;
	}else{
	    return $_ans;
	}
    }

    public function joinArr($arr, $sep, $keyValSep='='){
	assert(is_array($arr) && is_string($sep) && is_string($keyValSep));
	$_out = '';
	foreach($arr as $name => $value){
	    $_out .= $sep.$name.$keyValSep.
		"'".$this->_escapeString($value)."'";
	}
	// XXX: we have to remove `sep` from beginning of `_out`
	$_out = substr($_out, strlen($sep));
	return $_out;
    }

    public function setData($args){
	assert(is_array($args) && count($args) > 0);
	$_map = '';
	$_values = '';
	foreach($args as $name => $value){
		$_map .= $this->_escapeString($name)." ,";
		$_values .= "'".$this->_escapeString($value)."' ,";
	}
	// removing ' ,' from the ends
	$_map = substr($_map, 0, -2);
	$_values = substr($_values, 0, -2);
	$_query = 'INSERT INTO '.$this->tbName.' ( '.$_map.' ) VALUES 
	    ( '.$_values.' )';
	return $this->_exec($_query);
    }

    public function replaceData($args){
	assert($args && count($args)>0);
	assert(is_array($args) && count($args) > 0);
	$_map = '';
	$_values = '';
	foreach($args as $name => $value){
		$_map .= $this->_escapeString($name)." ,";
		$_values .= "'".$this->_escapeString($value)."' ,";
	}
	// removing ' ,' from the ends
	$_map = substr($_map, 0, -2);
	$_values = substr($_values, 0, -2);
	$_query = 'REPLACE INTO '.$this->tbName.' ( '.$_map.' ) VALUES 
	    ( '.$_values.' )';
	return $this->_exec($_query);
    }

    private function _exec($query){
	$ans = $this->projdb->db->query($query);
	$this->projdb->db->commit();
	if (PEAR::isError($ans)) {
	    return False;
	}else{
	    return True;
	}
    }

    public function deleteData($args){
	$_where = $this->joinArr($args, ' AND ');
	$_query = 'DELETE FROM '.$this->tbName.' WHERE '.$_where;
	return $this->_exec($_query);
    }

    public function updateData($findArgs, $setVals){
	assert(is_array($findArgs) && is_array($setVals));
	$_where = $this->joinArr($findArgs, ' AND ');
	$_set = $this->joinArr($setVals, ', '); 
	$_query = 'UPDATE '.$this->tbName.' SET '.$_set.' WHERE '.$_where;
	return $this->_exec($_query);
    }


    const dateTimeFormat = '%Y-%m-%d %H:%M:%S';

    public static function translateTime($time){
	// function gets dateTime as unix time and returns it in SQL
	// notation
	return strftime(TableObj::dateTimeFormat, $time);
    }

    public static function getCurrentDTime(){
	return TableObj::translateTime(TableObj::getCurUnixTime());
    }

    public static function getCurUnixTime(){
	return mktime();
    }

}

?>
