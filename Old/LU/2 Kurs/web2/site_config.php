<?php
    class Config{
        const admin_email = 'vrghost@gmail.com';
        const default_from = 'from_our_site@42.lv';

        const useSSL = False;
        const sslPort = 42;

        const db_user = 'root';
        const db_pass = '';
        const db_name = 'projectdb';
        const db_host = 'localhost';
    }
    // vim: set sts=4 sw=4 et :
?>
