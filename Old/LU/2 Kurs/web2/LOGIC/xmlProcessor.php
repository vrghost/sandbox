<?php

class MoneyProcessor{

    public function processXmlFile($faddr){
	$_text = file_get_contents($faddr, false);
	if($_text){
	    return $this->processXmlString($_text);
	}else{
	    return false;
	}
    }

    public function processXmlString($text){
	$dom = new DomDocument();
	$dom->loadXML($text);
	$_arr = $this->domToArray($dom);
	return $this->_makeCoolMoneyArray($_arr);
    }

    private function _simplifyArr($arr){
	$_cur = array_values($arr);
	assert(count($_cur) == 1);
	return $_cur[0];
    }

    private function _makeCoolMoneyArray($arr){
	// this function takes array from `domToArray` and makes it cool ^_^
	// removing 'CRates' root node;
	$_cur = $this->_simplifyTree($arr);
	$_cur = $this->_convertLowestToClass($_cur);
	return $_cur;
    }

    private function _convertLowestToClass($arr){
	if(!is_array($arr))
	    return $arr;
	$_convert = true;
	foreach($arr as $value){
	    if(is_array($value)){
		$_convert = false;
	    }
	}
	if($_convert){
	    $_out = new stdClass();
	    foreach($arr as $key => $value){
		$_out->$key = $value;
	    }
	}else{
	    $_out = array();
	    foreach($arr as $key => $value){
		$_out[$key] = $this->_convertLowestToClass($value);
	    }
	}
	return $_out;
    }

    private function _simplifyTree($arr){
	// all leaves with one subling are being replaced with this
	// sibling
	if(!is_array($arr))
	    return $arr;
	if( count($arr) == 1){
	    $_arr = array_values($arr);
	    return $this->_simplifyTree($_arr[0]);
	}else{
	    assert( count($arr) > 1);
	    $_out = array();
	    foreach($arr as $_key => $_val){
		$_out[$_key] = $this->_simplifyTree($_val);
	    }
	    return $_out;
	}
    }

    public function domToArray($dom){
	$_out = array();
	$_node = $dom->firstChild;
	while(!is_null($_node)){
	    if(trim($_node->nodeName) != ''){
		if($_node->hasChildNodes()){
		    $_out[$_node->nodeName][] =
		       	$this->domToArray($_node);
		}elseif($_node->nodeType == XML_TEXT_NODE &&
			trim($_node->nodeValue) != ''
	       	){
		    $_out['data'] = $_node->nodeValue;
		}
	    }
	    $_node = $_node->nextSibling;
	}
	return $_out;
    }

}

// vim: set sts=4 sw=4 et :
?>
