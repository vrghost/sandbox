<?php
require_once('DB/main.php');

class Users{

    const sessionExpireAfter = 3600; //seconds

    public function __construct(){
	$this->userTable = new TableObj('users');
	$this->userSessions = new TableObj('sessions');
	$this->userSryles = new TableObj('styles');
    }

    public static function getCache($str){
	return md5($str);
    }

    public function regUser($uname, $password, $level=null){
	$_args = array();
	$_args['name'] = $uname;
	$_args['pass_hash'] = $this->getCache($password);
	if(!is_null($level)){
	    if($level >= -100 && $level < 100){
	    	$_args['level'] = $level;
	    }else{
		// TODO: add error logginge here
		return false;
	    }
	}
	$_args['reg_date'] = $this->userTable->getCurrentDTime();
	return $this->userTable->setData($_args);
    }

    public function delUser($uid){
	$_args = array();
	$_args['id'] = $uid;
	return $this->userTable->deleteData($_args);
    }

    public function getAllUsers(){
	return $this->userTable->getData();
    }

    private function _getUser($args){
	$_ans = $this->userTable->getData($args);
	if(count($_ans) != 1)
	    return false;
	$_user = $_ans[0];
	$_args = array();
	$_args['user_id'] = $_user->id;
	$_style = $this->userSryles->getData($_args);
	if($_style){
		assert(count($_style) == 1);
		$_user->style = $_style[0];
	}
	return $_user;
    }

    public function checkUser($uname, $pass){
	$_args = array();
	$_args['name'] = $uname;
	$_args['pass_hash'] = $this->getCache($pass);
	return $this->_getUser($_args);
    }

    private function getHash(){
	$_out = '';
	while(strlen($_out) < 42){
	    $_out .= rand();
	}
	return $_out;
    }

    public function dropSession($uid){
	session_unset();
	$_args = array();
	$_args['user_id'] = $uid;
	$this->userSessions->deleteData($_args);
    }

    public function newSession($userData){
	session_unset();
	$_hash = $this->getHash();
	$_args = array();
	$_args['user_id'] = $userData->id;
	$_args['hash'] = $_hash;
	$this->userSessions->replaceData($_args);
	$this->updateLastSeen($userData->id);
	$_SESSION['hash'] = $_hash;
	$_SESSION['id'] = $userData->id;
    }

    public function updateLastSeen($uid){
	$_findArgs = array();
	$_upArgs = array();
	$_findArgs['id'] = $uid; 
	$_upArgs['last_seen'] = TableObj::getCurUnixTime();
	$this->userTable->updateData($_findArgs, $_upArgs);
    }

    public function continueSession($id, $hash){
	$_args = array();
	$_args['user_id'] = $id;
	$_args['hash'] = $hash;
	$_ans = $this->userSessions->getData($_args);
	if(!$_ans)
	    return false;
	$_args = array();
	$_args['id'] = $id;
	$_user = $this->_getUser($_args);
	if($_user){
	    $_seen = $_user->last_seen;
	    if($_seen + Users::sessionExpireAfter < mktime()){
		$this->dropSession($_user->id);
		return false;
	    }else{
		// all is ok! edit last seen and all is fine ^_^.
		$this->updateLastSeen($_user->id);
		return $_user;
	    }
	}else{
	    return false;
	}
    }

    public function personalize($uid, $font, $size, $color){
	$_args = array();
	$_args['user_id'] = $uid;
	if($font)
	    $_args['text_font'] = $font;
	if($color && strlen($color) == 7 ) // color == #??????
	    $_args['text_color'] = $color;
	if($size)
	    $_args['text_size'] = $size;
	return $this->userSryles->replaceData($_args);	
    }
}

class Articles{

    public function __construct(){
	$this->articleTable = new TableObj('articles');
    }

    public function addArticle($header, $text, $level=100, $edit_level=-100){
	$_args = array();
	$_args['header'] = $header;
	$_args['text'] = $text;
	$_args['level'] = $level;
	$_args['edit_level'] = $edit_level;
	$_args['date'] = TableObj::getCurrentDTime();
	return $this->articleTable->setData($_args);
    }

    public function getArticles($level){
	return $this->articleTable->getData(' WHERE level >= '.$level);
    }

    public function getArticle($id, $level){
	$_ans = $this->articleTable->getData(
	    ' WHERE level >= '.$level.' AND id='.$id);
	if($_ans && count($_ans) == 1){
	    return $_ans[0];
	}else{
	    return false;
	}
    }

    public function canUserEdit($id, $level){
	assert($id && $level);
	$_ans = $this->articleTable->getData(
	    ' WHERE edit_level >= '.$level.' AND id='.$id);
	return($_ans && count($_ans) == 1);
    }

    public function editArticle(
	$id, $header, $text, $level=100, $edit_level=-100){
	$_args = array();
	$_args['header'] = $header;
	$_args['text'] = $text;
	$_args['level'] = $level;
	$_args['edit_level'] = $edit_level;
	$_args['date'] = TableObj::getCurrentDTime();
	$_args['id'] = $id;
	return $this->articleTable->replaceData($_args);
    }

    public function dropArticle($id){
	assert($id);
	$_args = array();
	$_args['id'] = $id;
	return $this->articleTable->deleteData($_args);
    }
}

?>
