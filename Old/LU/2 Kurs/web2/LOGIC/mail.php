<?php
    require_once('Mail.php');
    require_once('Mail/mime.php');

    require_once('site_config.php');


    class AdminMailSender{

        public function __construct(){
            $this->mail = Mail::factory("mail");
        }
    
        public function send($subj, $body, $attach=Null){
            if($attach)
                $this->_sendMimeMessage($subj, $body, $attach);
            else
                $this->_sendPlainMessage($subj, $body);
        }

        private function _sendPlainMessage($subj, $body){
            $headers = array("Subject"=>$subj);
            $this->_sendMail($headers, $body);
        }

        private function _sendMimeMessage($subj, $body, $attach){
            assert(file_exists($attach['file_path']));
            $message = new Mail_mime();
            $message->setTXTBody($body);
            $message->addAttachment(
                $attach['file_path'], 
                'application/octet-stream', 
                $attach['file_name']
            );
            $body = $message->get();
            $_extraHeaders = array("Subject"=>$subj);
            $headers = $message->headers($_extraHeaders);
            $this->_sendMail($headers, $body);
        }

        private function _sendMail($headers, $body){
            if(!array_key_exists("From", $headers))
                $headers["From"] = Config::default_from;
            $this->mail->send(Config::admin_email, $headers, $body);
        }

    }
// vim: set sts=4 sw=4 et :
?>
