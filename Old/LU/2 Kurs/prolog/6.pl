delete(_, [], []).
delete(X, [T|R], [T|K]):-
    not( T = X),
    delete(X, R, K).
delete(X, [X|R], R).

%%%%%%%%%%%%% trees
%
find(K, [_, K, _]).
find(K, [_, KS, TR]):-
    K < KS,
    find(K, TR).
find(K, [TL, KS, _]):-
    K > KS,
    find(K, TL).

insert(K, [TL, K, TR], [TL, K, TR]).
insert(K, [], [[], K, []]).
insert(K, [TL, KS, TR], [TL, KS, TR1]):-
    K > KS,
    insert(K, TR, TR1).
insert(K, [TL, KS, TR], [TL1, KS, TR]):-
    K < KS,
    insert(K, TL, TL1).

delroot([[], _, []], []).
delroot([A, _, []], A).
delroot([[], _, A], A).
delroot([TL, _, TR], Ans):-
    .

delmin([[], K, TR], TR, K).
delmin([TL, KS, TR], [TL1, KS, TR], K):-
    delmin(TL, TL1, K).

delete(K, [TL, K, TR], R):-
    delroot([TL, K, TR], R).
delete(K, [TL, KS, TR], [TL, KS, TR1]):-
    K > KS,
    delete(K, TR, TR1).
delete(K, [TL, KS, TR], [TL1, KS, TR]):-
    K < KS,
    delete(K, TL, TL1).

% 'op' is cool function! ^_^

:- op(500, yfx, --).
:- op(1000, xfx, my_is).

my_is(A, A):-
    number(A).
my_is(X, -(A, B)):-
    my_is(A, X1),
    my_is(B, X2),
    X is X1 - X2.


:-op(300, xfy, ***)
:- op(1000, xfx, my_is2).

my_is2(A, A):-
    number(A).
my_is2(X, A-B)):-
    my_is(A, X1),
    my_is(B, X2),
    X is X1 - X2.
my_is2(X, A+B)):-
    my_is(A, X1),
    my_is(B, X2),
    X is X1 + X2.
my_is2(X, A*B)):-
    my_is(A, X1),
    my_is(B, X2),
    X is X1 * X2.



% vim: set sts=4 sw=4 et syntax=prolog nospell:
