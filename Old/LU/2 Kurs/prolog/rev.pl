start([ [0, 0, 0,0],
        [0, 1,-1,0],
        [0,-1, 1,0],
        [0, 0, 0,0]
      ]
    ).

% move making

getItem([L|_], 1, L).
getItem([_|B], Iter, Ans):-
    Iter > 1,
    I is Iter - 1,
    getItem(B, I, Ans).

replaceItem([_|Where], 1, To, [To|Where]).
replaceItem([El|Where], Iter, To, [El|Ans]):-
    Iter > 1,
    I is Iter - 1,
    replaceItem(Where, I, To, Ans).

valAtPos(Board, X, Y, Val):-
    getItem(Board, Y, L),
    getItem(L, X, Val).

setValAtPos(Board, X, Y, Val, Result):-
    getItem(Board, Y, Line),
    replaceItem(Line, X, Val, Line2),
    replaceItem(Board, Y, Line2, Result).

fillLine2([What|Line], What, [What|Line]).
fillLine2([El|Line], What, [What|Out]):-
    MEl is -El,
    What = MEl,
    fillLine2(Line, What, Out).

fillLine([El|Line], What, [What|Out]):-
    MEl is -El,
    What = MEl,
    fillLine2(Line, What, Out).

processRow([0, I2, I3, I4], 1, What, [What|RestL]):-
    fillLine([I2, I3, I4], What, RestL).
processRow([I1, 0, I3, I4], 2, What, [I1, What, NI3, NI4]):-
    fillLine([I3, I4], What, [NI3, NI4]).
processRow([I1, I2, 0, I4], 3, What, [NI1, NI2, What, I4]):-
    fillLine([I2, I1], What, [NI2, NI1]).
processRow([I1, I2, I3, 0], 4, What, [NI1, NI2, NI3, What]):-
    fillLine([I3, I2, I1], What, [NI3, NI2, NI1]).

updateRows2([Y1, Y2, Y3, Y4], X, 1, What, [OutH, Y2, Y3, Y4]):-
    processRow(Y1, X, What, OutH).
updateRows2([Y1, Y2, Y3, Y4], X, 2, What, [Y1, OutH, Y3, Y4]):-
    processRow(Y2, X, What, OutH).
updateRows2([Y1, Y2, Y3, Y4], X, 3, What, [Y1, Y2, OutH, Y4]):-
    processRow(Y3, X, What, OutH).
updateRows2([Y1, Y2, Y3, Y4], X, 4, What, [Y1, Y2, Y3, OutH]):-
    processRow(Y4, X, What, OutH).

updateRows(Board, X, Y, What, Out):-
    updateRows2(Board, X, Y, What, Out).
updateRows(Board, X, Y, What, Board):-
    not(updateRows2(Board, X, Y, What, _)).

updateCols2([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            1, Y, What,
           [[NA1, A2, A3, A4],
            [NB1, B2, B3, B4],
            [NC1, C2, C3, C4],
            [ND1, D2, D3, D4]]):-
    processRow([A1, B1, C1, D1], Y, What, [NA1, NB1, NC1, ND1]).
updateCols2([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            2, Y, What,
           [[A1, NA2, A3, A4],
            [B1, NB2, B3, B4],
            [C1, NC2, C3, C4],
            [D1, ND2, D3, D4]]):-
    processRow([A2, B2, C2, D2], Y, What, [NA2, NB2, NC2, ND2]).
updateCols2([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            3, Y, What,
           [[A1, A2, NA3, A4],
            [B1, B2, NB3, B4],
            [C1, C2, NC3, C4],
            [D1, D2, ND3, D4]]):-
    processRow([A3, B3, C3, D3], Y, What, [NA3, NB3, NC3, ND3]).
updateCols2([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            4, Y, What,
           [[A1, A2, A3, NA4],
            [B1, B2, B3, NB4],
            [C1, C2, C3, NC4],
            [D1, D2, D3, ND4]]):-
    processRow([A4, B4, C4, D4], Y, What, [NA4, NB4, NC4, ND4]).
updateCols(Board, X, Y, What, Out):-
    updateCols2(Board, X, Y, What, Out).

updateCols(Board, X, Y, What, Board):-
    not(updateCols2(Board, X, Y, What, _)).

updateBigDiagPriv([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            Pos, Pos, What,
            [[NA1, A2, A3, A4],
            [B1, NB2, B3, B4],
            [C1, C2, NC3, C4],
            [D1, D2, D3, ND4]]):-
    processRow([A1, B2, C3, D4], Pos, What, [NA1, NB2, NC3, ND4]).
updateBigDiagPriv([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            X, Y, What,
            [[A1, A2, A3, NA4],
            [B1, B2, NB3, B4],
            [C1, NC2, C3, C4],
            [ND1, D2, D3, D4]]):-
    Sum is X + Y,
    Sum = 5,
    processRow([D1, C2, B3, A4], X, What, [ND1, NC2, NB3, NA4]).

updateBigDiag(Board, X, Y, What, NB):-
    updateBigDiagPriv(Board, X, Y, What, NB).
updateBigDiag(Board, X, Y, What, Board):-
    not(updateBigDiagPriv(Board, X, Y, What, _)).

processSmallRow([0, B, C], 1, What, [What, NB, NC]):-
    fillLine([B,C], What, [NB, NC]).
processSmallRow([A, B, 0], 3, What, [NA, NB, What]):-
    fillLine([B,A], What, [NB, NA]).

updateSmD1priv([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            X, Y, What, 
           [[A1, NA2, A3, A4],
            [B1, B2, NB3, B4],
            [C1, C2, C3, NC4],
            [D1, D2, D3, D4]]):-
    ((X=2, Y=1);(X=3,Y=2);(X=4,Y=3)),
    processSmallRow([A2, B3, C4], Y, What, [NA2, NB3, NC4]).

updateSmD1(Board, X, Y, What, Out):-
    updateSmD1priv(Board, X, Y, What, Out).
updateSmD1(Board, X, Y, What, Board):-
    not(updateSmD1priv(Board, X, Y, What, _)).

updateSmD2priv([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            X, Y, What, 
           [[A1, A2, NA3, A4],
            [B1, NB2, B3, B4],
            [NC1, C2, C3, C4],
            [D1, D2, D3, D4]]):-
    ((X=3, Y=1);(X=2,Y=2);(X=1,Y=3)),
    processSmallRow([A3, B2, C1], Y, What, [NA3, NB2, NC1]).

updateSmD2(Board, X, Y, What, Out):-
    updateSmD2priv(Board, X, Y, What, Out).
updateSmD2(Board, X, Y, What, Board):-
    not(updateSmD2priv(Board, X, Y, What, _)).


updateSmD3priv([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            X, Y, What, 
           [[A1, A2, A3, A4],
            [NB1, B2, B3, B4],
            [C1, NC2, C3, C4],
            [D1, D2, ND3, D4]]):-
    ((X=1, Y=2);(X=2,Y=3);(X=3,Y=4)),
    processSmallRow([B1, C2, D3], X, What, [NB1, NC2, ND3]).

updateSmD3(Board, X, Y, What, Out):-
    updateSmD3priv(Board, X, Y, What, Out).
updateSmD3(Board, X, Y, What, Board):-
    not(updateSmD3priv(Board, X, Y, What, _)).

updateSmD4priv([[A1, A2, A3, A4],
            [B1, B2, B3, B4],
            [C1, C2, C3, C4],
            [D1, D2, D3, D4]],
            X, Y, What, 
           [[A1, A2, A3, A4],
            [B1, B2, B3, NB4],
            [C1, C2, NC3, C4],
            [D1, ND2, D3, D4]]):-
    ((X=4, Y=2);(X=3,Y=3);(X=2,Y=4)),
    RevX is X -1 ,
    processSmallRow([B4, C3, D2], RevX, What, [NB4, NC3, ND2]).

updateSmD4(Board, X, Y, What, Out):-
    updateSmD4priv(Board, X, Y, What, Out).
updateSmD4(Board, X, Y, What, Board):-
    not(updateSmD4priv(Board, X, Y, What, _)).

updateSmallDiag(Board, X, Y, What, Out):-
    updateSmD1(Board, X, Y, What, B1),
    updateSmD2(B1, X, Y, What, B2),
    updateSmD3(B2, X, Y, What, B3),
    updateSmD4(B3, X, Y, What, Out).

updateBoard(OldB, X, Y, What, OutB):-
    valAtPos(OldB, X, Y, 0),
    updateRows(OldB, X, Y, What, B1),
    setValAtPos(B1, X, Y, 0, B1N),
    updateCols(B1N, X, Y, What, B2),
    setValAtPos(B2, X, Y, 0, B2N),
    updateBigDiag(B2N, X, Y, What, B3),
    setValAtPos(B3, X, Y, 0, B3N),
    updateSmallDiag(B3N, X, Y, What, OutBN),
    not(OldB = OutBN), !, %% RED CUT!
    setValAtPos(OutBN, X, Y, What, OutB).
updateBoard(OldB, _, _, _, OldB).

% output
writeField([L1, L2, L3, L4]):-
    writeLine(L1), nl,
    writeLine(L2), nl,
    writeLine(L3), nl,
    writeLine(L4), nl, nl.

writeLine([A, B, C, D]):-
    writeCell(A), tab(2),
    writeCell(B), tab(2),
    writeCell(C), tab(2),
    writeCell(D), tab(2).

writeCell(0):- write('-').
writeCell(1):- write('X').
writeCell(-1):- write('O').

% eval
evalLine([], 0).
evalLine([El|Line], Ans):-
    evalLine(Line, EvAns),
    Ans is El + EvAns.

eval([[A1, A2, A3, A4],
      [B1, B2, B3, B4],
      [C1, C2, C3, C4],
      [D1, D2, D3, D4]], Ans):-
    evalLine([A1, A2, A3, A4], L1),
    evalLine([B1, B2, B3, B4], L2),
    evalLine([C1, C2, C3, C4], L3),
    evalLine([D1, D2, D3, D4], L4),
    evalLine([A1, B1, C1, D1], Col1),
    evalLine([A2, B2, C2, D2], Col2),
    evalLine([A3, B3, C3, D3], Col3),
    evalLine([A4, B4, C4, D4], Col4),
    evalLine([A1, B2, C3, D4], Di1),
    evalLine([A4, B3, C2, D1], Di2),
    evalLine([A3, B2, C1], Sd1),
    evalLine([A2, B3, C4], Sd2),
    evalLine([B1, C2, D3], Sd3),
    evalLine([B4, C3, D2], Sd4),
    Ans is L1 + L2 + L3 + L4 + 
           Col1 + Col2 + Col3 + Col4 + 
           Di1 + Di2 + Sd1 + Sd2 + Sd3 + Sd4.

fullBoard(Board):-
    countBoard(Board, P, M),
    X is P + M,
    X = 16.

count([], 0, 0).
count([-1|L], Plus, Minus):-
    count(L, Plus, M),
    Minus is M + 1.
count([1|L], Plus, Minus):-
    count(L, P, Minus),
    Plus is P + 1.
count([0|L], Plus, Minus):-
    count(L, Plus, Minus).

countBoard([L1, L2, L3, L4], Plus, Minus):-
    count(L1, P1, M1),
    count(L2, P2, M2),
    count(L3, P3, M3),
    count(L4, P4, M4),
    Plus is P1 + P2 + P3 + P4,
    Minus is M1 + M2 + M3 + M4.

gameLostfull(Board, 1):-
    countBoard(Board, P, M),
    M >= P.
gameLostfull(Board, -1):-
    countBoard(Board, P, M),
    M =< P.

gameLostpre(Board, 1):-
    count(Board, 0, M),
    M > 0.
gameLostpre(Board, -1):-
    countBoard(Board, P, 0),
    P > 0.

gameLost(Board, Who):-
    fullBoard(Board),
    gameLostfull(Board, Who).
gameLost(Board, Who):-
    not(fullBoard(Board)),
    gameLostpre(Board, Who).

% AI
%
% Move generation:
lineGen([0|_], CurPos, CurPos).
lineGen([_|Line], CurPos, OutPos):-
    NP is CurPos + 1,
    lineGen(Line, NP, OutPos).
posGen([Line|_], CurLine, Pos, CurLine):-
    lineGen(Line, 1, Pos).
posGen([_|Board], CurLine, X, Y):-
    NY is CurLine + 1,
    posGen(Board, NY, X, Y).

sameBoards([L1, L2, L3, L4], [L1, L2, L3, L4]).

isCorrectMove(Board, X, Y, Who):-
    updateBoard(Board, X, Y, Who, B2),
    not(sameBoards(Board, B2)).

move(Board, Who, X, Y):-
    posGen(Board, 1, X, Y),
    isCorrectMove(Board, X, Y, Who).

moves2(Board, Who, Out):-
    setof([Ox, Oy], move(Board, Who, Ox, Oy), Out).
moves(Board, Who, Out):-
    moves2(Board, Who, Out).
moves(Board, Who, []):-
    not(moves2(Board, Who, _)).
% Alpha-beta
alphabeta(_, Board, Who, _, _, [0, 0], -1000):-
    gameLost(Board, Who), !.
alphabeta(_, Board, Who, _, _, [0, 0], 1000):-
    En is -Who,
    gameLost(Board, En), !.
alphabeta(0, Board, _, _, _, [0, 0], Value):-
    eval(Board, Value), !.
alphabeta(Depth, Board, Who, Alpha, Beta, BestMove, Value):-
    Depth > 0,
    A1 is -Alpha,
    B1 is -Beta,
    moves(Board, Who, Moves),
    Moves = [StartM|_],!,  % not empty
    D is Depth - 1,
    bestmove(Moves, Board, Who, D, A1, B1, StartM, BestMove, Value).
alphabeta(Depth, Board, Who, Alpha, Beta, [0,0], Value):-
    Depth > 0,
    A1 is -Alpha,
    B1 is -Beta,
    moves(Board, Who, []),!,
    D is Depth - 1,
    bestmove(Board, Who, D, A1, B1, Value).

bestmove([[X, Y]|Moves], Board, Who, Depth, Alpha, Beta, Move0, Move1, Value1):-
    updateBoard(Board, X, Y, Who, NewBoard),
    En is -Who,
    alphabeta(Depth, NewBoard, En, Alpha, Beta, _, V),
    cutoff(
        [X, Y], V, Depth, Alpha, Beta, 
        Moves, Board, Who, Move0, Move1, Value1
    ).
bestmove([], _, _, _, Alpha, _, Move, Move, Alpha).
bestmove(Board, Who, Depth, Alpha, Beta, Value1):-
    En is -Who,
    alphabeta(Depth, Board, En, Alpha, Beta, _, MinusVal),
    Value1 is -MinusVal.

cutoff(_, Value, Depth, Alpha, Beta, Moves, Board, Who, Move0, Move1, Value1):-
    Value =< Alpha, !,
    bestmove(Moves, Board, Who, Depth, Alpha, Beta, Move0, Move1, Value1).
cutoff(Move, Value, Depth, _, Beta, Moves, Board, Who, _, Move1, Value1):-
    Value < Beta, !,
    bestmove(Moves, Board, Who, Depth, Value, Beta, Move, Move1, Value1).
cutoff(Move, Value, _, _, _, _, _, _, _, Move, Value).

getMove(Depth, Board, Who, Move):-
    setof(M, alphabeta(Depth, Board, Who, 100000, -100000, M, _), MSet),
    MSet = [Move|_].

% user interface (:P)
go :-
  write('Choose recursion depth: (positive int)'), nl,
  read(Depth),
  write('Who will be first? (1 for You, -1 for computer)'), nl,
  read(First), 
  write('P.S.: you will be going with "X"'), nl,
  start(Board),
  moveCtrl(First, Board, Depth).

canMove(Board, Who):-
    moves(Board, Who, M),
    M = [_|_].

enemy(1, -1).
enemy(-1, 1).

moveCtrl(Who, Board, Depth):-
    canMove(Board, Who),
    nextmove(Who, Board, Depth, NewB),
    enemy(Who, E),
    moveCtrl(E, NewB, Depth).
moveCtrl(Who, Board, Depth):-
    not(canMove(Board, Who)),
    enemy(Who, E),
    canMove(Board, E),
    write('Player skips his turn.'),nl,
    moveCtrl(E, Board, Depth).
moveCtrl(Who, Board, _):-
    not(canMove(Board, Who)),
    enemy(Who, E),
    not(canMove(Board, E)),
    write('Game End.'), nl,
    writeWinner(Board).

writeWinner(Board):-
    gameLost(Board, Looser),
    enemy(Looser, Winner),!, % red cut
    write('Winner: '),
    writeCell(Winner), nl.
writeWinner(_):-
    write('DRAW!'), nl.

nextmove(1, Board, _, NewB):-
    write('Field: '), nl,
    writeField(Board),
    %getMove(Depth, Board, 1, [X, Y]),
    getUserInp(X, Y),
    updateBoard(Board, X, Y, 1, NewB).

nextmove(-1, Board, Depth, NewB):-
    % comp move
    write('Field: '), nl,
    writeField(Board),
    getMove(Depth, Board, -1, [X, Y]),
    updateBoard(Board, X, Y, -1, NewB).

getUserInp(X, Y):-
    write('Enter X'), nl,
    read(X),
    write('Enter Y'), nl,
    read(Y).

% vim: set sts=4 sw=4 et syntax=prolog nospell:
