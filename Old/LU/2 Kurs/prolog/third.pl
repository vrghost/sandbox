
% Our nasty math realization

add(X, nil, X).

add(nil, X, X).

add(s(X), Y, s(G)):-
    add(X, Y, G).

% Fibonacci

fib(0,1).
fib(1,1).

fib(X, Out):-
    X > 1,
    Prev1 is X - 1,
    Prev2 is X - 2,
    fib(Prev1, O1),
    fib(Prev2, O2),
    Out is O1 + O2.

% S
%
% S(0) = 0
% S(1) = 1
% S(i) = i*S(0) + (i-1)*S(1) + ... + 1*S(i-1)

% list
list_el(1, list(X, _), X).

list_el(I, list(_, Y), Out):-
    Prev is I -1,
    list_el(Prev, Y, Out).

% btw, norm massivi --> X= .(a,.(b,[])).
% OR 
% X= [a, b].
%
% [X|Y]= [a,b,c,d].
% ==>
% X = a
% Y = [b,c,d]
%
% [X1,X2|Y] = [1,2,3,4].
% ==>
% X1 = 1
% X2 = 2
% Y = [3,4]
%
% [A, 42, B] = [1, C, 3].
% ==>
% A=1
% B=3
% C=42
%

% member(X, [1,2,3]) <==> X in [1,2,3]
member(X, [X|_]).

member(X, [_|List]):-
    member(X, List).

% len - array length
len([], 0).

len([_|List], Out):-
    len(List, L1),
    Out is L1 + 1.

% genlist(X, Y) ==> 
% Y = [a,b,c,...]
%      ^^^^^
%        X reizes 

genlist(1, [a,b,c]).

genlist(X, [a, b, c |List]):-
    X > 1,
    L1 is X - 1,
    genlist(L1, List).

% vim: set sts=4 sw=4 et syntax=prolog:
