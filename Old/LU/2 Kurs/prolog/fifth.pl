%%%%%%% TicTacToe

start([0,0,0,0,0,0,0,0,0]).

eval([ 
        A1,A2,A3,
        B1,B2,B3,
        C1,C2,C3
     ],
     F):-
    eval2(A1,A2,A3,F1), 
    eval2(B1,B2,B3,F2), 
    eval2(C1,C2,C3,F3), 
    eval2(A1,B1,C1,F4), 
    eval2(A2,B2,C2,F5), 
    eval2(A3,B3,C3,F6), 
    eval2(A1,B2,C3,F7), 
    eval2(A3,B2,C1,F8),
    F is F1+F2+F3+F4+F5+F6+F7+F8.

eval2(A,B,C,F):-
    X is A+B+C,
    eval3(X, F).

eval3(1,1).
eval3(2,10).
eval3(3,1000).
eval3(-1,-1).
eval3(-2,-10).
eval3(-3,-1000).
eval3(0,0).

goal(X):-
    goal1(X).

goal(X):-
    goal2(X).

goal1(X):-
    eval(X, F),
    (F > 100 ; F < -100). %%<<-- smb. has won

goal2([1|T]):-
    goal2(T).

goal2([-1|T]):-
    goal2(T).

goal2([]).

move([0|T], P, [P|T]).
move([H|T], P, [H|T1]):-
    move(T,P, T1).

moves(X,P,L):-
    setof(Z, move(X,P,Z), L).

maxlist([], _, -10000, [0,0,0,0,0,0,0,0,0], A,B).
maxlist([X|T], N,V,M, A,B):-
    maxlist(T,N,V1,M1),
    minv(X,N,V2),
    max(V1,V2,M1,X,V,M).

minv(X,0,F):-
    eval(X,F).

minv(X, _, F):-
    goal(X),
    eval(X,F).

minv(X,N,F):-
    moves(X, -1, L),
    N1 is N-1,
    minlist(L, N1, F, _).

minlist([],_,10000,[0,0,0,0,0,0,0,0,0],A,B).
minlist([X|T],N,V,M,A,B):-
    minlist(T,N,V1,M1),
    maxv(X,N,V2),
    min(V1,V2,M1,X,V,M).

max(V1,V2,M1,_,V1,M1):-
    V1>V2.

max(V1,V2,_,X,V2,X):-
    V1=<V2.

min(V1,V2,M1,_,V1,M1):-
    V1<V2.

min(V1, V2, _, X, V2, X):-
    V1>=V2.

maxv(_,_,B,A,B):-
    A>=B.
maxv(X,0,F,A,B):-
    eval(X,F).
maxv(X,_,F,A,B):-
    goal(X),
    eval(X,F).
maxv(X,N,F,A,B):-
    moves(X,1,L),
    N1 is N-1,
    maxlist(L,N1,F,_,A,B).

maxmove(X,N,M):-
    moves(X,1,L),
    N1 is N-1,
    maxlist(L,N1,_,M).

play(N):-
    start(X),
    play1(X,N).

play1(X,_):-
    goal(X),
    !,fail.

play1(X,N):-
    maxmove(X,N,M),
    writemove(M),
    play2(M,N).

play2(X, _):-
    goal(X), !, fail.

play2(X,N):-
    readmove(X,M),
    play1(M,N).

writemove([ 
        A1,A2,A3,
        B1,B2,B3,
        C1,C2,C3
        ]):-
    writet(A1), tab(1), writet(A2), tab(1), writet(A3), nl,
    writet(B1), tab(1), writet(B2), tab(1), writet(B3), nl,
    writet(C1), tab(1), writet(C2), tab(1), writet(C3), nl, nl.

writet(0):-
    write('_').

writet(1):-
    write('+').

writet(-1):-
    write('0').

readmove(X,M):-
    read(S),
    move2(X,S,M).

move2([H|T], -1, [-1|T]).
move2([H|T], S, [H|T1]):-
    S1 is S-1,
    move2(T, S1, T1).


% vim: set sts=4 sw=4 et syntax=prolog:
