suits([A, B], [El|List]):-
    (A = El; B = El),
    noMore([A,B], List).
suits([A,B], [El|List]):-
    not(A = El),
    not(B = El),
    suits([A,B], List).
noMore(_, []).
noMore([A,B], [El|List]):-
    not(A = El),
    not(B = El),
    noMore([A,B], List).

% vim: set sts=4 sw=4 et syntax=prolog nospell:
