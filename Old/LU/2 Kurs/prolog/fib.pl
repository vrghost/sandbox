fib(X, Y) :-
	X =< 1,
	Y is 1.

fib(X, Y):-
	X > 1,
	Z is X - 1,
	G is X - 2,
	fib(Z, Q),
	fib(G, A),
	Y is Q + A.
