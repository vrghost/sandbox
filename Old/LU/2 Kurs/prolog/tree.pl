a(1).
a(2).
a(3).

b(1).
b(2).
b(3).

c(X):-
    b(X),
    a(X),
    b(X).

% vim: set sts=4 sw=4 et syntax=prolog nospell:
