sort2(A, B, B, A):-
    A =< B.
sort2(A,B, A, B):-
    B < A.

doNotDivide( _, Limit, Limit).
doNotDivide(A, Divisor, Limit) :- 
 Divisor < Limit,
 Res is A mod Divisor,
 Res =\= 0,
 NewDivisor is Divisor + 1,
 doNotDivide(A, NewDivisor, Limit).

simple(A, B):-
    sort2(A, B, Max, Min),
    M is Min +1,
    doNotDivide(Max, 2, M).

% vim: set sts=4 sw=4 et syntax=prolog nospell:
