datatype bst = Empty |
        Node of int * bst * bst;

fun newLeaf arg = Node(arg, Empty, Empty);

fun find(n, Empty) = false
  |  find(n, Node(v1, Left, Right)) =
        if n=v1 then true
        else if n<v1 then find(n, Left)
        else find(n, Right);

fun insert(n, Empty) = newLeaf n
  | insert(n, Node(v, Left, Right)) =
        if v=n then Node(v, Left, Right) (* do nothing ok key duplicte *)
        else if n<v then Node(v, insert(n, Left) ,Right)
        else Node(v, Left, insert(n, Right));

fun insertNode(Empty, n) = Empty
  | insertNode(N1, Empty) = N1
  | insertNode(Node(v1, L1, R1), Node(v2, L2, R2)) =
        if v1=v2 then Node(v1, L1, R1) (* shouldn't happen! *)
        else if v1<v2 then Node(v1, insertNode(Node(v2, L2, R2), L1),R1)
        else Node(v1, L1, insertNode(Node(v2, L2, R2), R1));

fun delete(n, Empty) = Empty
  | delete(n, Node(v, Left, Right)) =
        if v=n then insertNode(Right, Left)
        else if n<v then Node(v, delete(n, Left), Right)
        else Node(v, Left, delete(n, Right));

fun inorder(Empty) = []
  | inorder(Node(v, Left, Right)) =
        inorder(Left) @ [v] @ inorder(Right);

fun insertList([], R) = R|
 insertList(item::lst, R) = insertList(lst, insert(item, R));

fun listToTree([]) = Empty|
  listToTree(root::lst) = insertList(lst, Node(root, Empty, Empty));

fun treesort([]) = []|
  treesort(L) = 
         inorder(listToTree(L));

(* vim: set sts=4 sw=4 et *)
