max(A, B, A) :- A >= B, !.
max(_, B, B).

min(A, B, A) :- A =< B, !.
min(_, B, B).

pickBest(A, _, A, C, D) :- C =< D, !.
pickBest(_, B, B, _, _).

remove(This, [This|From], From) :- !.
remove(This, [A|From], [A|Result]) :- remove(This, From, Result).

convertBestWeight(-1, Weight, NewBestWeight) :- NewBestWeight is Weight + 1, !.
convertBestWeight(BestWeight, _, BestWeight).

addNearest([none, none, -1], _, _, []).
addNearest(BestResultingEdge, Added, Remaining, [[From|[To|[Weight]]]|Edges]) :-
	(member(From, Added), member(To, Remaining); member(To, Added), member(From, Remaining)),
	!,
	addNearest([BestFrom|[BestTo|[BestWeight]]], Added, Remaining, Edges),
	convertBestWeight(BestWeight, Weight, NewBestWeight),
	pickBest([From|[To|[Weight]]], [BestFrom|[BestTo|[BestWeight]]], BestResultingEdge, Weight, NewBestWeight).
addNearest(BestEdge, Added, Remaining, [_|Edges]) :-
	addNearest(BestEdge, Added, Remaining, Edges).

formNewEdges(Added, Remaining, Edges, [To|Added], NewRemaining, NewEdges, From, To, Edge) :-
	member(From, Added),
	!,
	remove(To, Remaining, NewRemaining),
	remove(Edge, Edges, NewEdges).
formNewEdges(Added, Remaining, Edges, [From|Added], NewRemaining, NewEdges, From, _, Edge) :-
	remove(From, Remaining, NewRemaining),
	remove(Edge, Edges, NewEdges).


iteration(_, [], _, 0).
iteration(Added, Remaining, Edges, Sum) :-
	addNearest([From|[To|[Weight]]], Added, Remaining, Edges),
	Weight =\= -1,
	formNewEdges(Added, Remaining, Edges, NewAdded, NewRemaining, NewEdges, From, To, [From|[To|[Weight]]]),
	iteration(NewAdded, NewRemaining, NewEdges, OtherSum),
	Sum is OtherSum + Weight.
	

mst(_,[],0) :- !.
mst([First|Remaining], E, X) :- iteration([First], Remaining, E, X).