in(El, [El|_]).
in(El, [_|Rest]):-
    in(El, Rest).
not_in(El, L):- not(in(El,L)).

way([[A, B, Len]|_], [A, B, Len]).
way([[A, B, Len]|_], [B, A, Len]).
way([_|Rest], Way):- way(Rest, Way).

add([], S, S).
add([El|Rest], Set, Ans):-
    in(El, Set),
    add(Rest, Set, Ans).
add([El|Rest], Set, [El|Ans]):-
    not_in(El, Set),
    add(Rest, Set, Ans).

remove(Set1, Set2, Ans):-
    findall(El,(in(El, Set1), not_in(El, Set2)), Ans).

sameSets(A, B):-
    remove(A, B, []),
    remove(B, A, []).

minWay([], _, _).
minWay([[_, _, Length]|Rest], VisitedPts, [Src, Dst, Len]):-
    Length >= Len,
    minWay(Rest, VisitedPts, [Src, Dst, Len]).
minWay([[B, A, _]|Rest], VisitedPts, Way):-
    in(A, VisitedPts), 
    in(B, VisitedPts), 
    minWay(Rest, VisitedPts, Way).
minWay([[B, A, _]|Rest], VisitedPts, Way):-
    not_in(A, VisitedPts), 
    not_in(B, VisitedPts), 
    minWay(Rest, VisitedPts, Way).

shortestWay(Points, VisitedPts, Ways, [Source, Dest, Length]):-
    in(Source, VisitedPts),
    in(Dest, Points), not_in(Dest, VisitedPts),
    way(Ways, [Source, Dest, Length]),
    minWay(Ways, VisitedPts, [Source, Dest, Length]).

walk(Visited, Points, _, 0):- sameSets(Visited, Points).
walk(Visited, Points, Ways, Length):-
    shortestWay(Points, Visited, Ways, [Src, Dest, Len]),
    add([Src, Dest], Visited, V2),
    walk(V2, Points, Ways, L2),
    Length is L2 + Len.

mst(V, E, X):-
    V = [Start|_],
    walk([Start], V, E, X), !.

test(X):-
    V = ['a', 'b', 'c', 'd', 'e'],
    E = [['a', 'b', 98], ['a', 'c', 43], ['a', 'd', 69], 
        ['a', 'e', 20], ['b', 'c', 55], ['b', 'd', 93], 
        ['b', 'e', 44], ['c', 'd', 81], ['c', 'e', 56], 
        ['d', 'e', 52]],
    mst(V, E, X).

% vim: set sts=4 sw=4 et syntax=prolog:
