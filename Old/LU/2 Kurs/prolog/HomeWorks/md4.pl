in(El, [El|_]).
in(El, [_|Rest]):-
    in(El, Rest).

actions(Action):-
    in(Action, [
        fill_a, fill_b,
        empty_a, empty_b, 
        use_a, use_b,
        tr_ab, tr_ba
    ]).

stupid_actions(Same, Same).
stupid_actions(fill_a, empty_a).
stupid_actions(fill_b, empty_b).
stupid_actions(fill_b, empty_a).
stupid_actions(fill_a, empty_b).
stupid_actions(empty_b, fill_b).
stupid_actions(empty_a, fill_a).
stupid_actions(empty_a, fill_b).
stupid_actions(empty_b, fill_a).
stupid_actions(tr_ab, tr_ba).
stupid_actions(tr_ba, tr_ab).
stupid_actions(use_a, empty_a).
stupid_actions(use_b, empty_b).
stupid_actions(empty_b, use_b).
stupid_actions(empty_a, use_a).

extendTree([], [Act]):- actions(Act).
extendTree([El|T], [El,Prev|NT]):- 
    extendTree(T, [Prev|NT]),
    not(stupid_actions(El, Prev)).

extendTrees(_, [], []).
extendTrees(Stats, [Tree|Trees], Ans):-
    extendTrees(Stats, Trees, Oth),
    findall(Tr, (
        extendTree(Tree, Tr),
        possibleTree(Stats, Tree)
    ), My),
    merge(Oth, My, Ans).

possibleTree(Stats, Tree):- initArc(Stats, Tree, _).
correctTree(Stats, Tree):- initArc(Stats, Tree, 1).

initArc(Stats, Tree, Ans):-
    arc(Stats, [0,0,0], Tree, [], Ans).

arc(_, State, _, BeenStates, _):- in(State, BeenStates), !, fail.
arc([_, _, C], [_, _, C], [], _, 1):- !.
arc(_, _, [], _, 0):- !.
arc(Stats, State, [Action|Path], BeenStates, Status):-
    perform(Stats, State, Action, NewState),
    arc(Stats, NewState, Path, [State|BeenStates], Status).

perform([Ma, _, _], [Ca, B, C], fill_a, [Ma, B, C]):- Ca < Ma.
perform([_, Mb, _], [A, Cb, C], fill_b, [A, Mb, C]):- Cb < Mb.
perform(_, [Ca, B, C], empty_a, [0, B, C]):- Ca > 0.
perform(_, [A, Cb, C], empty_b, [A, 0, C]):- Cb > 0.
perform([_, _, Mc], [Ca, B, Cc], use_a, [0, B, Nc]):- use(Mc, Ca, Cc, Nc).
perform([_, _, Mc], [A, Cb, Cc], use_b, [A, 0, Nc]):- use(Mc, Cb, Cc, Nc).
perform([_, Mb, _], [Ca, Cb, C], tr_ab, [Na, Nb, C]):- trans(Mb, Ca, Cb, Na, Nb).
perform([Ma, _, _], [Ca, Cb, C], tr_ba, [Na, Nb, C]):- trans(Ma, Cb, Ca, Nb, Na).


trans(MWhere, What, Where, 0, NWhere):-
    What > 0, MWhere > Where,
    Delta is MWhere - Where,
    Delta >= What,
    NWhere is What + Where.
trans(MWhere, What, Where, NWhat, MWhere):-
    What > 0, MWhere > Where,
    Delta is MWhere - Where,
    What > Delta,
    NWhat is What - Delta.

use(MWhere, What, Where, NWhere):-
    What > 0, MWhere > Where,
    Delta is MWhere - Where,
    Delta >= What,
    NWhere is What + Where.

iter(_, [], _):- !, fail.
iter(Stats, Wood, Tree):-
    extendTrees(Stats, Wood, NewWood),
    in(Tree, NewWood),
    correctTree(Stats, Tree), !.
iter(Stats, Wood, Ans):-
    extendTrees(Stats, Wood, NewWood),
    iter(Stats, NewWood, Ans).

start(Stats, Ans):-
    Roots = [[fill_a], [fill_b]],
    iter(Stats, Roots, Ans).

% vim: set sts=4 sw=4 et syntax=prolog nospell:
