from time import time
from os import system

_cmdLine = """plcon -g "[md3], longest_rps(%s, _)." -t halt """

def doTest(length):
    _arr = list(range(1, length + 1))
    _argL = _cmdLine % _arr
    _startT = time()
    system(_argL)
    _endTime = time()
    print "Time took: %s, arg count: %s" % (_endTime - _startT, length)

if __name__ == "__main__":
    for _testL in xrange(1, 38, 5):
        doTest(_testL)

# vim: set sts=4 sw=4 et :
