is_subsequence([] ,_).

is_subsequence([Elem1 | List1], [Elem1 | List2]):-
    is_subsequence(List1, List2).

is_subsequence([Elem1 | List1], [_ | List2]):-
    is_subsequence([Elem1 | List1], List2).

is_strict_substring([], _).

is_strict_substring([El1 | List1], [El1 | List2]):-
    is_strict_substring(List1, List2).

is_substring([], _).

is_substring([El1 | List1], [El1 | List2]):-
    is_strict_substring(List1, List2).

is_substring(List1, [_ | List2]):-
    is_substring(List1, List2).

% vim: set sts=4 sw=4 et syntax=prolog:
