simple(0,1). 
simple(1,0).
simple(A, B):-
    A >= B, B > 0,
    M is A mod B,
    simple(M, B).
simple(A,B):-
    B > A, A > 0,
    M is B mod A,
    simple(A, M).

not_simple(A, B):- not(simple(A, B)).

% [ prev_num_index, length, number ]

startof([], _).
startof([El|L1], [El|L2]):- startof(L1, L2).

in(El, [El|_], 1).
in(El, [_|Arr], Pos):-
    in(El, Arr, P2),
    Pos is P2 + 1.

split([El], [], El).
split([El|List], [El|L2], Last):- split(List, L2, Last).

maxSimpleEl([], [0, 0, _], _).
maxSimpleEl([[Idx, Len, El]|List], [Idx, Len, El], Num):-
    simple(El, Num),
    maxSimpleEl(List, [_, M2El, _], Num),
    Len >= M2El.
maxSimpleEl([[_, Len, El]|List], [PrevEl, MaxElLen, MaxEl], Num):-
    simple(El, Num),
    maxSimpleEl(List, [PrevEl, MaxElLen, MaxEl], Num),
    MaxElLen > Len.
maxSimpleEl([[_, _, El]|List], OthMax, Num):-
    not_simple(El, Num),
    maxSimpleEl(List, OthMax, Num).

lastArrayEl([], Num, [0, 1, Num]).
lastArrayEl(Arr, Num, [PrevIdx, Len, Num]):-
    maxSimpleEl(Arr, [MPrevEl, MaxL, MaxEl], Num),
    in([MPrevEl, MaxL, MaxEl], Arr, PrevIdx),
    Len is MaxL + 1.
lastArrayEl(Arr, Num, [0, 1, Num]):- maxSimpleEl(Arr, [_, 0, _], Num).

elList([], []).
elList(Data, List):-
    split(Data, HData, El),
    split(List, HList, [Prev, Len, El]),
    elList(HData, HList),
    lastArrayEl(HList, El, [Prev, Len, El]), !.


listUntil(_, [0, _, El] , [El]).
listUntil(List, [PrevIdx, _ ,El], [El|Str]):-
    in(Prev, List, PrevIdx),
    startof(SList, List), split(SList, _, Prev),
    listUntil(SList, Prev, Str).

reversed([], []).
reversed([El|List], Str):-
    split(Str, HStr, El),
    reversed(List, HStr), !.

strUntil(List, El, Str):-
    listUntil(List, El, L),
    reversed(L, Str).

maxLen(_, []).
maxLen(Len, [[_, L2, _]|List]):-
    Len >= L2,
    maxLen(Len, List).

longest_rps(X, Y):-
    elList(X, List),
    in([Prev, Len, El], List, _), maxLen(Len, List),
    strUntil(List, [Prev, Len, El], Y), !.

% vim: set sts=4 sw=4 et syntax=prolog nospell:
