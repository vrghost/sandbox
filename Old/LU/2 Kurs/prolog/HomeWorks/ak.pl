findX([Value|_],Value,FromWhere,FromWhere) :- true, !.
findX([_|List],Value,FromWhere,Res) :- NewPos is FromWhere + 1, findX(List,Value,NewPos,Res).

add2list(List,Var,Res) :- append([],List,Tmp), append(Tmp,[Var],Res).

deleteNth([_|List],1,CurRes,Res) :- append(CurRes,List,Res), !.
deleteNth([FVal|List],N,CurRes,Res) :- add2list(CurRes,FVal,Tmp), X is N - 1,
	deleteNth(List,X,Tmp,Res).

findMinE([[_,SV,FV]|_],UsedV,CurPos,CurPos) :- findX(UsedV,FV,1,_), 
	not(findX(UsedV,SV,1,_)), !.
findMinE([[_,SV,FV]|_],UsedV,CurPos,CurPos) :- findX(UsedV,SV,1,_), 
	not(findX(UsedV,FV,1,_)), !.
findMinE([_|ELeft],UsedV,CurPos,ResPos) :- Tmp is CurPos + 1, 
	findMinE(ELeft,UsedV,Tmp,ResPos).

chkadd(List,Element,List) :- findX(List,Element,1,_), !.
chkadd(List,Element,[Element|List]) :- true, !.

iteration(UsedV,ELeft,ResVal,ResVal,UsedV) :- 
	not(findMinE(ELeft,UsedV,1,_)), !.
iteration(UsedV,ELeft,CurVal,ResVal,UsedLeft) :- 
	findMinE(ELeft,UsedV,1,TmpPos),
	nth1(TmpPos,ELeft,Adding), deleteNth(ELeft,TmpPos,[],Tmp),
	Adding = [Value,To,From], chkadd(UsedV,From,NewUsed),
	chkadd(NewUsed,To,NewUsed2), TmpVal is CurVal + Value,
	iteration(NewUsed2,Tmp,TmpVal,ResVal,UsedLeft), !.
	
eachReverse([],[]) :- true, !.
eachReverse([FVal|Inp],[NewVal|Res]) :- reverse(FVal,NewVal),
	eachReverse(Inp,Res), !.

eachFound([],_) :- true, !.
eachFound([FVal|ListA],ListB) :- findX(ListB,FVal,1,_),
	eachFound(ListA,ListB), !.

mst([],_,0) :- true, !.
mst([_],_,0) :- true, !.
mst([FVal|V],E,X) :- eachReverse(E,Tmp), sort(Tmp,Sorted), 
	iteration([FVal],Sorted,0,X,VLeft), eachFound(V,VLeft), !.





