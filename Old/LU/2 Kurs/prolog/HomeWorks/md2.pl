%%% my debugging func

print_all([]).
print_all([El|List]):-
    write(El),nl,
    print_all(List).

%%main part

len([], 0).
len([_|L], Ln):-
    len(L, K),
    Ln is K+1.

append2([], E, [E]).
append2([El|List], Item, Res):-
    append2(List, Item, Res2),
    Res = [El|Res2].

trim([El|List], Len, [El|Res]):-
    L2 is Len-1,
    L2 >= 0,
    trim(List, L2, Res).
trim(_, Len, []):-
    Len=<0.

max([], Curr_max, Curr_max).
max([El|List], Curr_max, Ans):-
    len(El, Len),
    Len > Curr_max,
    max(List, Len, Ans).
max([El|List], Curr_max, Ans):-
    len(El, Len),
    Len =< Curr_max,
    max(List, Curr_max, Ans).

prefix([], E, E).
prefix([El|List], Accum, Pref):-
    append2(Accum, El, Pref);
    len(List, L),
    L > 0,
    prefix(List, Accum, Pref2),
    Pref=[El|Pref2].

postfix([], []).
postfix([El|List], Result):-
    Result = [El|List];
    postfix(List, Result).

p(List, Res):-
    len(List, LL),
    prefix(List, [], Pref),
    len(Pref, L),
    LL > L,
    postfix(List, Post),
    Post = Pref,
    Res = Pref.

p(List, []):-
    len(List, LL),
    prefix(List, [], Pref),
    len(Pref, L),
    LL =< L.
 
pi(List, Len, Ans):-
    trim(List, Len, RList),
    setof(Pres, p(RList, Pres), Set),
    max(Set, 0, Ans).

pDec(_, 0, []).
pDec(List, Len, Ans):-
    Len>0,
    pi(List, Len, El),
    L2 is Len-1,
    pDec(List, L2, Ans2),
    %% Small 'XXX' to give result in correct order
    append2(Ans2, El, Ans).

prefix_function(List, Ans):-
    len(List, Len),
    pDec(List, Len, Ans).

% vim: set sts=4 sw=4 et syntax=prolog nospell:
