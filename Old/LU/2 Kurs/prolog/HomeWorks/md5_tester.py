from time import time
from os import system
from random import randint

_cmdLine = """plcon -g "[%s], mst(%s, %s, X), tell('out.md5'), write(X)." -t halt """

def getPoints():
    _out = []
    _el = 'a'
    _cnt = randint(8, 10)
    for _p in xrange(0, _cnt):
        _out.append(_el)
        _el = chr(ord(_el) + 1)
    return _out

def getWays(points):
    _ways = []
    for _pos in xrange(len(points)):
        _el = points[_pos]
        _others = points[_pos+1:]
        for _oth in _others:
            _ways.append([_el, _oth, randint(1,100)])
    return _ways

def doTest(length):
    _files = ("ak", "orb", "md5")
    _pts = getPoints()
    _ways = getWays(_pts)
    _ans = None
    for _work in _files:
        _line = _cmdLine % (_work, _pts, _ways)
        system(_line)
        _out = file("out.md5").read()
        if _ans is None:
            _ans = _out
        if _ans != _out:
            print _out
            print "%s: %s" % (_work, _out)

if __name__ == "__main__":
    for _testL in xrange(38):
        doTest(_testL)
        print "=" *30

# vim: set sts=4 sw=4 et :
