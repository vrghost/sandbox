simple(0,1). 
simple(1,0).
simple(A, B):-
    A >= B, B > 0,
    M is A mod B,
    simple(M, B).
simple(A,B):-
    B > A, A > 0,
    M is B mod A,
    simple(A, M).

not_simple(A, B):- not(simple(A, B)).

% [ prev_num_index, length, number ]

len([], 0).
len([_|L], A):-
    len(L, A2),
    A is A2 + 1.

in(El, [El|_], 1).
in(El, [_|Arr], Pos):-
    in(El, Arr, Pos2),
    Pos is Pos2 + 1.

maxSimpleEl([], [0, 0, _], _).
maxSimpleEl([[Idx, Len, El]|List], [Idx, Len, El], Num):-
    simple(El, Num),
    maxSimpleEl(List, [_, M2El, _], Num),
    Len >= M2El.
maxSimpleEl([[_, Len, El]|List], [PrevEl, MaxElLen, MaxEl], Num):-
    simple(El, Num),
    maxSimpleEl(List, [PrevEl, MaxElLen, MaxEl], Num),
    MaxElLen > Len.
maxSimpleEl([[_, _, El]|List], OthMax, Num):-
    not_simple(El, Num),
    maxSimpleEl(List, OthMax, Num).

lastArrayEl([], Num, [0, 1, Num]).
lastArrayEl(Arr, Num, [PrevIdx, Len, Num]):-
    maxSimpleEl(Arr, [MPrevEl, MaxL, MaxEl], Num),
    in([MPrevEl, MaxL, MaxEl], Arr, PrevPos),
    len(Arr, ArrL),
    PrevIdx is ArrL - PrevPos + 1,
    Len is MaxL + 1.
lastArrayEl(Arr, Num, [0, 1, Num]):- maxSimpleEl(Arr, [_, 0, _], Num).

reverse([], Ans, Ans).
reverse([El|List], TmpL, Ans):- reverse(List, [El|TmpL], Ans).
reverse(List, Ans):- reverse(List, [], Ans).

process_array([], Ans, Ans).
process_array([El|List], CurAns, Ans):-
    lastArrayEl(CurAns, El, NewEl),
    process_array(List, [NewEl|CurAns], Ans).
process_array(List, Ans):- process_array(List, [], Ans).

create_array(List, Ans):-
    process_array(List, Ans2),
    reverse(Ans2, Ans).

get_line(List, Ans):-
    max_el(List, Ans).

max_el([], [_, 0, _]).
max_el([[Prev, Len, El]|Lst],  [Prev,Len,El]):-
    max_el(Lst, [_, L, _]),
    Len >= L.
max_el([[_, Len, _]|Lst], [PIdx, PLen, PEl]):-
    max_el(Lst, [PIdx, PLen, PEl]),
    PLen > Len.

gen_line_to(_, [0, 1, El], [El]).
gen_line_to(Arr, [PIdx, _, El], [El|Str]):-
    in(Prev, Arr, PIdx),
    gen_line_to(Arr, Prev, Str).

getLine(List, Line):-
    create_array(List, Arr),
    max_el(Arr, MEl),
    gen_line_to(Arr, MEl, Line2),
    reverse(Line2, Line).

longest_rps(X, Y):-
    getLine(X, Y), !.


% vim: set sts=4 sw=4 et syntax=prolog nospell:
