% Program that finds longest possible subsequence
%  ( LCS )
%  (see presentation for more smart formulas)
%  Detailed description: "LCS- idea"

genlist2(1, [1]).
genlist2(X, [X|L]):-
    X2 is X - 1,
    genlist2(X2, L).

%%%%%%%% mine
%

lcs_strict([], _, []).
lcs_strict(_, [], []).

lcs_strict([A|L], [A|L2], [A|R]):-
    lcs_strict(L, L2, R).

%%%%%%%%%%%5 end of mine

fill_0([], []).
fill_0([_|L], [0|R]):-
    fill_0(L, R).

reverse1([], X, X).
reverse1([X|T] S, R):-
    reverse1(T, [X|S], R).
reverse1(N,R):-
    reverse1(N, [], R).

fill_row([], _, _, N, NR):-
    reverse1(N, NR).
fill_row([A|L1], Elem, [RH1, RH2| PrevLine], [P| NextL]):-
    find_el(A, Elem, RH1, RH2, P, X),
    fill_row(L1, Elem, PrevLine, [X,P|NextL]).

find_el(A,A,RH1, _, _, X):-
    X is RH1 + 1.
find_el(A,B, _, RH2, P, RH2):-
    A \= B,
    RH2 > P.
find_el(A, B, _, RH2, P, P):-
    A \= B,
    RH2 =< P.


lcs(_, [], N, N).

lcs(L1, [B|Tl2], R,X):-
    

lcs([_|L], [_|L2], R):-
    lcs(L, L2, R).



% vim: set sts=4 sw=4 et syntax=prolog:
