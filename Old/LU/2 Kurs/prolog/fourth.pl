list_el(1, list(X, _), X).
% list_el(I, list(_,Y), R, <- bla-bla-bla...
%

member(X, [X|_]).
member(X, [_|L]):-
    member(X, L).

length1([], 0).
length1([_|T], N):-
    length1(T, N1),
    N is N1 + 1.

length2(L, N):-
    length2(L, 0, N).

length2([], N, N).
length2([_|T], I, N):-
    I1 is I + 1,
    length2(T, I1, N).

genlist(1, [a,b,c]).
genlist(X, [a,b,c | L]):-
    X1 is X-1,
    genlist(X1, L).

concat1([], L, L).
concat1([X|T], L, [X|R]):-
    concat1(T, L, R).

reverse1([], []).
reverse1([X|T], R):-
    reverse1(T, T1),
    concat1(T1, [X], R).

reverse2(L, R):- 
    reverse2(L, [], R).
reverse2([], L, L).
reverse2([X|T], L, R):-
    reverse2(T, [X|L], R).

setify([], []).
setify([X|T], R):-
    setify(T, R),
    member(X, R).
setify([X|T], [X|R]):-
    setify(T, R).

% bubble sort
%
%
% `swap` performs one bbsort iteration
% Last var - flag(bool) "Was smth changed?"
swap([], [], 0).
swap([A], [A], 0).
swap([X,Y|L], [X|R], I):-
    X =< Y,
    swap([Y|L], R, I).
swap([X,Y|T], [Y|L], 1):-
    X > Y,
    swap([X|T], L, _).

bs(X, R):-
    swap(X, R, Stat),
    Stat = 0.

bs(X, R):-
    swap(X, R1, Stat),
    Stat = 1,
    bs(R1, R).

genlist2(1, [1]).
genlist2(X, [X|L]):-
    X1 is X -1,
    genlist2(X1, L).

% Merge sort
%
split([], [], []).
split([X], [X], []).
split([X,T|M], [X|R1], [T|R2]):-
    split(M, R1, R2).

merg(X,[],X).
merg([], X, X).
merg([X|M1], [Y|M2], [X|R]):-
    X =< Y,
    merge(M1, [Y|M2], R).

merg([X|M1], [Y|M2], [Y|R]):-
    X > Y,
    merge([X|M1], M2, R).

ms([], []).
ms([X], [X]).
ms(L, R):-
    split(L, M1, M2),
    ms(M1, R1),
    ms(M2, R2),
    merg(R1, R2, R).

% vim: set sts=4 sw=4 et syntax=prolog:
