package HelpPanel;


import java.awt.Container;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;




public class HelpPanel extends JFrame {
	private static final long serialVersionUID = 4450931889926763503L;
	private JScrollPane main_infoScrollPane;
	
	public HelpPanel(){
		super();
	Container contentPanel=getContentPane();
		
	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();
	contentPanel.setLayout(gridbag);
	c.fill = GridBagConstraints.BOTH;
	c.weightx = 1.0f;
	c.anchor=GridBagConstraints.PAGE_START;
	c.gridwidth = GridBagConstraints.REMAINDER;


    
	JEditorPane main_info = new JEditorPane("text/html","text");

	main_info.setEditable(false); main_info.setFocusable(false);
	main_info.setOpaque(false);
	
	main_info.setText(
  "<table width=\"682\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"4\"><div align=\"center\"><strong>The RiSC-16 emulator/translator help file</strong></div></td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>1. The history</strong></td>"+
  "  <td width=\"52\">&nbsp;</td>"+
  "  <td width=\"383\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"4\"><div align=\"left\">RiSC 16 stands for &quot;Ridiculously Simple Computer&quot;. It is a 16 bit, 8 register processor that does not exist in real physical world - only in the heads of students and teachers :) You have probably guessed that it has been invented for education purposesonly:) (the original idea belongs to Prof. Bruce Jacob).<br>"+
  "  The idea of making our own RiSC-16 emulator came into our heads when we understood that our teacher's made emuator (based on Adobe SVG and javascript) did not function/functioned poorly under Opera and Firefox browsers. The next thing is that personally I thought that programming in machine code was absolutely awkward (teacher's emuator did not support asm). So we (me, Paul &quot;tr33g&quot; Fyodorov and Ilja &quot;[VR]Ghost&quot; Orlov) started building our personal emul :)</div></td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>2. Supported commands</strong></td>"+
  "  <td>&nbsp;</td>"+
  "  <td>&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "  <td colspan=\"2\">please remember that the possible number formats are: <b>decimal</b> is simply [number], <b>hexadecimal</b> is 0x[number]</td>"+
  "  <td>&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>Processor instructions:</strong></td>"+
  "  <td>&nbsp;</td>"+
  "  <td>&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">add regA, regB, regC</td>"+
  "  <td colspan=\"2\">adds regB contents to regC contents and writes the result into regA</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">addi regA, regB, imm</td>"+
  "  <td colspan=\"2\">adds an integer number in the range of [-64..63] to the regB contents and saves the result to regA. imm can be decimal or hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">nand regA, regB, regC</td>"+
  "  <td colspan=\"2\">does bitewise NAND with the contents of regB and regC saves the result in regA</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">lui regA, imm</td>"+
  "  <td colspan=\"2\">shifts left imm by 6 and saves the result in regA imm range in [0..0x3FF]. imm can be decimal or hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">sw regA, regB, imm</td>"+
  "  <td colspan=\"2\">saves regA contents in memory at address of: regB contents + imm (imm range is still [-64..63]).  imm can be decimal or hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">lw regA, regB, imm</td>"+
  "  <td colspan=\"2\">loads a word from memory to regA at address of: regB contents + imm (imm range is still [-64..63]).  imm can be decimal or hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">beq regA, regB, imm</td>"+
  "  <td colspan=\"2\">if regA contents are equal to regB contents then adds imm+1 to the instrinction pointer (imm range is still [-64..63]).  imm can be decimal or hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">jalr regA, regB</td>"+
  "  <td colspan=\"2\">sets the operation pointer to regB contents, saves in regA current instrinction pointer (ip+1)</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>Translator instructions</strong></td>"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">.space [size]</td>"+
  "  <td colspan=\"2\">clears [size] memory words starting at current. size must be hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">.fill [what]</td>"+
  "  <td colspan=\"2\">sets te current memory word to [what]. size must be hexadecimal</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">halt </td>"+
  "  <td colspan=\"2\">stops program execution</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>Additional translator instructions</strong></td>"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">.org [address]</td>"+
  "  <td colspan=\"2\">applies only to &quot;new style&quot; (see appendix) tells the translator hat the next command should be written to the memory at address &quot;address&quot; :). address must be hexadecimal </td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>3. How to use it</strong></td>"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>3.1. Buttons</strong></td>"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Step&quot; </td>"+
  "  <td colspan=\"2\">executes command found at the current ip</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Run&quot;</td>"+
  "  <td colspan=\"2\">starts program execution</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Stop&quot;</td>"+
  "  <td colspan=\"2\">suspends program execution</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Clear memory&quot;</td>"+
  "  <td colspan=\"2\">clears the whole memory</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Clear registers&quot;</td>"+
  "  <td colspan=\"2\">clears all the registers</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Up&quot; and &quot;Down&quot; </td>"+
  "  <td colspan=\"2\">scroll memory editor correspondingly up and down</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;Take deep breath&quot;</td>"+
  "  <td colspan=\"2\">translates asm and loads it to memory</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\"><strong>3.2 Menus</strong></td>"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td colspan=\"2\">&quot;File&quot;</td>"+
  "  <td colspan=\"2\">&nbsp;</td>"+
  "</tr>"+
  "<tr valign=\"top\">"+
  "  <td width=\"54\">&nbsp;</td>"+
  "  <td width=\"165\">&quot;Memory operations&quot;</td>"+
  "  <td colspan=\"2\"> save/load all memory, registers and ip to the file<br>"+
"save debug info (same as above but more human readable)</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td>&nbsp;</td>"+
"    <td>&quot;Asm operations&quot;</td>"+
"    <td colspan=\"2\">save/load asm program</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"2\">&quot;Prefs&quot;</td>"+
"    <td colspan=\"2\">&nbsp;</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td>&nbsp;</td>"+
"    <td>&quot;Set run speed&quot;</td>"+
"    <td colspan=\"2\">sets the delay between steps for the &quot;Run&quot; command</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td>&nbsp;</td>"+
"    <td>&quot;Set asm style&quot;</td>"+
"    <td colspan=\"2\">the most significant one! switches between &quot;New style&quot; and &quot;Old style&quot; see appendix to get to know what they mean</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"2\">&quot;Help&quot;</td>"+
"    <td colspan=\"2\">&nbsp;</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"2\">&nbsp;</td>"+
"    <td colspan=\"2\">:)</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"2\"><strong>3.3 How to execute a program</strong></td>"+
"    <td colspan=\"2\">&nbsp;</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"4\">Write your program in the asm panel on the right, press the &quot;Take deep breath&quot; button and, if no errors occured, whole your program is now in the memory. Just press &quot;Run&quot; or &quot;Step&quot; now. If there were syntax errors in your code, the errored lines will be highlited red.</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"2\"><strong>4.Appendix</strong></td>"+
"    <td colspan=\"2\">&nbsp;</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td colspan=\"4\">You are wondering what is &quot;old style&quot; and &quot;new style&quot;? So here goes the answer :) When you choose &quot;old style&quot; you have to write the address before each command. When you choose &quot;new style&quot; you do not have to do this silly stuff :) if you want your program to be written at some specific address in memory - use &quot;.org&quot;. If no &quot;.org&quot; exists before the command - translator will put the it to the next memory cell.<br>"+
"    Examples of &quot;old style&quot; and &quot;new style&quot;. They do absolutely identical things - nothing :) Just to show you the Difference:</td>"+
"  </tr>"+
"  <tr valign=\"top\">"+
"    <td height=\"134\"><div align=\"right\"></div></td>"+
"    <td height=\"134\">&quot;Old Style&quot;<br>"+
"0000: add r2,r0,r0 <br>"+
"0001: addi r1,r1,0xF<br>"+
"0008: nand r1,r1,r1<br>"+
"0009: sw r1,r1,0</td>"+
"    <td>&nbsp;</td>"+
"    <td>&quot;New Style&quot;<br>"+
"add r2,r0,r0 <br>"+
"addi r1,r1,0xF<br>"+
".org 0x8<br>"+
"nand r1,r0,r1<br>"+
"sw r1,r1,0<br></td>"+
"  </tr>"+
"</table>");
	
	gridbag.setConstraints(main_info, c);
	 contentPanel.add(main_info);
	 
	    main_infoScrollPane = new JScrollPane(main_info);
		Dimension preferred = new Dimension(main_info.getPreferredSize());
		preferred.height=400;
		preferred.width+=20;
		main_infoScrollPane.setPreferredSize(preferred);
		main_infoScrollPane.setMinimumSize(main_infoScrollPane.getMinimumSize());
		gridbag.setConstraints(main_infoScrollPane, c);
		add(main_infoScrollPane);
		
	
	JButton closeButton=new JButton("OK, I've got it!");
	gridbag.setConstraints(closeButton, c);
	contentPanel.add(closeButton);

	
	setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	
	setResizable(false);
    pack();
    
    closeButton.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent e) {
			setVisible(false);
		}
	});
    
   // setVisible(true);    
}
	public void ScrollUp()
	{
		main_infoScrollPane.getVerticalScrollBar().setValue(main_infoScrollPane.getVerticalScrollBar().getMinimum());
	}
	
}
