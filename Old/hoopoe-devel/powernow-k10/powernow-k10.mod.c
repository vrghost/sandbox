#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x9d7db99b, "struct_module" },
	{ 0xc80ac501, "cpufreq_freq_attr_scaling_available_freqs" },
	{ 0xd1e626ba, "cpufreq_register_driver" },
	{ 0xa9ee1cd6, "boot_cpu_data" },
	{ 0x23b99e92, "node_states" },
	{ 0x486b6407, "hweight64" },
	{ 0x583f3bd, "__next_cpu" },
	{ 0x8ff996a3, "__first_cpu" },
	{ 0xe2d5255a, "strcmp" },
	{ 0x7b0a340, "pci_bus_read_config_dword" },
	{ 0x73bcd634, "mutex_unlock" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0xda3d7ad, "mutex_lock" },
	{ 0x6067a146, "memcpy" },
	{ 0x9e9104f9, "pci_dev_put" },
	{ 0x207ec0f, "pci_bus_write_config_dword" },
	{ 0x765b492d, "pci_get_bus_and_slot" },
	{ 0xda77c9a8, "kmem_cache_alloc" },
	{ 0x74a87498, "malloc_sizes" },
	{ 0xe2a6d1e8, "cpu_online_map" },
	{ 0x37a0cba, "kfree" },
	{ 0xb0145b6f, "_cpu_pda" },
	{ 0x9b7b7247, "per_cpu__cpu_info" },
	{ 0xb915c848, "set_cpus_allowed_ptr" },
	{ 0xb645a0c9, "cpumask_of_cpu_map" },
	{ 0xc1633c4f, "cpufreq_unregister_driver" },
	{ 0xdd132261, "printk" },
	{ 0x19700765, "pv_cpu_ops" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=freq_table";

