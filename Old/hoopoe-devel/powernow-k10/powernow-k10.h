#ifndef __POWERNOW_K10_H_INCLUDED__
#define __POWERNOW_K10_H_INCLUDED__

#define MODULE_NAME "powernow-k10"

#define BIT_SLICE_MASK(bit_start, bit_end) (((-1UL << (63 - bit_start)) >> (63 - bit_start + bit_end)) << bit_end)

#define MAX_DID	0x4	/* Maximum divisor id that is still supported by CPU */
#define MAX_FID	7	/* Maximum fid */

#define CPU_PCI_BUS	0
#define CPU_PCI_DEV 0x18
#define MAX_NAME_LEN 100

/* Slices are inclusive from both borders (as seen in manual) */
struct register_slice {
	const int max_bit, min_bit;
	char name[MAX_NAME_LEN];
};

#define T_MSR	1
#define T_PCI	2
#define KEEP_OLD	0
#define ZERO_OUT	1

/* struct that, when defined, is able to hold register parsing information */
#define MAX_SLICE_COUNT		25 /* Maximum count of slices that one 'register_parse' struct is able to contain */
struct register_parse {
	unsigned int slice_count, reg_size;
	u64 address; /* address of given register */
	int type; /* type of given register - T_MSR of T_PCB */
	int unmaped_bits; /* What should be done with unmaped bits -- should they be preserved (KEEP_OLD) or zeroed out (ZERO_OUT) when struct is written back to register */
	struct register_slice slices[MAX_SLICE_COUNT];
	int func; /* function number (for PCI-based registers) */
	char name[MAX_NAME_LEN];
};

#define CPUID_PROCESSOR_SIGNATURE	1	/* function 1 */
#define CPUID_XFAM					0x0ff00000	/* extended family */
#define CPUID_XFAM_10H				0x00100000	/* family 0x10 */

#define MAX_PSTATES			5

/* P-State Current Limit Register (page 344) */
static struct register_parse PSTATE_CUR_LIM_STRUCT = {
	.name = "P-State Current Limit Register",
	.slices = {
		{2, 0, "CurPstateLimit"},
		{6, 4, "PstateMaxVal"},
	},
	.slice_count = 2,
	.reg_size = 64,
	.address = 0xc0010061,
	.type = T_MSR,
	.unmaped_bits = ZERO_OUT,
};

/* COFVID Status Register, page 347 */
static struct register_parse COFVID_STRUCT = {
	.name = "COFVID Status Register",
	.slices = {
		{5, 0, "CurCpuFid"},
		{8, 6, "CurCpuDid"},
		{15, 9, "CurCpuVid"},
		{18, 16, "CurPstate"},
		{22, 22, "CurNbDid"},
		{31, 25, "CurNbVid"},
		{34, 32, "StartupPstate"},
		{41, 35, "MaxVid"},
		{48, 42, "MinVid"},
		{54, 49, "MaxCpuCof"},
		{58, 56, "CurPstateLimit"},
		{63, 59, "MaxNbFid"},
	},
	.slice_count = 12,
	.reg_size = 64,
	.address = 0xc0010071,
	.type = T_MSR,
	.unmaped_bits = KEEP_OLD,
};

/* P-State Control Register  */
static struct register_parse PSTATE_CONTROL_STRUCT = {
    .slices = {
        {2, 0, "PstateCmd"},
    },
	.slice_count = 1,
	.reg_size = 64,
	.address = 0xc0010062,
	.type = T_MSR,
	.unmaped_bits = ZERO_OUT,
};

/* Clock Power/Timing Control0 Register, see page 256 of manual*/
static struct register_parse TIMING_CONTROL_STRUCT = {
	.name = "Clock Power/Timing Control0 Register",
	.slices = {
		{4, 0, "NbFid"},
		{5, 5, "NbFidEn"},
		{11, 8,	"ClkRampHystSel"},
		{12, 12, "ClkRampHystCtl"},
		{17, 16, "LnkPllLock"},
		{23, 20, "PowerStepDown"},
		{27, 24, "PowerStepUp"},
		{30, 28, "NbClkDiv"},
		{31, 31, "NbClkDivApplyAll"},
	},
	.slice_count = 9,
	.reg_size = 32,
	.type = T_PCI,
	.address = 0xd4,
	.func = 3,
	.unmaped_bits = KEEP_OLD,
};

/* Power Control Miscellaneous Register, see page 253 of manual */
static struct register_parse POWER_CONTROL_MISC_STRUCT = {
	.name = "Power Control Miscellaneous Register",
	.slices = {
		{6, 0, "PsiVid"},
		{7, 7, "PsiVidEn"},
		{8, 8, "PviMode"},
		{13, 11, "PllLockTime"},
		{27, 16, "PstateId"},
		{29, 29, "SlamVidMode"},
		{31, 31, "CofVidProg"},
	},
	.slice_count = 7,
	.reg_size = 32,
	.type = T_PCI,
	.address = 0xa0,
	.func = 3,
	.unmaped_bits = KEEP_OLD,
};

/* The following template is used to define all possible pstates */
/* 
 * Each of pstate templates will link to same 'slices' structure.
 * This should be ok since these structures wont be changed. ever. during program execution. 
 */
 
 /* F3x68 Software Thermal Control (STC) Register (page 242-243) */
 static struct register_parse SOFTWARE_THERMAL_CONTROL_STRUCT = {
	.name = "Software Thermal Control (STC) Register",
    .slices = {
        {0, 0, "StcSbcTmpHiEn"},
        {1, 1, "StcSbcTmpLoEn"},
        {2, 2, "StcApcTmpHiEn"},
        {3, 3, "StcApcTmpLoEn"},
        {5, 5, "StcPstateEn"},
		{6, 6, "StcTmpHiSts"},
        {7, 7, "StcTmpLoSts"},
		{10, 8, "StcTmpLmt"},
        {14, 12, "StcSlewSel"},
        {22, 16, "StcHystLmt"},
        {23, 23, "StcPstateLimit"},
    },
    .slice_count = 11,
    .address = 0x68,
    .reg_size = 32,
    .func = 3,
    .type = T_PCI,
	.unmaped_bits = KEEP_OLD,
 };
 
  /* F3xDC Clock Power/Timing Control 2 Register (page 260-261) */
 static struct register_parse CLOCK_POWER_TIMING_CONTROL_2_STRUCT = {
	.name = "Clock Power/Timing Control 2 Register",
    .slices = {
        {10, 8, "PstateMaxVal"},
        {14, 12, "NbsynPtrAdj"},
        {18, 16, "CacheFlushOnHaltCtl"},
        {25, 19, "CacheFlushOnHaltTmr"},
    },
    .slice_count = 4,
    .address = 0xdc,
    .reg_size = 32,
    .func = 3,
    .type = T_PCI,
	.unmaped_bits = KEEP_OLD,
 };
 
/* F3xD8 Clock Power/Timing Control 1 Register (page 259-560) */
 static struct register_parse CLOCK_POWER_TIMING_CONTROL_1_STRUCT = {
	.name = "Clock Power/Timing Control 1 Register",
    .slices = {
        {2, 0, "VSSlamTime"},
        {6, 4, "VSRampTime"},
        {14, 8, "TdpVid"},
        {27, 24, "ReConDel"},
    },
    .slice_count = 4,
    .address = 0xd8,
    .reg_size = 32,
    .func = 3,
    .type = T_PCI,
	.unmaped_bits = KEEP_OLD,
 };

 /* F4x1[F0:E0] P-state Specification Registers (page 293-294) */
#define __TMP_PSTATE_TEMPLATE(shift) \
	 { \
		.name = "P-state Specification Register " #shift ,\
		.slices = { \
			{5, 0, "CpuFid"}, \
			{8, 6, "CpuDid"}, \
			{15, 9,	"CpuVid"}, \
			{16, 16, "NbDid"}, \
			{24, 17, "IddValue"}, \
			{26, 25, "IddDiv"}, \
			{27, 27, "PstateEn"}, \
		}, \
		.slice_count = 7, \
		.reg_size = 32, \
		.address = (0x1e0 + shift * 4), \
		.func = 4, \
		.type = T_PCI, \
		.unmaped_bits = ZERO_OUT, \
	}

static struct register_parse ALL_PSTATES_SPEC_STRUCT[MAX_PSTATES] = {
	__TMP_PSTATE_TEMPLATE(0),
	__TMP_PSTATE_TEMPLATE(1),
	__TMP_PSTATE_TEMPLATE(2),
	__TMP_PSTATE_TEMPLATE(3),
	__TMP_PSTATE_TEMPLATE(4),
};

#undef __TMP_PSTATE_TEMPLATE

#define __TMP_PSTATE_TEMPLATE(shift) \
	 { \
		.name = "P-state register " #shift ,\
		.slices = { \
			{5, 0, "CpuFid"}, \
			{8, 6, "CpuDid"}, \
			{15, 9, "CpuVid"}, \
			{22, 22, "NbDid"}, \
			{31, 25, "NbVid"}, \
			{39, 32, "IddValue"}, \
			{41, 40, "IddDiv"}, \
			{63, 63, "PstateEn"}, \
		}, \
		.slice_count = 8, \
		.reg_size = 64, \
		.address = (0xc0010064 + shift), \
		.type = T_MSR, \
		.unmaped_bits = ZERO_OUT, \
	}

static struct register_parse ALL_PSTATES_STRUCT[MAX_PSTATES] = {
	__TMP_PSTATE_TEMPLATE(0),
	__TMP_PSTATE_TEMPLATE(1),
	__TMP_PSTATE_TEMPLATE(2),
	__TMP_PSTATE_TEMPLATE(3),
	__TMP_PSTATE_TEMPLATE(4),
};



#undef __TMP_PSTATE_TEMPLATE

struct applied_reg_template {
	const struct register_parse *parsing_template;
	u64	raw_value;
	u64	parsed_values[MAX_SLICE_COUNT]; /* array where all parsed values will be stored */
};

struct powernow_k10_data {
	unsigned int cpu;

	unsigned int transaction_latency; /* in microseconds */
	unsigned int cur_pstate_limit, pstate_max_val;
	
	struct cpufreq_frequency_table  powernow_table[MAX_PSTATES];

	struct applied_reg_template pstates[MAX_PSTATES];

	struct applied_reg_template power_cntrl_misc;
	struct applied_reg_template timing_cntrl;
	struct applied_reg_template pstate_cur_lim;
	struct applied_reg_template software_thermal_control;
	struct applied_reg_template clock_power_timing_control1;
	struct applied_reg_template clock_power_timing_control2;
	struct applied_reg_template pstate_control;

};

/** Table of pre-defined power states follow **/

struct known_pstate {
	int cof;
	int vid;
};


static struct known_pstate known_pstates[] = {
	{.cof = 2300, .vid = 28},
	{.cof = 1150, .vid = 28}, 
};

#define KNOWN_PSTATE_COUNT (sizeof(known_pstates)/sizeof(*known_pstates))


#endif
