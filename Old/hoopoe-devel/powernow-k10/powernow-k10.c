#include <linux/kernel.h>
#include <linux/smp.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/cpufreq.h>
#include <linux/slab.h>
#include <linux/cpumask.h>
#include <linux/sched.h>	/* for current / set_cpus_allowed() */

#include <asm/msr.h>
#include <asm/io.h>
#include <asm/delay.h>

#include <linux/acpi.h>
#include <linux/pci.h>
#include <linux/mutex.h>
#include <linux/delay.h>
#include <acpi/processor.h>

#include "powernow-k10.h"

#define dprintk printk

#define PFX MODULE_NAME ": "
#define BFX PFX "BIOS error: "
#define VERSION "version 0.00.00"

#ifndef CONFIG_SMP
#	error "Please enable SMP"
#endif

#define FORCE_CPU_LIMIT_START(limit_cpu) \
    oldmask = current->cpus_allowed; \
    set_cpus_allowed_ptr(current, &cpumask_of_cpu(limit_cpu)); \
    do{ \
	    if (smp_processor_id() != limit_cpu) { \
		    printk(KERN_ERR PFX \
			"limiting to cpu %u failed (line %i)\n", \
			limit_cpu, __LINE__); \
		    break; \
	    }

#define FORCE_CPU_LIMIT_END }while(0); \
	set_cpus_allowed_ptr(current, &oldmask);

#define cpu_t unsigned int
#define reg_t u64

#define _TOSTR(x) #x
#define TOSTR(x) _TOSTR(x)

#define ASSERT_FAIL(cmd)	 do{ printk(KERN_ERR PFX " " TOSTR(__FILE__) "@" TOSTR( __LINE__) " ASSERT FAIL.\n"); cmd;}while(0)
#define ASSERT_ON(cond, cmd) if(!(cond)){ ASSERT_FAIL(cmd); }


/* structure to hold constant system info */
struct {
		struct applied_reg_template cpu_defined_pstates[MAX_PSTATES];
		struct applied_reg_template computed_pstates[MAX_DID * MAX_FID];
		
		struct {
			int fid, did, cof;
		} fid_did_cof_table[MAX_DID * MAX_FID];
		int fid_did_cof_table_size;
		
		struct applied_reg_template cofvid;

} sys_info;


/* serialize global changes  */
static DEFINE_MUTEX(fidvid_mutex);
static DEFINE_MUTEX(msr_mutex);

/*** parsing/assebling raw register values functionality follow ***/

#define SLICE_TO_MASK(slice) BIT_SLICE_MASK((slice).max_bit, (slice).min_bit)

static reg_t get_register_slice(const u64 data, const struct register_slice slice){
	return (SLICE_TO_MASK(slice) & data) >> slice.min_bit;
}

static u64 to_register_slice(reg_t value,  struct register_slice slice){
	return ((value << slice.min_bit) & SLICE_TO_MASK(slice));
}

static void parse_raw_value(struct applied_reg_template *where){
	int ii;
	
	ASSERT_ON(where, return);
	for(ii = 0; ii < where->parsing_template->slice_count; ii++){
		where->parsed_values[ii] = get_register_slice(where->raw_value, where->parsing_template->slices[ii]);
	}
}

static u64 get_slice_value(struct applied_reg_template *where, const char *name){
	int ii;
	
	ASSERT_ON(where && name, return 0);
	
	for(ii = 0; ii < where->parsing_template->slice_count; ii++){
		if(strcmp(where->parsing_template->slices[ii].name, name) == 0){
			return where->parsed_values[ii];
		}
	}
}

static void set_slice_value(struct applied_reg_template *where, const char *name, u64 value){
	int ii, found = -1, size;
	
	for(ii = 0; ii < where->parsing_template->slice_count; ii++){
		if(strcmp(where->parsing_template->slices[ii].name, name) == 0){
			found = ii;
			break;
		}
	}
	
	ASSERT_ON(found >= 0, return);
	size = where->parsing_template->slices[ii].max_bit - where->parsing_template->slices[ii].min_bit + 1;
	ASSERT_ON(value <= ~(~0 << size), return);
	where->parsed_values[found] = value;
}

static void assemble_raw_value(struct applied_reg_template *where){
	int ii;
	u64 applied_bits = 0, raw = 0;

	ASSERT_ON(where, return);
	for(ii = 0; ii < where->parsing_template->slice_count; ii++){
		raw |= to_register_slice(where->parsed_values[ii], where->parsing_template->slices[ii]);
		applied_bits |= SLICE_TO_MASK(where->parsing_template->slices[ii]);
	}
	/* explicitly zeroing out all unused bits */
	raw = raw & applied_bits;
	if(where->parsing_template->unmaped_bits == KEEP_OLD){
		/* unused bits are currently zeroued out, remember? */
		raw = raw | (~applied_bits & where->raw_value);
	}
	where->raw_value = raw;
}

/*** Working with reagisters (on different devices) ***/
u64 rdmsr64(const reg_t addr){
	u32 lo, hi;
	rdmsr(addr, lo, hi);
	return (((u64)hi << 32) | lo);
}

void wrmsr64(const reg_t addr, u64 val){
	u32 lo, hi;
	lo = val & 0xFFFFFFFF;
	hi = (val >> 32) & 0xFFFFFFFF;
	wrmsr(addr, lo, hi);
}

u64 read_msr_reg(const struct register_parse template){
	u64 rv;
	
	ASSERT_ON(template.type == T_MSR, return 0);
	rv = rdmsr64(template.address);
	if(template.reg_size != 64){
		rv = rv >> (64 - template.reg_size);
	}
	return rv;
}

u64 read_pci_reg(const struct register_parse template){
	struct pci_dev *dev;
	u32 rv;

	ASSERT_ON(template.type == T_PCI, return 0);
	dev = pci_get_bus_and_slot(CPU_PCI_BUS, PCI_DEVFN(CPU_PCI_DEV, template.func));
	if(!dev){
		printk(KERN_ERR PFX "PCI device not found %i:%i.%i\n", CPU_PCI_BUS, CPU_PCI_DEV, template.func);
		ASSERT_FAIL(return 0);
	}
	do{
		switch(template.reg_size){
			case 32:
				pci_read_config_dword(dev, template.address, &rv);
				break;
			default:
				ASSERT_FAIL(return 0);
		}
	}while(0);
	pci_dev_put(dev);
	return rv;
}

void write_pci_reg(const struct applied_reg_template *where){
	struct pci_dev *dev;

	ASSERT_ON(where->parsing_template->type == T_PCI, return);
	dev = pci_get_bus_and_slot(CPU_PCI_BUS, PCI_DEVFN(CPU_PCI_DEV, where->parsing_template->func));
	if(!dev){
		printk(KERN_ERR PFX "PCI device not found %i:%i.%i\n", CPU_PCI_BUS, CPU_PCI_DEV, where->parsing_template->func);
		ASSERT_FAIL(return);
	}
	do{
		switch(where->parsing_template->reg_size){
			case 32:
				pci_write_config_dword(dev, where->parsing_template->address, (u32)(where->raw_value));
				break;
			default:
				ASSERT_FAIL(return);
		}
	}while(0);
	pci_dev_put(dev);
}

static int pending_msr_update(const struct applied_reg_template *where){
	int ii = 0, rv = 1;
	u64 addr, val;

	/* Thou shalt not write non-64 bit registers with this function */
	ASSERT_ON(where && where->parsing_template->type == T_MSR, return 0);
	mutex_lock(&msr_mutex);

	addr = where->parsing_template->address;
	val = where->raw_value;
	wrmsr64(addr, val);
	do{
		mdelay(10);
		if(ii++ > 4200){
			printk(KERN_ERR PFX "pending_msg_update failed\n");
			rv = 0;
			break;
		}
	}while(rdmsr64(addr) != val);
	mutex_unlock(&msr_mutex);
	return rv;
}

void load_and_parse_reg(struct applied_reg_template *where){
	ASSERT_ON(where, return);
	switch(where->parsing_template->type){
		case T_MSR:
			where->raw_value = read_msr_reg(*(where->parsing_template));
			break;
		case T_PCI:
			where->raw_value = read_pci_reg(*(where->parsing_template));
			break;
		default:
			ASSERT_FAIL(return);
	};
	parse_raw_value(where);
}

void save_reg_value(struct applied_reg_template *where){
	ASSERT_ON(where, return);

	assemble_raw_value(where);
	switch(where->parsing_template->type){
		case T_MSR:
			pending_msr_update(where);
			break;
		case T_PCI:
		    write_pci_reg(where);
			break;
		default:
			ASSERT_FAIL(return);
	};
}

/*** structure initializators/terminators (they shalt not read registers by themself) ***/
#define SET_APPLIED_TEMPLATE(appl_templ, templ) (appl_templ).parsing_template = &(templ)

void init_powernow_k10_data(struct powernow_k10_data *what){
	int ii;

	ASSERT_ON(what, return);
	SET_APPLIED_TEMPLATE(what->power_cntrl_misc, POWER_CONTROL_MISC_STRUCT);
	SET_APPLIED_TEMPLATE(what->timing_cntrl, TIMING_CONTROL_STRUCT);
	
	
	SET_APPLIED_TEMPLATE(what->pstate_cur_lim, PSTATE_CUR_LIM_STRUCT);
	SET_APPLIED_TEMPLATE(what->software_thermal_control, SOFTWARE_THERMAL_CONTROL_STRUCT);
    SET_APPLIED_TEMPLATE(what->clock_power_timing_control2, CLOCK_POWER_TIMING_CONTROL_2_STRUCT);
    SET_APPLIED_TEMPLATE(what->pstate_control, PSTATE_CONTROL_STRUCT);
	SET_APPLIED_TEMPLATE(what->clock_power_timing_control1, CLOCK_POWER_TIMING_CONTROL_1_STRUCT);

	for(ii = 0; ii < MAX_PSTATES; ii++){
		SET_APPLIED_TEMPLATE(what->pstates[ii], ALL_PSTATES_STRUCT[ii]);
	}
}

void free_powernow_k10_data(struct powernow_k10_data *what){
	kfree(what);
}

/*** Utility functions ***/
reg_t fid_did_to_cof(reg_t fid, reg_t did){
	return (fid + 0x10) / (1 << did);
}

u32 cof_to_hz(u32 cof){
	return cof * 100;
}

u32 fid_did_to_hz(reg_t fid, reg_t did){
	return cof_to_hz(fid_did_to_cof(fid, did));
}

static int check_supported_cpu(cpu_t cpu)
{
	cpumask_t oldmask;
	u32 eax;
	int rc = 0;

	FORCE_CPU_LIMIT_START(cpu);

	eax = cpuid_eax(CPUID_PROCESSOR_SIGNATURE);
	rc = (current_cpu_data.x86_vendor == X86_VENDOR_AMD) && \
	     ((eax & CPUID_XFAM) == CPUID_XFAM_10H);

	FORCE_CPU_LIMIT_END;
	
	return rc;
}

/* Driver entry point to verify the policy and range of frequencies */
static int powernowk10_verify(struct cpufreq_policy *pol)
{
	return -EINVAL;
}

void dump_reg(struct applied_reg_template *what){
	int ii;

	ASSERT_ON(what, return);	

	printk("---\n%s\n", what->parsing_template->name);
	printk("\t\t|%llu\n", what->raw_value);
	for(ii = 0; ii < what->parsing_template->slice_count; ii++){
		printk("\t\t[%i] %s: %lli\n", ii,  what->parsing_template->slices[ii].name, what->parsed_values[ii]);
	}
}

/* per CPU init entry point to the driver */
static int __cpuinit powernowk10_cpu_init(struct cpufreq_policy *pol)
{
	struct powernow_k10_data *data;
	
	if (!cpu_online(pol->cpu))
		return -ENODEV;

	if (!check_supported_cpu(pol->cpu))
		return -ENODEV;
	
	data = kzalloc(sizeof(struct powernow_k10_data), GFP_KERNEL);
	if (!data) {
		printk(KERN_ERR PFX "unable to alloc powernow_k10_data");
		return -ENOMEM;
	}
	init_powernow_k10_data(data);
	data->cpu = pol->cpu;
	
	free_powernow_k10_data(data);
	return -ENODEV;
}

/* Driver entry point to switch to the target frequency */
static int powernowk10_target(struct cpufreq_policy *pol, unsigned targfreq, unsigned relation)
{
	return -EINVAL;
}

static unsigned int powernowk10_get (unsigned int cpu){
	return -EINVAL;
}

static struct freq_attr* powernow_k10_attr[] = {
	&cpufreq_freq_attr_scaling_available_freqs,
	NULL,
};

static struct cpufreq_driver cpufreq_amd64_k10_driver = {
	.verify = powernowk10_verify,
	.target = powernowk10_target,
	.init = powernowk10_cpu_init,
	//.exit = __devexit_p(powernowk8_cpu_exit),
	.get = powernowk10_get,
	.name = MODULE_NAME,
	.owner = THIS_MODULE,
	.attr = powernow_k10_attr,
};

/* Collect static system info (shouldn't be changed after this function) */
void collect_static_sysinfo(void){
	int fid, did, ii;
	
	sys_info.cofvid.parsing_template = &(COFVID_STRUCT);
	load_and_parse_reg(&(sys_info.cofvid));
	dump_reg(&(sys_info.cofvid));
	
	ii = 0;
	for(fid = 1; fid <= MAX_FID; fid++){
		for(did = 0; did <= MAX_DID; did++){
			sys_info.fid_did_cof_table[ii].did = did;
			sys_info.fid_did_cof_table[ii].fid = fid;
			sys_info.fid_did_cof_table[ii].cof = fid_did_to_cof(fid, did);
			ii++;
		}
	}
	sys_info.fid_did_cof_table_size = ii;
	
	/* Disable all values that are greater than MaxCpuCof */
	printk("%i\n", get_slice_value(&(sys_info.cofvid), "MaxCpuCof"));
}

/* driver entry point for init */
static int __cpuinit powernowk10_init(void)
{
	unsigned int ii, supported_cpus = 0;

	for_each_online_cpu(ii) {
		if (check_supported_cpu(ii))
			supported_cpus++;
	}

	if (supported_cpus == num_online_cpus()) {
		printk(KERN_INFO PFX "Found %d %s "
			"processors (%d cpu cores) (" VERSION ")\n",
			num_online_nodes(),
			boot_cpu_data.x86_model_id, supported_cpus);
		collect_static_sysinfo();
		return cpufreq_register_driver(&cpufreq_amd64_k10_driver);
	}

	return -ENODEV;
}

/* driver entry point for term */
static void __exit powernowk10_exit(void)
{
	dprintk("exit\n");

	cpufreq_unregister_driver(&cpufreq_amd64_k10_driver);
}

MODULE_LICENSE("GPL");

late_initcall(powernowk10_init);
module_exit(powernowk10_exit);
