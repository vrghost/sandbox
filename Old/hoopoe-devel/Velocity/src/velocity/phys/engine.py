# -*- coding: utf-8 -*-
"""This package contains physics engine main objects.

History (most recent first):

22-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 9 $"[11:-2]
__date__ = "$Date: 2007-07-21 12:10:02 +0300 (Se, 21 Jūl 2007) $"[7:-2]

from src.velocity.base import engine, binding

class PhysicsUniverse(engine.BaseEngine):
    """This object should contain the whole physics universe."""
    
    ips = 30

    def doWork(self, dt):
        print "Physics engine callback. dt=%s" % dt

# vim: set et sts=4 sw=4 :
