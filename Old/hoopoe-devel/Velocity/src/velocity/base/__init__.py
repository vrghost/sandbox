# -*- coding: utf-8 -*-
"""This package contains base objects.

History (most recent first):

20-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 9 $"[11:-2]
__date__ = "$Date: 2007-07-21 12:10:02 +0300 (Se, 21 Jūl 2007) $"[7:-2]

# vim: set et sts=4 sw=4 :
