# -*- coding: utf-8 -*-
"""This file contains logging classes.

History (most recent first):

22-jul-2007 [vrg]   added logFormat field
20-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 9 $"[11:-2]
__date__ = "$Date: 2007-07-21 12:10:02 +0300 (Se, 21 Jūl 2007) $"[7:-2]

from twisted.python import log
import sys

def _getLogger(level):
    def _doLog(self, data, *args):
        log.msg(self.logFormat % {
            "logPrefix": self.logPrefix,
            "msg": (data % args)
        }, system=level)
    return _doLog

class LoggingClass(object):
    """Object class that can write to logs withou this twisted magic."""

    logDebug = _getLogger("DEBUG")
    logInfo = _getLogger("INFO")
    logError = _getLogger("ERROR")

    logPrefix = ""
    logFormat = "%(logPrefix)s: %(msg)s"

    def logException(self, data, *args):
        log.err(None, "Exception logging called")
        log.msg(self.logFormat % {
            "logPrefix": self.logPrefix,
            "msg": (data % args)
        }, system="EXCEPTION")


class LoggingSettings(object):
    """Class that should just contain global logging settings."""

    logFile = file("velocity.log", "w")
    timeFormat = "%d.%m.%Y %H:%m:%S"

def start():
    """Starts logging facilities."""
    _stdout = sys.stdout
    # setting the system-wide time format
    log.FileLogObserver.timeFormat = LoggingSettings.timeFormat
    log.startLogging(LoggingSettings.logFile, setStdout=True)
    # during debug I would like to see log in the sys.stdout too.
    if __debug__:
        _stdoutObserver = log.FileLogObserver(_stdout)
        log.addObserver(_stdoutObserver.emit)

def finish():
    """Finishes logging facilities."""
    log.discardLogs()

# vim: set et sts=4 sw=4 :
