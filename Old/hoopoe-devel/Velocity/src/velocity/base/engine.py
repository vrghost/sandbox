# -*- coding: utf-8 -*-
"""This file contains base engine objects.

History (most recent first):

26-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 9 $"[11:-2]
__date__ = "$Date: 2007-07-21 12:10:02 +0300 (Se, 21 Jūl 2007) $"[7:-2]

from time import time

from . import base, binding

class BaseEngine(base.Object):
    """Base engine class."""

    ips = binding.Require("Iterations per second")
    _timeDelta = binding.Make(
        lambda s: 1.0 / s.ips, doc="Time between two iterations")
    _lastCall = binding.Make(
        lambda s: time(), uponAssembly=True, doc="Last call time")

    def __init__(self, **kwargs):
        super(BaseEngine, self).__init__(**kwargs)
        # start engine loop
        self.callLater(self._timeDelta, self._doLoop)
    
    def _doLoop(self):
        # we schedule next callback _befoure_ doing any work
        self.callLater(self._timeDelta, self._doLoop)
        _dt = time() - self._lastCall
        self._lastCall = time()
        self.doWork(_dt)

    def foWork(self, dt):
        """Children should override this method.
        
        `dt` is amount of seconds passed since last call.
        """
        raise NotImplementedError
        

# vim: set et sts=4 sw=4 :
