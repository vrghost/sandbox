# -*- coding: utf-8 -*-
"""This file contains base objects.

History (most recent first):

21-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 9 $"[11:-2]
__date__ = "$Date: 2007-07-21 12:10:02 +0300 (Se, 21 Jūl 2007) $"[7:-2]

from twisted.internet import reactor
from twisted.internet.defer import Deferred

from . import logging, binding

class Object(binding.Component, logging.LoggingClass):
    """Base object class that is compatible of logging and binding."""

    logParent = binding.Make(lambda s: None,
        doc="Reference to a parent object. (Used for logging only)")
    parentLogPrefix = binding.Make(lambda s:
        s.logParent.logPrefix if s.logParent else "")

    @binding.Make
    def logName(self):
        """Default logging name."""
        return self.__class__.__name__

    @binding.Make
    def logPrefix(self):
        """Object log prefix."""
        if self.parentLogPrefix:
            _out = ".".join((self.parentLogPrefix, self.logName))
        else:
            _out = self.logName
        return _out

    def getChild(self, klass, **kwargs):
        """Create children object."""
        if "logParent" not in kwargs:
            kwargs["logParent"] = self
        return klass(**kwargs)

    def callLater(self, timeDelta, callable):
        """Schedule method for calling later.

        Return Delayed call object
        """
        return reactor.callLater(timeDelta, callable)

# vim: set et sts=4 sw=4 :
