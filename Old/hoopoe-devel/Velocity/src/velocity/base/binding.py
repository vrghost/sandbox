# -*- coding: utf-8 -*-
"""This is re-implementation of some my favorite PEAK functionality.

History (most recent first):

22-jul-2007 [vrg]   corrected bindings,
                    added nice docstring handling methods.
20-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 9 $"[11:-2]
__date__ = "$Date: 2007-07-21 12:10:02 +0300 (Se, 21 Jūl 2007) $"[7:-2]

class _MagicAttr(object):
    """Class that obtains and saves docstrings in itself."""

    def __init__(self, docList):
        """Receive list of objects that could provide docstrings.

        Save the first non-empty docstring.
        """
        for _item in docList:
            if isinstance(_item, basestring):
                if _item:
                    self.__doc__ = _item
                    break
            else:
                _doc = _item.__doc__
                if _doc:
                    self.__doc__ = _doc
                    break

    def copy(self):
        """The object must be able to copy itself."""
        raise NotImplementedError

class Require(_MagicAttr):
    """A placeholder for required attribute."""

    def __init__(self, doc=""):
        super(Require, self).__init__((doc, ))

    def copy(self):
        return Require(self.__doc__)

class Make(_MagicAttr):
    """Lazy function calculation."""

    func = None
    _parent = None
    _name = None
    _uponAssembly = None

    def __init__(self, func, uponAssembly=False, doc=""):
        super(Make, self).__init__((func, doc))
        self.func = func
        self._uponAssembly = uponAssembly

    def shouldPoke(self):
        return self._uponAssembly

    def setName(self, name):
        """Set attribute name."""
        self._name = name

    def _calcValue(self, instance):
        return self.func(instance)

    def __get__(self, instance, owner):
        if instance is None or self._name is None:
            return self
        assert self._name, "Name must be known"
        _value = self._calcValue(instance)
        setattr(instance, self._name, _value)
        return _value

    def __str__(self):
        return "<%s object @ %i, function:%r>" % (
           self.__class__.__name__, id(self), self.func)

    def copy(self):
        return Make(self.func, self._uponAssembly, self.__doc__)

class Component(object):

    def __init__(self, **kwargs):
        _attrs = dict([
            (_name, getattr(self, _name))
            for _name in dir(self)])
        for (_name, _value) in kwargs.iteritems():
            if _name in _attrs:
                setattr(self, _name, _value)
                _attrs[_name] = _value
            else:
                raise AttributeError("Class field '%s' not defined." % _name)
        # now we will provide names to 'Make' objects
        # and raise exception if 'Required' attribute not provided.
        _pokeValues = []
        for (_name, _value) in _attrs.iteritems():
            if isinstance(_value, _MagicAttr):
                # we need to copy all magic attrs
                _value = _value.copy()
                setattr(self.__class__, _name, _value)
            if isinstance(_value, Make):
                _value.setName(_name)
                if _value.shouldPoke():
                    # touch value
                    _pokeValues.append(_name)
            elif isinstance(_value, Require):
                raise AttributeError(
                    "Required class field '%s' not defined." % _name)
        # now poke values that need to be poked
        for _name in _pokeValues:
            getattr(self, _name)
        # 'super' is called last cuz we can't guarantee object functionality
        # before this __init__ call.
        super(Component, self).__init__()


# vim: set et sts=4 sw=4 :
