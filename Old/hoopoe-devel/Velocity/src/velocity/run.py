#!python
# -*- coding: utf-8 -*-
"""This file must serve as program execution start point.

History (most recent first):

22-jul-2007 [vrg]   added root application,
                    now starting reactor
20-jul-2007 [vrg]   created

"""

__version__ = "$Revision: 10 $"[11:-2]
__date__ = "$Date: 2007-07-22 14:31:06 +0300 (Sun, 22 Jul 2007) $"[7:-2]

from twisted.internet import reactor

from src.velocity.base import logging
from src.velocity import application

def run():
    logging.start()
    try:
        _mainApp = application.GameApplication()
        print "Starting reactor."
        reactor.run()
    finally:
        logging.finish()

if __name__ == "__main__":
    run()

# vim: set et sts=4 sw=4 :
